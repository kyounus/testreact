
export let B2B_API_ENDPOINT = 'https://qa1api1.molekule.com:8044';
export let CLYDE_URL = 'https://api.sb.joinclyde.com/';
export let CLYDE_KEY = 'ck_live_ZD9d44cCYjEyEzsm';
export let CLYDE_SECRET = 'sk_live_NSjjKK92DZk31qek'
export let SERVICES = {}
export let ProjectKey = '';
export const [ENV_US, ENV_EU] = ["US", 'EU'];
export let code = ENV_US;
export const setProjectServices = (key) => {
    ProjectKey = key;
    try {
        switch (key) {
            case 'mo-dev':
                B2B_API_ENDPOINT = 'https://dev1api1.molekule.com:8099';
                CLYDE_URL = 'https://api.sb.joinclyde.com/';
                CLYDE_KEY = 'ck_live_ZD9d44cCYjEyEzsm';
                CLYDE_SECRET = 'sk_live_NSjjKK92DZk31qek';
                SERVICES = {
                    ACCOUNT: 'https://useraccount-service-dev.molekule.com/',
                    CART: 'https://cart-service-dev.molekule.com/',
                    CHECKOUT: 'https://checkout-service-dev.molekule.com/',
                    ORDER: 'https://order-service-dev.molekule.com/',
                    PRODUCT: 'https://product-service-dev.molekule.com/',
                    CSR: 'https://csr-service-dev.molekule.com/'
                }
                break;
            case 'mo-preprod-1':
                SERVICES = {
                    ACCOUNT: 'https://useraccount-service-preprod.molekule.com/',
                    CART: 'https://cart-service-preprod.molekule.com/',
                    CHECKOUT: 'https://checkout-service-preprod.molekule.com/',
                    ORDER: 'https://order-service-preprod.molekule.com/',
                    PRODUCT: 'https://product-service-preprod.molekule.com/',
                    CSR: 'https://csr-service-preprod.molekule.com/'
                }
                break;
            case 'mo-qa-1':
                SERVICES = {
                    ACCOUNT: 'https://useraccount-service-qa.molekule.com/',
                    CART: 'https://cart-service-qa.molekule.com/',
                    CHECKOUT: 'https://checkout-service-qa.molekule.com/',
                    ORDER: 'https://order-service-qa.molekule.com/',
                    PRODUCT: 'https://product-service-qa.molekule.com/',
                    CSR: 'https://csr-service-qa.molekule.com/'
                }
                break;
            case 'mo-dev-eu-1':
                code = ENV_EU
                SERVICES = {
                    ACCOUNT: 'https://useraccount-service-qa.molekule.uk/',
                    CART: 'https://cart-service-qa.molekule.uk/',
                    CHECKOUT: 'https://checkout-service-qa.molekule.uk/',
                    ORDER: 'https://order-service-qa.molekule.uk/',
                    PRODUCT: 'https://product-service-qa.molekule.uk/',
                    CSR: 'https://csr-service-qa.molekule.uk/'
                }
                break;
            case 'mo-preprod1-eu':
                code = ENV_EU
                SERVICES = {
                    ACCOUNT: 'https://useraccount-service-preprod.molekule.uk/',
                    CART: 'https://cart-service-preprod.molekule.uk/',
                    CHECKOUT: 'https://checkout-service-preprod.molekule.uk/',
                    ORDER: 'https://order-service-preprod.molekule.uk/',
                    PRODUCT: 'https://product-service-preprod.molekule.uk/',
                    CSR: 'https://csr-service-preprod.molekule.uk/'
                }
                break;
            case 'mo-prod-eu':
                code = ENV_EU
                SERVICES = {
                    ACCOUNT: 'https://useraccount-service-prod.molekule.uk/',
                    CART: 'https://cart-service-prod.molekule.uk/',
                    CHECKOUT: 'https://checkout-service-prod.molekule.uk/',
                    ORDER: 'https://order-service-prod.molekule.uk/',
                    PRODUCT: 'https://product-service-prod.molekule.uk/',
                    CSR: 'https://csr-service-prod.molekule.uk/'
                }
                break;
            default:
                SERVICES = {
                    ACCOUNT: 'https://useraccount-service-preprod.molekule.uk/',
                    CART: 'https://cart-service-preprod.molekule.uk/',
                    CHECKOUT: 'https://checkout-service-preprod.molekule.uk/',
                    ORDER: 'https://order-service-preprod.molekule.uk/',
                    PRODUCT: 'https://product-service-preprod.molekule.uk/',
                    CSR: 'https://csr-service-preprod.molekule.uk/'
                }
                break;
    
        }
    } catch (e) {
        console.log(e)
    }
}

export const centsToDollars = (value, dollar = true) => {
    if (!value) return 0;
    value = (value + '').replace(/[^\d.-]/g, '');
    value = parseFloat(value);
    const final = value ? value / 100 : 0;
    const retVal = final.toFixed(2);
    if (!dollar) {
        return retVal;
    }
    return code == ENV_US ? `$${retVal}`: `£${retVal}`
}