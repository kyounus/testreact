import React, { useEffect, useState } from "react";
import Grid from '@commercetools-uikit/grid';
import Card from '@commercetools-uikit/card';
import Spacings from '@commercetools-uikit/spacings';
import TextField from '@commercetools-uikit/text-field';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import { PlusBoldIcon, FilterIcon, SearchIcon } from '@commercetools-uikit/icons';
import './add-quote.css'

const AddQuote = (props) => {
    const [addQuote, setAddQuote] = useState(false);
    const[add,setAdd] = useState('')


    const quoteEvent = (e) => {
        setAdd(e.target.value)
    }

    const quoteItem = (item) => {
        props.history.push("quotes/"+item.id);
        props.history.push(item)
    }
    return(
        <div>
           <Grid
              gridGap="16px"
              gridAutoColumns="1fr"
              gridTemplateColumns="repeat(2, 1fr)"
           >
              <Grid.Item >
                 <Card>
                 <p style={{fontSize:'20px'}}>Add Item</p><br/>
                 <Spacings.Inline>
                    <TextField title="" name="add" placeholder="Search by term" value={add} onChange={quoteEvent}/> 
                    <div>
                      <SecondaryButton iconLeft={<SearchIcon />} label=""  />
                    </div>
                 </Spacings.Inline>
                 </Card>
               </Grid.Item>
               <Grid.Item>
               <div className="fonts">
                 <p style = {{fontSize:"20px",paddingTop:'5px'}}> Quote Summary </p>
                 <p style={{paddingTop:'5px'}}>Currency set to </p>
                 <p style={{paddingTop:'5px'}}>Company</p>
                 <p style={{paddingTop:'5px'}}>for ...</p><br/>
                 <Card>
                   <h4> ___ items in your cart</h4>
                   <hr/>
                   <h4>Sub total</h4>
                   <hr/>
                   <h4>Total(gross)</h4>
                 </Card>
                 <br/>
                   <SecondaryButton label="save" onClick={quoteItem}/>
               </div>
             </Grid.Item>
          </Grid>
         </div>
    )
}

export default AddQuote