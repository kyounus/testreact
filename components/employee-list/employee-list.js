import React, { useState } from 'react';
import Spacings from '@commercetools-uikit/spacings';
import { useIntl } from 'react-intl';
import { FilterOption } from '../../common/Filter';
import { Header } from '../../common/Header';
import { addHyperLink } from '../../utils';
import { Table } from '../../common/Table';
import { SERVICES } from '../../config/api';
import './employee-list.css';

const employeeList = (props) => {
  const [employeeData, setEmployeeData] = useState([]);
  const [totalItems, setTotalItems] = useState(0);
  const { formatTime, formatDate } = useIntl();
  const [searchByField, setSearchByField] = useState('');
  const[customerChannelB2B,setCustomerChannelB2B]=useState(false)
  const[customerChannelD2C,setCustomerChannelD2C]=useState(false)
  const[customerChannelDefault,setCustomerChannelDefault]=useState(true)
  const [searchValue, setSearchValue] = useState('')
  const[channel,setChannel]=useState('')

  let employeeList = []
  
  employeeData.map((employeeData) => {
    const createdOn = formatDate(employeeData.createdAt) + " " + formatTime(employeeData.createdAt)
    const modifiedOn = formatDate(employeeData.lastModifiedAt) + " " + formatTime(employeeData.lastModifiedAt)
    let employeeDetails = {
        id: employeeData.id,
        number: employeeData.customerNumber,
        externalId: <div className="emptyData">-----</div>,
        firstname: employeeData.firstName,
        lastname: employeeData.lastName,
        company: employeeData.companyName,
        email: employeeData.email,
        customergroup: employeeData.customerGroup?.obj.name,
        customerChannel:employeeData?.custom?.fields?.channel,
        createdon: createdOn,
        modifiedon: modifiedOn,
        customerStore:employeeData?.custom?.fields?.store,
    }
    employeeList = [...employeeList, employeeDetails] 
    })
    const columns = [
        { key: "number", label: "Customer number" },
        { key: "externalId", label: "External ID" },
        { key: "firstname", label: "First name" },
        { key: "lastname", label: "Last name" },
        { key: "company", label: "Company" },
        { key: "email", label: "Email" },
        { key: "customergroup", label: "Customer group" },
        { key: "createdon", label: "Created on" },
        { key: "modifiedon", label: "Modified on" },
        { key: "customerChannel", label: "Customer Channel"},
        { key: "customerStore", label: "Customer Store"},
    ];
    //Search Option
    const handleOnChange = (value) => {
        if(value === "b2b"){
            setCustomerChannelB2B(true)
            setCustomerChannelD2C(false)
            setCustomerChannelDefault(false)
            setChannel('B2B')
        }
        if(value === 'd2c'){
            setCustomerChannelB2B(false)
            setCustomerChannelD2C(true)
            setCustomerChannelDefault(false)
            setChannel('D2C')
        }
    }
    const searchOption = [
        { value: 'email', label: 'Email' },
        { value: 'lastName', label: 'Customer Last Name' },
    ];

    const channelOption = [
        { value: 'b2b', label: 'B2B'},
        { value: 'd2c', label: 'D2C' }
    ]
    return (
        <div  className="company-list box-style">
          <Spacings.Stack scale="m">
            <Header
                title={'Customers'}
                results={totalItems}
                linkUrl={addHyperLink('customers/new', props)}
                linkLabel={'Add Customer'}
            />
            <FilterOption
                multiSearch={true}
                searchFieldOption={searchOption}
                searchByField={(value) => setSearchByField(value)}
                searchFilter = {(searchValue) => setSearchValue(searchValue)}
                searchSelectOption = {channelOption}
                searchSelectChange = {(status) => handleOnChange(status)}
                searchSelectText = "Select Type of Channel"
            />
            <Table
                baseUrl={`${SERVICES.ACCOUNT}api/v1/customers`}
                detailPage={'customers'}
                searchText={searchValue ? `&${searchByField}=${encodeURIComponent(searchValue)}` : ''}
                searchSelect={channel ? `&customerChannel=${encodeURIComponent(channel)}` : ''}
                tableColumns={columns}
                tableRow={employeeList}
                getTabelData={(data) => setEmployeeData(data)}
                getTableRecord={(totlaCount) => setTotalItems(totlaCount)}
            /> 
        </Spacings.Stack>
    </div>
    );
}

export default employeeList;
