import React, { useState, useEffect } from 'react';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import Spacings from '@commercetools-uikit/spacings';
import FlatButton from '@commercetools-uikit/flat-button';
import "./orderTag.css"
import { SERVICES } from '../../config/api';
import LoadingSpinner from '@commercetools-uikit/loading-spinner';
import { Pagination } from '@commercetools-uikit/pagination';
import { useIntl } from 'react-intl';
import {Header} from "../../common/Header";
import { addHyperLink } from '../../utils';
import { CallApi } from '../../plugin/Axios';


const OrderTag = (props) =>{
  
    const [pageLimit, setPageLimit] = useState(20);
    const [pageNumber, setPageNumber] = useState(1);
    const [totalResults, setTotalResults] = useState(0);
    const [spinner, setSpinner] = useState(true);
    const[orderTagsData,setOrderTagsData] = useState([])
    const[orderTagsConst,setOrderTagsConst] = useState('')
    const { formatTime, formatDate } = useIntl();

    useEffect(() => {
        let offset= (pageNumber - 1) * pageLimit;
        if(totalResults && totalResults < offset){
            setPageNumber(0);
            return 
        }
        setSpinner(true);
        let url = `${SERVICES.ORDER}api/v1/orders/tags?&limit=${pageLimit}&offset=${offset}/`
        CallApi(url,'get')
        .then((res) => {
            console.log(res,"response")
            setOrderTagsConst(res.data)
            setOrderTagsData(res.data.value.orderTags)
            setTotalResults(res.data.value.orderTags ? res.data.value.orderTags.length : 0)
        })
        .then(res =>
            setSpinner(false)
            )
            .catch(error => {
                console.log("error", error)
                setSpinner(false)
              })
    },[])

    const handleEditClick = (item) => {
        props.history.push("ordertag/" + item.id);
        props.history.push(item)
    }
    let orderTagList = []
    orderTagsData?.length > 0 ? 
    orderTagsData.map(items =>{
        const createdOn = formatDate(orderTagsData.createdAt) + " " + formatTime(orderTagsData.createdAt)
        const modifiedOn = formatDate(orderTagsData.lastModifiedAt) + " " + formatTime(orderTagsData.lastModifiedAt)
        const orderTagId=orderTagsConst.id
        const orderTagDetails ={
            id:orderTagId,
            labels:items.label,
            created:createdOn,
            modified:modifiedOn,
            actions: <div> <FlatButton  label="Edit" onClick={handleEditClick}/></div>
        }
       orderTagList = [...orderTagList,orderTagDetails]
    }) : null 

    const columns = [
        { key: 'id', label: 'ID',align :'center' },
        { key: 'labels', label: 'Label',align :'center' },
        { key: 'created', label: 'Created', align :'center'},
        { key: 'modified', label: 'Modified' ,align :'center'},
        { key: 'actions', label: 'Actions',align :'center',} 
    ]
    return(
        <div>
           <div className="company-list box-style">
               <Header
                 title={'Order tags'}
                 results={totalResults}
                 linkUrl={addHyperLink('orderTag/new', props)}
                 linkLabel={'Add Order Tag'}
               />
               <DataTableManager columns={columns}>
                 <DataTable
                    rows={orderTagList}
                    onRowClick={
                        (item) => handleEditClick(item)
                      }
                 />
                </DataTableManager>  <br/>
                {spinner ? <div className="spinner">
           <LoadingSpinner size="s">Loading</LoadingSpinner></div> : null}
           {!spinner ?
             <Pagination
                totalItems={totalResults}
                page={pageNumber}
                perPageRange="m"
                onPageChange={(page) => {
                    console.log(page)
                    setPageNumber(page)
                }}
                onPerPageChange={(lmt) => {
                    console.log(lmt)
                    setPageNumber(lmt)
                }}
             /> 
            : null}
           </div>
        </div>
    )
}

export default OrderTag
