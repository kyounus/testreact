import React, { useState } from 'react';
import TextInput from '@commercetools-uikit/text-input';
import CheckboxInput from '@commercetools-uikit/checkbox-input';
import SecondaryIconButton from '@commercetools-uikit/secondary-icon-button'
import FlatButton from '@commercetools-uikit/flat-button';
import { BackIcon } from '@commercetools-uikit/icons';
import PrimaryButton from '@commercetools-uikit/primary-button';
import "./orderTagDetail.css";
import { useHistory } from "react-router-dom";

const OrderTagDetail = (props) => {
    let history = useHistory();
    const [frontEndDefault, setFrontEndDefault] = useState(false)
    const [zeroDollarOrder, setZeroOrderDollar] = useState(false)
    const [defaultText, setDefaultText] = useState('default')

    const handleResponse = (selectedCheckBox) => { 
        if (selectedCheckBox) {
            setFrontEndDefault(!frontEndDefault)
        }
        else {
            setZeroOrderDollar(!zeroDollarOrder)
        }
    }
    const  handleClose = () => {
        history.goBack()
    }
    const handleReset = () => {
        setFrontEndDefault(false)
        setZeroOrderDollar(false)
    }
    let data = props.history.location
    const getLastItem = thePath => thePath.substring(thePath.lastIndexOf('/') + 1)
    console.log(data,"props")
    
    return (
        <div className="orderTagDetail box-style"> 
          <h2 className='add-order-tag'>Add Order Tag</h2> 
          <div className="invoice_button">
               <div className="invoice_buttons">
                    <SecondaryIconButton
                        onClick={handleClose}
                        icon={<BackIcon size='medium' />}
                        label=''
                    />
                </div>
                <div className="invoice_buttons">
                    <FlatButton
                        tone="primary"
                        label="Back"
                        onClick={handleClose}
                        isDisabled={false}
                    />
                </div>
                <div className="invoice_buttons">
                    <FlatButton
                        tone="primary"
                        label="Reset"
                        onClick={handleReset}
                        isDisabled={false}
                    />
                </div>
                <div style={{paddingTop:'10px'}}>
                    <PrimaryButton
                        label="Save Tag"
                    />
                </div>
            </div> 
           <h3 className='tag-inf'>Tag Information</h3><br/><hr/>
              <div className="label" >
                 <label style={{paddingTop:'10px'}}><h4>Label <span className="asterisk-color">*</span></h4></label>&nbsp;&nbsp;
                 <div className='text' style={{display:'wrap',width:'50%'}}>
                 <TextInput  
                        title="default"
                        isReadOnly={false}  
                        onChange={()=> setDefaultText()}  
                        value={defaultText}  
                    />
                 </div>    
              </div><br/>
              <div className="checkbox">
                <CheckboxInput
                    value="foo-radio-value"
                    onChange={() => handleResponse(true)}
                    isChecked={frontEndDefault}
                    >Frontend default
                </CheckboxInput>
                <CheckboxInput
                     value="foo-radio-value"
                     onChange={() => handleResponse(false)}
                     isChecked={zeroDollarOrder}
                     >Zero dollar order
                </CheckboxInput>
              </div>  
        </div>
    )
}

export default OrderTagDetail
