import React from "react";
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';

const AddressTable = (props) =>{
  let addressList = []
  {props.address.shippingData.length>0 ? 
  props.address.shippingData.map((data) => {
    const addressDetails = {
      contactname: '',
      companyname: '',
      address: data.streetAddress1,
      city: data.city,
      postalcode: data.postalCode ,
      state: data.state,
      region: data.region,
      country: data.country
    }
    addressList = [...addressList, addressDetails]
  })
  :null}
    const columns = [
        { key: 'contactname', label: 'Contact Name' },
        { key: 'companyname', label: 'Compnay Name' },
        { key: 'address', label: 'Address' },
        { key: 'city', label: 'City' },
        { key: 'postalcode', label: 'Postal Code' },
        { key: 'state', label: 'State' },
        { key: 'region', label: 'Region' },
        { key: 'country', label: 'Country' }
      ];

  return(
      <div>
          {console.log(props,"table")}
          {props.address.shippingData.length>0 ?
          <DataTableManager columns={columns}>
          <DataTable 
          rows={addressList}
           />
        </DataTableManager>
        : null}
      </div>
  );
}

export default AddressTable;