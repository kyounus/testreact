import React from 'react';
import { Formik } from 'formik';
import Card from '@commercetools-uikit/card'
import Spacings from '@commercetools-uikit/spacings';
import TextField from '@commercetools-uikit/text-field';
import Grid from '@commercetools-uikit/grid';
import "./add-address.css"
import SelectInput from '@commercetools-uikit/select-input';
import State from "../../state-list.json";
import { ProjectKey } from "../../config/api";
import { getPhoneFormat } from '../../utils';

const AddAdress = (props) => {

    
    let countryCode = 'US';
    if (ProjectKey && (ProjectKey === 'mo-dev-eu-1' || ProjectKey === 'mo-preprod1-eu' || ProjectKey === 'mo-prod-eu')) {
        countryCode = 'EU';
    }
console.log(props,"props")
    return (
   <div>
     <Formik
       initialValues={{ 
        city : (props?.address?.city) ,
        postalCode : (props?.address?.postalCode) ,
        country : props?.address ? (props?.address?.country) :"" ,
        firstName : (props?.address?.firstName) , 
        lastName: (props?.address?.lastName) ,
        phoneNumber: (props?.address?.phoneNumber),
        streetAddress1 : (props?.address?.streetName) ,
        streetAddress2 : (props?.address?.streetAddress2), 
        companyName : (props?.address?.companyName), 
        //stateList : State,
        //state : props?.address ? (props?.address?.state) :""
        }}
        validateOnChange={false}
        validateOnBlur={false}
        // enableReinitialize={true}
       validate={values => {
         const errors = {};
         if (!values.city) {
          errors.city = 'Required';
        }
        if (!values.firstName) {
          errors.firstName = 'Required';
        }
         
        if (!values.lastName) {
          errors.lastName = 'Required';
        }
        if (!values.postalCode) {
          errors.postalCode = 'Required';
        }
        if (!values.state) {
          errors.state = 'Required';
        }
        if (!values.country) {
          errors.country = 'Required';
        }
        
         return errors;
       }}
       onSubmit={(values, { setSubmitting }) => {
           console.log(JSON.stringify(values, null, 2));
          //  setSubmitting(false);
       }}
     >
       {({
         values,
         errors,
        //  touched,
         handleChange,
         handleBlur,
         handleSubmit,
         closeAddressPage
        //  isSubmitting,
       }) => (
         <form onSubmit={handleSubmit}>
            <div className="modal-container">
           <Spacings.Inset scale="m">
                <Spacings.Stack scale="m">
                    <div className="container">
                        <Grid
                            gridGap="16px"
                            gridAutoColumns="1fr"
                            gridTemplateColumns="repeat(2, 1fr)"
                        >
                            {props.companyField ? 
                           <Grid.Item>
                                <Card>
                                    <Spacings.Inline>
                                        <TextField title="Company Name" name="companyName"  onChange={handleChange}/>
                                    </Spacings.Inline>
                                </Card>
                            </Grid.Item>
                            :null}
                            <Grid.Item>
                                <Card>
                                    <Spacings.Inline>
                                        <TextField title="First Name" name="firstName" 
                                        value = {values.firstName}
                                        onChange={handleChange}
                                        isRequired={true}
                                        />
                                        {errors.firstName ? <div className="validation">{errors.firstName}</div> : null}
                                    </Spacings.Inline>
                                </Card>
                            </Grid.Item>
                            <Grid.Item>
                                <Card>
                                    <Spacings.Inline>
                                        <TextField title="Last Name" name="lastName" value = { values.lastName} onChange={handleChange} isRequired={true}/>
                                        {errors.lastName ? <div className="validation">{errors.lastName}</div> : null}
                                    </Spacings.Inline>
                                </Card>
                            </Grid.Item>
                            <Grid.Item>
                                <Card>
                                    <Spacings.Inline>
                                        <TextField title="Street Address 1" name="streetAddress1" value = {values.streetAddress1} onChange={handleChange} isRequired={true}/>
                                    </Spacings.Inline>
                                </Card>
                            </Grid.Item>
                            <Grid.Item>
                                <Card>
                                    <Spacings.Inline>
                                        <TextField title="Street Address 2" name="streetAddress2" value = {values.streetAddress2} onChange={handleChange}/>
                                    </Spacings.Inline>
                                </Card>
                            </Grid.Item>

                            <Grid.Item>
                                <Card>
                                    <Spacings.Inline>
                                        <TextField title="City" name="city"  value = {values.city} onChange={handleChange} isRequired={true}/>
                                        {errors.city ? <div className="validation">{errors.city}</div> : null}
                                    </Spacings.Inline>
                                </Card>
                            </Grid.Item>
                            
                               
                                
                           { countryCode === 'US' ? 
                           <>
                           <Grid.Item>
                            <Card>
                           <label><h4>State<span className="asterisk-color">*</span></h4></label>                        
                            <SelectInput
                                    name="state"
                                    value={values.state}
                                    onChange={handleChange}  
                                    options={values.stateList.map((items) =>{
                                        return{ label:items.label, value: items.value}
                                    })}
                                />
                                {errors.state ? <div className="country-validation">{errors.state}</div> : null}
                                </Card>
                                </Grid.Item>
                                </>
                                : ''}
                                        
                           
                            <Grid.Item>
                                <Card>
                                        <label><h4>Country<span className="asterisk-color">*</span></h4></label>
                        <SelectInput
                             value="US"
                            value={values.country}

                            name="country" 
                            onChange={handleChange}
                            options={ countryCode == 'US' ? 
                            [{ value: "US", label: "US" }]: [{ value: "UK", label: "UK"}
                        ]}
                        />
                        {errors.country ? <div className="country-validation">{errors.country}</div> : null}
                                </Card>
                            </Grid.Item>
                            <Grid.Item>
                                <Card>
                                    <Spacings.Inline>
                                        <TextField title="Postal code" name="postalCode" value = {values.postalCode} onChange={handleChange} isRequired={true}/>
                                        {errors.postalCode ? <div className="validation">{errors.postalCode}</div> : null}
                                    </Spacings.Inline>
                                </Card>
                            </Grid.Item>
                            <Grid.Item>
                                <Card>
                                    <Spacings.Inline>
                                        <TextField title="Phone" name="phoneNumber"  value = {(values.phoneNumber)} onChange={handleChange} isRequired={true}/>
                                    </Spacings.Inline>
                                </Card>
                            </Grid.Item>
                        </Grid>
                    </div>
                    {props?.addressData?.length > 0 ? props.addressData(values) :null}
                </Spacings.Stack>
            </Spacings.Inset>
            </div>
         </form>
       )}
     </Formik>
   </div>);
};
 
 export default AddAdress;