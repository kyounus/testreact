import React, { useState } from "react";
import TextField from '@commercetools-uikit/text-field';
import Grid from '@commercetools-uikit/grid';
import Card from '@commercetools-uikit/card'
import Spacings from '@commercetools-uikit/spacings';
import PrimaryButton from '@commercetools-uikit/primary-button';
import { SERVICES } from '../../config/api'
import { ContentNotification } from '@commercetools-uikit/notifications';
import CheckboxInput from '@commercetools-uikit/checkbox-input';
import Address from  "./add-address";
import Text from '@commercetools-uikit/text';
import { CallApi } from '../../plugin/Axios';

const Carrier = (props) => {
    const [notification, setNotification] = useState(false)
    const [errorNotification, setErrorNotification] = useState(false)
    const [notificationMessage, setNotificationMessage] = useState('')
    const carrier = props?.carrier;
    const carrierId = props?.carrier?.id
    const employeeId = props?.employeeId ? props.employeeId : props.customerId;
    const [parcel, setParcel] = useState(props?.carrier?.delivery === "PARCEL" ? true : false)
    const [ addressData , setAddressData] = useState({})
    const [frieght, setFrieght] = useState(props?.carrier?.delivery === "FREIGHT" ? true : false)
    const [selectedDelivery, setSelectedDelivery] = useState(carrier ?carrier?.type : "")
    const [carrierName, setCarrierName] = useState(carrier ? carrier?.carrierName : "")
    const [carrierAccountNumber, setCarrierAccountNumber] = useState(carrier ? carrier?.carrierNumber : "")
    let selectedBillingAddress = props?.billingAddress?.find(data => data.billingAddressId === props?.carrier?.id_)

    const handleSubmit = () => {
        if(carrierName === "" || carrierAccountNumber === "" || selectedDelivery === "" || addressData.city === "" || addressData.firstName  === "" || addressData.lastName === "" ||
        addressData.streetAddress1 === "" || addressData.state  === "" || addressData.phoneNumber === "" ||  
        addressData.postalCode === "" ){
            setNotification(false)
            setErrorNotification(true)
            setNotificationMessage("Please enter the required details")
        }
        else{
            setErrorNotification(false)
        let requestData = {
            "carriers": [
                {
                    "billingAddress": {
                        // "billingAddressId": "string",
                        "city": addressData.city,
                        "country": addressData.country,
                        "firstName": addressData.firstName,
                        "lastName": addressData.lastName,
                        "phoneNumber": addressData.phoneNumber,
                        "postalCode": addressData.postalCode,
                        "shippingAddressId": addressData.id,
                        "state": addressData.state,
                        "streetAddress1": addressData.streetAddress1,
                        "streetAddress2": addressData.streetAddress2
                      },
                    "carrierAccountNumber": carrierAccountNumber,
                    "carrierName": carrierName,
                    "type": selectedDelivery
                }
            ],
        }
        const url=`${SERVICES.ACCOUNT}/api/v1/user/${employeeId}/carriers/${carrierId}/`
        CallApi(url,'post',requestData)
            .then(response => {
                console.log(response)
                setNotification(true)
                setNotificationMessage("Updated the details")
            })
            .catch(error => {
                console.log(error)
                setErrorNotification(true)
                setNotificationMessage(error)
            })
    }
    }

    const handleNewSubmit = () => {
        if(carrierName === "" || carrierAccountNumber === "" || selectedDelivery === "" || addressData.city === "" || addressData.firstName  === "" || addressData.lastName === "" ||
        addressData.streetAddress1 === "" || addressData.state  === "" || addressData.phoneNumber === "" ||  
        addressData.postalCode === "" ){
            setNotification(false)
            setErrorNotification(true)
            setNotificationMessage("Please enter the required details")
        }
        else{
            setErrorNotification(false)
        let requestData = {
            "carriers": [
                {
                    "billingAddress": {
                        "billingAddressId": "string",
                        "city": addressData.city,
                        "country": addressData.country,
                        "firstName": addressData.firstName,
                        "lastName": addressData.lastName,
                        "phoneNumber": addressData.phoneNumber,
                        "postalCode": addressData.postalCode,
                        "shippingAddressId": addressData.id,
                        "state": addressData.state,
                        "streetAddress1": addressData.streetAddress1,
                        "streetAddress2": addressData.streetAddress2
                      },
                    "carrierAccountNumber": carrierAccountNumber,
                    "carrierName": carrierName,
                    "type": selectedDelivery
                }
            ],
        }
        console.log(requestData)
        const url = `${SERVICES.ACCOUNT}api/v1/user/${employeeId}/carriers/`
        CallApi(url,'post',requestData)
            .then(response => {
                console.log(response)
                setNotification(true)
                setNotificationMessage("Added the details")
            })
            .catch(error => {
                console.log(error)
                setErrorNotification(true)
                setNotificationMessage(error)
            })
        }
    }
    const handleDelivery = (selectedCheckBox) => {
        setParcel(false)
        setFrieght(false)
        if (selectedCheckBox) {
            setFrieght(!frieght)
            setSelectedDelivery('FREIGHT')
        }
        else {
            setParcel(!parcel)
            setSelectedDelivery('PARCEL')
        }
    }
    return (
        <>
            <Grid
                gridGap="16px"
                gridAutoColumns="1fr"
                gridTemplateColumns="repeat(2, 1fr)"
            >

                <Grid.Item>
                    <Card>
                        <Spacings.Inline>
                            <TextField title="Carrier Name" name="companyName"
                                value={carrierName}
                                onChange={(event) => setCarrierName(event.target.value)}
                                isRequired={true}
                            />
                        </Spacings.Inline>
                    </Card>
                </Grid.Item>
                <Grid.Item>
                    <Card>
                        <Spacings.Inline>
                            <TextField title="Carrier account number" name="firstName"
                                value={carrierAccountNumber}
                                onChange={(event) => setCarrierAccountNumber(event.target.value)}
                                isRequired={true}
                            />
                        </Spacings.Inline>
                    </Card>
                </Grid.Item>
            </Grid>
            <div className="carrier-checkbox">
                <div><b>Delivery :</b><span className="asterisk-color">*</span> &nbsp;&nbsp;</div>
                <Spacings.Inline>
                    <CheckboxInput
                        onChange={() => handleDelivery(false)}
                        isChecked={parcel}
                    >Parcel</CheckboxInput>
                    <CheckboxInput
                        onChange={() => handleDelivery(true)}
                        isChecked={frieght}
                    >Frieght</CheckboxInput>
                </Spacings.Inline>
            </div>
            <Text.Headline as="h4"><b>Billing Address</b></Text.Headline>
            <Address address={selectedBillingAddress} 
                    addressData = {(data) => setAddressData(data)}/>
            <div className="save_button">
                {carrier ? <PrimaryButton label="Save" onClick={handleSubmit} /> :
                    <PrimaryButton label="Save" onClick={handleNewSubmit} />}
            </div>
            <div className="notification">
                {notification ? <ContentNotification type="success">{notificationMessage}</ContentNotification> : null}
                {errorNotification ? <ContentNotification type="error">{notificationMessage}</ContentNotification> : null}
            </div>
        </>
    );
}
export default Carrier;