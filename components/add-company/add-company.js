import React, { useState, useEffect, useRef } from "react";
import SecondaryButton from '@commercetools-uikit/secondary-button';
import PrimaryButton from '@commercetools-uikit/primary-button';
import TextField from '@commercetools-uikit/text-field';
import './add-company.css';
import Text from '@commercetools-uikit/text';
import AddAddress from './add-address';
import SelectField from '@commercetools-uikit/select-field';
import { ContentNotification } from '@commercetools-uikit/notifications';
import Spacings from '@commercetools-uikit/spacings';
import { SERVICES } from "../../config/api";
import PaymentDetails from "../payments";
import ToggleInput from '@commercetools-uikit/toggle-input';
import CustomerDetails from "./displayCustomerDetails"
import AddressList from './address-list';
import { ListIcon } from '@commercetools-uikit/icons';
import LinkButton from '@commercetools-uikit/link-button';
import NoRecords from '../shared/messages';
import Stamp from '@commercetools-uikit/stamp';
import { addHyperLink } from '../../utils';
import { CallApi } from '../../plugin/Axios';

const [general, address, files, paymentterms, taxexemption, customers] = ['General', 'Address', 'files', 'Payment terms', 'Tax exemption', 'Customers'];

const AddCompany = (props) => {

    const [companyName, setCompanyName] = useState('');
    const [categoryDropdownValue, setCategoryDropdownValue] = useState('');
    const [employeesDropdownValue, setEmployeesDropdownValue] = useState("")
    const [officeDropdownValue, setOfficeDropdownValue] = useState("")
    const [addCompanyFlow, setAddCompanyFlow] = useState(true);
    const [notificationMessage, setNotificationMessage] = useState('Updated');
    const [errorMessage, setErrorMessage] = useState('');
    const [accountId, setAccountId] = useState(0)
    const [isSaved, setIsSaved] = useState(false)
    const [isTaxExemptionEnabled, setTaxExemptionEnabled] = useState(false);
    const [isTaxExemptionSaved, setTaxExemptionSaved] = useState(false);
    const [paymentData, setPaymentData] = useState([])
    const [categories, setCategories] = useState([])
    const [addresses, setAddresses] = useState([]);
    const [carrier, setCarrier] = useState([]);
    const [additionalFreightInfo, setAdditionalFreightInfo] = useState([]);
    const [employeeId, setEmployeeId] = useState([]);
    const [updateAddress, setUpdateAddress] = useState(false);
    const [newAddress, setNewAddress] = useState(false);
    const [spinner, setSpinner] = useState(false);
    const [requiredCategory, setRequiredCategory] = useState(false)
    const [activeTab, setActiveTab] = useState(general);
    const [modal, setModal] = useState(false);

    let data = props.history.location
    const getLastItem = thePath => thePath.substring(thePath.lastIndexOf('/') + 1)
    let companyId = data.id ? data.id : getLastItem(props.location.pathname)

    const handleNotification = (message) => {
        setNotificationMessage(message);
        if (message) {
            setTimeout(() => {
                setNotificationMessage('');
            }, 5000);
        }
    }

    const handleError = (error) => {
        setErrorMessage(error);
        console.log('handleError', error, errorMessage)
        if (error) {
            setTimeout(() => {
                setErrorMessage('');
            }, 5000);
        }
    }

    const updateCompanyInfo = (accId, payload, message = 'Company info updated') => {
        let companyData = { ...payload, accountId: accId };
        let url = `${SERVICES.ACCOUNT}/api/v1/user/signup/company-info/`
        CallApi(url,'post',companyData)
            .then(res => {
                handleNotification(message)
            }).catch(e => {
                handleError('Unable to process your request');
            })
    }

    useEffect(() => {
        let url = `${SERVICES.ACCOUNT}/api/v1/accounts/categories/`
        CallApi(url,'get')
            .then(res => {
                handleCategorynew(res?.data?.value?.companyCategories.companyCategory)
            })
            .catch((error) => {
                console.log('ERROR:', error);
            })
        if (companyId === 'new') {
            setAccountId('')
            setAddCompanyFlow(true);
            resetData()
            return;
        }
        setAccountId(companyId);
        let urls = `${SERVICES.CHECKOUT}api/v1/users/checkout/account/${companyId}/`
        CallApi (urls,'get')
            .then(res => {
                setSpinner(false)
                setAddCompanyFlow(false);
                setEmployeeId(res?.data?.value?.employeeIds)
                setCompanyName(res?.data?.value?.companyName || '')
                setEmployeesDropdownValue(res?.data?.value?.noOfEmployees || '')
                setOfficeDropdownValue(res?.data?.value?.noOfOfficeSpaces || '')
                setCategoryDropdownValue(res?.data?.value?.companyCategory?.id)
                setAddresses(res?.data?.value?.addresses);
                setCarrier(res?.data?.value?.carriers)
                setAdditionalFreightInfo(res?.data?.value?.additionalFreightInfos)
                setPaymentData(res?.data?.value?.payments);
                setTaxExemptionEnabled(res?.data?.value?.taxExemptFlag)
            })
            .catch((error) => {
                console.log('ERROR:', error);
                resetData()
            })

    }, [companyId, updateAddress, newAddress]);

    useEffect(() => {
        handleError('');
        handleNotification('');
        setRequiredCategory('');
        document.addEventListener("mousedown", (e) => {
            if (e.target.tagName === 'INPUT' || e.target.innerText === 'Select...') {
                handleError('');
                handleNotification('');
                setRequiredCategory('');
            }
        });
    }, [])

    const resetData = () => {
        setPaymentData('')
        handleError('')
        handleNotification('')
    }

    const handleCategorynew = (data) => {
        let tempCategories = []
        {
            data.map((category) => {
                const tempObj = {
                    value: category.id, label: category.name
                }
                tempCategories = [...tempCategories, tempObj]
            })
        }
        setCategories(tempCategories)
    }

    const saveTaxExemption = () => {
        updateCompanyInfo(accountId, { taxExempt: isTaxExemptionEnabled }, `Tax Exemption status : ${isTaxExemptionEnabled ? 'ENABLED' : 'DISABLED'}`);
        setTaxExemptionSaved(true)
    }
    const handleUpdateClick = () => {
        setUpdateAddress(!updateAddress)
    }
    const handleNewAddress = () => {
        setNewAddress(!newAddress)
    }
    const handleSpinner = () => {
        setSpinner(!spinner)
    }
    const onSubmit = async () => {
        if (categoryDropdownValue === "") {
            return setRequiredCategory(true)
        }
        // setAccountId('')
        setRequiredCategory(false)
        let requestData = {
            "companyCategoryId": categoryDropdownValue,
            "companyName": companyName
        }
        let officeData = {
            "numberOfEmployees": employeesDropdownValue,
            "numberOfOfficeSpaces": officeDropdownValue
        }
        if (!accountId) {
            let url = `${SERVICES.ACCOUNT}/api/v1/accounts/`
            await CallApi(url ,'post',requestData)
                .then(response => {
                    if (response.data.errorMessage) {
                        handleError(response.data.errorMessage)
                    } else {
                        setAccountId(response.data.id);
                        updateCompanyInfo(response.data.id, officeData);
                        handleNotification('Successfully company added');
                        setIsSaved(true)
                        setAddCompanyFlow(false);
                    }
                }).catch(error => {
                    handleError(error.message)
                });
        } else {
            officeData = {
                ...requestData,
                ...officeData,
            }
            updateCompanyInfo(accountId, officeData);
        }
    }

    return (
        <div>
            <div className="title-text">
                {!addCompanyFlow && <LinkButton
                    to={addHyperLink('companies', props)}
                    iconLeft={<ListIcon />}
                    label="Company List"
                    isDisabled={false}
                />
                }
                <Text.Headline as="h2">{addCompanyFlow ? "Add a company" : companyName}</Text.Headline>
                <div>
                    <Spacings.Inline scale="s">
                        <div>
                            <div className="main-tab-list">
                                {!addCompanyFlow ? <ul className="sub-tab-list">
                                    <li><a active={activeTab === general} id={general} className={activeTab === general ? 'active' : ''} onClick={(e) => setActiveTab(e.target.id)}>General</a></li>
                                    <li><a active={activeTab === address} id={address} className={activeTab === address ? 'active' : ''} onClick={(e) => setActiveTab(e.target.id)}>Addresses</a></li>
                                    <li><a active={activeTab === paymentterms} id={paymentterms} className={activeTab === paymentterms ? 'active' : ''} onClick={(e) => setActiveTab(e.target.id)}>Payment terms</a></li>
                                    <li><a active={activeTab === taxexemption} id={taxexemption} className={activeTab === taxexemption ? 'active' : ''} onClick={(e) => setActiveTab(e.target.id)}>Tax exemption</a></li>
                                    <li><a active={activeTab === customers} id={customers} className={activeTab === customers ? 'active' : ''} onClick={(e) => setActiveTab(e.target.id)}>Customers</a></li>
                                </ul> : null}
                            </div>
                        </div>
                    </Spacings.Inline>
                </div>
            </div>
            {activeTab === general && <div className="info-text">
                <div className="text-field">
                    <TextField title="Company Name" value={companyName} onChange={(event) => { setCompanyName(event.target.value) }} />
                </div>
                <div className="text-field">
                    <SelectField
                        title="Category"
                        value={categoryDropdownValue}
                        options={categories}
                        onChange={(event) => setCategoryDropdownValue(event.target.value)}
                    />
                </div>
                <div className="text-field">
                    <SelectField
                        title="Number of Employees"
                        value={employeesDropdownValue}
                        options={[
                            { value: '< 20', label: '< 20' },
                            { value: '21-50', label: '21-50' },
                            { value: '50-100', label: '50-100' },
                            { value: '100-500', label: '100-500' },
                            { value: '500-1000', label: '500-1000' },
                            { value: '1000+', label: '1000+' },
                        ]}
                        onChange={(event) => setEmployeesDropdownValue(event.target.value)}
                    />
                </div>
                <div className="text-field">
                    <SelectField
                        title="Number of Office Spaces"
                        value={officeDropdownValue}
                        options={[
                            { value: '1', label: '1' },
                            { value: '2-5', label: '2-5' },
                            { value: '5+', label: '5+' },
                        ]}
                        onChange={(event) => setOfficeDropdownValue(event.target.value)}
                    />
                </div>
                <div className="company-save-button">
                    <SecondaryButton label="Save" onClick={onSubmit} isDisabled={isSaved} />
                </div>
            </div>}
            {activeTab === address && <AddressList
                shippingAddress={true}
                billingAddress={true}
                carrierInfo={true}
                addresses={addresses} employeeId={employeeId}
                updateAddress={handleUpdateClick}
                newAddress={handleNewAddress}
                handleSpinner={handleSpinner}
                spinner={spinner}
                carrier={carrier}
                additionalFreightInfo={additionalFreightInfo} />}
            {activeTab === files && <div className="info-text"><h5>Files Tab</h5></div>}
            {activeTab === taxexemption && <div className="info-text">
                <table className="tax-toggle">
                    <tr>
                        <td>Tax exemption status:</td>
                        <td>
                            <Stamp tone={isTaxExemptionEnabled ? 'positive' : 'warning'}><Text.Detail>{isTaxExemptionEnabled ? 'Approved' : 'In-Review'}</Text.Detail></Stamp>
                        </td>
                    </tr>
                    <tr>
                        <td>Approve Tax exemption</td>
                        <td>
                            <ToggleInput isDisabled={false} isChecked={isTaxExemptionEnabled} onChange={(event) => setTaxExemptionEnabled(event.target.checked)} size="small" />
                        </td>
                    </tr>
                </table>
                <div>
                    <PrimaryButton label="Save" onClick={saveTaxExemption} isDisabled={isTaxExemptionSaved} />
                </div>
            </div>}
            {activeTab === paymentterms && <div>
                <div className="info-text">
                    {
                        paymentData?.length ?
                            <PaymentDetails payments={paymentData} accountId={accountId} fnDisplayNotification={handleNotification} />
                            :
                            <NoRecords message='No data found' />
                    }
                </div>
            </div>}
            {activeTab === customers && <div>
                <div className="info-text">
                    <CustomerDetails accountId={accountId} />
                </div>
            </div>}
            <div>{modal && <div className="ModalContainer">
                <div ><AddAddress
                    handleClose={() => setModal(false)}
                    accountId={accountId}
                /> </div>
            </div>}</div>
            <div className="notification">
                {notificationMessage && <div> <ContentNotification type="success"> {notificationMessage} </ContentNotification></div>}
                {errorMessage && <div> <ContentNotification type="error"> {errorMessage} </ContentNotification> </div>}
                {requiredCategory && <div> <ContentNotification type="error"> Select Company Category </ContentNotification> </div>}
            </div>
        </div>
    );
}

export default AddCompany;
