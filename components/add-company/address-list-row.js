import React, { useState } from "react";
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import "./address-list-row.css";
import NoRecords from '../shared/messages';
import { FormModalPage, CustomFormModalPage } from '@commercetools-frontend/application-components';
import ShippingAddress from "./shipping"
import LoadingSpinner from '@commercetools-uikit/loading-spinner';

const AddressListRow = (props) =>{
  const [address ,setAddress] = useState(false)
  const [addressData ,setAddressData] = useState({})
  const [shippingAddressId ,setShippingAddressId] = useState('')
  const [customerShippingAddressId ,setCustomerShippingAddressId] = useState('')
  const [billingAddress ,setBillingAddress] = useState(false)
  const [modal , setModal] =useState(false)
  let { addresses } = props;
  let addressList = addresses || [] ;
  
  addressList = addressList.map(data => {
      return {
          ...data,
          id: data?.shippingAddressId || data?.billingAddressId,
          id1 : data.id,
          contactname: data?.firstName + ' ' + data?.lastName,
          companyname: data?.companyName || '',
          address: data?.streetName ? data?.streetName : (data?.streetAddress1 + (data?.streetAddress2 ? ',' + data?.streetAddress2 : '')),
          phoneNumber: data?.phone || ''
        }
  });
  const columns = [
    { key: 'contactname', label: 'Contact Name' },
    { key: 'address', label: 'Address' },
    { key: 'city', label: 'City' },
    // { key: 'state', label: 'State' },
    { key: 'postalCode', label: 'Postal Code' },
    { key: 'country', label: 'Country' },
    { key: 'phoneNumber', label: 'Phone' }
  ];
 const handleRowClick =(item) =>{
   item.billingAddressId ? setBillingAddress(true) : null
   setAddressData(item)
   setAddress(true)
   setShippingAddressId(item.shippingAddressId)
   setCustomerShippingAddressId(item.id1)
   setModal(props.modal)
 }
 const handleClose =() =>{
  setAddress(false)
  props.updateAddress(true)
  props.handleSpinner(true)
}
  return(
      <div className="address-list">
        {props.spinner ? <div className="address-spinner">
          <LoadingSpinner size="s">Loading</LoadingSpinner></div> : addressList.length > 0 ?
            <DataTableManager columns={columns}>
                <DataTable rows={addressList} 
                onRowClick={
                  (item) => handleRowClick(item)
                }/>
            </DataTableManager>
        : <NoRecords message="No Addresses added" />}
      { modal ? 
      <CustomFormModalPage
          title={"Address"}
          isOpen={address}
          onClose={handleClose}
          topBarCurrentPathLabel={"Address"}
          topBarPreviousPathLabel="Back"
        >
          <ShippingAddress  
          ShippingAddress={addressData} billingAddress={billingAddress} employeeId={props.employeeId}
          shippingAddressId={shippingAddressId} carrier={props.carrier} additionalFreightInfo={props.additionalFreightInfo}
          customerId={props.customerId}
          customerShippingAddressId={customerShippingAddressId}
          defaultShippingAddressId={props.defaultShippingAddressId}
          />
        </CustomFormModalPage>
          :null}
      </div>
  );
}
export default AddressListRow;