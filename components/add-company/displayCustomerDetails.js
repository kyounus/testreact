import React, {useState, useEffect} from 'react';
import { Pagination } from '@commercetools-uikit/pagination';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import {SERVICES} from "../../config/api";
import axios from "axios";
import { useIntl } from 'react-intl';
import {CallApi } from "../../plugin/Axios"
import "./shipping-address.css"


import LoadingSpinner from '@commercetools-uikit/loading-spinner';

const displayCustomerDetails = (props) => {

    
    const [customerData, setCustomerData] = useState([]);
    const [totalResults, setTotalResults] = useState(0);
    const [spinner, setSpinner] = useState(true);
    const [pageLimit, setPageLimit] = useState(20);
    const { formatTime, formatDate } = useIntl();
    const [pageNumber, setPageNumber] = useState(1);
    const [custID, setCustID] = useState('');
    const { accountId } = props;
    const [shippingDetails, setShippingDetails] = useState([]);
    const [billingDetails, setBillingDetails] = useState([])

   useEffect(() => {
        let offset = (pageNumber - 1) * pageLimit;
        if (totalResults && totalResults < offset) {
            setPageNumber(0);
            return;
        }
        setSpinner(true);
        let url = `${SERVICES.ACCOUNT}/api/v1/accounts/${accountId}/users?limit=${pageLimit}&offset=${offset}`
        CallApi(url, 'get')
            .then(res => {
                console.log(res, "response")
                setCustomerData(res.data);
                setTotalResults(res.data.length);
                
                    
            })
            .then(res =>
                setSpinner(false)
            )
            .catch(error => console.log("error", error))
    }, [pageNumber, pageLimit])

    // useEffect(() =>{
    //     axios.get(`https://dev1-checkout-service-api1.molekule.com/api/v1/users/checkout/account/${accountId}`)
    //         .then(res => {
    //             console.log(res, "response")
                
                
                    
    //         })
    //         .then(res =>
    //             setSpinner(false)
    //         )
    //         .catch(error => console.log("error", error))
    // },[])

    

    let customerList = [];
    let IdList = [];

    {   customerData.length >=0 ?
        customerData.map(customerData => {
        const createdOn = formatDate(customerData.createdAt) + " " + formatTime(customerData.createdAt)
        const modifiedOn = formatDate(customerData.lastModifiedAt) + " " + formatTime(customerData.lastModifiedAt)
        let customerDetails = {
            id : customerData.id,
            firstName: customerData.firstName,
            lastName : customerData.lastName,
            email : customerData.email,
            createdon: createdOn,
            modifiedon: modifiedOn
        }
        IdList=[...IdList, customerData.id]
        customerList= [...customerList, customerDetails]
    })
    : null
    }
        console.log(IdList);

      const columns = [
        
        { key: 'firstName', label: 'First Name' },
        { key: 'lastName', label: 'Last Name' },
        { key: 'email', label: 'Email' },
        { key: "createdon", label: "Created on" },
        { key: "modifiedon", label: "Modified on" },
        
      ];

    return (
        <div>
              <div className="main-info" >
                <DataTableManager columns={columns}
                    onSettingsChange={(action, nextValue) => { }} >
                    <DataTable rows={customerList}/>
                </DataTableManager>

                {spinner ? <div className="spinner">
                    <LoadingSpinner size="s">Loading</LoadingSpinner></div> : null}
                {!spinner ? 
                <div style={{paddingTop: "10px"}}>
                    <Pagination 
                    totalItems={totalResults}
                    page={pageNumber}
                    perPageRange="m"
                    onPageChange={(page) => {
                        console.log(page)
                        setPageNumber(page)
                    }}
                    onPerPageChange={(lmt) => {
                        console.log(lmt)
                        setPageLimit(lmt);
                    }}
                />
                </div>
                
                 : null}
            </div>
        </div>
    )
}

export default displayCustomerDetails
