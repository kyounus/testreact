import React, { useState } from "react";
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import "./address-list-row.css";
import NoRecords from '../shared/messages';
import { FormModalPage, CustomFormModalPage } from '@commercetools-frontend/application-components';
import LoadingSpinner from '@commercetools-uikit/loading-spinner';
import Carrier from "./carrier";

const CarrierList = (props) => {
    const [modal, setModal] = useState(false)
    const [carrierModal, setCarrierModal] = useState(true)
    const [selectedCarrier, setSelectedCarrier] = useState([])
    const carrier = props.carrier
    let carrierList = []
    carrier?.map(data => {
        const carrierDetails = {
            id: data.carrierId,
            id_: data.billingAddressId,
            carrierName: data.carrierName,
            carrierNumber: data.carrierAccountNumber,
            delivery: data.type
        }
        carrierList.push(carrierDetails)
    })
    const columns = [
        { key: 'carrierName', label: 'Carrier Name' },
        { key: 'carrierNumber', label: 'Carrier account number' },
        { key: 'delivery', label: 'Delivery type' },
    ];
    const handleRowClick = (item) => {
        setSelectedCarrier(item)
        setModal(!modal)
    }
    const handleClose = () => {
        setModal(!modal)
        props.handleSpinner(true)
        props.updateAddress(true)
    }
    return (
        <div className="address-list">
            {props.spinner ?
                <div className="address-spinner">
                    <LoadingSpinner size="s">Loading</LoadingSpinner></div> :
                carrierList.length > 0 ?
                    <DataTableManager columns={columns}>
                        <DataTable
                            rows={carrierList}
                            onRowClick={
                                (item) => handleRowClick(item)
                            }
                        />
                    </DataTableManager>
                    : <NoRecords message="No Carriers added" />}
            {modal ?
                <CustomFormModalPage
                    title={"Carrier"}
                    isOpen={carrierModal}
                    onClose={handleClose}
                    topBarCurrentPathLabel={"Carrier"}
                    topBarPreviousPathLabel="Back"
                // onSecondaryButtonClick={handleClose}
                // onPrimaryButtonClick={handleSubmit}
                >
                    <Carrier carrier={selectedCarrier} employeeId={props.employeeId} billingAddress={props.billingAddress}/>
                </CustomFormModalPage>
                : null}
        </div>
    );
}
export default CarrierList;