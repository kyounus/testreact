import React, { useState } from 'react';
import CollapsiblePanel from '@commercetools-uikit/collapsible-panel';
import "./shipping-address.css"
import Spacings from '@commercetools-uikit/spacings';
import AddressListRow from "./address-list-row"
import SecondaryButton from '@commercetools-uikit/secondary-button';
import ShippingAddress from "./shipping"
import { FormModalPage, CustomFormModalPage } from '@commercetools-frontend/application-components';
import CarrierList from "./carrier-list";
import Carrier from "./carrier";
import { ProjectKey } from "../../config/api";

const AddressList = (props) => {

    const [address ,setAddress] = useState(true)
    const [modal ,setModal] = useState(false)
    const [modal1 ,setModal1] = useState(true)
    const [carrierPage ,setCarrierPage] = useState(false)
    const [shipping ,setShipping] = useState(false)
    const { addresses } = props;
    const { billingAddresses = [], shippingAddresses = [] } = addresses || {};
    const handleClick =() =>{
       setModal(true)
       setShipping(true)
       setCarrierPage(false)
    }
    const handleCarrierClick = () =>{
        setModal(true)
        setCarrierPage(true)
        setShipping(false)
    }
    const handleClose =() =>{
        props.newAddress(true)
        setModal(false)
        props.handleSpinner(true)
    }
    let countryCode = 'US';
    if (ProjectKey && (ProjectKey === 'mo-dev-eu-1' || ProjectKey === 'mo-preprod1-eu' || ProjectKey === 'mo-prod-eu')) {
        countryCode = 'EU';
    }
    return (
        <div className="main-info">
            <Spacings.Stack>
            {props.shippingAddress ? 
                <CollapsiblePanel header="Shipping Address">
                    <AddressListRow addresses={shippingAddresses} employeeId={props.employeeId} 
                    updateAddress={props.updateAddress} handleSpinner={props.handleSpinner} spinner={props.spinner} modal={modal1}
                    carrier={props.carrier} additionalFreightInfo={props.additionalFreightInfo}
                    customerId={props.customerId}
                    defaultShippingAddressId={props.defaultShippingAddressId}
                    />
                    <div className="save_button">
                        <SecondaryButton label="Add new address" 
                        onClick={handleClick}/>
                    </div>
                </CollapsiblePanel> : null }
                {props.billingAddress ? 
                <CollapsiblePanel header="Billing Address">
                    <AddressListRow addresses={billingAddresses} employeeId={props.employeeId}
                    updateAddress={props.updateAddress} handleSpinner={props.handleSpinner} spinner={props.spinner}
                    customerId={props.customerId}
                    />
                </CollapsiblePanel>
                :null}
               {!props.addOrder && props.carrierInfo && countryCode === 'US' ? <CollapsiblePanel header="Carrier Info">
                    <CarrierList carrier={props.carrier} employeeId={props.employeeId} billingAddress={billingAddresses}
                     customerId={props.customerId}/>
                    <div className="save_button">
                        <SecondaryButton label="Add new carrier" 
                        onClick={handleCarrierClick}/>
                    </div>
                </CollapsiblePanel>: null}
            </Spacings.Stack>
            { modal ?  <CustomFormModalPage
                            title={"Address"}
                            isOpen={address}
                            onClose={handleClose}
                            topBarCurrentPathLabel={"Address"}
                            topBarPreviousPathLabel="Back"
                            onSecondaryButtonClick={handleClose}
                             // onPrimaryButtonClick={handleSubmit}
                        >
           {shipping ? 
             <ShippingAddress   employeeId={props.employeeId} customerId={props.customerId} />:null}
                {carrierPage ? <div> <Carrier  employeeId={props.employeeId} billingAddress={billingAddresses} customerId={props.customerId}/> </div>:null}
                </CustomFormModalPage>
            :null}
        </div>
    );
};
export default AddressList;