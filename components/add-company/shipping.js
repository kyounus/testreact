import React, { useState } from 'react';
import CheckboxInput from '@commercetools-uikit/checkbox-input';
import Label from '@commercetools-uikit/label';
import MultilineTextField from '@commercetools-uikit/multiline-text-field';
import SelectField from '@commercetools-uikit/select-field';
import "./shipping-address.css"
import Grid from '@commercetools-uikit/grid';
import Spacings from '@commercetools-uikit/spacings';
import PrimaryButton from '@commercetools-uikit/primary-button';
import AddAddress from './add-address';
import CollapsiblePanel from '@commercetools-uikit/collapsible-panel';
import {SERVICES,code,  ENV_EU} from '../../config/api'
import { ContentNotification } from '@commercetools-uikit/notifications';
import Carrier from "./carrier";
import { CallApi } from '../../plugin/Axios';
import { ProjectKey } from "../../config/api";


const ShippingAddress = (props) => {

    let countryCode = 'US';
    if (ProjectKey && (ProjectKey === 'mo-dev-eu-1' || ProjectKey === 'mo-preprod1-eu' || ProjectKey === 'mo-prod-eu')) {
        countryCode = 'EU';
    }
    console.log(props)
    let customerId = props?.customerId
    let additionalFreightInfo = props?.additionalFreightInfo?.find(data => data.shippingAddressId === props?.shippingAddressId)
    const [isChecked, setIsChecked] = useState(additionalFreightInfo ? true :false)
    const [selectedLoadingDock, setSelectedLoadingDock] = useState(false)
    const [loadingDockYes, setLoadingDockYes] = useState((additionalFreightInfo?.loadingDock === true) ? true :false)
    const [loadingDockNo, setLoadingDockNo] = useState((additionalFreightInfo?.loadingDock === false) ? true :false)
    const [selectedForkLift, setSelectedForkLift] = useState(false)
    const [forkLiftYes, setForkLiftYes] = useState((additionalFreightInfo?.forkLift === true) ? true :false)
    const [forkLiftkNo, setForkLiftNo] = useState((additionalFreightInfo?.forkLift === false) ? true :false)
    const [receivingHours, setReceivingHours] = useState(['12:00 AM', '1:00 AM', '2:00 AM', '3:00 AM', '4:00 AM', '5:00 AM', '6:00 AM', '7:00 AM', '8:00 AM', '9:00 AM', '10:00 AM', '11:00 AM', '12:00 PM', '1:00 PM', '2:00 PM', '3:00 PM', '4:00 PM', '5:00 PM', '6:00 PM', '7:00 PM', '8:00 PM', '9:00 PM', '10:00 PM', '11:00 PM'
    ])
    const [deliveryAppointment, setDeliveryAppointment] = useState((additionalFreightInfo?.deliveryAppointmentRequired === true) ? true :false)
    const [deliveryInstructions, setDeliveryInstructions] = useState((additionalFreightInfo?.instructions?.length>0) ? additionalFreightInfo?.instructions : "")
    const [carrierName, setCarrierName] = useState('')
    const [carrierNumber, setCarrierNumber] = useState('')
    const [parcelDelivery, setParcelDelivery] = useState(false)
    const [freightDelivery, setFreightDelivery] = useState(false)
    const [dropDown, setDropDown] = useState(additionalFreightInfo?.receivingHours ? additionalFreightInfo.receivingHours.from :'')
    const [toDropDown, setToDropDown] = useState(additionalFreightInfo?.receivingHours ? additionalFreightInfo.receivingHours.to :'')
    const [ addressData , setAddressData] = useState({})
    const [ newAddress, setnewAddress] = useState(false)
    const [ addAddressButton, setAddressButton] = useState(true)
    const [ notification ,setNotification] =useState(false)
    const [ errorNotification ,setErrorNotification] =useState(false)
    const [ carrierNotification ,setCarrierNotification] =useState(false)
    const [ carrierErrorNotification ,setCarrierErrorNotification] =useState(false)
    const [notificationMessage ,setNotificationMessage] = useState('')
    const [carrierNotificationMessage ,setCarrierNotificationMessage] = useState('')
    const [errorMessage ,setErrorMessage] = useState('')
    const employeeId = props?.employeeId ? props.employeeId[0] : props.customerId; 
    const shippingAddressId=props.shippingAddressId;
        const handleLoadingDock = (selectedCheckBox) => {
        setLoadingDockYes(false)
        setLoadingDockNo(false)
        if (selectedCheckBox) {
            setLoadingDockNo(!loadingDockNo)
            setSelectedLoadingDock(false)
        }
        else {
            setLoadingDockYes(!loadingDockYes)
            setSelectedLoadingDock(true)
        }
    }
    const handleForkLift = (selectedCheckBox) => {
        setForkLiftNo(false)
        setForkLiftYes(false)
        if (selectedCheckBox) {
            setForkLiftNo(!forkLiftkNo)
            setSelectedForkLift(false)
        }
        else {
            setForkLiftYes(!forkLiftYes)
            setSelectedForkLift(true)
        }
    }
    const handleDelivery = (selectedCheckBox) => {
        setParcelDelivery(false)
        setFreightDelivery(false)
        if (selectedCheckBox) {
            setFreightDelivery(!freightDelivery)
        }
        else {
            setParcelDelivery(!parcelDelivery)
        }
    }
    const handleAddressButton = () => {
        setnewAddress(true)
        setAddressButton(false)
    }
    //update address
    const onSubmit =() =>{
        if(addressData.city === "" || addressData.firstName  === "" || addressData.lastName === "" ||
        addressData.streetAddress1 === "" 
        //|| addressData.state  === "" 
        || addressData.phoneNumber === "" || addressData.postalCode === "" ){
            setNotification(false)
            setErrorNotification(true)
            setErrorMessage("Please enter the required details")
        }
        else{
            setErrorNotification(false)
            if(shippingAddressId){
        let requestData = {
            "acceptFreight": isChecked,
                "additionalFreightInfo": {
                  "deliveryAppointmentRequired": deliveryAppointment,
                  "forkLift": selectedForkLift,
                  "instructions": deliveryInstructions,
                  "loadingDock": selectedLoadingDock,
                  "receivingHours": {
                    "from": dropDown,
                    "to": toDropDown
                  },
                  "shippingAddressId": shippingAddressId
                },

                "primaryShipping": true,
                "shippingAddress": {
                  "city": addressData.city,
                  "country": addressData.country,
                  "firstName": addressData.firstName,
                  "lastName": addressData.lastName,
                  "phoneNumber": addressData.phoneNumber,
                  "postalCode": addressData.postalCode,
                  "shippingAddressId": addressData.id,
                  "state": addressData.state,
                  "streetAddress1": addressData.streetAddress1,
                  "streetAddress2": addressData.streetAddress2
            }
          }
          
        const url = `${SERVICES.ACCOUNT}/api/v1/user/${employeeId}/shipping-options/${shippingAddressId}/`
        CallApi(url,'post',requestData)
        .then(response =>{
            console.log(response)
            setNotification(true)
            setNotificationMessage('Updated the details')
        })
        .catch ( error => 
            {
                console.log(error)
                setNotification(true)
                setErrorMessage(error)
            })
        }
            else{
        let payload={
                "action": "UPDATE_SHIPPING_ADDRESS",
                "isDefaultAddress": true,
                "shippingAddress": {
                    "city": addressData.city,
                    "company": addressData.company,
                    "country": addressData.country,
                    "firstName": addressData.firstName,
                    "id": props.customerShippingAddressId,
                    "isBusiness": true,
                    "lastName": addressData.lastName,
                    "phone": addressData.phoneNumber,
                    "postalCode": addressData.postalCode,
                    "region": addressData.region,
                    "state": addressData.state,
                    "streetName": addressData.streetAddress1,
                    "streetNumber": addressData.streetNumber
                  }
        }
        console.log(payload,"PL")
        let numLength = 10
        let storedLength = (addressData.phoneNumber).length
          if(code === ENV_EU)
          {
              numLength = 11
          }
        const url = `${SERVICES.ACCOUNT}/api/v1/users/${customerId}/`
          if(storedLength === numLength){
        CallApi(url,'post',payload)
        .then(response =>{
            console.log(response,"neww")
            setNotification(true)
            setNotificationMessage('Updated the details')
        })
        .catch ( error => 
            {
                console.log(error)
                setErrorNotification(true)
                setErrorMessage("Incorrect number")
            })  
    }
    else{
        setErrorNotification(true)
                setErrorMessage("Invalid mobile number")
    }
    }
}
}
    //add new address
    const saveNewAddress = () => {
        if(addressData.city === "" ||  addressData.firstName  === "" || addressData.lastName === "" ||
        addressData.streetAddress1 === "" || addressData.phoneNumber === "" || addressData.country === "" ||
        //addressData.state ||
        addressData.postalCode === "" || addressData.firstName === undefined ){
            setNotification(false)
            setErrorNotification(true)
            setErrorMessage("Please enter the required details")
        }
        else{
            setErrorNotification(false)
            if(!customerId){
        let requestData ={
                "acceptFreight": isChecked,
                "additionalFreightInfo": {
                  "deliveryAppointmentRequired": deliveryAppointment,
                  "forkLift": selectedForkLift,
                  "instructions": deliveryInstructions,
                  "loadingDock": selectedLoadingDock,
                  "receivingHours": {
                    "from": dropDown,
                    "to": toDropDown
                  },
                  "shippingAddressId": "string"
                },
                "primaryShipping": true,
                "shippingAddress": {
                  "city": addressData.city,
                  "country": addressData.country,
                  "firstName": addressData.firstName,
                  "lastName": addressData.lastName,
                  "phoneNumber": addressData.phoneNumber,
                  "postalCode": addressData.postalCode,
                  "shippingAddressId": addressData.id,
                  "state": addressData.state,
                  "streetAddress1": addressData.streetAddress1,
                  "streetAddress2": addressData.streetAddress2
              }
          }
         
          console.log(requestData,employeeId)
           const url=`${SERVICES.ACCOUNT}/api/v1/user/${employeeId}/shipping-options/`
            CallApi(url,'post',requestData)
            .then(response =>{
                console.log(response)
                setNotification(true)
                setNotificationMessage("Added new address")
            })
            .catch ( error => {
                console.log(error)
                setNotification(true)
                setErrorMessage(error)}
            )
            }
            else{
        let payload ={
                "action": "ADD_SHIPPING_ADDRESS",
                "isDefaultAddress": true,
                "shippingAddress": {
                  "city": addressData.city,
                  "company": addressData?.company,
                  "country": addressData.country,
                  "firstName": addressData.firstName,
                  "isBusiness": true,
                  "lastName": addressData.lastName,
                  "phone": addressData.phoneNumber,
                  "postalCode": addressData.postalCode,
                  "region": addressData.region,
                  "state": addressData.state,
                  "streetName": addressData.streetAddress1,
                  "streetNumber": addressData?.streetNumber
              }  
        }
        console.log(payload,"PL")
        if(addressData.phoneNumber){
            var validatedMobile = (addressData.phoneNumber).length
          }
          console.log(validatedMobile,"len")
          let numLength = 10
          if(code === ENV_EU)
          {
              numLength = 11
          }
        if (addressData.phoneNumber && validatedMobile  === numLength) {  
            const url= `${SERVICES.ACCOUNT}/api/v1/users/${customerId}/`
            CallApi(url,'post',payload)
            .then(response =>{
                console.log(response)
                setNotification(true)
                setNotificationMessage("Added new address")
            })
            .catch ( error => {
                console.log(error)
                setNotification(true)
                setErrorMessage(error)
            })}
        
        else {
                setErrorNotification(true)
                setErrorMessage('Invalid mobile number')
            } }
            }
        
    }
        const onSubmitCarrier = () => {
            let requestData ={
                "carriers": [
                    {
                      "billingAddress": {
                        "city": addressData.city,
                        "country": addressData.country,
                        "firstName": addressData.firstName,
                        "lastName": addressData.lastName,
                        "phoneNumber": addressData.phoneNumber,
                        "postalCode": addressData.phoneNumber,
                        "shippingAddressId": addressData.id,
                        "state": addressData.state,
                        "streetAddress1": addressData.streetAddress1,
                        "streetAddress2": addressData.streetAddress2
                      },
                      "carrierAccountNumber": carrierNumber,
                      "carrierName": carrierName,
                      "defaultCarrier": true,
                      "deliveryAppointmentRequired": deliveryAppointment,
                      "forkLift": selectedForkLift,
                      "instructions": deliveryInstructions,
                      "loadingDock": selectedLoadingDock,
                      "receivingHours": {
                        "from": dropDown,
                        "to": toDropDown
                      },
                      "type": "PARCEL"
                    }
                  ]     
                  }
                  const url=`${SERVICES.ACCOUNT}/api/v1/user/${employeeId}/carriers/`
                CallApi(url,'post',requestData)
                .then(response =>{
                    console.log(response)
                    setCarrierNotification(true)
                    setCarrierNotificationMessage('Updated the details')
                })
                .catch ( error => {console.log(error)
                setCarrierErrorNotification(true)
                setCarrierNotificationMessage(error)
            })
        }
    return (
        <div>
            {props.billingAddress ? 
            <CollapsiblePanel header = "Carrier Info">
             <Carrier />
            </CollapsiblePanel>
            :null}
            <CollapsiblePanel
                header="Address"
            >
                <div style={{ width: "100%" }}>
                    <AddAddress address={props.ShippingAddress} 
                    addressData = {(data) => setAddressData(data)}/>
                    {!props.billingAddress ? <>
                        <div className="freight">
                            {/* <div  className="label_text"> */}
                            {
                                countryCode === 'US' ?
                                <CheckboxInput
                                value="foo-radio-value"
                                onChange={() => setIsChecked(!isChecked)}
                                isChecked={isChecked}
                                isDisabled={false }
                            >
                                Freight enabled
                    </CheckboxInput>
                    :
                    null
                    }
                        </div>
                        {isChecked ?
                            <div className="freight_info">
                                <Grid
                                    gridGap="16px"
                                    gridAutoColumns="1fr"
                                    gridTemplateColumns="repeat(2, 1fr)"
                                >
                                    <Grid.Item>
                                        <div className="label_text">
                                            <Label>Has a Loading dock</Label>
                                        </div>
                                        <Spacings.Inline>
                                            <CheckboxInput
                                                onChange={() => handleLoadingDock(false)}
                                                isChecked={loadingDockYes}
                                            >Yes</CheckboxInput>
                                            <CheckboxInput
                                                onChange={() => handleLoadingDock(true)}
                                                isChecked={loadingDockNo}
                                            >No</CheckboxInput>
                                        </Spacings.Inline>
                                    </Grid.Item>
                                    <Grid.Item>
                                        <div className="label_text">
                                            <Label>Has a fork-lift</Label>
                                        </div>
                                        <Spacings.Inline>
                                            <CheckboxInput
                                                onChange={() => handleForkLift(false)}
                                                isChecked={forkLiftYes}
                                            >Yes</CheckboxInput>
                                            <CheckboxInput
                                                onChange={() => handleForkLift(true)}
                                                isChecked={forkLiftkNo}
                                            >No</CheckboxInput>
                                        </Spacings.Inline>
                                    </Grid.Item>

                                    <Grid.Item>
                                        <div className="label_text">
                                            <Label>Receiving hours</Label>
                                        </div>
                                        <div className="drop_down">
                                            <Spacings.Inline>
                                                <SelectField
                                                    title=""
                                                    value={dropDown}
                                                    options={receivingHours.map(data => {
                                                        return { value: data, label: data }
                                                    })}
                                                    onChange={(event) => setDropDown(event.target.value)}
                                                />

                                                <SelectField
                                                    title=""
                                                    value={toDropDown}
                                                    options={receivingHours.map(data => {
                                                        return { value: data, label: data }
                                                    })}
                                                    onChange={(event) => setToDropDown(event.target.value)}
                                                />
                                            </Spacings.Inline>
                                        </div>
                                    </Grid.Item>
                                    <Grid.Item>
                                        <div className="delivery-text">
                                            <CheckboxInput onChange={() => setDeliveryAppointment(!deliveryAppointment)}
                                                isChecked={deliveryAppointment}>
                                                Delivery appointment required?
                                    </CheckboxInput>

                                        </div>
                                    </Grid.Item>
                                </Grid>
                                <div className="text-area">
                                    <MultilineTextField
                                        title="Special delivery instructions"
                                        value={deliveryInstructions}
                                        onChange={(event) => setDeliveryInstructions(event.target.value)}
                                    />
                                </div>
                            </div>
                            : null}
                            {console.log(employeeId,"employeeId")}
                            {!customerId ? 
                            <div className="save_button">
                                    <PrimaryButton label="Save" onClick={shippingAddressId ? onSubmit : saveNewAddress}
                                    />
                                </div>
                                :
                                <div className="save_button">
                                    <PrimaryButton label="Save ID" onClick={props.defaultShippingAddressId ? onSubmit : saveNewAddress}
                                    />
                                </div>
                            }
                                <div className="notification">
                                {notification ? <ContentNotification type="success">{notificationMessage}</ContentNotification> :null}
                                {errorNotification ? <ContentNotification type="error">{errorMessage}</ContentNotification> :null}
                                </div>
                    </> : null}
                </div>
            </CollapsiblePanel>
            {/* {!props.billingAddress ?
                <div className="address_panel">
                    <CollapsiblePanel
                        header="Carrier Information"
                    >
                        <div style={{ width: "100%" }}>
                            <div className="carrier_info">
                                <div className="carrier">
                                    <div className="label_text">
                                        <Label>Company’s Own Carrier</Label>
                                    </div>
                                </div>
                                <Grid
                                    gridGap="16px"
                                    gridAutoColumns="1fr"
                                    gridTemplateColumns="repeat(2, 1fr)"
                                >
                                    <Grid.Item>
                                        <Spacings.Inline>
                                            <TextField title="Carrier name" value={carrierName} onChange={(event) => setCarrierName(event.target.value)} />
                                        </Spacings.Inline>
                                    </Grid.Item>
                                    <Grid.Item>
                                        <Spacings.Inline>
                                            <TextField title="Carrier account number" value={carrierNumber} onChange={(event) => setCarrierNumber(event.target.value)} />
                                        </Spacings.Inline>
                                    </Grid.Item>
                                </Grid>
                                <div className="carrier-checkbox">
                                    <Grid
                                        gridGap="16px"
                                        gridAutoColumns="1fr"
                                        gridTemplateColumns="repeat(2, 1fr)"
                                    >
                                        <Grid.Item>
                                            <Spacings.Inline>
                                                <CheckboxInput
                                                    onChange={() => handleDelivery(false)}
                                                    isChecked={parcelDelivery}
                                                >Parcel</CheckboxInput>
                                                <CheckboxInput
                                                    onChange={() => handleDelivery(true)}
                                                    isChecked={freightDelivery}
                                                >Freight</CheckboxInput>
                                            </Spacings.Inline>
                                        </Grid.Item>

                                    </Grid>
                                </div>
                                <div className="label_text">
                                    <Label>Company’s billing address with carrier</Label>
                                </div>
                                <AddAddress address={props.ShippingAddress}
                                />
                                <div className="save_button">
                                    <SecondaryButton label="Save" onClick={onSubmitCarrier}
                                    />
                                    <div className="notification">
                                     {carrierNotification ? <ContentNotification type="success">{carrierNotificationMessage}</ContentNotification> :null}
                                     {carrierErrorNotification ? <ContentNotification type="error">{carrierNotificationMessage}</ContentNotification> :null}
                                     </div>
                                </div>
                            </div>
                        </div>
                    </CollapsiblePanel>
                </div>
                : null} */}
        </div>

    );
}
export default ShippingAddress