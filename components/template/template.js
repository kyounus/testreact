import React, { useEffect, useState } from 'react';
import DataTable from '@commercetools-uikit/data-table';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import PrimaryButton from '@commercetools-uikit/primary-button';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import { DragDropContext } from 'react-beautiful-dnd';
import { useRowSelection, useSorting } from '@commercetools-uikit/hooks';

const UPDATE_ACTIONS = {
  COLUMNS_UPDATE: 'columnsUpdate',
  IS_TABLE_WRAPPING_TEXT_UPDATE:'isTableWrappingTextUpdate', 
  IS_TABLE_CONDENSED_UPDATE: 'isTableCondensedUpdate'
}

const Template=(props) =>{

    const [isCondensed, setIsCondensed] = React.useState(false);
    const [isWrappingText, setIsWrappingText] = React.useState(false);
    const[update,setUpdate]= useState('')
    const[columnsUpdate,setColumnsUpdate] = useState('')
    const[isTableCondensed,setIsTableCondensed] = useState(false)
    const[isTableWrappingTextUpdate,setIsTableWrappingTextUpdate] = useState(false)

  

  const showDisplaySettingsConfirmationButtons = false;
  const showColumnManagerConfirmationButtons = false;
  const withRowSelection = true;

    const items = [
        { id: 'parasite ', name: 'Raja', customColumn: 'sekhar', phone: "9891234", age: "27", about: "dental" },
        {
            id: 'parasite ', name: 'Raj', customColumn: 'sekhar', phone: "98912345", age: "26", about: "Hospital"
        },
    ];
    const columns = [
        { key: 'name', label: 'Name',align :'center', isSortable: true },
        { key: 'customColumn', label: 'Custom Column',align :'center' },
        { key: 'age', label: 'Age', align :'center'},
        { key: 'about', label: 'About' ,align :'center'},
        { key: 'phone', label: 'Phone',align :'center' }
    ]
  
  const [tableData, setTableData] = useState({
    columns: columns,
    visibleColumnKeys: columns.map(({ key }) => key),
  });

  const {
    items: rows,
    sortedBy,
    sortDirection,
    onSortChange,
  } = useSorting(items);

  const {
    rows: rowsWithSelection,
    toggleRow,
    selectAllRows,
    deselectAllRows,
    getIsRowSelected,
    getNumberOfSelectedRows,
  } = useRowSelection('checkbox', rows);

  const countSelectedRows = getNumberOfSelectedRows();
  const isSelectColumnHeaderIndeterminate =
    countSelectedRows > 0 && countSelectedRows < rowsWithSelection.length;
  const handleSelectColumnHeaderChange =
    countSelectedRows === 0 ? selectAllRows : deselectAllRows;

  const mappedColumns = tableData.columns.reduce(
    (columns, column) => ({
      ...columns,
      [column.key]: column,
    }),
    {}
  );
  const visibleColumns = tableData.visibleColumnKeys.map(
    (columnKey) => mappedColumns[columnKey]
  );

  const tableSettingsChangeHandler = {
    [UPDATE_ACTIONS.COLUMNS_UPDATE]: (visibleColumnKeys) =>
      setTableData({
        ...tableData,
        visibleColumnKeys,
      }),
    [UPDATE_ACTIONS.IS_TABLE_CONDENSED_UPDATE]: setIsCondensed,
    [UPDATE_ACTIONS.IS_TABLE_WRAPPING_TEXT_UPDATE]: setIsWrappingText,
  };

  const displaySettingsButtons = showDisplaySettingsConfirmationButtons
    ? {
      primaryButton: <FooterPrimaryButton />,
      secondaryButton: <FooterSecondaryButton />,
    }
    : {};

  const columnManagerButtons = showColumnManagerConfirmationButtons
    ? {
      primaryButton: <FooterPrimaryButton />,
      secondaryButton: <FooterSecondaryButton />,
    }
    : {};

  const displaySettings = {
    disableDisplaySettings: false,
    isCondensed,
    isWrappingText,
    ...displaySettingsButtons,
  };

  const columnManager = {
    areHiddenColumnsSearchable: true,
    searchHiddenColumns: () => { },
    disableColumnManager: false,
    visibleColumnKeys: tableData.visibleColumnKeys,
    hideableColumns: tableData.columns,
    ...columnManagerButtons,
  };

    return(
        <div>
           <DragDropContext>
           <DataTableManager
              columns={visibleColumns}
              onSettingsChange={(action, nextValue) => {
                tableSettingsChangeHandler[action](nextValue);
              }}
              columnManager={columnManager}
              displaySettings={displaySettings}
            >
              <DataTable
                rows={withRowSelection ? rowsWithSelection : rows}
                sortedBy={sortedBy}
                onSortChange={onSortChange}
                sortDirection={sortDirection}
              />
            </DataTableManager>
           </DragDropContext>
           
            
        </div>
    )
}

export default Template

