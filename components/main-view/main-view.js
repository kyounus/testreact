import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import Spacings from '@commercetools-uikit/spacings';

import './main-view.css';

import CompanyList from '../company-list';
import AddCompany from '../add-company';
import EmployeeList from '../employee-list'
import AddEmployee from '../add-employee'
import Salesrepresentative from '../sales-representative'
import SalesRepList from '../sales-replist';

import Quoteslist from '../quotes/quotes-list'
import OrderList from '../orders/order-list'
import SubscriptionDetailPage from "../subscription/subscriptionDetailPage"

import Quote from '../quotes/quote-details';
import Subscription from '../subscription';
import Orderdetails from "../orders/order-details"
import Survey from "../survey";
import CronJob from "../cron-job";
import Template from '../template/template'
import AddQuote from '../add-quote/add-quote'
import AddSubscription from "../subscription/add-subscription"
import NewSub from "../subscription/newSubscription"
import DeviceList from '../devices/deviceList';
import OrderTag from '../orderTag/orderTag'
import OrderTagDetail from '../orderTag/orderTagDetail'
import DeviceDetails from "../devices/deviceDetails"
import AddOrder from '../add-order/add-order'
import CheckoutSurvey from "../check-outsurvey/checkoutsurvey"
import CheckoutDetail from "../check-outsurvey/checkoutsurveydetail"
import ManageRMA from '../orders/manage-rma';
import OrderProcess from '../orders/failure-order'

const MainView = (props) => {
  return (
    <Spacings.Inset scale="m">
      <Switch>
        {/*B2B Company*/}
        <Route path={`/:projectKey/:entryPointUriPath/customers/:id`} component={AddEmployee} />
        <Route path={`/:projectKey/:entryPointUriPath/customers`} component={EmployeeList} />

        <Route path={`/:projectKey/:entryPointUriPath/companies/:id`} component={AddCompany} />
        <Route path={`/:projectKey/:entryPointUriPath/companies`} component={CompanyList} />

        <Route path={`/:projectKey/:entryPointUriPath/sales-reps`} component={SalesRepList} />
        <Route path={`/:projectKey/:entryPointUriPath/sales-rep-category-mapping`} component={Salesrepresentative} />

        {/*B2B Admin*/}
        <Route path={`/:projectKey/:entryPointUriPath/orders/new`} component={AddOrder} />
        <Route path={`/:projectKey/:entryPointUriPath/orders/:id`} component={Orderdetails} />
        <Route path={`/:projectKey/:entryPointUriPath/orders`} component={OrderList} />
        
        <Route path={`/:projectKey/:entryPointUriPath/quotes/new`} component={AddQuote} />
        <Route path={`/:projectKey/:entryPointUriPath/quotes/:id`} component={Quote} />
        <Route path={`/:projectKey/:entryPointUriPath/quotes`} component={Quoteslist} />

        <Route path={`/:projectKey/:entryPointUriPath/subscriptions/:id`} component={SubscriptionDetailPage} />
        <Route path={`/:projectKey/:entryPointUriPath/subscriptions`} component={Subscription} />
        <Route path={`/:projectKey/:entryPointUriPath/subscription/new`} component={AddSubscription} />
        <Route path={`/:projectKey/:entryPointUriPath/subscription/new-subscription/:id`} component={NewSub} />

        <Route path={`/:projectKey/:entryPointUriPath/subscriptions-survey`} component={Survey} />
        <Route path={`/:projectKey/:entryPointUriPath/checkout-survey`} component={CheckoutSurvey} />
        <Route path={`/:projectKey/:entryPointUriPath/checkout-survey-id/:id`} component={CheckoutDetail} />

        <Route path={`/:projectKey/:entryPointUriPath/cron-job`} component={CronJob} />
        <Route path={`/:projectKey/:entryPointUriPath/template`} component={Template} />

        <Route path={`/:projectKey/:entryPointUriPath/device/:id`} component={DeviceDetails} />
        <Route path={`/:projectKey/:entryPointUriPath/devices`} component={DeviceList} />
        <Route path={`/:projectKey/:entryPointUriPath/orderTag/:id`} component={OrderTagDetail} />
        <Route path={`/:projectKey/:entryPointUriPath/orderTag`} component={OrderTag} />
        <Route path={`/:projectKey/:entryPointUriPath/failure-order`} component={OrderProcess} />

        <Route path={`/:projectKey/:entryPointUriPath/manage-rma`} component={ManageRMA} />
      </Switch>
    </Spacings.Inset>
  );
};
MainView.displayName = 'MainView';
MainView.propTypes = {
  match: PropTypes.shape({
    path: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    params: PropTypes.shape({
      projectKey: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default MainView;
