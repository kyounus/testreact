import React, { useEffect, useState } from 'react'
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import { Pagination } from '@commercetools-uikit/pagination';
import LoadingSpinner from '@commercetools-uikit/loading-spinner';
import { FormModalPage } from '@commercetools-frontend/application-components';
import Grid from '@commercetools-uikit/grid';
import Spacings from '@commercetools-uikit/spacings';
import Card from '@commercetools-uikit/card';
import TextField from '@commercetools-uikit/text-field';
import { ContentNotification } from '@commercetools-uikit/notifications';
import SelectField from '@commercetools-uikit/select-field';
import { SERVICES } from '../../config/api'
import './sales-replist.css'
import { Header } from '../../common/Header';
import { CallApi } from '../../plugin/Axios'

const SalesRepList = () => {
    const [salesRepData, setSalesRepData] = useState([]);
    const [totalResults, setTotalResults] = useState(0);
    const [spinner, setSpinner] = useState(true);
    const [pageLimit, setPageLimit] = useState(20);
    const [pageNumber, setPageNumber] = useState(1);
    const [enableAddSalesRep, setEnableAddSalesRep] = useState(false);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [phone, setPhone] = useState('');
    const [email, setEmailAddress] = useState('');
    const [calendlyLink, setCalendlyLink] = useState('');
    const [companyCategory, setCategory] = useState([]);
    const [salesRepId, setSalesRepId] = useState('');
    const [reloadList, setReloadList] = useState(false);
    const [notification, setNotification] = useState(false);
    const [notificationMessage, setNotificationMessage] = useState('Saved');
    const [errorNotifiaction, setErrorNotification] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [disableButton, setDisableButton] = useState(false);
    const [categories, setCategories] = useState([])
    const [companyCategoryList, setCompanyCategoryList] = useState([])

    useEffect(() => {
        let offset = (pageNumber - 1) * pageLimit;
        if (totalResults && totalResults < offset) {
            setPageNumber(0);
            return;
        }
        setSpinner(true);
        let url = `${SERVICES.ACCOUNT}/api/v1/accounts/salesrep?limit=${pageLimit}&offset=${offset}`
        CallApi(url, 'get')
            .then(res => {
                setSalesRepData(res.data.results);
                setTotalResults(res.data.total);
            })
            .then(res =>
                setSpinner(false)
            )
            .catch(error => console.log("error", error))
        let url1 = `${SERVICES.ACCOUNT}/api/v1/accounts/categories`
        CallApi(url1,'get')
            .then(res => {
                handleCategorynew(res.data.value.companyCategories.companyCategory)
            })
            .catch((error) => {
                console.log('ERROR:', error);
            })
    }, [pageNumber, pageLimit, reloadList]);

    let rows = [];
    salesRepData.map((items) => {
        items.companyCategories.map((category) => {
            return {
                id: category.id,
                name: category.name,
            }
        });
        let categoryDisplay = [];
        items.companyCategories.forEach(item => {
            item.name && categoryDisplay.push(item.name);
        });
        categoryDisplay = categoryDisplay.join(', ');
        rows.push(
            {
                id: items.salesRep.id,
                firstName: items.salesRep.firstName,
                lastName: items.salesRep.lastName,
                phone: items.salesRep.phone,
                calendlyLink: items.salesRep.calendlyLink,
                email: items.salesRep.email,
                companyCategories: items.companyCategories,
                categoryDisplay
            }
        )
    })

    const columns = [
        { key: 'firstName', label: 'First name' },
        { key: 'lastName', label: 'Last name' },
        { key: 'phone', label: 'Phone number' },
        { key: 'calendlyLink', label: 'Calendly link' },
        { key: 'email', label: 'Email address' },
        { key: 'categoryDisplay', label: 'Company category' }
    ]

    const handleAddSalesRep = () => {
        setEnableAddSalesRep(true);
        clearFields();
        setDisableButton(false);
    }
    const handleCancel = () => {
        setEnableAddSalesRep(false);
        clearFields();
    }

    const handleClose = () => {
        setEnableAddSalesRep(false);
    }

    const clearFields = () => {
        setSalesRepId('');
        setFirstName('');
        setCategory('');
        setEmailAddress('');
        setLastName('');
        setPhone('');
        setCalendlyLink('');
        setCompanyCategoryList([])
    }

    const handleRowClick = (item) => {
        setEnableAddSalesRep(true);
        setSalesRepId(item.id);
        setFirstName(item.firstName);
        setLastName(item.lastName);
        setPhone(item.phone);
        setCalendlyLink(item.calendlyLink);
        setEmailAddress(item.email);
        setCategory(item.companyCategories);
        setDisableButton(false);
        const selectedCompany = [];
        item.companyCategories.forEach((eachCategory) => {
            selectedCompany.push(eachCategory.id)
        })
        setCompanyCategoryList(selectedCompany)
    }

    const hideNotification = () => {
        setTimeout(() => {
            setErrorNotification(false)
            setNotification(false)
        }, 3000);
    }

    const handleSubmit = () => {
        const payload = {
            'firstName': firstName,
            'lastName': lastName,
            'phone': phone,
            'calendlyLink': calendlyLink,
            'email': email,
            'categoryId': companyCategoryList
        }
        let url = `${SERVICES.ACCOUNT}/api/v1/accounts/salesrep`
        CallApi(url, 'post', payload)
            .then((resp) => {
                const response = resp.data;
                if (response.salesRep) {
                    setReloadList(!reloadList);
                    setDisableButton(true);
                    setNotification(true);
                    setNotificationMessage('Successfully added')
                } else {
                    setErrorMessage(response.message);
                    setErrorNotification(true);
                }
            })
            .catch(error => {
                setErrorMessage(error.message);
                setErrorNotification(true);
            })
    }

    const handleUpdate = () => {
        const updatePayload = {
            'firstName': firstName,
            'lastName': lastName,
            'phone': phone,
            'calendlyLink': calendlyLink,
            'email': email,
            "categoryId": companyCategoryList,
        }
        let url = `${SERVICES.ACCOUNT}/api/v1/accounts/salesrep/${salesRepId}`
        CallApi(url,'post', updatePayload)
            .then((resp) => {
                const response = resp.data;
                if (response.salesRep) {
                    setReloadList(!reloadList);
                    setNotification(true);
                    setNotificationMessage('Successfully updated')
                    setDisableButton(true);
                } else {
                    setErrorMessage(response.message);
                    setErrorNotification(true);
                }
            })
            .catch(error => {
                setErrorMessage(error.message);
                setErrorNotification(true);
            })
    }

    const handleCategorynew = (data) => {
        let tempCategories = []
        {
            data.map((category) => {
                const tempObj = {
                    value: category.id, label: category.name
                }
                tempCategories = [...tempCategories, tempObj]
            })
        }
        setCategories(tempCategories)
    }

    return (
        <div>
            <Spacings.Stack scale="m">
                <Header
                    title={'Sales Representative Details'}
                    results={totalResults}
                    linkLabel={'Add new Sales Representative'}
                    popUp={true}
                    popUpHandel={(status) => status && handleAddSalesRep()}
                />
                <FormModalPage
                    title={salesRepId ? "Update Sales Representative" : "Add new Sales Representative"}
                    isOpen={enableAddSalesRep}
                    onClose={handleClose}
                    topBarCurrentPathLabel={salesRepId ? "Update Sales Representative" : "Add new Sales Representative"}
                    topBarPreviousPathLabel="Back"
                    onSecondaryButtonClick={handleCancel}
                    onPrimaryButtonClick={salesRepId ? handleUpdate : handleSubmit}
                    labelPrimaryButton={salesRepId ? "Update" : "Save"}
                    isPrimaryButtonDisabled={disableButton}
                >
                    <div className="notification">
                        {notification ?
                            <div>
                                <ContentNotification type="success"> {notificationMessage} </ContentNotification>
                                {hideNotification()}
                            </div>
                            : null
                        }
                        {errorNotifiaction ?
                            <div>
                                <ContentNotification type="error"> {errorMessage} </ContentNotification>
                                {hideNotification()}
                            </div>
                            : null
                        }
                    </div>
                    <Grid
                        gridGap="16px"
                        gridAutoColumns="1fr"
                        gridTemplateColumns="repeat(2, 1fr)"
                    >
                        <Grid.Item>
                            <Card>
                                <Spacings.Inline>
                                    <TextField title="First name" name="firstName" value={firstName}
                                        onChange={event => setFirstName(event.target.value)} />
                                </Spacings.Inline>
                            </Card>
                        </Grid.Item>
                        <Grid.Item>
                            <Card>
                                <Spacings.Inline>
                                    <TextField title="Last name" name="lastName" value={lastName}
                                        onChange={event => setLastName(event.target.value)} />
                                </Spacings.Inline>
                            </Card>
                        </Grid.Item>
                        <Grid.Item>
                            <Card>
                                <Spacings.Inline>
                                    <TextField title="Phone number" name="phone" value={phone}
                                        onChange={event => setPhone(event.target.value)} />
                                </Spacings.Inline>
                            </Card>
                        </Grid.Item>
                        <Grid.Item>
                            <Card>
                                <Spacings.Inline>
                                    <TextField title="Calendly link" name="calendlyLink" value={calendlyLink}
                                        onChange={event => setCalendlyLink(event.target.value)} />
                                </Spacings.Inline>
                            </Card>
                        </Grid.Item>
                        <Grid.Item>
                            <Card>
                                <Spacings.Inline>
                                    <TextField title="Email address" name="email" value={email}
                                        onChange={event => setEmailAddress(event.target.value)} />
                                </Spacings.Inline>
                            </Card>
                        </Grid.Item>
                        {salesRepId ? <Grid.Item>
                            <Card className="card">
                                <SelectField
                                    title="Company category"
                                    value={companyCategoryList}
                                    options={categories}
                                    onChange={(event) => setCompanyCategoryList(event.target.value)}
                                    isMulti={true}
                                    backspaceRemovesValue={true}
                                    isDisabled={true}
                                />
                            </Card>
                        </Grid.Item>: null}
                    </Grid>
                </FormModalPage>
                
                {/* <Table
                    functionalUrl={`accounts/salesrep`}
                    tableColumns={columns}
                    tableRow={rows}
                    getTabelData={(data) => setSalesRepData(data)}
                    getTableRecord={(totlaCount) => setTotalResults(totlaCount)}
                /> */}
                {!enableAddSalesRep ?
                    <DataTableManager columns={columns} >
                        <DataTable rows={rows}
                            onRowClick={
                                (item) => handleRowClick(item)
                            } />
                    </DataTableManager> : null
                }
                {spinner ? <div className="spinner">
                    <LoadingSpinner size="s">Loading</LoadingSpinner></div> : null}
                {!spinner ? <Pagination
                    totalItems={totalResults}
                    page={pageNumber}
                    perPage={pageLimit}
                    perPageRange="m"
                    onPageChange={(page) => {
                        setPageNumber(page)
                    }}
                    onPerPageChange={(lmt) => {
                        setPageLimit(lmt);
                    }}
                /> : null}
            </Spacings.Stack>
        </div>
    )
}

export default SalesRepList