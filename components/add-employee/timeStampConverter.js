import React from 'react';
import { useIntl } from 'react-intl';
import TextField from '@commercetools-uikit/text-field';


const timeStampConverter = (props) => {

    const { formatTime, formatDate } = useIntl();
    let dateDisplay = props.date
    let customerSince = formatDate(dateDisplay) + " " + formatTime(dateDisplay)
    return (
        <div>
            <TextField
             title="Customer Since" 
             value={customerSince} 
             isReadOnly   
            onChange={(event) => alert(event)} />
               
        </div>
    )
}

export default timeStampConverter
