import React from 'react'
import Grid from '@commercetools-uikit/grid';
import SelectInput from '@commercetools-uikit/select-input';
import "./add-employee.css"
import TextField from '@commercetools-uikit/text-field';
import Card from '@commercetools-uikit/card'
import Text from '@commercetools-uikit/text';
import PrimaryButton from '@commercetools-uikit/primary-button';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import { ContentNotification } from '@commercetools-uikit/notifications';
import { code, ENV_US, SERVICES } from '../../config/api';
import { ListIcon } from '@commercetools-uikit/icons';
import LinkButton from '@commercetools-uikit/link-button';
import CheckboxInput from '@commercetools-uikit/checkbox-input';
import AddressList from '../add-company/address-list';
import { addHyperLink, getPhoneFormat } from '../../utils';
import CustomerGroup from '../shared/customer-group'
import Spacings from '@commercetools-uikit/spacings';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import { CallApi } from '../../plugin/Axios';
import TimeConverter from "./timeStampConverter"
import { ProjectKey } from '../../config/api';
import axios from 'axios';


 class addEmployee extends React.Component{

    

    constructor(props){
        super(props)
        this.state = {
            companyCategoryList:[],
            companyCategoryId: "",
            company: "",
            email: "",
            firstName: "",
            lastName: "",
            mobileNumber: "",
            dateOfBirth:"",
            customerChannel:"",
            password: "",
            confirmPassword: "",
            companyList: [],
            customerStatus: "",
            notification: false,
            errorMessage: "",
            errorNotifiaction: false,
            data : this.props.location,
            customerId :'',
            generalTab:true,
            sendEmailChecked:false,
            addressesTab:false,
            paymentsTab:false,
            addresses: {},
            rowData: [],
            addCustomerFlow: true,
            selectedCustomergroup:{},
            customerGroupName:"",
            customerGroup:false,
            submit:false,
            successMessage:"",
            accountId:"",
            updateAddress:false,
            newAddress:false,
            categoryValue: {},
            customerCheckD2C:false,
            enableNewCustomer:false,
            customerCheckB2B:false,
            customerCheckDef:false,
            customerCheck:'',
            errorNotificationEmail:false,
            notificationMessageEmail:'',
            customerSource:'',
            customerStore:'',
            isLockedChecked:false,
            registeredChannel:'',
            comments:'',
            D2CSearch:false,
            B2BSearch:false,
            source:'',
            emailUpdate:false,
            customerSave:false,
            commentEdit:false,
            isDisabled:false,
            spinner: false,
            terms: false,
            marketingOffers: false,
            customerTimestamp : '',
            defaultShippingAddressId:'',
            marketingFlags:0,
            emailFlag:0
        }
        this.loadCompany();
        this.messageTiemout = null;
    }
    handleChange = (e) => {   
        const {name,value} = e.target
        this.setState({[name]:value},() =>{
        })
        // this.setState({emailUpdate:true})
        this.setState({customerSave:true})
        //this.setState({commentEdit:true})
    }
    handleEmailChange = (e) => {
        const {name,value} = e.target
        this.setState({[name]:value},() =>{
        })
        this.setState({emailFlag:this.state.emailFlag + 1})
    }
    handleComments = (e) => {
        const {name,value} = e.target
        this.setState({[name]:value},() =>{
        })
        this.setState({commentEdit:true})
    }
    handleCompanyChange = (e) => {        
        const {name,value} = e.target
        console.log(name, value)
        this.setState({
            [name]:value ,
            accountId: value
        })
    }
    clearFields(){
        this.setState({
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            confirmPassword: '',
            company: '',
            companyCategoryId: '',
            mobileNumber: '',
            customerStatus: '',
            registeredChannelCustomer: "",
            registeredChannel: "",
            customerStore:'',
            terms: false,
            marketingOffers: false,
            customerSource: '',
            comments: "",
            source: "",
            sendEmailChecked: false,
            customerChannel: ""
        })
    }
    sendEmployeeDetails = (e) =>{
        this.setErrorMessage('');
        this.setNotification(false);
        this.setErrorNotification(false);

        if(this.state.mobileNumber){
            var validatedMobile = this.state.mobileNumber.replace(/\D/g,'').length == 10
        }
        var emailCheck = /^([A-Za-z0-9_\-\.+])+\@([A-Za-z0-9_\-\.+])+\.([A-Za-z]{2,4})$/;
        let channelB2BData = (this.state.firstName && this.state.lastName && this.state.company && validatedMobile && emailCheck.test(this.state.email) == true)
        let channelD2CData = (this.state.firstName && this.state.lastName && emailCheck.test(this.state.email) == true)

        if (this.state.mobileNumber && !validatedMobile) {
            this.setErrorNotification(true)
            this.setErrorMessage('Invalid Mobile number')
        }
        if (this.state.email && emailCheck.test(this.state.email) == false) {
            this.setErrorNotification(true)
            this.setErrorMessage('Invalid Email Address')
        }
        if (this.state.customerCheckB2B 
            && this.state.firstName 
            && this.state.lastName 
            && this.state.company 
            && validatedMobile 
            && emailCheck.test(this.state.email) == true) 
            {
            const B2Bdata = {
                firstName:this.state.firstName,
                lastName:this.state.lastName,
                phoneNumber:this.state.phoneNumber,
                email:this.state.email,
                customerGroupId : this.state.selectedCustomergroup.value,
                registeredChannel:this.state.registeredChannel,
                customerStore:this.state.customerStore,
                comments:this.state.comments
            }
            let url = `${SERVICES.ACCOUNT}/api/v1/users/${this.state.customerId}/group?customerGroupId=${this.state.selectedCustomergroup.value}`
            CallApi(url,'post',B2Bdata)
                 .then((resp) => 
                {       
                    if(this.state.customerSave && this.state.emailUpdate){
                        this.setNotification(true)
                        this.setState(this.setSuccessMessage('Successfully updated customer'));      
                    }
                    else{
                        this.setErrorNotification(true)
                        this.setErrorMessage('No changes made')
                    }
                })
                .catch(error => {
                        this.setErrorMessage(error.message);
                        this.setErrorNotification(true);
                    })

        } 
        else if (this.state.customerCheckD2C 
            && this.state.firstName 
            && this.state.lastName  
            &&  emailCheck.test(this.state.email) == true)
        {
            const D2Cdata = {
             "action": "UPDATE_PROFILE",
             "customerProfileDetails": {
                "firstName": this.state.firstName,
                "lastName": this.state.lastName,
                "registeredChannel": this.state.registeredChannel
            },
        }
        console.log(this.state.registeredChannel,"reg")
            let url = `${SERVICES.ACCOUNT}/api/v1/users/${this.state.customerId}/`
            CallApi(url,'post',D2Cdata)
                 .then((resp) => 
                {   
                   if(this.state.marketingFlags > 0) {
                    this.editMarkettingFlags() 
                   }
                   console.log(this.state.marketingFlags,"flag")
                    
                    this.editComments() 
                    console.log(this.state.emailUpdate,"update")
                    if(this.state.emailFlag > 0){
                        this.emailUpdate() } 
                    if(this.state.customerSave && this.state.marketingFlags === 0 || this.state.emailFlag || this.state.commentEdit){
                        this.setNotification(true)
                        this.setState(this.setSuccessMessage('Successfully updated customer'));      
                    }
                    else{
                        this.setErrorNotification(true)
                        this.setErrorMessage('No changes made')
                    }
                   
                    //this.editComments() 
        //    else if(this.state.commentEdit && resp.data.statusCode !== 400){
        //         this.setNotification(true)
        //         this.setState(this.setSuccessMessage('Successfully updated comments'));      
        //     }
        //     else if(this.state.customerSave && resp.data.statusCode !== 400){
        //         this.setNotification(true)
        //         this.setState(this.setSuccessMessage('Successfully updated customer'));      
        //     }
        //             else{
        //                 this.setErrorNotification(true)
        //                 this.setErrorMessage('No changes made')
        //             }
                })
                .catch(error => {
                        this.setErrorMessage(error.message);
                        this.setErrorNotification(true);
                    })
        }
        // else if (this.state.firstName && this.state.lastName &&  emailCheck.test(this.state.email) == true) {
        //     this.editComments() 
        //     if(this.state.commentEdit){
        //         this.setNotification(true)
        //         this.setState(this.setSuccessMessage('Successfully updated comments'));      
        //     }
        //     else if(this.state.customerSave){
        //         this.setNotification(true)
        //         this.setState(this.setSuccessMessage('Successfully updated customer'));      
        //     }
        //     else{
        //         this.setErrorNotification(true)
        //         this.setErrorMessage('No changes made')
        //     }
        // }
        else {
            this.setErrorNotification(true)
            this.setErrorMessage('Please fill in required fields')
        }
    }

    handleSubmit = () => {
        //signup
        const payload = {
            "accountId": this.state.accountId,
            "companyName": 'Restaurant',
            "customerGroupId":  this.state.selectedCustomergroup.value,
            "email": this.state.email,
            "firstName": this.state.firstName,
            "lastName": this.state.lastName,
            "mobileNumber": this.state.mobileNumber,
            "channel":this.state.customerChannel,
            "store":this.state.customerStore,
            "registeredChannel":this.state.registeredChannel,
            "comments":this.state.comments,
            "customerSource":this.state.source,
            "termsOfService": this.state.terms,
            "marketingOffers": this.state.marketingOffers    
        }          
        if(payload)
        {
          var emailCheck = /^([A-Za-z0-9_\-\.+])+\@([A-Za-z0-9_\-\.+])+\.([A-Za-z]{2,4})$/;
    
          if (this.state.email && emailCheck.test(this.state.email) == true && this.state.firstName
          &&  this.state.lastName && this.state.registeredChannel && this.state.customerStore && this.state.source) 
          {
            if (this.state.sendEmailChecked === false){
                this.setErrorNotification(true)
                this.setErrorMessage('Please select Welcome mail option')
                return
            }
              if(code !== ENV_US && this.state.terms!== true )
              {
                this.setErrorNotification(true)
                this.setErrorMessage('Please select terms and conditions to proceed')
                return
              }
            let url = `${SERVICES.ACCOUNT}/api/v1/user/signup/`
               CallApi(url,'post',payload)
               .then(res => {
                   console.log(this.state.sendEmailChecked,'check')
                if(res.data.message){
                    console.log(res)
                    this.setErrorNotification(true)
                    this.setErrorMessage(res.data.message)
                    return
                }
                else{ 
                   this.resetPassword()
                   this.setNotification(true);
                   this.setSuccessMessage('Added new Customer') 
                   
               }})
               .then(res => {
                   setTimeout(()=>{
                    this.clearFields()
                   }, 5000)
                
               })
               .catch(err =>{
                console.log(err)
                this.setErrorNotification(true)
                this.setErrorMessage('Please fill in required fields')
            })
         }
        
          else if (this.state.email && emailCheck.test(this.state.email) === false ){
            this.setErrorNotification(true)
            this.setErrorMessage('Invalid Email Address')
          }
        else {
              this.setErrorNotification(true)
              this.setErrorMessage('Please fill all the fields')
        }
     }    
    }
     componentDidMount() {
         const { match } = this.props;  
         if(this.state.customerId === 'new') {
             this.setState({
                customerId: '',
                addCustomerFlow: true,
                customerGroup:true,
                submit:true,
                terms: false,
                marketingOffers: false
             })
        }else{
            this.setState({
                customerId: match.params.id,
                customerCheckD2C:true,
                addCustomerFlow: false
             })
            this.addressBar(match.params.id)
        }
        
        // if(this.props.location.customerChannel === 'D2C'){
        //     this.setState( {customerCheckD2C :true })
        //   }
        //   if(this.props.location.customerChannel === 'B2B'){
        //     this.setState( {customerCheckB2B :true })

        // }
        if(this.state.customerChannel === '' ){
            this.setState( {customerCheckDef :true})
        }
        if(this.props.location.accountLocked === 'true'){
            this.setState({isLockedChecked:true,isDisabled:true})
        }

        let url=`${SERVICES.ACCOUNT}api/v1/accounts/categories/`
            CallApi(url,'get')
             .then(res => {
                 this.setState({
                     companyCategoryList: res?.data.value.companyCategories.companyCategory,
                 })
             })
             .catch((error) => {
                 console.log('ERROR:', error);
             });
     }

        addressBar = (customerId) => {
         let rows = []
         let url=`${SERVICES.ACCOUNT}api/v1/users?customerId=${customerId}`
         CallApi(url,'get')
             .then(res => {
                res?.data.channel === 'D2C' ? this.setState( {customerCheckD2C :true }) : this.setState( {customerCheckD2C : false }),
                 console.log("terms",res?.data.termsOfService)
                 console.log("marketting",res?.data.customerSince);
                 let setAddress = { 
                     billingAddresses: res?.data.billingAddressList,
                     shippingAddresses: res?.data.shippingAddressList               
                  } 
                 this.setState({
                     addresses: setAddress,
                     firstName: res?.data.firstName,
                     lastName: res?.data.lastName,
                     email: res?.data.email,
                     company: res?.data.companyName,
                     mobileNumber: res?.data.phoneNumber,
                     customerGroupName:res?.data.customerGroupName,
                     customerGroup:true,
                     customerChannel: res?.data.channel,
                     registeredChannel : res?.data.registeredChannel,
                     comments:res?.data.comments,
                     customerStore:res?.data.store,
                     customerSource:res?.data.customerSource === "D2C_WEB" ? `Molekule Site` : `Created by Admin `,
                     isLockedChecked:res?.data.accountLocked,
                     terms: res?.data.termsOfService,
                     marketingOffers: res?.data.marketingOffers,
                     customerTimestamp : res?.data.customerSince,
                     defaultShippingAddressId:res?.data.defaultShippingAddressId
                     
                 })
                 // need to check response from API for Payment
                 res?.data && res?.data.payments && res?.data.payments.map((items) => {
                     if(items && items.paymentType === "CREDITCARD"){
                     rows.push(
                         {
                             id: items.paymentId,
                             paymentType: items.paymentType,
                             cardType:items.cardObject.brand,
                             lastDigits:items.cardObject.last4

                         }
                     )}
                     this.setState({
                         rowData: rows
                     })
                 })
             });
        }
    emailUpdate = () => {
        let reqEmailUpdate ={
            "action": "UPDATE_EMAIL",
            "email": this.state.email,
        }
        console.log(this.state.emailUpdate,"Emailll")
        let url = `${SERVICES.ACCOUNT}/api/v1/users/${this.state.customerId}/`
        if(reqEmailUpdate){
            CallApi(url,'post',reqEmailUpdate)
            .then(response =>{
                console.log(response, "Email Response")
                if(this.state.emailFlag && response.data.statusCode !== 400){
                    this.setNotification(true)
                    this.setState(this.setSuccessMessage('Successfully updated Email'));      
                }
                else{
                    this.setErrorNotification(true)
                    this.setErrorMessage('Please Change the Email Address')
                }
                if(response.data.statusCode === 400)
                {
                    this.setErrorNotification(true)
                    this.setErrorMessage('Email id already exists. Please enter new one')
                }
               
            })
            .catch(error => {
                console.log(error)
                this.setState({errorNotificationEmail:true})  
            });
            
            
         }
 
    }
    resetPassword = () => {
        console.log(this.state.email,"mail")
        let pay_load = ""
        if(this.state.customerId === 'new'){
            pay_load = "createPassword"
        }
        else {
            pay_load = "forgotPassword"
        }
        let reset_password = {
            "email": this.state.email,
            "passwordType": pay_load
        }
        if(reset_password)
        {
            let url = `${SERVICES.ACCOUNT}api/v1/user/forgot-password/`
            CallApi(url,'post',reset_password)
            .then(res => {
                this.setState({errorNotificationEmail:false})
                this.setNotification(true)
                this.setState(this.setSuccessMessage('Successfully sent email')); 
            })
            .catch(error => {
                this.setState({errorNotificationEmail:true}) 
            });
       }
    }
    editComments = () => {
        console.log(this.state.comments,"comments")
       let edit_comment =
      {
        "action": "UPDATE_COMMENTS",
        "comments": this.state.comments
     }
     let url = `${SERVICES.ACCOUNT}/api/v1/users/${this.state.customerId}/`
        CallApi(url,'post',edit_comment)
        .then(res => {
            console.log(res,"res")
        }) 

    }

    editMarkettingFlags = () => {
        console.log(this.state.marketingOffers, "marFlag");
        let marPayload = {
            "action": "UPDATE_MARKETING_OFFERS",
            "marketingOffers": this.state.marketingOffers
        }
        let url = `${SERVICES.ACCOUNT}/api/v1/users/${this.state.customerId}/`
       CallApi(url,'post', marPayload)
       .then(res => {
        this.setNotification(true)
        this.setState(this.setSuccessMessage('Successfully updated Customer'));     
       })
       .catch(err=>{
        this.setErrorMessage("Error");
        this.setErrorNotification(true);
       })
    }
   
    //todo: movie it as functional component;
    setNotification = (notify) => {
        this.setState({
            notification: notify
        });
        if(notify) {
            clearTimeout(this.messageTiemout)
        }
    }
    setErrorNotification = (flag) => {
        this.setState({
            errorNotification: flag
        });
        if(flag) {
            clearTimeout(this.messageTiemout)
        }
    }
    setErrorMessage = (msg) => {
        this.setState({
            errorMessage: msg
        })
    }
    setSuccessMessage =(msg)=>{
        this.setState({
            successMessage:msg
        })
    }
    hideNotification =() =>{
        this.messageTiemout = setTimeout(() => {
            this.setErrorNotification(false)
            this.setNotification(false)
        }, 5000);
    }
      
    async loadCompany(optionList = []) {
        const selOptions = [];
        let url = `${SERVICES.ACCOUNT}/api/v1/accounts?limit=20&offset=200`
        await CallApi(url,'get')
              .then(response => {
                const results = (response?.data.results)
                results?.forEach(item => {
                    selOptions.push({
                        label: item?.value.companyName || item?.id,
                        value: item?.id
                    })
                });
                this.setState({
                    companyList: selOptions,
                })
                return selOptions;
          },[]);
    }

    handleSendEmailCheckbox = (check_box_value) => {
        this.setState({
            sendEmailChecked: !!check_box_value
        })
    }
    handleTermsCheckbox = (check_box_value) => {
        this.setState({
            marketingFlags:this.state.marketingFlags + 1,
            terms: !!check_box_value
        })
    }

    handleMarkettingCheckbox = (check_box_value) => {
        this.setState({
            marketingOffers: !!check_box_value
        })
    }
    handleIsLockedCheckbox = (check_box_value) => {
        this.setState({
           isLockedChecked: !!check_box_value
        })
    }
    handleGeneralTab = () => {
        this.setState({generalTab : true})
        this.setState({addressesTab : false})
        this.setState({paymentsTab : false})
    }
    handleAddressesTab = () => {
        this.setState({generalTab : false})  
        this.setState({addressesTab : true}) 
        this.setState({paymentsTab : false})
    }
    handlePaymentsTab = () => {
        this.setState({generalTab : false})
        this.setState({addressesTab : false})
        this.setState({paymentsTab : true})
    }
    handleSelectedGrp =(data) =>{
        this.setState({selectedCustomergroup : data})
    }
    handleUpdateClick=()=>{
        this.setState({updateAddress : !this.state.updateAddress })
        if(this.state.updateAddress === true){
            this.addressBar(this.state.customerId)}
    }
    handleNewAddress=()=>{
        this.setState({newAddress : !this.state.newAddress})
        if(this.state.newAddress === true){
        this.addressBar(this.state.customerId)}
    }
    handleAddNewCustomer=() => {
       this.setState({enableNewCustomer:true})
       this.setState({generalTab:false})  
    }
    onSubmit = () => {
     console.log(this.state.firstName, this.state.lastName,this.state.email,this.state.customerChannel)
    }
    newCustomerTableOff =() =>{
      this.setState({generalTab:true})
      this.setState({addressesTab:false})
      this.setState({paymentsTab:false})
    }
    handleSpinner=()=>{
        this.setState({
            spinner: true
        })
    }


    render(){
        this.state.customerId= this.props.match.params.id
          const columns = [
            { key: 'paymentType', label: 'Payment type' },
            { key: 'id', label: 'Payment Id' },
            {key : 'cardType' , label : 'Card Type'},
            {key : 'lastDigits' , label : 'Last 4 Digits'}
          ];
          const optionsUS = [
            {
              label: 'United States',
              options: [
                { value: 'MLK US - Website', label: 'MLK US - Website' },
                { value: 'MLK US - Store', label: 'MLK US - Store' },
                { value: 'MLK US - Store View”', label: 'MLK US - Store View”' },
              ],
            },
            {
              label: 'Canada',
              options: [
                { value: 'MLK CA - Website', label: 'MLK CA - Website'},
                { value: 'MLK CA - Store', label: 'MLK CA - Store' },
                { value: 'MLK - Store View', label: 'MLK - Store View' },
              ],
            },
            
          ];

          

          const optionsUK = [
           {
              label: 'Europe',
              options: [
                { value: 'MLK_UK', label: 'MLK_UK'},
              
                ],
              },
          ];

          
          const controlView = (controlValue) => {
              if(controlValue === 'D2C'){
                 this.setState({D2CSearch:true})
                 this.setState({B2BSearch:false})
              }
              if(controlValue === 'B2B'){
                  this.setState({B2BSearch:true})
                  this.setState({D2CSearch:false})
              }
        
        }
        return ( 
            <div className="main">
                <div className="main-head">
                    {console.log(this.props.location,"props")}
                
                  <LinkButton
                        to={addHyperLink('customers', this.props)}
                        iconLeft={<ListIcon />}
                        label="Customer List"
                        isDisabled={false}
                  />
                    <Text.Headline as="h2">{this.state.customerId !== "new" ? this.state.firstName +" "+ this.state.lastName :'Add a customer'}</Text.Headline>
                    {this.state.customerId ? null :<p>Enter and manage customer details</p>}
                    {this.state.customerId ? null : <p>Mandatory fields are marked with an asterisk(<span className="asterisk-color">*</span>)</p>}
                
                    {!this.state.addCustomerFlow ? <ul className="sub_tab-list" >
                        <li><a 
                        className={this.state.generalTab ? 'background_green' : 'background-black'} onClick={()=>this.handleGeneralTab() }
                        >General</a></li> 
                        <li><a
                         className={this.state.addressesTab ?  'background_green' : 'background-black' } onClick={()=>this.handleAddressesTab()}
                        >Addresses </a> </li> 
                         <li><a
                         className={this.state.paymentsTab ? 'background_green' : 'background-black' } onClick={()=>this.handlePaymentsTab()}
                         >Payments</a> </li> 
                    </ul>
                    : null
                    }
                </div>
                {this.state.generalTab ? 
                <div className="sub-main">
                    <Grid
                        gridGap="16px"
                        gridAutoColumns="1fr"
                        gridTemplateColumns="repeat(2, 1fr)"
                    >
                        {this.state.customerCheckD2C && !this.state.customerCheckB2B  ? 
                        <>
                        {this.state.customerId !== "new" ?  
                        <Grid.Item>
                            <Card  className="card">
                                <TextField title="Channel" name="customerChannel" isDisabled value={this.state.customerChannel}/>
                            </Card>
                        </Grid.Item>: null }
                        </>
                        : null }
                       { this.state.customerId === "new" ? 
                        <Grid.Item>
                            <Card className="card">
                              <label><h4>Channel<span className="asterisk-color">*</span></h4></label>
                                <SelectInput
                                    name="customerChannel"
                                    value={this.state.customerChannel}
                                    placeholder={this.state.customerChannel}
                                    onChange={
                                        (event) => {
                                          controlView(event.target.value);
                                         this.setState({customerChannel:event.target.value});
                                        } }
                                    options={[
                                        { value: 'D2C', label: 'D2C' },
                                        { value: 'B2B', label: 'B2B' },
                                    ]}   
                                />
                            </Card>
                        </Grid.Item>
                        :null}
                        {this.state.D2CSearch || this.state.customerCheckB2B || this.state.customerCheckD2C || this.state.B2BSearch 
                          || this.state.customerId !== "new" ?                     
                        <Grid.Item>
                            <Card  className="card">
                                <TextField title="First Name" name="firstName" isRequired value={this.state.firstName} onChange={this.handleChange}/> 
                            </Card>
                        </Grid.Item>
                         : null}
                        {this.state.D2CSearch || this.state.customerCheckB2B || this.state.customerCheckD2C || this.state.B2BSearch || this.state.customerId !== "new" ? 
                          <Grid.Item>
                             <Card  className="card">
                                <TextField title="Last Name" name="lastName" isRequired value={this.state.lastName} onChange={this.handleChange}/>
                            </Card>
                          </Grid.Item> 
                          : null }
                       
                        {this.state.D2CSearch || this.state.customerCheckB2B || this.state.customerCheckD2C || this.state.B2BSearch || this.state.customerId !== "new" ? 
                        <Grid.Item>
                            <Card  className="card">
                                <TextField title="Email"  name="email" isRequired value={this.state.email} onChange={this.handleEmailChange}/>
                            </Card>
                        </Grid.Item> 
                         : null } 
                         {this.state.customerCheckD2C && !this.state.customerCheckB2B || this.state.D2CSearch ? 
                         <Grid.Item>
                            <Card className="card"> 
                              <label><h4>Registered Channel<span className="asterisk-color">*</span></h4></label>
                                <SelectInput
                                   name="registeredChannel"
                                   value={this.state.registeredChannel}
                                   onChange={this.handleChange}
                                   isRequired
                                   options={[
                                    { value: 'mo', label: 'MO' },
                                    { value: 'retailer', label: 'Retailer' },
                                   ]}
                                />
                            </Card> 
                        </Grid.Item> : null }
                        {/* checking */}
                        {this.state.customerCheckD2C && !this.state.customerCheckB2B && this.state.customerId !== "new" || this.state.D2CSearch ? 
                        <>
                        {this.state.customerId !== "new" ?  
                        <Grid.Item>
                            <Card  className="card">
                                <TextField  title="Originated From" name="customerSource" isRequired isDisabled={this.state.customerId !== "new" ? true : false} value={this.state.customerSource} ></TextField>
                            </Card>
                        </Grid.Item>: null }
                        </>
                        : null }
                       { this.state.customerId === "new" && this.state.customerChannel === "D2C" ? 
                        <Grid.Item>
                            <Card className="card">
                            <label><h4>Originated From<span className="asterisk-color">*</span></h4></label>
                              <SelectInput
                                 name="source"
                                 value={this.state.source}
                                 options={[
                                    { value: 'D2C_WEB', label: 'Molekule Site' },
                                    { value: 'MC', label: 'Created by Admin' },
                                ]}
                                 onChange={this.handleChange}
                                 isDisabled={this.state.customerId !== "new" ? true : false}
                                 isRequired
                              />
                            </Card>
                        </Grid.Item>
                        :null}
                        {/* check */}
                        {/* {this.state.customerCheckD2C && !this.state.customerCheckB2B && this.state.customerId !== "new" || this.state.D2CSearch ? 
                       <div> 
                        <Grid.Item>
                            <Card className="card"> 
                           { this.state.customerId !== "new"  ?
                              <TextField title="Originated From" name="customerSource" isRequired isDisabled={this.state.customerId !== "new" ? true : false} value={this.state.customerSource} 
                              onChange={this.handleChange}/> : 
                              <div>
                               <label><h4>Originated From<span className="asterisk-color">*</span></h4></label>
                              <SelectInput
                                 name="source"
                                 value={this.state.source}
                                 options={[
                                    { value: 'D2C_WEB', label: 'Molekule Site' },
                                    { value: 'MC', label: 'Created by Admin' },
                                ]}
                                 onChange={this.handleChange}
                                 isDisabled={this.state.customerId !== "new" ? true : false}
                                 isRequired
                              /> </div> }
                            </Card> 
                        </Grid.Item>  
                        </div>   */}
                        {/* :  */}
                       
                        {this.state.customerCheckB2B || this.state.B2BSearch || this.state.customerId !== "new" && !this.state.customerCheckD2C 
                        && !this.state.D2CSearch ? 
                         <Grid.Item>
                           <Card className="card">
                             <TextField title="Mobile number" name="mobileNumber" isRequired value={getPhoneFormat(this.state.mobileNumber)} onChange={this.handleChange}/>
                          </Card>
                        </Grid.Item>
                        :null}
                      
            
                        {this.state.customerCheckD2C && !this.state.customerCheckB2B && !this.state.B2BSearch || this.state.D2CSearch ? 
                        <Grid.Item>
                            <Card className="card">
                              <label><h4>Store<span className="asterisk-color">*</span></h4></label>
                              <SelectInput
                                 name="customerStore"
                                 value={this.state.customerStore}
                                 onChange={this.handleChange}
                                 isRequired
                                 options={code === ENV_US? optionsUS : optionsUK}
                                 isDisabled={this.state.customerId !== "new" ? true : false}
                              />
                            </Card>
                        </Grid.Item> 
                        : null }
                        {this.state.customerCheckD2C && !this.state.customerCheckB2B || this.state.D2CSearch ? 
                        <Grid.Item>
                            <Card  className="card">
                                <TextField title="Comments" name="comments" value={this.state.comments} onChange={this.handleComments}/>
                            </Card>
                        </Grid.Item>
                        : null }
                        
                        {this.state.customerCheckB2B || this.state.B2BSearch || this.state.customerId !== "new" && !this.state.customerCheckD2C && !this.state.D2CSearch  ? 
                        this.state.customerId !== "new" ?  <Grid.Item>
                            <Card  className="card">
                                <TextField title="Company" name="company" isDisabled value={this.state.company}/>
                            </Card>
                        </Grid.Item>:
                        <Grid.Item>
                            <Card className="card">
                                <label><h4>Company<span className="asterisk-color">*</span></h4></label>
                                <SelectInput
                                    name="company"
                                    value={this.state.company}
                                    onChange={this.handleCompanyChange}  
                                    options={this.state.companyList}
                                />
                            </Card>
                        </Grid.Item>:null}
                        {this.state.customerCheckB2B || this.state.B2BSearch || this.state.customerId !== "new" && !this.state.customerCheckD2C && !this.state.D2CSearch  ? 
                        <Grid.Item>
                            <Card className="card">
                                <label><h4>Customer Status <span className="asterisk-color">*</span></h4></label>
                                <SelectInput
                                    name="customerStatus"
                                    value={this.state.customerStatus}
                                    onChange={this.handleChange}
                                    options={[
                                        { value: 'lead', label: 'Lead' },
                                        { value: 'registered', label: 'Registered' },
                                        { value: 'active', label: 'Active'}
                                    ]}
                                />
                            </Card>
                        </Grid.Item>
                        : null }
                        <div style={{width:'205%'}}>
                        <Grid.Item>
                           { (this.state.customerCheckB2B || this.state.B2BSearch || this.state.customerId !== "new" &&!this.state.customerCheckD2C && !this.state.D2CSearch) ?
                           <div>{this.state.customerGroup ? 
                            <CustomerGroup customerGroup={this.state.customerGroupName} selectedGroup={this.handleSelectedGrp}/> 
                            :null}</div> 
                            : null }
                        </Grid.Item>
                        </div>
                    </Grid>
                    <div style={{paddingTop: "10px"}}>
                    {this.state.D2CSearch || this.state.customerCheckB2B || this.state.customerCheckD2C || this.state.B2BSearch || this.state.customerCheckDef 
                          && this.state.customerId !== "new" ?
                    <CheckboxInput
                        value="Send Email"
                        onChange={(event) => this.handleSendEmailCheckbox(event.target.checked)}
                        isChecked={this.state.sendEmailChecked}
                        >   Send Welcome Email
                    </CheckboxInput>  : null}
                    </div> 
                    <br/>
                    <div>
                    {this.state.D2CSearch || this.state.customerCheckB2B || this.state.customerCheckD2C || this.state.B2BSearch || this.state.customerCheckDef 
                        && this.state.customerId !== "new"  ?
                          <CheckboxInput
                            value="Is Locked"
                            isDisabled={!this.state.isLockedChecked}
                            onChange={(event) => this.handleIsLockedCheckbox(event.target.checked)}
                            isChecked={this.state.isLockedChecked}
                          >   
                          IsLocked?

                      </CheckboxInput>  : null}
                    </div><br/>
                    
                        {/* ---------------------------------------------- */}
                    {/* //ROHIT CHNAGE */}
                    {
                      code !== ENV_US && 
                      this.state.customerChannel==='D2C' && 
                      this.state.customerChannel!=='B2B' ?
                      <>
                            <div style={{width: "30%"}}>
                         <TimeConverter date={this.state.customerTimestamp} />
                            </div>
                      <hr />
                        <div style={{margin: "10px"}}>
                    <CheckboxInput
                            value="Service"
                            isReadOnly={ this.state.customerId === "new" ? false : true}
                            onChange={(event) => this.handleTermsCheckbox(event.target.checked)}
                            isChecked={this.state.terms}
                          >
                              
                      <b><span className="asterisk-color">*</span>
                        Accept our terms of service and privacy policy. 
                        Molekule is based in the U.S. , so the personal data you provide 
                        will be transferred to the U.S. Learn more in our Privacy Policy. </b>  
                      </CheckboxInput>
                       
                        </div>
                        <div style={{margin: "10px"}}>
                      <CheckboxInput
                            value="marketting"
                            isDisabled={false}
                            onChange={(event) => this.handleMarkettingCheckbox(event.target.checked)}
                            isChecked={this.state.marketingOffers}
                          >  
                         
                         <b> Receive product news & exclusive offers. 
                          You can unsubscribe at any time - please see our privacy policy for details.
                          </b>
                      </CheckboxInput>
                    </div>
                    </> : null
                    }

                    {/* ---------------------------------------------------- */}

                   <div className="submitButtons">
                      <Spacings.Inline>
                      <PrimaryButton  label="Save" onClick={this.state.submit? this.handleSubmit : this.sendEmployeeDetails} />
                            <SecondaryButton label="Cancel" onClick={() =>this.props.history.push(addHyperLink('customers',this.props))} />
                            {this.state.customerId !== 'new' ? 
                            <div>
                            <SecondaryButton label="Reset Password" onClick={this.resetPassword} /> &nbsp;&nbsp;
                            <SecondaryButton label="Update Email" onClick={this.emailUpdate}/> </div>: null }
                        </Spacings.Inline>
                  </div>
                </div>
                : null 
                }
                <div className="notification">
                { this.state.notification ?
                    <div>
                        <ContentNotification type="success">{this.state.successMessage} </ContentNotification>
                        {this.hideNotification()}
                    </div>
                :null
                }
                { this.state.errorNotification ?
                    <div>
                        <ContentNotification type="error"> {this.state.errorMessage} </ContentNotification>
                        {this.hideNotification()}
                    </div>  
                :null
                }
                </div>
                { this.state.addressesTab ? <div> 
                 <AddressList 
                    billingAddress={false}
                    shippingAddress={true}
                    carrierInfo={true}
                    addresses={this.state.addresses}
                    customerId={this.state.customerId}
                    updateAddress={this.handleUpdateClick}
                    newAddress={this.handleNewAddress}
                    handleSpinner ={this.handleSpinner}
                    defaultShippingAddressId={this.state.defaultShippingAddressId}
                 /> </div> 
                : null 
                }
                { this.state.paymentsTab ? <div>
                    {this.state.rowData.length > 0 ? 
                    <DataTableManager columns={columns} >
                        <DataTable rows={this.state.rowData}/>
                    </DataTableManager>: <div>No Payment data</div>}
                    <AddressList 
                        billingAddress={true}
                        shippingAddress={false}
                        carrierInfo={false}
                        addresses={this.state.addresses}
                        customerId={this.state.customerId}
                        updateAddress={this.handleUpdateClick}
                        newAddress={this.handleNewAddress}
                        handleSpinner ={this.handleSpinner}
                        defaultShippingAddressId={this.state.defaultShippingAddressId}
                    /></div>
                :null 
                }
           </div>
        )
    }
}
export default addEmployee

