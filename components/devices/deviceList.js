import React, { useState , useEffect } from "react";
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import TextField from '@commercetools-uikit/text-field';
import Grid from '@commercetools-uikit/grid';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import PrimaryButton from '@commercetools-uikit/primary-button';
import { FilterIcon } from '@commercetools-uikit/icons';
import "./device.css";
import {Header} from "../../common/Header";
import { addHyperLink } from '../../utils';
import { SERVICES } from '../../config/api';
import { Pagination } from '@commercetools-uikit/pagination';
import LoadingSpinner from '@commercetools-uikit/loading-spinner';
import { useRowSelection, useSorting } from '@commercetools-uikit/hooks';
import { CallApi } from "../../plugin/Axios";

const DeviceList = (props) => {
    const [isClicked, setIsClicked] = useState(false);
    const [ deviceData,setDeviceData] = useState([])
    const [pageLimit, setPageLimit] = useState(20);
    const [pageNumber, setPageNumber] = useState(1);
    const [totalResults, setTotalResults] = useState(0);
    const [spinner, setSpinner] = useState(true);
    const [sku ,setSku] = useState('');
    const [skuSearch, setSkuSearch] = useState('');
    const [serialNumber ,setSerialNumber] = useState('');
    const [serialNumberSearch, setSerialNumberSearch] = useState('');
    const [purchaseDate ,setPurchaseDate] = useState('');
    const [purchaseDateSearch, setPurchaseDateSearch] = useState('');
    const [customerNumber ,setcustomerNumber] = useState('');
    const [customerNumberSearch, setcustomerNumberSearch] = useState('');
    const [isCondensed, setIsCondensed] = useState(false);
    const [isWrappingText, setIsWrappingText] = useState(false);

    const UPDATE_ACTIONS = {
        COLUMNS_UPDATE: 'columnsUpdate',
        IS_TABLE_WRAPPING_TEXT_UPDATE:'isTableWrappingTextUpdate', 
        IS_TABLE_CONDENSED_UPDATE: 'isTableCondensedUpdate'
      }
    useEffect(() => {
        let offset= (pageNumber - 1) * pageLimit;
        if(totalResults && totalResults < offset){
            setPageNumber(0);
            return 
        }
        setSpinner(true);
        let query = '';
        if (sku) {
            query += `&sku=${encodeURIComponent(sku)}`; 
        }
        if (serialNumber) {
            query += `&serialNumber=${encodeURIComponent(serialNumber)}`; 
        }
        if (purchaseDate) {
            query += `&purchaseDate=${encodeURIComponent(purchaseDate)}`; 
        }
        if (customerNumber) {
            query += `&customerNumber=${encodeURIComponent(customerNumber)}`; 
        }
        const url =`${SERVICES.ACCOUNT}api/v1/users/devices?&limit=${pageLimit}&offset=${offset}&${query}/`
        CallApi(url, 'get').then((res) => {
           console.log(res , "response")
           setDeviceData(res.data.results)
            setTotalResults(res.data.total || 20);
        })
        .then(res =>
            setSpinner(false)
            )
            .catch(error => {
                console.log("error", error)
                setSpinner(false)
              })
     },[serialNumber,purchaseDate,customerNumber,sku])

    const devices = []
    let deviceList = []
    deviceData.map( data =>{
        const deviceDetails ={
            id: data.value.deviceId,
            serialNumber : data.value.serialNumber,
            sku : data.value.sku,
            channel : data.value.channel,
            gift : data.value.gift.toString(),
            retailOrder : data.value.retailSeller,
            customerNumber : data.value.customerNumber,
            registraionDate : data.value.registraionDate,
            purchaseDate : data.value.purchaseDate,
        }
      deviceList = [...deviceList,deviceDetails]
    })
    const columnsB2B = [
        // { key: 'id', label: 'ID' },
        { key: 'serialNumber', label: 'Serial Number' },
        { key: 'sku', label: 'SKU' },
        { key: 'channel', label: 'Channel' },
        { key: 'gift', label: 'Whether the Device is Gift' },
        { key: 'retailOrder', label: 'Retail Order' },
        { key: 'customerNumber', label: 'Customer ID' },
        { key: 'registraionDate', label: 'Device Registered Date' },
        { key: 'purchaseDate', label: 'Purchase Date' },
    ];
   
    const handleRowClick = (item) =>{
        props.history.push("device/" + item.id);
        props.history.push(item)
    }
    const searchFilters = () => {
        setSku(skuSearch)
        setSerialNumber(serialNumberSearch)
        setPurchaseDate(purchaseDateSearch)
        setcustomerNumber(customerNumberSearch)
    }

    const clearFilters = () =>{
        setSku("")
        setSerialNumber("")
        setSerialNumberSearch("")
        setPurchaseDate("")
        setcustomerNumber("")
        setSkuSearch("")
        setPurchaseDateSearch("")
        setcustomerNumberSearch("")
    }
    const [tableData, setTableData] = useState({
        columns: columnsB2B,
        visibleColumnKeys: columnsB2B.map(({ key }) => key),
      });
    
      const showDisplaySettingsConfirmationButtons = false;
      const showColumnManagerConfirmationButtons = false;
      const withRowSelection = true;
    
      const {
        items: rows,
      } = useSorting(devices);
    
      const {
        rows: rowsWithSelection,
        toggleRow,
        selectAllRows,
        deselectAllRows,
        getIsRowSelected,
        getNumberOfSelectedRows,
      } = useRowSelection('checkbox', rows);
    
      const countSelectedRows = getNumberOfSelectedRows();
      const isSelectColumnHeaderIndeterminate =
        countSelectedRows > 0 && countSelectedRows < rowsWithSelection.length;
      const handleSelectColumnHeaderChange =
        countSelectedRows === 0 ? selectAllRows : deselectAllRows;
    
      const mappedColumns = tableData.columns.reduce(
        (columns, column) => ({
          ...columns,
          [column.key]: column,
        }),
        {}
      );
      const visibleColumns = tableData.visibleColumnKeys.map(
        (columnKey) => mappedColumns[columnKey]
      );
    
      const tableSettingsChangeHandler = {
        [UPDATE_ACTIONS.COLUMNS_UPDATE]: (visibleColumnKeys) =>
          setTableData({
            ...tableData,
            visibleColumnKeys,
          }),
        [UPDATE_ACTIONS.IS_TABLE_CONDENSED_UPDATE]: setIsCondensed,
        [UPDATE_ACTIONS.IS_TABLE_WRAPPING_TEXT_UPDATE]: setIsWrappingText,
      };
    
      const displaySettingsButtons = showDisplaySettingsConfirmationButtons
        ? {
          primaryButton: <FooterPrimaryButton />,
          secondaryButton: <FooterSecondaryButton />,
        }
        : {};
    
      const columnManagerButtons = showColumnManagerConfirmationButtons
        ? {
          primaryButton: <FooterPrimaryButton />,
          secondaryButton: <FooterSecondaryButton />,
        }
        : {};
    
      const displaySettings = {
        disableDisplaySettings: false,
        isCondensed,
        isWrappingText,
        ...displaySettingsButtons,
      };
    
      const columnManager = {
        areHiddenColumnsSearchable: true,
        searchHiddenColumns: () => { },
        disableColumnManager: false,
        visibleColumnKeys: tableData.visibleColumnKeys,
        hideableColumns: tableData.columns,
        ...columnManagerButtons,
      };
    
    return (
        <div>
            <div className="device-headline">
                <Header
                    title={'Devices'}
                    results={totalResults}
                    linkUrl={addHyperLink('device/new', props)}
                    linkLabel={'Add Device'}
                />
                <div className="filter-icon">
                    <SecondaryButton
                        iconLeft={<FilterIcon />}
                        label="Filter"
                        onClick={() => setIsClicked(!isClicked)}
                    />
                    </div>
                {isClicked ?
                    <div className="device-grid">
                        <Grid
                            gridGap="16px"
                            gridAutoColumns="1fr"
                            gridTemplateColumns="repeat(4, 1fr)"
                        >
                            <TextField title="Serial Number" value={serialNumberSearch} onChange={event => setSerialNumberSearch(event.target.value)} />
                            <TextField title="SKU" value={skuSearch} onChange={event => setSkuSearch(event.target.value)}/>
                            <TextField title="Purchase Date" value={purchaseDateSearch} onChange={event => setPurchaseDateSearch(event.target.value)} />
                            <TextField title="Customer ID" value= {customerNumberSearch} onChange={event => setcustomerNumberSearch(event.target.value)} />
                        </Grid>
                        <div className="filter-button">
                        <div className="device-filter">
                        <SecondaryButton label="Clear Filters" onClick={clearFilters}/>
                        </div>
                        <PrimaryButton label="Apply Filters" onClick={searchFilters}/>
                        </div>
                    </div>
                    : null}
            </div>
            <DataTableManager
            columns={visibleColumns}
            onSettingsChange={(action, nextValue) => {
              tableSettingsChangeHandler[action](nextValue);
            }}
            columnManager={columnManager}
            displaySettings={displaySettings}>
                <DataTable
                    rows={deviceList}
                    onRowClick={
                        (item) => handleRowClick(item)
                      }
                />
            </DataTableManager>
            {spinner ? <div className="spinner">
           <LoadingSpinner size="s">Loading</LoadingSpinner></div> : null}
           {!spinner ?
             <Pagination
             totalItems={totalResults}
             page={pageNumber}
             perPage={pageLimit}
             perPageRange="m"
                onPageChange={(page) => {
                    console.log(page)
                    setPageNumber(page)
                }}
                onPerPageChange={(lmt) => {
                    console.log(lmt)
                    setPageNumber(lmt)
                }}
             /> 
            : null}
        </div>
    );
}

export default DeviceList;