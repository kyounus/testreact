import React, { useState ,useEffect } from "react";
import TextInput from '@commercetools-uikit/text-input';
import SelectInput from '@commercetools-uikit/select-input';
import DateInput from '@commercetools-uikit/date-input';
import "./device.css";
import Text from '@commercetools-uikit/text';
import FlatButton from '@commercetools-uikit/flat-button';
import { BackIcon } from '@commercetools-uikit/icons';
import SecondaryIconButton from '@commercetools-uikit/secondary-icon-button'
import PrimaryButton from '@commercetools-uikit/primary-button';
import { useHistory } from "react-router-dom";
import { CallApi } from "../../plugin/Axios";
import { SERVICES } from '../../config/api';
import { ContentNotification } from '@commercetools-uikit/notifications';
import LinkButton from '@commercetools-uikit/link-button';
import { addHyperLink } from '../../utils';
import { ProjectKey } from "../../config/api";

const DeviceDetails = (props) => {
    const [serialNumber, setSerialNumber] = useState('')
    const [channel, setChannel] = useState('')
    const [gift, setGift] = useState('')
    const [customerNumber, setcustomerNumber] = useState('')
    const [registeredDate, setRegisteredDate] = useState('')
    const [retailOrder, setRetailOrder] = useState('')
    const [sku, setSku] = useState('')
    const [purchaseDate,setPurchaseDate]=useState('')
    const [notification , setNotification] =  useState(false); 
    const [notificationMessage, setNotificationMessage] = useState('');
    const [errorNotification , setErrorNotification] =  useState(false);
    const [errorMessage , setErrorMessage] =  useState('');
    
    let data = props.history.location
    const getLastItem = thePath => thePath.substring(thePath.lastIndexOf('/') + 1)
    let deviceId= data.id ? data.id : getLastItem(props.location.pathname)

    let today = new Date();
    let date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

    let countryCode = 'US';
    if (ProjectKey && (ProjectKey === 'mo-dev-eu-1' || ProjectKey === 'mo-preprod1-eu' || ProjectKey === 'mo-prod-eu')) {
        countryCode = 'EU';
    }
    
    useEffect(() => {
        const url =`${SERVICES.ACCOUNT}api/v1/users/devices/${deviceId}/`
        CallApi(url, 'get').then((res) => {
           let data = res.data.results[0].value
           setSerialNumber(data.serialNumber)
           setChannel(data.channel)
           setGift(data.gift)
           setcustomerNumber(data.customerNumber)
           setRegisteredDate(data.registraionDate)
           setRetailOrder(data.retailSeller)
           setSku(data.sku)
           setPurchaseDate(data.purchaseDate)
        })
        .catch(error => {
        console.log("error", error)
        })
     },[])
    
    const clearFields =() =>{
        setPurchaseDate('')
        setSku('')
        setSerialNumber('')
    }

    const handleSubmit = () => {
        if(deviceId === "new"){
            const payload ={
                "purchaseDate": purchaseDate,
                "serialNumber": serialNumber,
                "sku": sku
            }
            const url = `${SERVICES.ACCOUNT}/api/v1/users/devices/`
            CallApi(url,'post',payload).then( res =>{
                console.log(res,"response")
                clearFields()
                setErrorNotification(false)
                setNotification(true)
                setNotificationMessage('Added new device')
            })
            
            .catch( error =>{
                if (error.response) {
                    setErrorNotification(true)
                    setErrorMessage(error.response.data.errorMessage)
                  }
                else{
                console.log(error,"error")
                setErrorNotification(true)
                setErrorMessage(error.message)
                  }
            })
        }
        else{
        const payload ={
            "purchaseDate": purchaseDate,
            "serialNumber": serialNumber,
            "sku": sku
        }
        const url = `${SERVICES.ACCOUNT}/api/v1/users/devices/${deviceId}/`
        CallApi(url,'post',payload).then( res =>{
            console.log(res,"response")
            setNotification(true)
            setNotificationMessage('Updated the device')
        })
        .catch( err =>{
            console.log(err,"error")
            setErrorNotification(true)
            setErrorMessage(err.message)
        }) 
    } 
    }
    const handleDelete = () => {
        const url =`${SERVICES.ACCOUNT}/api/v1/users/devices/${deviceId}/`
        CallApi(url,'delete').then( res =>{
            console.log(res,"response")
            setNotification(true)
            setNotificationMessage('Deleted the device')
        })
        .catch( err =>{
            console.log(err,"error")
            setErrorNotification(true)
            setErrorMessage(err.message)
        })
    }
    return (
        <div>
            <Text.Headline as="h2">{deviceId === "new" ?'New Device' : 'Edit Device'}</Text.Headline>
            <div className="device-buttons">
                <div className="invoice_buttons">
                    <LinkButton
                to={addHyperLink('devices', props)}
                iconLeft={<BackIcon size='medium' />}
                label=""
                isDisabled={false}
            />
                </div>
                <div className="invoice_buttons">
                <LinkButton
                to={addHyperLink('devices', props)}
                label="Back"
                isDisabled={false}
            />
                </div>
                <div className="invoice_buttons">
                    <FlatButton
                        tone="primary"
                        label="Delete Device"
                        onClick={handleDelete}
                        isDisabled={false}
                    />
                </div>
                <div className="save-device">
                    <PrimaryButton
                        label="Save Device"
                        onClick={handleSubmit}
                    />
                </div>
            </div>
            <div className="device-list">
                <div className="edit-device">
                    <div className="detail-attributes">Serial Number</div>
                    <div className="device-field">
                        <TextInput value={serialNumber} isReadOnly={false} onChange={(event) => setSerialNumber(event.target.value)} />
                    </div>
                </div>
                <div className="edit-device">
                    <div className="detail-attributes">SKU</div>
                    <div className="device-field">
                        <SelectInput
                            name="form-field-name"
                            value={sku}
                            placeholder={sku}
                            onChange={(event) => setSku(event.target.value)}
                            options={countryCode == 'US' ? [
                                { value: 'MN2P-US', label: 'MN2P-US' },
                                { value: 'SQ2P-US', label: 'SQ2P-US' },
                                { value: 'MN1B-US', label: 'MN1B-US' },
                                { value: 'MH1-BBB-1', label: 'MH1-BBB-1' },
                            ] : [
                                { value: 'MN2P-UK', label: 'MN2P-UK' },
                                { value: 'SQ2P-UK', label: 'SQ2P-UK' },
                            ] }
                        />
                    </div>
                </div>
                <div className="edit-device">
                    <div className="detail-attributes">Purchase Date</div>
                    <div className="device-field">
                        <DateInput
                            placeholder="Select a date..."
                            value={purchaseDate}
                            maxValue={date}
                            onChange={(event) =>setPurchaseDate(event.target.value)}
                        />
                    </div>
                </div>
                {deviceId !== "new" ? 
                <div>
                <div className="edit-device">
                    <div className="detail-attributes">Channel</div>
                    <div className="device-field">
                        <TextInput value={channel} isReadOnly={true} onChange={(event) => setChannel(event.target.value)} />
                    </div>
                </div>
                <div className="edit-device">
                    <div className="detail-attributes">Whether the device is a gift</div>
                    <div className="device-field">
                        <TextInput value={gift} isReadOnly={true} onChange={() => { }} />
                    </div>
                </div>
                <div className="edit-device">
                    <div className="detail-attributes">Retail Order</div>
                    <div className="device-field">
                        <SelectInput
                            name="form-field-name"
                            value={retailOrder}
                            isReadOnly={true}
                            placeholder={retailOrder}
                            onChange={(event) => setRetailOrder(event.target.value)}
                            options={[
                                { value: 'Amazon', label: 'Amazon' },
                                { value: 'Best Buy US', label: 'Best Buy US' },
                                { value: 'b8ta', label: 'b8ta' },
                                { value: 'MoMa(NYC)', label: 'MoMa(NYC)' },
                                { value: 'Sprout SF', label: 'Sprout SF' },
                                { value: 'Alo Yoga', label: 'Alo Yoga' },
                            ]}
                        />
                    </div>
                </div>
                <div className="edit-device">
                    <div className="detail-attributes">Customer ID</div>
                    <div className="device-field">
                        <TextInput value={customerNumber} isReadOnly={true} onChange={() => { }} />
                    </div>
                </div>
                <div className="edit-device">
                    <div className="detail-attributes">Device registered date</div>
                    <div className="device-field">
                        <TextInput value={registeredDate}  isReadOnly={true} onChange={() => { }} />
                    </div>
                </div>
                </div>
                :null}
            <div className="device-notification">
                { notification ?
                <div>
                <ContentNotification type="success"> {notificationMessage} </ContentNotification>
                </div>
                :null
                }
                { errorNotification ?
                <div>
                    <ContentNotification type="error"> {errorMessage} </ContentNotification>
                </div>  
                :null
                }
            </div>
            </div>
        </div>
    );
}

export default DeviceDetails;