import React, { useState, useEffect } from 'react';
import axios from "axios";
import { SERVICES, centsToDollars } from '../../config/api';
import CollapsiblePanel from '@commercetools-uikit/collapsible-panel';
import Spacings from '@commercetools-uikit/spacings';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import Grid from '@commercetools-uikit/grid';
import Text from '@commercetools-uikit/text';
import { TruckIcon } from '@commercetools-uikit/icons';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import PrimaryButton from '@commercetools-uikit/primary-button'
import LoadingSpinner from '@commercetools-uikit/loading-spinner';
import SelectField from '@commercetools-uikit/select-field';
import { ContentNotification } from '@commercetools-uikit/notifications';
import "../orders/order-details.css";
import Card from '@commercetools-uikit/card';
import { CallApi } from '../../plugin/Axios';

const ReviewOrder = (props) => {
    // console.log(props, 'ReviewOrder11111')

    const { address, product, quantity, customerCountry, customerChannel, customerId, cartID, productId, shippingMethodDetails, loader } = props;
    const [customerPanel, setCustomerPanel] = useState(false);
    const [shippingAddPanel, setShippingAddPanel] = useState(false);
    const [productPanel, setProductPanel] = useState(false);
    const [spinner, setSpinner] = useState(true);
    const [productDetails, setProductDetails] = useState(props.product);
    const [shippingMethods, setShippingMethods] = useState();
    const [productPrice, setProductPrice] = useState();
    const [paymentData, setPaymentData] = useState([]);
    const [cardDetails, setCardDetails] = useState([]);
    const [paymentTokenData, setPaymentToken] = useState([]);
    const [placeOrderButton, setPlaceOrder] = useState(false);
    const [paymentMethod, setPaymentMethod] = useState(false)
    const [notification , setNotification] =  useState(false); 
    const [notificationMessage, setNotificationMessage] = useState('');
    const [errorNotifiaction , setErrorNotification] =  useState(false);
    const [errorMessage , setErrorMessage] =  useState('');
    const [orderNumber, setOrderNumber] = useState();
    const [paymentPanel, setPaymentPanel] = useState(false)

    useEffect(() => {
        setSpinner(true)
        setProductDetails(props.product)
        if(cartID && customerCountry){
            axios.get(`${SERVICES.CART}/api/v1/carts/${cartID}?country=${customerCountry}`, '', {headers: {country: customerCountry}})
            .then(res => {
                setShippingMethods(res.data.shippingInfo);
                setSpinner(false)
            })
            .then(res => {
                setSpinner(false)
                // setNotification(true)
                // setNotificationMessage(message);
            })
            .catch(e => {
                setSpinner(false)
                console.log('error', e);
                setErrorNotification(true);
                setErrorMessage(e.message)
            })
        }
        if(customerId){
            CallApi(`${SERVICES.ACCOUNT}/api/v1/users?customerId=${customerId}`, 'get')
            .then(res => {
                setPaymentData(res.data.payments);
                handleCategorynew(res.data.payments)
                setSpinner(false)
            })
            .then(res =>
                setSpinner(false)
            )
            .catch(error => {
                setSpinner(false)
                console.log('error', error);
                setErrorNotification(true);
                setErrorMessage(error.message)
            })
        }
    }, [customerId, customerChannel, cartID, product, customerCountry]);

    useEffect(() => {
        if (loader == false) {
            setSpinner(false)
        }else{
            setSpinner(true)
        }
    }, [loader])

    let rows = [];
    productDetails.map((items) => {
        rows.push(
            {
                productname: items.name,
                price: items.price,
                quantity: items.quantity ? items.quantity : props.quantity,
                rowtotal: items.price
            }
        )
    })

    const handleCategorynew = (data) => {
        let tempCategories = []
        let creditCard
        {
            if (props.customerChannel == "D2C") {
                data?.map((items) => {
                    if (items.paymentType == 'CREDITCARD') {
                        creditCard = items
                    }
                    const tempObj = {
                        value: creditCard.paymentToken, label: creditCard.cardObject.last4
                    }
                    tempCategories = [...tempCategories, tempObj]
                })
                setCardDetails(tempCategories)
            }
            if (props.customerChannel == "B2B") {
                let tempObj
                data?.map((items) => {
                    if (items.paymentType == 'CREDITCARD') {
                        creditCard = items
                        tempObj = {
                            value: items.paymentToken, label: items.cardObject.last4
                        }
                    } else {
                        tempObj = {
                            value: items.paymentType, label: items.paymentType
                        }
                    }
                    tempCategories = [...tempCategories, tempObj]
                })
                setCardDetails(tempCategories)
            }
        }
    }

    let paymentsList = [];
    let creditCard
    paymentData && paymentData.map((items) => {
        if (items.paymentType == 'CREDITCARD') {
            creditCard = items
        }
        let paymentData = {
            cardLast4: creditCard.cardObject.last4,
            expMonth: creditCard.cardObject.expMonth,
            expYear: creditCard.cardObject.expYear,
            brand: creditCard.cardObject.brand,
            paymentToken: creditCard.paymentToken

        }
        paymentsList = [...paymentsList, paymentData]
    })

    const columns = [
        { key: 'productname', label: 'Product' },
        { key: 'price', label: 'Price' },
        { key: 'quantity', label: 'Quantity' },
        { key: 'rowtotal', label: 'Total' }
    ];

    const placeOrder = () => {
        let placeOrderData
        if (paymentTokenData == 'ACH') {
            placeOrderData = {
                cartId: cartID,
                paymentType: paymentTokenData
            }
        } else {
            placeOrderData = {
                cartId: cartID,
                paymentToken: paymentTokenData,
                // paymentType: "CREDITCARD"
            }
        }
        setSpinner(true)
        axios.post(`${SERVICES.CHECKOUT}/api/v1/users/checkout/order?country=${customerCountry}`, placeOrderData, {headers: {country: customerCountry}})
            .then(res => {
                console.log(res, 'placeOrder')
                setOrderNumber(res.data.orderNumber)
                setSpinner(false)
                setNotification(true)
                setNotificationMessage("Order placed sucessfully")
            })
            .then(res =>
                setSpinner(false)
            )
            .catch(error => {
                console.log('error', error);
                setSpinner(false)
                setErrorNotification(true);
                setErrorMessage(error.message)
            })
    }

    const sendConfirmationMail = () => {
        setSpinner(true)
        CallApi(`${SERVICES.CHECKOUT}/api/v1/users/checkout/sendOrderConfirmationEmail/${orderNumber}/`, 'get')
            .then(res =>{
                setSpinner(false)
                setNotification(true)
                setNotificationMessage(res.data.status)
            })
            .catch(error => {
                setSpinner(false)
                setErrorNotification(true);
                setErrorMessage(error.message)
            })
    }

    const hideNotification = () => {
        setTimeout(() => {
            setErrorNotification(false)
            setNotification(false)
        }, 5000);
    }

    return (
        <div className="main-info">
            {spinner ? <div className="spinnerOrder">
                        <LoadingSpinner size="s"></LoadingSpinner></div> : null}
            {productDetails?.length > 0 && address ? <div>
                <Spacings.Stack>
                    <div className="addresses">
                        <CollapsiblePanel
                            isClosed={shippingAddPanel}
                            onToggle={() => setShippingAddPanel(!shippingAddPanel)}
                            header="Shipping Address"
                        >
                            <Grid
                                gridGap="16px"
                                gridAutoColumns="1fr"
                                gridTemplateColumns="repeat(1, 1fr)"
                            >
                                <Grid.Item>
                                    <div className="order-details-address">
                                        <div className="address-icon"><TruckIcon /></div>
                                        <Text.Headline as="h4">Shipping address</Text.Headline>
                                    </div>
                                    <ul className="address-list">
                                        <li><b>Phone Number:</b>&nbsp;&nbsp;{address?.phoneNumber ? address?.phoneNumber : address?.phone}</li>
                                        <li><b>First name:</b>&nbsp;&nbsp;{address?.firstName}</li>
                                        <li><b>Last name:</b>&nbsp;&nbsp;{address?.lastName}</li>
                                        <li><b>Street address 1:</b>&nbsp;&nbsp;{address?.streetName}</li>
                                        <li><b>Street address 2:</b>&nbsp;&nbsp;{address?.streetNumber}</li>
                                        <li><b>City:</b>&nbsp;&nbsp;{address?.city}</li>
                                        <li><b>State:</b>&nbsp;&nbsp;{address?.state}</li>
                                        <li><b>Postal code:</b>&nbsp;&nbsp;{address?.postalCode}</li>
                                        <li><b>Country:</b>&nbsp;&nbsp;{address?.country}</li>
                                    </ul>
                                </Grid.Item>
                            </Grid>
                        </CollapsiblePanel>
                    </div>
                    <div className="order-panel">
                        <CollapsiblePanel
                            isClosed={productPanel}
                            onToggle={() => setProductPanel(!productPanel)}
                            header="Products"
                        >
                            <div className="order-table">
                                <DataTableManager columns={columns}>
                                    <DataTable
                                        rows={rows}
                                    />
                                </DataTableManager>
                            </div>
                        </CollapsiblePanel>
                    </div>
                   {shippingMethodDetails !== null && shippingMethodDetails !== undefined ? <div className="order-panel">
                        <CollapsiblePanel
                            isClosed={customerPanel}
                            onToggle={() => setCustomerPanel(!customerPanel)}
                            header="Shipping Methods"
                        >
                            <div className="order-table">
                                <Card>
                               <b>{shippingMethods ? shippingMethods?.shippingMethodName:shippingMethodDetails?.shippingMethodName}{centsToDollars(shippingMethods ? shippingMethods?.price?.centAmount:shippingMethodDetails?.price?.centAmount)}</b>
                               </Card>
                            </div>
                        </CollapsiblePanel>
                    </div>: <div style={{ fontWeight: 'bold'}}>No shipping method selected</div>}
                  {shippingMethodDetails !== null && shippingMethodDetails !== undefined && paymentData ? 
                    <div className="order-panel">
                    <CollapsiblePanel
                        isClosed={paymentPanel}
                        onToggle={() => setPaymentPanel(!paymentPanel)}
                        header="Payment"
                        >  
                            <SelectField
                                title=""
                                value={paymentTokenData}
                                options={cardDetails}
                                onChange={(event) => { setPaymentToken(event.target.value), setPlaceOrder(true) }}
                            />
                         </CollapsiblePanel>
                    </div>: <div style={{ fontWeight: 'bold'}}>No Credit card mapped</div>}
                    {placeOrderButton ? <div>
                        <SecondaryButton
                            onClick={() => { placeOrder() }}
                            label='Create order'
                        />
                    </div> : null}
                    {orderNumber ? <div style={{ width: '45%'}}><PrimaryButton
                        tone="primary"
                        label="Send order confirmation"
                        onClick={() => sendConfirmationMail()}
                    /></div>: null}
                <div className="notification">
                {notification ? <ContentNotification type="success">{notificationMessage}{hideNotification()}</ContentNotification> : null}
                {errorNotifiaction ? <ContentNotification type="error">{errorMessage}{hideNotification()}</ContentNotification> : null}
                </div></Spacings.Stack>
            </div> : <div>No Products & Shipping methods</div>}
        </div>
    );
};
export default ReviewOrder;