import React, { useState, useEffect, useRef } from "react";
import Text from '@commercetools-uikit/text';
import axios from "axios";
import { SERVICES, centsToDollars } from '../../config/api';
import Spacings from '@commercetools-uikit/spacings';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import { ConfirmationDialog } from '@commercetools-frontend/application-components';
import SelectInput from '@commercetools-uikit/select-input';
import RadioInput from '@commercetools-uikit/radio-input';
import Grid from '@commercetools-uikit/grid';
import Card from '@commercetools-uikit/card';
import TextInput from '@commercetools-uikit/text-input';
import { useIntl } from 'react-intl';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import { CheckInactiveIcon, PlusBoldIcon, CartIcon, BoxIcon, CloseIcon } from '@commercetools-uikit/icons';
import AddressList from "../add-company/address-list";
import NumberInput from '@commercetools-uikit/number-input';
import { useHistory } from "react-router-dom";
import { Pagination } from '@commercetools-uikit/pagination';
import LoadingSpinner from '@commercetools-uikit/loading-spinner';
import { ContentNotification } from '@commercetools-uikit/notifications';
import CheckboxInput from '@commercetools-uikit/checkbox-input';
import CollapsiblePanel from '@commercetools-uikit/collapsible-panel';
import { useRowSelection } from '@commercetools-uikit/hooks';
import SelectField from '@commercetools-uikit/select-field';
import ReviewOrder from "./review-order";
import '../orders/order-details.css';
import { FilterOption } from "../../common/Filter";
import { FormModalPage } from '@commercetools-frontend/application-components';
import TextField from '@commercetools-uikit/text-field';
import State from "../../state-list.json";
import MultilineTextInput from '@commercetools-uikit/multiline-text-input'
import { Asterisk } from "../../common/Asterisk";
import IconButton from '@commercetools-uikit/icon-button';
import MultilineTextField from '@commercetools-uikit/multiline-text-field';
import loadClyde from "../../common/loadClyde";
import { CallApi } from "../../plugin/Axios";
import { ProjectKey } from "../../config/api";

const maxphonelength = 10;
const maxqty = 20;
const AddOrder = (props) => {

    let data = props.history.location
    const getLastItem = thePath => thePath.substring(thePath.lastIndexOf('/') + 1)
    let orderID = data.id ? data.id : getLastItem(props.location.pathname)
    // console.log(orderID, 'orderID')

    const [addOrder, setAddOrder] = useState(true);
    const [addOrderDialog, setAddOrderDialog] = useState(true);
    const [customerStore, setCustomerStore] = useState('');
    const [currency, setCurrency] = useState('');
    const [trackingNone, setTrackingNone] = useState(false);
    const [trackingOnly, setTrackingOnly] = useState(false);
    const [trackingReserve, setTrackingReserve] = useState(false);
    const [trackingValue, setTrackingValue] = useState('');
    const [customerSelection, setCustomerSelection] = useState(false);
    const [pageLimit, setPageLimit] = useState(20);
    const [pageNumber, setPageNumber] = useState(1);
    const [totalResults, setTotalResults] = useState(0);
    const [totalProductResults, setTotalProductResults] = useState(0);
    const [employeeData, setEmployeeData] = useState([]);
    const [spinner, setSpinner] = useState(true);
    const { formatTime, formatDate } = useIntl();
    const [shippingAddress, setShippingAddress] = useState({});
    const [customerEmail, setCustomerEmail] = useState();
    const [updateAddress, setUpdateAddress] = useState(false);
    const [newAddress, setNewAddress] = useState(false);
    const [customerId, setCustomerId] = useState()
    const [customerTab, setCustomerTab] = useState(true)
    const [productTab, setProductTab] = useState(false)
    const [shippingTab, setShippingTab] = useState(false)
    const [confirmationTab, setConfirmationTab] = useState(false);
    const [cartItems, setCartItems] = useState(false)
    let history = useHistory();
    const [discountCode, setDiscountCode] = useState();
    const [productItem, setProductItem] = useState([]);
    const [productId, setProductId] = useState();
    const [productData, setProductData] = useState([]);
    const [cartDetails, setCartDetails] = useState();
    const [priceDetails, setPriceDetails] = useState();
    const [productQuantity, setProductQuantity] = useState()
    const [searchByField, setSearchByField] = useState('');
    const [searchValue, setSearchValue] = useState('');
    const [customerChannel, setCustomerChannel] = useState();
    const [channel, setChannel] = useState()
    const [productLineItem, setProductLineItem] = useState();
    const [productArray, setProductArray] = useState([]);
    const [paymentData, setPaymentData] = useState();
    const [shippingAdd, setShippingAdd] = useState()
    const [discountMsg, setDiscountMsg] = useState();
    const [giftOrder, setGiftOrder] = useState(false);
    const [customerPanel, setCustomerPanel] = useState(false);
    const [shippingMethods, setShippingMethods] = useState();
    const [shippingData, setShippingData] = useState(false)
    const [shippingMethodDetails, setShippingMethodDetails] = useState()
    const [notification, setNotification] = useState(false);

    // warranty messages
    const [warrantyNotification, setWarrantyNotification] = useState(false);
    const [warrantynotificationMessage, setwarrantyNotificationMessage] = useState('');
    const [warrantyerrorNotifiaction, setwarrantyErrorNotification] = useState(false);
    const [warrantyerrorMessage, setwarrantyErrorMessage] = useState('');

    const [notificationMessage, setNotificationMessage] = useState('');
    const [errorNotifiaction, setErrorNotification] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [productNewArr, setProductNewArr] = useState([]);
    const [shippingMethodValue, setShippingMethodValue] = useState(false);
    const [orderData, setOrderData] = useState();
    const [tableData, setTableData] = useState([]);
    const [shippingInfo, setShippingInfo] = useState();
    const [customShippingAddress, setCustomShippingAddress] = useState();
    const [addressList, setAddressList] = useState([]);
    const [orderTags, setOrderTags] = useState();
    const [orderTagData, setOrderTagData] = useState();
    const [customerDetails, setCustomerDetails] = useState()
    const [disableButton, setDisableButton] = useState(false);
    const [first, setFirst] = useState('')
    const [second, setSecond] = useState('')
    const [streetname, setStreetName] = useState('')
    const [state, setState] = useState('')
    const [post, setPost] = useState('')
    const [country, setCountry] = useState('')
    const [city, setCity] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')
    const [shippingId, setShippingId] = useState()
    const [streetNumber, setStreetNumber] = useState('')
    const [enableAddSalesRep, setEnableAddSalesRep] = useState(false);
    const [notificationAddress, setNotificationAddress] = useState(true);
    const [notificationAddressMessage, setNotificationAddressMessage] = useState('You have selected 0 address');
    const [shippingNotification, setShippingNotification] = useState('')
    const [shipNotification, setShipNotification] = useState(false)
    const [stateList, setStateList] = useState(State)
    const [giftEmail, setGiftEmail] = useState()
    const [giftMessage, setGiftMessage] = useState()
    const [discountErrorMsg, setDiscountErrorMsg] = useState()
    const [productPageLimit, setProductPageLimit] = useState(20);
    const [productPageNumber, setProductPageNumber] = useState(1)
    const [shippingAddressList, setShippingAddressList] = useState([])
    const [customerCountry, setCustomerCountry] = useState()
    const [customerCompany, setCustomerCompany] = useState()

    const [warrentyOrderNumber, setWarrentyOrderNumber] = useState('')

    const [loader, setLoader] = useState(true)
    const [comment, setComment] = useState('')

    const [clydeData, setClydeData] = useState();
    const [addClyde, setAddClyde] = useState(false);
    const [clydeProduct, setClydeProduct] = useState()
    const [clydeProductItem, setClydeProductItem] = useState()


    let countryCode = 'US';
    if (ProjectKey && (ProjectKey === 'mo-dev-eu-1' || ProjectKey === 'mo-preprod1-eu' || ProjectKey === 'mo-prod-eu')) {
        countryCode = 'EU';
    }

    useEffect(() => {
        let offset = (pageNumber - 1) * pageLimit;
        if (totalResults && totalResults < offset) {
            setPageNumber(0);
            return;
        }
        setSpinner(true);
        let query = '';
        if (searchValue) {
            query += `${searchByField}=${encodeURIComponent(searchValue)}`;
        }
        if (channel) {
            query += `&customerChannel=${encodeURIComponent(channel)}`;
        }
        if(customerStore){
        CallApi(`${SERVICES.ACCOUNT}/api/v1/customers?&limit=${pageLimit}&offset=${offset}&${query}`, 'get')
            .then(res => {
                setEmployeeData(res.data.results);
                setTotalResults(res.data.total);
            })
            .then(res =>
                setSpinner(false)
            )
            .catch(error => {
                setErrorMessage(error.message);
                setErrorNotification(true);
            })}

    }, [pageNumber, pageLimit, channel, searchValue, customerStore]);

    useEffect(() => {
        let offsetData = (productPageNumber - 1) * productPageLimit;
        if (totalProductResults && totalProductResults < offsetData) {
            setProductPageNumber(0);
            return;
        }
        if(customerCountry){
        setSpinner(true);
        axios.get(`${SERVICES.PRODUCT}/api/v1/products?limit=${productPageLimit}&offset=${offsetData}`, {headers: {country: customerCountry}})
            .then(res => {
                setProductData(res.data.productResponseList);
                setTotalProductResults(res.data.total);
            })
            .then(res =>
                setSpinner(false)
            )
            .catch(error => {
                setErrorMessage(error.message);
                setErrorNotification(true);
                setSpinner(false)
            })
        }
    }, [productPageNumber, productPageLimit, customerCountry])

    useEffect(() => {
        if (orderID !== 'new') {
            setAddOrder(false)
            setCustomerTab(false)
            setConfirmationTab(true)
            let country
            if(countryCode == 'EU'){
                country = 'GB'
            }else{
                country = 'US'
            }
            let queryParams = {
                country: country
            }
            axios.post(`${SERVICES.CHECKOUT}/api/v1/users/checkout/reorder/${orderID}/loadShippingAddress/true/loadShippingMethod/true?country=${country}`, queryParams, {headers: {country: country}})
                .then(response => {
                    if (response.data) {
                        // console.log(response.data,"response.data")
                        setOrderData(response.data);
                        setCartItems(true)
                        setCartDetails(response.data.id)
                        setCustomerStore(response.data.store.key)
                        setCurrency(response.data.totalPrice.currencyCode)
                        setCustomerId(response.data.customerId)
                        setCustomerChannel(response.data.custom.fields.channel)
                        setPriceDetails(response.data)
                        setCustomShippingAddress(response.data.shippingAddress)
                        setCustomerEmail(response.data.customerEmail)
                        setShippingAdd(response.data.shippingAddress)
                        setCustomerCountry(response.data.country)
                        response?.data?.lineItems?.map((lineItem) => {
                            let clydelineItem = []
                            clydelineItem.push(lineItem)
                            let productDetails = {
                                id: lineItem?.productId,
                                name: countryCode == 'US' ? lineItem.name["en-US"] : lineItem.name["en-GB"],
                                price: centsToDollars(lineItem?.price?.value?.centAmount),
                                quantity: lineItem?.quantity,
                                lineItemId: lineItem?.id,
                                sku: lineItem?.variant?.sku,
                                protectionPlanLineItem: lineItem?.custom?.fields?.protectionPlanLineItem,
                                clydeEnabled: undefined
                            }
                            if (lineItem?.custom?.fields?.subscriptionEnabled != true) {
                                tableData.push(productDetails)
                            }

                            tableData.map(clydeReorder => {
                                clydelineItem.map(itemValue => {
                                    if (clydeReorder.protectionPlanLineItem !== undefined) {
                                        if (clydeReorder.protectionPlanLineItem == itemValue?.custom?.fields?.protectionPlanLineItem) {
                                            clydeReorder.clydeEnabled = false
                                        }
                                        if (clydeReorder.protectionPlanLineItem == lineItem?.id) {
                                            clydeReorder.protectionId = lineItem?.productId
                                        }
                                    }
                                    if (clydeReorder.protectionPlanLineItem == undefined && itemValue.productType.obj.name == "Warranty") {
                                        clydeReorder.isProtectionItem = true
                                    }
                                })
                            })
                            setProductArray(tableData)
                            lineItem?.variant?.attributes?.map(prodAttribute => {
                                if (prodAttribute?.name == "SubscriptionIdentifier") {
                                    let productSubsId = lineItem.productId
                                    if (prodAttribute?.value?.id) {
                                        prodAttribute?.value?.obj?.masterData?.current?.masterVariant?.attributes?.map(subsPeriod => {
                                            if (subsPeriod?.name == "period") {
                                                tableData.map((arrayItem) => {
                                                    if (productSubsId == arrayItem.id) {
                                                        arrayItem.subscription = subsPeriod.value
                                                    }
                                                    setProductArray(tableData)
                                                })
                                            }
                                        })
                                    }
                                }
                            })
                            let skuquery = '';
                            tableData.map((product, key) => {
                                skuquery += (key !== 0 ? ',' : '') + product.sku;
                            })
                            if(skuquery && countryCode == 'US'){
                            loadClyde(skuquery, (data) => {
                                tableData.map((productItem) => {
                                    if (productItem.id) {
                                        data?.data?.map(clyde => {
                                            if (productItem.clydeEnabled === undefined && productItem.sku == clyde?.attributes?.sku) {
                                                productItem.clydeEnabled = true
                                                productItem.clydeContracts = clyde?.attributes?.contracts
                                                setClydeData(clyde?.attributes?.contracts)
                                            }
                                            if (productItem.sku == clyde?.attributes?.sku) {
                                                productItem.clydeContracts = clyde?.attributes?.contracts
                                                setClydeData(clyde?.attributes?.contracts)
                                            }
                                        });
                                    }
                                    setProductArray(tableData)
                                })
                            });
                        }
                        })
                        setShippingInfo(response.data.shippingInfo)
                        if (response.data.customerId) {
                            CallApi(`${SERVICES.ACCOUNT}/api/v1/users?customerId=${response.data.customerId}`, 'get')
                                .then(res => {
                                    setCustomerDetails(res.data)
                                    setShippingAddressList(res?.data?.shippingAddressList)
                                    setAddressList(res.data.addressList)
                                    setSpinner(false)
                                })
                                .catch(error => {
                                    setErrorNotification(true);
                                    setErrorMessage(error.message)
                                })
                        }
                    }
                })
                .catch(error => {
                    setLoader(false)
                    setSpinner(false)
                    setErrorMessage(error.message);
                    setErrorNotification(true);
                })
        }
    }, []);

    useEffect(() => {
        if (orderID == 'new') {
            setCustomerStore()
            setCurrency()
            setCustomerEmail('')
            setConfirmationTab(false)
            setCartItems(false)
            setCustomerTab(true)
        }
    }, [orderID])

    useEffect(() => {
        setSpinner(true);
        axios.get(`${SERVICES.ORDER}api/v1/orders/tags`)
            .then((res) => {
                setOrderTags(res.data.value.orderTags)
            })
            .then(res =>
                setSpinner(false)
            )
            .catch(error => {
                setSpinner(false)
                setErrorMessage(error.message);
                setErrorNotification(true);
            })
    }, [])

    let employeeList = []

    employeeData && employeeData.map((employeeData) => {
        if (countryCode == 'EU' && employeeData?.custom?.fields?.store === customerStore) {
            let employeeDetails = {
                id: employeeData.id,
                number: employeeData.customerNumber,
                externalId: <div className="emptyData">-----</div>,
                firstname: employeeData.firstName,
                lastname: employeeData.lastName,
                company: employeeData.companyName,
                email: employeeData.email,
                customergroup: employeeData.customerGroup?.obj.name,
                address: employeeData.addresses,
                customerChannel: employeeData?.custom?.fields?.channel,
            }
            employeeList = [...employeeList, employeeDetails]
        }
        if(countryCode == 'US' && (employeeData?.custom?.fields?.store !== 'MLK_UK' && employeeData?.custom?.fields?.store !== 'MLK_EU')){
            let employeeDetails = {
                id: employeeData.id,
                number: employeeData.customerNumber,
                externalId: <div className="emptyData">-----</div>,
                firstname: employeeData.firstName,
                lastname: employeeData.lastName,
                company: employeeData.companyName,
                email: employeeData.email,
                customergroup: employeeData.customerGroup?.obj.name,
                address: employeeData.addresses,
                customerChannel: employeeData?.custom?.fields?.channel,
            }
            employeeList = [...employeeList, employeeDetails]
        }
        
    })

    const columns = [
        { key: "number", label: "Customer number" },
        { key: "externalId", label: "External ID" },
        { key: "firstname", label: "First name" },
        { key: "lastname", label: "Last name" },
        { key: "company", label: "Company" },
        { key: "email", label: "Email" },
        { key: 'customerChannel', label: 'Customer Channel' },
    ];

    let productList = []
    productData && productData.map((productData) => {
        if (productData?.productBean?.productType?.obj?.key == "purifier" || productData?.productBean?.productType?.obj?.key == "filter") {
            let productDetails = {
                id: productData.productBean.id,
                name: countryCode == 'US' ? productData.productBean.masterData.current.name['en-US']: productData.productBean.masterData.current.name['en-GB'],
                price: countryCode == 'US' ? centsToDollars(productData?.productBean?.masterData?.current?.masterVariant?.price?.value?.centAmount): centsToDollars(productData?.productBean?.masterData?.current?.masterVariant?.prices[0]?.value?.centAmount),
                sku: productData.productBean.masterData.current.masterVariant.sku,
                quantity: 1,
                subscription: '',
                addProduct: <div><IconButton
                    onClick={() => handleProductClick(productDetails)}
                    icon={<PlusBoldIcon />}
                /></div>
            }
            productList = [...productList, productDetails]
        }
        // console.log(productList, 'productList2222222222')
    })

    const productColumns = [
        { key: "name", label: "Name", shouldIgnoreRowClick: true },
        { key: "price", label: "Price", shouldIgnoreRowClick: true },
        { key: "sku", label: "SKU", shouldIgnoreRowClick: true },
        // { key: "quantity", label: "Quantity", shouldIgnoreRowClick: true },
        { key: 'addProduct', label: 'Add' }
    ];

    const searchOption = [
        { value: 'email', label: 'Email' },
        { value: 'lastName', label: 'Customer Last Name' },
        { value: 'firstName', label: 'First Name' },
        { value: 'dob', label: 'Date Of Birth' },
        { value: 'customerChannel', label: 'Customer Channel' },
        { value: 'customerStore', label: 'Customer Store' },
    ];

    const channelOption = [
        { value: 'b2b', label: 'B2B' },
        { value: 'd2c', label: 'D2C' }
    ]

    const handleOnChange = (value) => {
        if (value === "b2b") {
            setChannel('B2B')
        }
        if (value === 'd2c') {
            setChannel('D2C')
        }
    }

    const handleRowClick = (item) => {
        setSpinner(true)
        CallApi(`${SERVICES.ACCOUNT}api/v1/users?customerId=${item.id}`, 'get')
            .then(res => {
                setCustomerDetails(res.data)
                setAddressList(res.data?.addressList)
                setShippingAddressList(res?.data?.shippingAddressList)
                setCustomerChannel(res.data.channel)
                setCustomerCountry(res.data.country)
                if(res.data.channel == 'B2B'){
                    CallApi(`${SERVICES.ACCOUNT}/api/v1/accounts/users/${item.id}/dashboard?limit=5&offset=0`, 'get')
                    .then(res => {
                        let b2bAddress = []
                        res?.data?.customerResponseBean?.addresses?.map(addressData=> {
                            res?.data?.customerResponseBean?.shippingAddressIds?.map(shipData=>{
                                if(addressData.id == shipData){
                                    b2bAddress.push(addressData)
                                    setShippingAddressList(b2bAddress)
                                }
                            })
                        })
                        setCustomerCompany(res.data.customerResponseBean.custom.fields.accountId)
                    })
                    .then(res =>
                        setSpinner(false)
                    )
                    .catch(error => {
                        setErrorMessage(error.message);
                        setErrorNotification(true);
                        setSpinner(false)
                    })
                }
                let customerData = {
                    customerId: item.id,
                    country: res.data.country
                }
                setSpinner(true)
                axios.post(`${SERVICES.CART}/api/v1/carts?country=${res.data.country}`, customerData, {headers: {country: res.data.country}})
                    .then(res => {
                        setCartDetails(res.data.id);
                    })
                    .then(res =>
                        setSpinner(false)
                    )
                    .catch(error => {
                        setErrorMessage(error.message);
                        setErrorNotification(true);
                        setSpinner(false)
                    })
            }).catch(error => {
                setSpinner(false);
                setErrorMessage(error.message);
                setErrorNotification(true);
            })
        setCustomerId(item.id)
        setCustomerEmail(item.email)
        setAddOrder(false);
        setCustomerSelection(true);
        setProductArray([])
        setDiscountCode('')
        setNotificationAddressMessage('You have selected 0 address')
        setGiftEmail()
        setGiftMessage()
        setOrderTagData()
        setGiftOrder(false)
        // setShippingTab(true)
        setProductTab(true)
        setCustomerTab(false)
        setNotificationAddress(true)
        setWarrantyNotification(false)
        setwarrantyErrorNotification(false)
    }

    const handleClose = () => {
        setAddOrder(false);
        setAddOrderDialog(false);
        history.goBack()
    }

    const handleConfirm = () => {
        let hasError = isConfirm();
        if (typeof (hasError) === "string") {
            setErrorNotification(true)
            setErrorMessage(hasError)
            return;
        }
        setAddOrder(false);
        setAddOrderDialog(false);

    }

    const isConfirm = () => {
        if (!customerStore) {
            return "Please select Store";
        } else if (!currency) {
            return "Please select Currency";
        }
        return true;
    }

    const tracking = (e) => {
        setTrackingValue(e.target.value)
    }

    const handleUpdateClick = () => {
        setUpdateAddress(!updateAddress)
    }
    const handleNewAddress = () => {
        setNewAddress(!newAddress)
    }

    const handleSpinner = () => {

    }

    const handleCustomerTab = () => {
        setCustomerTab(true)
        setProductTab(false)
        setShippingTab(false)
        setConfirmationTab(false)
        setCartItems(false)
    }
    const handleShippingTab = () => {
        setProductTab(false)
        setCustomerTab(false)
        setShippingTab(true)
        setConfirmationTab(false)
    }
    const handleProductTab = () => {
        setProductTab(true)
        setCustomerTab(false)
        setShippingTab(false)
        setConfirmationTab(false)
    }
    const handleConfirmationTab = () => {
        setConfirmationTab(true)
        setProductTab(false)
        setCustomerTab(false)
        setShippingTab(false)
    }

    const handleProductClick = (item) => {
        setCartItems(true)
        let isExist = productArray.filter((product) => {
            if (product.id === item.id) {
                product.quantity = parseInt(product.quantity) + 1;
                return true;
            }
            return false;
        });
        if (isExist.length === 0) {
            productArray.push(item);
        }
        let itemData = productArray.filter((data) => { return data.id === item.id });
        itemData = itemData[0] ? itemData[0] : null;
        itemData && productArray.map((product) => {
            if (itemData.protectionId === product.id) {
                if (parseInt(itemData.quantity) > parseInt(product.quantity)) {
                    product.disabled = false
                } else {
                    product.quantity = itemData.quantity;
                    product.disabled = false
                }
            }
        })
        setProductItem(item)
        setProductId(item.id)
        setProductQuantity(1)
        let cartData = {
            quantity: item.quantity,
            productId: item.id,
            action: "ADD_LINE_ITEM",
            variantId: 1,
            country: customerCountry
        }
        setSpinner(true)
        axios.post(`${SERVICES.CART}/api/v1/carts/${cartDetails}?country=${customerCountry}`, cartData, {headers: {country: customerCountry}})
            .then(res => {
                setPriceDetails(res.data);
                setShippingMethodValue(true)
                if (res?.data?.message || res?.data?.error?.message || res?.data?.errorMessage) {
                    setErrorNotification(true);
                    setErrorMessage(res?.data?.message || res?.data?.error?.message || res?.data?.errorMessage)
                }
                if (res && res.data) {
                    res?.data && res?.data?.lineItems?.map((lineItem) => {
                        setProductLineItem(lineItem.id)
                        let lineItemId = {
                            lineItemId: lineItem.id,
                            productId: lineItem.productId,
                            name: countryCode == 'US' ? lineItem.name["en-US"] : lineItem.name["en-GB"],
                            price: centsToDollars(lineItem.price.value.centAmount)
                        }
                        productNewArr.push(lineItemId)

                        lineItem?.variant?.attributes?.map(prodAttribute => {
                            if (prodAttribute?.name == "SubscriptionIdentifier") {
                                let productSubsId = lineItem.productId
                                if (prodAttribute?.value?.id) {
                                    prodAttribute?.value?.obj?.masterData?.current?.masterVariant?.attributes?.map(subsPeriod => {
                                        if (subsPeriod?.name == "period") {
                                            productArray.map((arrayItem) => {
                                                if (productSubsId == arrayItem.id) {
                                                    arrayItem.subscription = subsPeriod.value
                                                }
                                                setProductArray(productArray)
                                            })
                                        }
                                    })
                                }
                            }
                        })
                    })
                }
            })
            .then(res =>
                setSpinner(false)
            )
            .catch(error => {
                setSpinner(false)
                setErrorMessage(error.message);
                setErrorNotification(true);
            })
        let skuquery = '';
        productArray.map((product, key) => {
            skuquery += (key !== 0 ? ',' : '') + product.sku;
        })
        if (item.clydeEnabled === undefined && countryCode == 'US') {
            loadClyde(skuquery, (data) => {
                productArray.map((productItem) => {
                    if (item.id == productItem.id) {
                        data?.data?.map(clyde => {
                            if (productItem.clydeEnabled === undefined && productItem.sku == clyde?.attributes?.sku) {
                                productItem.clydeEnabled = true
                                productItem.clydeContracts = clyde?.attributes?.contracts
                                setClydeData(clyde?.attributes?.contracts)
                            }
                        });
                    }
                    setProductArray(productArray)
                })
            });
        }
    }

    const addProduct = (qty, productValue) => {
        let errorQuantity = false
        if (qty > maxqty) {
            setErrorNotification(true);
            setErrorMessage('You can add maxmimum of ' + maxqty + ' counts only')
            qty = 20
        }
        let productLineItemId
        let productNewID
        if (productValue && productValue.lineItemId) {
            productLineItemId = productValue.lineItemId
        }
        productNewArr?.map((value) => {
            if (productValue.id == value.productId) {
                productLineItemId = value.lineItemId
                productNewID = value.productId
            }
        })
        if ((qty == null) || (qty == '0')) {
            productArray.map((item) => {
                if (productValue.id == item.protectionId) {
                    item.clydeEnabled = true
                }
            })
            setProductArray(productArray);
            var array = productArray
            var filtered = array.filter(function (value, index, arr) {
                return (value.id !== productValue.id && productValue.protectionId !== value.id);
            });
            setProductArray(filtered);
        }
        if (qty) {
            productArray.map((item) => {
                if (productValue.id === item.id) {
                    item.quantity = qty
                }
                if (productValue.protectionId === item.id && !productValue.isProtectionItem) {
                    if (parseInt(productValue.quantity) > item.quantity) {
                        item.disabled = false
                    } else {
                        item.quantity = productValue.quantity;
                        item.disabled = false
                    }
                }
            })
            productArray.map((item) => {
                if (productValue.id === item.id && productValue.isProtectionItem) {
                    let _productData = productArray.filter((_item) => { return productValue.id === _item.protectionId })[0];
                    if (_productData && parseInt(qty) == parseInt(_productData.quantity)) {
                        item.disabled = false;
                        item.quantity = _productData.quantity
                    } if (_productData && parseInt(qty) > parseInt(_productData.quantity)) {
                        item.disabled = true;
                        item.quantity = _productData.quantity
                        setErrorNotification(true);
                        errorQuantity = true
                        setErrorMessage('You can add maxmimum of ' + _productData.quantity + ' counts only')
                    }
                }
            })
        }

        let cartData = {
            quantity: qty ? qty : '0',
            lineItemId: productLineItemId,
            // productId: productValue.id,
            action: qty ? "CHANGE_LINE_ITEM_QTY" : "REMOVE_LINE_ITEM",
            variantId: 1,
            country: customerCountry
        }
        if (qty <= maxqty && qty != '' && !errorQuantity) {
            setSpinner(true)
            axios.post(`${SERVICES.CART}/api/v1/carts/${cartDetails}?country=${customerCountry}`, cartData, {headers: {country: customerCountry}})
                .then(res => {
                    setPriceDetails(res.data);
                    if (res?.data?.message) {
                        setErrorNotification(true);
                        setErrorMessage(res?.data?.message)
                    }
                })
                .then(res =>{ 
                    setSpinner(false)
                })
                .catch(e => {
                    setErrorNotification(true);
                    setErrorMessage(e.message)
                    setSpinner(false)
                })
        }
    }

    const shippingSelect = () => {
        setSpinner(true)
        let params = {
            country: customerCountry
        }
        axios.get(`${SERVICES.CHECKOUT}/api/v1/users/checkout/${cartDetails}/shipping-methods?country=${customerCountry}`, params, {headers: {country: customerCountry}})
            .then(res => {
                setShippingMethods(res.data);
                if (res?.data?.message || res?.data?.error?.message) {
                    setErrorNotification(true);
                    setErrorMessage(res?.data?.message ? res.data.message : res?.data?.error?.message)
                }
            })
            .then(res => {
                setSpinner(false)
                // setNotification(true)
                // setNotificationMessage('Added shipping method');
            })
            .catch(e => {
                setSpinner(false)
                setErrorNotification(true);
                setErrorMessage('Please check shipping address details or add at least one line item')
            })
    }

    const shippingDetails = [
        { key: 'name', label: 'Name' },
        { key: 'shippingRate', label: 'Shipping rate' },
        { key: 'freeAbove', label: 'Free above' },
        { key: 'duration', label: 'Duration' },
        { key: 'serviceType', label: 'Service type' }
    ]

    let rows = []
    const {
        rows: rowsWithSelection,
        toggleRow,
        selectAllRows,
        deselectAllRows,
        getIsRowSelected,
        getNumberOfSelectedRows,
    } = useRowSelection('checkbox', rows);

    const countSelectedRows = getNumberOfSelectedRows();
    const isSelectColumnHeaderIndeterminate =
        countSelectedRows > 0 && countSelectedRows < rowsWithSelection.length;
    const handleSelectColumnHeaderChange =
        countSelectedRows === 0 ? selectAllRows : deselectAllRows;

    const shippingMethodscolumns = [
        { key: 'name', label: 'Name' },
        { key: 'shippingRate', label: 'Shipping rate' },
        { key: 'freeAbove', label: 'Free above' },
        { key: 'duration', label: 'Duration' },
        { key: 'serviceType', label: 'Service type' }
    ];

    let methodsList = [];
    shippingMethods && shippingMethods?.map((items) => {
        let shippingMethods = {
            name: items.serviceName,
            shippingRate: items.cost,
            freeAbove: '-',
            duration: items.duration,
            serviceType: items.serviceType
        }
        methodsList = [...methodsList, shippingMethods]
    })

    const showrowData = (rowData) => {
        setShippingMethodDetails(rowData)
        setShipNotification(true);
        setShippingNotification(`You have selected ${rowData.name} method`)
        let cartData = {
            // quantity: quantity,
            lineItemId: productLineItem,
            productId: productId,
            action: "SET_SHIPPING_METHOD",
            variantId: 1,
            country: customerCountry,
            shippingMethod: {
                "cost": rowData.shippingRate,
                "name": rowData.name
            }
        }
        setSpinner(true)
        axios.post(`${SERVICES.CART}/api/v1/carts/${cartDetails}?country=${customerCountry}`, cartData, {headers: {country: customerCountry}})
            .then(res => {
                if (res?.data?.custom) {
                    setPriceDetails(res?.data)
                }
            })
            .then(res =>
                setSpinner(false)
            )
            .catch(error => {
                setErrorMessage(error.message);
                setErrorNotification(true);
            })
    }

    const applyDiscount = () => {
        setSpinner(true)
        if(priceDetails?.custom?.fields?.totalDiscountPrice > '0' ){
            setDiscountMsg('Unable to Apply Multiple Promos.Already Promo setted.')
            setSpinner(false)
        }else{
        axios.post(`${SERVICES.CHECKOUT}/api/v1/users/checkout/${cartDetails}/discounts/${discountCode}?country=${customerCountry}`, '', {headers: {country: customerCountry}})
            .then(res => {
                // console.log(res, 'applyDiscount')
                if (res?.data?.custom) { setPriceDetails(res.data) }
                if (res?.data.custom?.fields?.discountMessage) {
                    setDiscountMsg('Code does not match the cart')
                }
                if (res?.data?.custom?.fields?.totalDiscountPrice !== '0') {
                    setDiscountMsg('Your coupon was successfully applied.')
                }
                if (res?.data?.message) {
                    setDiscountMsg(res.data.message)
                }
            })
            .then(res =>
                setSpinner(false)
            )
            .catch(error => {
                setSpinner(false)
                setDiscountErrorMsg('Invalid code. Please check and try again');
            })}
    }

    const removeDiscount = () => {
        setDiscountCode('')
        setSpinner(true)
        axios.delete(`${SERVICES.CHECKOUT}/api/v1/users/checkout/${cartDetails}/discounts/${discountCode}?country=${customerCountry}`, '', {headers: {country: customerCountry}})
            .then(res => {
                // console.log(res, 'removeDiscount')
                if (res?.data?.custom) { setPriceDetails(res.data) }
                if (res?.data.custom?.fields?.discountMessage) {
                    setDiscountMsg('Code does not match the cart')
                }
                if (res?.data?.custom?.fields?.totalDiscountPrice == '0') {
                    setDiscountMsg('Your coupon was successfully removed.')
                }
            })
            .then(res =>
                setSpinner(false)
            )
            .catch(error => {
                setSpinner(false)
                setDiscountErrorMsg('Invalid code. Please check and try again');
            })
    }

    const placeGiftOrder = (gift) => {
        if (giftMessage && giftMessage?.length > 200) {
            setErrorMessage('Gift message should be less than 200 chars');
            setErrorNotification(true);
        }
        let giftOrder
        if (gift == false) {
            giftOrder = true
        } else {
            giftOrder = false
        }
        setGiftOrder(giftOrder)
        let giftOrderData = {
            action: "GIFT",
            gift: giftOrder,
        }
        setSpinner(true)
        CallApi(`${SERVICES.CART}/api/v1/carts/${cartDetails}`, 'post', giftOrderData)
            .then(res => {
                // console.log(res, 'cartData111111111111111')
            })
            .then(res =>
                setSpinner(false)
            )
            .catch(error => {
                setSpinner(false)
                setErrorMessage(error.message);
                setErrorNotification(true);
            })
    }

    const shippingColumns = [
        { key: 'contactname', label: 'Contact Name' },
        { key: 'address', label: 'Address' },
        { key: 'city', label: 'City' },
        { key: 'state', label: 'State' },
        { key: 'postalCode', label: 'Postal Code' },
        { key: 'country', label: 'Country' },
        { key: 'phoneNumber', label: 'Phone' },
        {
            key: 'customRenderer', label: 'Action', renderItem: (rows) => (
                <SecondaryButton
                    onClick={() => handleBulkUpdateOpen(rows)}
                    label="Edit"
                />)
        },
    ];

    const billingColumns = [
        { key: 'contactname', label: 'Contact Name' },
        { key: 'address', label: 'Address' },
        { key: 'city', label: 'City' },
        { key: 'state', label: 'State' },
        { key: 'postalCode', label: 'Postal Code' },
        { key: 'country', label: 'Country' },
        { key: 'phoneNumber', label: 'Phone' }
    ];

    let billAddressList = [];
        customerDetails?.billingAddressList?.map(billId => {
                let billAddressLists = {
                    id: billId?.id || '',
                    contactname: billId?.firstName + ' ' + billId?.lastName,
                    companyname: billId?.company || '',
                    address: billId?.streetName ? (billId?.streetName + '' + billId?.streetNumber) : (billId?.streetAddress1 + (billId?.streetAddress2 ? ',' + billId?.streetAddress2 : '')),
                    phoneNumber: billId?.phone || '',
                    postalCode: billId?.postalCode || '',
                    country: billId?.country || '',
                    city: billId?.city || '',
                    state: billId?.state || ''
                }
                billAddressList = [...billAddressList, billAddressLists]
        })

    let shipAddressList = [];
    shippingAddressList && shippingAddressList?.map(data => {
        //   console.log(data, 'data111')
        let addressLists = {
            id: data?.id || '',
            contactname: data?.firstName + ' ' + data?.lastName,
            companyname: data?.company || '',
            address: data?.streetName ? (data?.streetName + '' + data?.streetNumber) : (data?.streetAddress1 + (data?.streetAddress2 ? ',' + data?.streetAddress2 : '')),
            phoneNumber: data?.phone || '',
            postalCode: data?.postalCode || '',
            country: data?.country || '',
            city: data?.city || '',
            state: data?.state || '',
            firstName: data?.firstName || '',
            lastName: data?.lastName || '',
            streetName: data?.streetName || '',
            streetNumber: data?.streetNumber
        }
        shipAddressList = [...shipAddressList, addressLists]
    });

    const handleAddressClick = (item) => {
        //   console.log(item, 'item address')
        setShippingAdd(item);
        setNotificationAddress(true);
        setNotificationAddressMessage(`You have selected ${item.contactname ? item.contactname : '0'} address`)
        let cartData = {
            action: "SET_SHIPPING_ADDRESS",
            shippingAddress: item,
            gift: giftOrder == true ? giftOrder : '',
            recipientsEmail: giftOrder == true ? giftEmail : '',
            giftMessage: giftOrder == true ? giftMessage : '',
            country: customerCountry
        }
        setSpinner(true)
        axios.post(`${SERVICES.CART}/api/v1/carts/${cartDetails}?country=${customerCountry}`, cartData, {headers: {country: customerCountry}})
            .then(res => {
                if (res?.data?.errorMessage) {
                    setErrorNotification(true);
                    setErrorMessage(res.data.errorMessage)
                }
                shippingSelect();
            })
            .then(res =>
                setSpinner(false)
            )
            .catch(error => {
                setErrorMessage(error.message);
                setErrorNotification(true);
            })
    }

    const hideNotification = () => {
        setTimeout(() => {
            setErrorNotification(false)
            setNotification(false)
            setNotificationAddress(false);
            setShipNotification(false)
            setDiscountMsg('');
            setDiscountErrorMsg('')
        }, 5000);
    }

    const handleAddNewAddress = () => {
        let hasError = isValid();
        let _phoneNumber = ''
        if (typeof (hasError) === "string") {
            setErrorNotification(true)
            setErrorMessage(hasError)
            return;
        }
        if (phoneNumber) {
            _phoneNumber = phoneNumber.replace(/\D/g, '');
            if (_phoneNumber.length !== phoneNumber.length) {
                setErrorNotification(true)
                setErrorMessage('Invalid Mobile number')
                return;
            }
            if (_phoneNumber.length !== maxphonelength) {
                setErrorNotification(true)
                setErrorMessage('Invalid Mobile number')
                return;
            }
        }
        let payload = {
            "action": shippingId ? "UPDATE_SHIPPING_ADDRESS" : "ADD_SHIPPING_ADDRESS",
            "isDefaultAddress": true,
            "shippingAddress": {
                "city": city,
                "company": "",
                "country": country == 'UK'? 'GB' : country,
                "firstName": first,
                "id": shippingId,
                "isBusiness": true,
                "lastName": second,
                "phone": _phoneNumber,
                "postalCode": post,
                "region": "",
                "state": state,
                "streetName": streetname,
                "streetNumber": streetNumber
            }
        }
        setSpinner(true)
        if(customerChannel == 'D2C'){
        axios.post(`${SERVICES.ACCOUNT}api/v1/users/${customerId}?country=${customerCountry}`, payload, {headers: {country: customerCountry}})
            .then(resp => {
                setNotification(true)
                setNotificationMessage(shippingId ? 'Successfully updated' : 'Successfully Added New Address')
                setErrorNotification(false)
                setSpinner(false);
                CallApi(`${SERVICES.ACCOUNT}api/v1/users?customerId=${customerId}`, 'get')
                    .then(res => {
                        setShippingAddressList(res?.data?.shippingAddressList)
                        setAddressList(res.data?.addressList)
                    }).catch(error => {
                        setErrorMessage(error.message);
                        setErrorNotification(true);
                    })
            }).catch(err => {
                setNotification(false)
                setErrorNotification(true)
                setErrorMessage("Error!!Please check again")
                setSpinner(false)
            })
        } else {
            axios.post(`${SERVICES.ACCOUNT}/api/v1/user/${customerId}/shipping-options?country=${customerCountry}`, payload, {headers: {country: customerCountry}})
                .then(response => {
                    axios.get(`${SERVICES.CHECKOUT}api/v1/users/checkout/account/${customerCompany}?customerId=${customerId}&country=${customerCountry}`, '', {headers: {country: customerCountry}})
                        .then(res => {
                                setShippingAddressList(res?.data?.value?.addresses?.shippingAddresses)
                                setErrorNotification(true);
                                setErrorMessage(res.data.errorMessage)
                        })
                        .catch(error => {
                            setErrorMessage(error.message);
                            setErrorNotification(true);
                        })
                    setNotification(true)
                    setSpinner(false);
                    setNotificationMessage(shippingId ? 'Successfully updated' : 'Successfully Added New Address')
                })
                .catch(error => {
                    console.log(error)
                    setNotification(true)
                    setErrorMessage(error)
                }
                )
        }
        handleCloseEdit();
    }

    const isValid = () => {
        if (!first) {
            return "Please enter first name";
        } else if (!second) {
            return "Please enter second name";
        } else if (!streetname) {
            return "Please enter street name";
        } else if (!city) {
            return "Please enter city";
        } else if (countryCode == 'US' && !state) {
            return "Please enter state";
        } else if (!country) {
            return "Please enter country";
        } else if (!post) {
            return "Please enter postalCode";
        } else if (!phoneNumber) {
            return "Please enter phone";
        }
        return true;
    }

    const handleBulkUpdateOpen = (rowItem) => {
        setNotificationAddress(false);
        if (rowItem) {
            setShippingId(rowItem.id)
            setFirst(rowItem.firstName)
            setSecond(rowItem.lastName)
            setStreetName(rowItem.streetName)
            setCity(rowItem.city)
            setState(rowItem.state)
            setCountry(rowItem.country)
            setPost(rowItem.postalCode)
            setPhoneNumber(rowItem.phoneNumber)
            setStreetNumber(rowItem.streetNumber)
        }
        setEnableAddSalesRep(true);
        setDisableButton(false);
    }

    const handleCancel = () => {
        setEnableAddSalesRep(false);
    }

    const handleCloseEdit = () => {
        setEnableAddSalesRep(false);
    }
    const handleIconClick = () => {
        setOrderTagData('')
    }
    const handleWarrenty = () => {
        const payload = {
            "action": "WARRANTY",
            "orderTag": orderTagData,
            "previousOrderNumber": warrentyOrderNumber,
            "warrantyComment": comment
        }
        if(warrentyOrderNumber)
        {  
            CallApi(`${SERVICES.CART}/api/v1/carts/${cartDetails}/`, 'post', payload)
            .then(res => {
                setWarrantyNotification(true)
                setwarrantyErrorNotification(false)
                setwarrantyNotificationMessage("Order Tag is Updated")
                setTimeout(() => {
                    setWarrantyNotification(false)
                    
                }, 2000);
                
            })
            .catch(error => {
                setwarrantyErrorNotification(true)
                setWarrantyNotification(false)
                setwarrantyErrorMessage(error.message)
            })}
        else {
            setwarrantyErrorNotification(true)
            setWarrantyNotification(false)
            setwarrantyErrorMessage("Please enter the order number")
            setTimeout(() => {
                setwarrantyErrorNotification(false)
                
            }, 2000);
        }
    }

    const closeClyde = () => {
        setAddClyde(false)
        if (clydeProduct) {
            clydeProduct.checked = false
            setClydeProduct(clydeProduct)
        }
    }

    const addProtection = (clydePlan) => {
        setClydeProduct(clydePlan)
    }

    const addProductProtection = (productValue) => {
        let lineItemId
        productNewArr?.map((value) => {
            if (clydeProductItem.id == value.productId) {
                lineItemId = value.lineItemId
            }
        })
        let cartData = {
            quantity: clydeProductItem.quantity,
            action: "ADD_LINE_ITEM",
            variantId: 1,
            variantSKU: clydeProduct?.attributes?.sku,
            lineItemId: lineItemId
        }
        if(clydeProductItem && clydeProduct){
            CallApi(`${SERVICES.CART}/api/v1/carts/${cartDetails}`, 'post', cartData)
            .then(res => {
                if (res?.data?.custom) { setPriceDetails(res.data) }
                productArray.map((productItem) => {
                    if (clydeProductItem.id == productItem.id) {
                        res?.data?.lineItems?.map(clydeProductLineItem => {
                            let lineItemId = {
                                lineItemId: clydeProductLineItem.id,
                                productId: clydeProductLineItem.productId,
                                name: clydeProductLineItem.name["en-US"],
                                price: centsToDollars(clydeProductLineItem.price.value.centAmount)
                            }
                            productNewArr.push(lineItemId)
                            if (clydeProduct?.attributes?.sku == clydeProductLineItem.variant.sku) {
                                let protectionPlan = {
                                    name: clydeProductLineItem.name["en-US"],
                                    price: centsToDollars(clydeProductLineItem.price.value.centAmount),
                                    id: clydeProductLineItem.productId,
                                    quantity: clydeProductItem.quantity,
                                    isProtectionItem: true,
                                }
                                protectionPlan.disabled = true
                                productArray.push(protectionPlan)
                                productItem.protectionId = clydeProductLineItem.productId
                                clydeProductItem.clydeEnabled = false
                            }
                        })
                    }
                    setProductArray(productArray)
                })
                closeClyde()
            })
            .then(res =>
                setSpinner(false)
            )
            .catch(error => {
                setSpinner(false)
                setErrorMessage(error.message);
                setErrorNotification(true);
            })
        }else{
            setErrorMessage('Select any protection plan');
            setErrorNotification(true);
        }
    }

    const showrowId = (selectedCheckBox) => {
        clydeData.map(clydeCheck => {
            if (clydeCheck.id == selectedCheckBox) {
                clydeCheck.checked = true
                setClydeProduct(clydeCheck)
            } else {
                clydeCheck.checked = false
                setClydeProduct(clydeCheck)
            }
        })
    }

    const setClydeView = (productValue) => {
        setClydeProductItem(productValue)
        productArray.map(clydeContract => {
            if (productValue.id == clydeContract.id) {
                setClydeData(productValue.clydeContracts)
            }
        })
    }

    return (
        <div>
            <div>
                {orderID == 'new' ? <ConfirmationDialog
                    title="Select store, currency and inventory mode"
                    isOpen={addOrderDialog}
                    onClose={handleClose}
                    onCancel={handleClose}
                    onConfirm={handleConfirm}
                >
                    <Spacings.Stack scale="m">
                        <Text.Body>Store, currency or inventory mode cannot be changed at a later step. If an update is required during order creation, abandon this order and create a new one.</Text.Body>
                    </Spacings.Stack>
                    <br />
                    {errorMessage && <div className="notification"><ContentNotification type="warning"> {errorMessage} </ContentNotification></div>}
                    <label>Store</label>
                    <br />
                    <SelectInput
                        placeholder="No store selected"
                        name="form-field-name"
                        horizontalConstraint={7}
                        onChange={(e) => {
                            setCustomerStore(e.target.value)
                        }}
                        value={customerStore}
                        options={ countryCode == 'US' ?
                            // { value: 'MLK CA', label: 'MLK CA' },
                            [{ value: 'MLK_US', label: 'MLK US' }]:
                            [{ value: 'MLK_UK', label: 'MLK United Kingdom' },
                            { value: 'MLK_EU', label: 'MLK Europe' }]
                        }
                    />
                    <br />
                    <label>Currency</label>
                    <br />
                    <SelectInput
                        placeholder="Select..."
                        name="form-field-name"
                        horizontalConstraint={7}
                        onChange={(e) => {
                            setCurrency(e.target.value)
                        }}
                        value={currency}
                        options={ countryCode == 'US' ?
                            [{ value: 'USD', label: 'USD' }] :
                            [{ value: 'GBP', label: 'GBP' },
                            { value: 'EUR', label: 'EUR' }]
                            // { value: 'CAD', label: 'CAD' },
                        }
                    />
                    <br />
                    <label>Inventory tracking</label>
                    <br />
                    <Spacings.Inline scale="m">
                        <RadioInput.Group
                            id={'fruits'}
                            name={'fruits'}
                            value={trackingValue}
                            onChange={(e) => {
                                tracking(e)
                            }}
                        >
                            <RadioInput.Option
                                value={'None'}
                                onChange={(e) => setTrackingNone(e.target.value)}
                                isChecked={trackingNone}
                            >
                                <Spacings.Inline scale="xs" alignItems="center">
                                    <div>None: No inventory tracking.</div>
                                </Spacings.Inline>
                            </RadioInput.Option>
                            <RadioInput.Option
                                value={'Track Only'}
                                onChange={(e) => { console.log(e.target) }}
                                isChecked={trackingOnly}
                            >
                                <Spacings.Inline scale="xs" alignItems="center">
                                    <div>Track only: Order is tracked on inventory.</div>
                                </Spacings.Inline>
                            </RadioInput.Option>
                            <RadioInput.Option
                                value={'Reserve'}
                                onChange={(e) => setTrackingReserve(e.target.value)}
                                isChecked={trackingReserve}
                            >
                                <Spacings.Inline scale="xs" alignItems="center">
                                    <div>Reserve on order: Order is tracked on inventory with the chance to place the order if all line items are available.</div>
                                </Spacings.Inline>
                            </RadioInput.Option>
                        </RadioInput.Group>
                    </Spacings.Inline>
                </ConfirmationDialog> : null}
            </div>

            {!addOrder ? <div style={{ paddingTop: '10px', fontSize: 'large' }}>Create Order <div className="main-tab-list">
                <ul className="sub-tab-list">
                    <li><a
                        className={customerTab ? 'background_green' : 'background_black'}
                        onClick={() => handleCustomerTab()}
                    >Customer</a></li>
                    <li><a
                        className={productTab ? 'background_green' : 'background_black'}
                        onClick={() => handleProductTab()}
                    >Products</a></li>
                    <li><a
                        className={shippingTab ? 'background_green' : 'background_black'}
                        onClick={() => handleShippingTab()}
                    >Shipping </a></li>
                    <li><a
                        className={confirmationTab ? 'background_green' : 'background_black'}
                        onClick={() => handleConfirmationTab()}
                    >Review & Payment </a></li>
                </ul>
            </div></div> : null}

            {orderID !== 'new' && confirmationTab ? <div className="notification">
                {errorNotifiaction ?
                    <div>
                        <ContentNotification type="error"> {errorMessage} </ContentNotification>
                        {hideNotification()}
                    </div>
                    : null
                }
            </div> : null}

            {!confirmationTab && spinner ? <div className="spinnerOrder">
                <LoadingSpinner size="m"></LoadingSpinner></div> : null}

            {!addOrder ?
                <div>
                    <Grid
                        gridGap="16px"
                        gridAutoColumns="1fr"
                        gridTemplateColumns="repeat(2, 1fr)"
                    >
                        {customerTab ?
                            <div>
                                <FilterOption
                                    multiSearch={true}
                                    searchFieldOption={searchOption}
                                    searchByField={(value) => setSearchByField(value)}
                                    searchFilter={(searchValue) => setSearchValue(searchValue)}
                                    searchSelectOption={channelOption}
                                    searchSelectChange={(status) => handleOnChange(status)}
                                    searchSelectText="Select Type of Channel"
                                />
                                <div>
                                    <DataTableManager columns={columns}
                                        onSettingsChange={(action, nextValue) => { }} >
                                        <DataTable rows={employeeList}
                                            onRowClick={
                                                (item) => handleRowClick(item)}
                                        />
                                    </DataTableManager>
                                    <Pagination
                                        totalItems={totalResults}
                                        perPage={pageLimit}
                                        page={pageNumber}
                                        perPageRange="m"
                                        onPageChange={(page) => {
                                            // console.log(page)
                                            setPageNumber(page)
                                        }}
                                        onPerPageChange={(lmt) => {
                                            // console.log(lmt)
                                            setPageLimit(lmt);
                                        }}
                                    />
                                </div>
                            </div>
                            : null
                        }

                        {shippingTab ? <div>
                            <div className="notification">
                                {notificationAddress && shipAddressList.length > 0 ?
                                    <div>
                                        <ContentNotification type="info"> {notificationAddressMessage} </ContentNotification>
                                        {hideNotification()}
                                    </div>
                                    : null
                                }</div>
                            <CollapsiblePanel horizontalConstraint={12} header="Shipping Address">
                                {shipAddressList.length > 0 ? <DataTableManager columns={shippingColumns}
                                    onSettingsChange={(action, nextValue) => { }} >
                                    <DataTable rows={shipAddressList}
                                        maxWidth={'80%'}
                                        onRowClick={
                                            (item) => handleAddressClick(item)}
                                    />
                                </DataTableManager> : null}
                                <SecondaryButton style={{ marginTop: "10px", marginLeft: "10px", marginBottom: "10px" }} iconLeft={<PlusBoldIcon />}
                                    label="Add new address" onClick={handleBulkUpdateOpen} />
                            </CollapsiblePanel>
                            {errorNotifiaction && !enableAddSalesRep ?
                                <div>
                                    <ContentNotification type="error"> {errorMessage} </ContentNotification>
                                    {hideNotification()}
                                </div>
                                : null
                            }
                            <FormModalPage
                                title={shippingId ? "Update address" : "Add new address"}
                                isOpen={enableAddSalesRep}
                                onClose={handleCloseEdit}
                                topBarCurrentPathLabel={shippingId ? "Update address" : "Add new address"}
                                topBarPreviousPathLabel="Back"
                                onSecondaryButtonClick={handleCancel}
                                onPrimaryButtonClick={handleAddNewAddress}
                                labelPrimaryButton={shippingId ? "Update" : "Save"}
                                isPrimaryButtonDisabled={disableButton}
                            >

                                <div>

                                    <Grid
                                        gridGap="16px"
                                        gridAutoColumns="1fr"
                                        gridTemplateColumns="repeat(2, 1fr)"
                                    >
                                        <Grid.Item>
                                            <TextField title="First Name" isRequired
                                                value={first} onChange={(event) => setFirst(event.target.value)} />
                                        </Grid.Item>
                                        <Grid.Item>
                                            <TextField title="Second Name" isRequired
                                                value={second} onChange={(event) => setSecond(event.target.value)} />
                                        </Grid.Item>
                                        <Grid.Item>
                                            <TextField title="Street Address 1" isRequired
                                                value={streetname} onChange={(event) => setStreetName(event.target.value)} />
                                        </Grid.Item>
                                        <Grid.Item>
                                            <TextField title="Street Address 2"
                                                value={streetNumber} onChange={(event) => setStreetNumber(event.target.value)} />
                                        </Grid.Item>
                                        <Grid.Item>
                                            <TextField title="City" isRequired
                                                value={city} onChange={(event) => setCity(event.target.value)} />
                                        </Grid.Item>
                                        {countryCode == 'US' ? <Grid.Item>
                                            <label style={{ fontWeight: 'bold' }}>State  <Asterisk /> </label>
                                            <SelectInput
                                                name="state"
                                                value={state}
                                                onChange={(event) => setState(event.target.value)}
                                                options={stateList.map((items) => {
                                                    return { label: items.label, value: items.value }
                                                })}
                                            />
                                        </Grid.Item>: ''}
                                        <Grid.Item>
                                            <label style={{ fontWeight: 'bold' }}>Country <Asterisk /></label>
                                            <SelectInput
                                                value="US"
                                                value={country}
                                                name="country"
                                                onChange={(event) => setCountry(event.target.value)}
                                                options={ countryCode == 'US' ? 
                                                    [{ value: "US", label: "US" }]: [{ value: "UK", label: "UK"}
                                                ]}
                                            />
                                        </Grid.Item>
                                        <Grid.Item>
                                            <TextField title="Postal Code" isRequired
                                                value={post} onChange={(event) => setPost(event.target.value)} />
                                        </Grid.Item>
                                        <Grid.Item>
                                            <TextField title="Phone number" isRequired
                                                value={phoneNumber} onChange={(event) => setPhoneNumber(event.target.value)} />
                                        </Grid.Item>
                                    </Grid>
                                </div>
                                <div className="notification">
                                    {enableAddSalesRep && notification ?
                                        <div>
                                            <ContentNotification type="success"> {notificationMessage} </ContentNotification>
                                            {hideNotification()}
                                        </div>
                                        : null
                                    }
                                    {enableAddSalesRep && errorNotifiaction ?
                                        <div>
                                            <ContentNotification type="error"> {errorMessage} </ContentNotification>
                                            {hideNotification()}
                                        </div>
                                        : null
                                    }
                                </div>
                            </FormModalPage>
                            {!enableAddSalesRep && notification ? <div className="notification" style={{ width: '60%' }}>
                                <ContentNotification type="success">{notificationMessage}</ContentNotification>
                                {hideNotification()}</div> : null}

                            <div style={{ paddingTop: '10px' }}> <CollapsiblePanel horizontalConstraint={12}
                                header="Billing Address">
                                {billAddressList.length > 0 ? <DataTableManager columns={billingColumns}
                                    onSettingsChange={(action, nextValue) => { }} >
                                    <DataTable rows={billAddressList} maxWidth={'92%'} />
                                </DataTableManager> : <div>No Billing address</div>}</CollapsiblePanel> </div>
                            {shipNotification && shippingMethodDetails && !enableAddSalesRep ?
                                <div>
                                    <ContentNotification type="success"> {shippingNotification} </ContentNotification>
                                    {hideNotification()}
                                </div>
                                : null
                            }
                            <div style={{ paddingTop: '10px' }}>
                                <CollapsiblePanel
                                    isClosed={customerPanel}
                                    onToggle={() => setCustomerPanel(!customerPanel)}
                                    header="Shipping Methods"
                                >
                                    <div className="order-table">
                                        {methodsList.length > 0 ? <DataTableManager columns={shippingMethodscolumns}>
                                            <DataTable
                                                rows={methodsList}
                                                onRowClick={(item) => showrowData(item)}
                                            />
                                        </DataTableManager> : <div>No shipping method</div>}
                                    </div>
                                </CollapsiblePanel></div>
                        </div> : null}

                        {productTab ? <div>
                            {/* {spinner ? <div className="spinner">
                <LoadingSpinner size="s">Loading</LoadingSpinner></div> : null} */}
                            {errorNotifiaction ?
                                <div>
                                    <ContentNotification type="error"> {errorMessage} </ContentNotification>
                                    {hideNotification()}
                                </div>
                                : null
                            }
                            <div>
                                <DataTableManager columns={productColumns}
                                    onSettingsChange={(action, nextValue) => { }} >
                                    <DataTable rows={productList}
                                        maxHeight={'700px'}
                                    />
                                </DataTableManager>
                                <Pagination
                                    totalItems={totalProductResults}
                                    perPage={productPageLimit}
                                    page={productPageNumber}
                                    perPageRange="m"
                                    onPageChange={(page) => {
                                        // console.log(page)
                                        setProductPageNumber(page)
                                    }}
                                    onPerPageChange={(lmt) => {
                                        // console.log(lmt)
                                        setProductPageLimit(lmt);
                                    }}
                                />
                            </div>
                            {shippingData ? <div className="order-panel">
                                {notificationAddress ?
                                    <div>
                                        <ContentNotification type="success"> {notificationAddressMessage} </ContentNotification>
                                        {hideNotification()}
                                    </div>
                                    : null
                                }
                            </div> : null}
                        </div> : null}

                        {confirmationTab ? <div>
                            <ReviewOrder product={productArray} productLineItem={productLineItem} shippingMethodDetails={orderID !== 'new' ? shippingInfo : shippingMethodDetails}
                                productId={productId} quantity={productQuantity} cartID={cartDetails}
                                address={orderID !== 'new' ? orderData?.shippingAddress : shippingAdd} customerChannel={customerChannel}
                                customerId={customerId} loader={loader} customerCountry={customerCountry} />

                        </div> : null}
                        {confirmationTab && cartItems && spinner ? <div className="spinnerOrder">
                            <LoadingSpinner size="m"></LoadingSpinner></div> : null}
                        <div className="fonts" style={{ height: '1150px' }}>
                            <p style={{ fontSize: "20px", paddingTop: '5px' }}> Order summary </p>
                            <p style={{ paddingTop: '5px' }}>Store : <strong>{customerStore}</strong></p>
                            <p style={{ paddingTop: '5px' }}>Currency : <strong>{currency}</strong></p>
                            <p style={{ paddingTop: '5px' }}>Customer Email : <strong>{customerEmail}</strong></p>
                            <br />
                            {cartItems ? <div className="order-grid">
                               {countryCode == 'US'? <div style={{ paddingTop: '10px' }}>
                                    <CheckboxInput
                                        value="foo-radio-value"
                                        onChange={() => { placeGiftOrder(giftOrder), setGiftOrder(!giftOrder) }}
                                        isChecked={giftOrder}
                                        isDisabled={confirmationTab ? true : false}

                                    >This is a gift
                                    </CheckboxInput>
                                </div>: null}
                                {giftOrder ? <div>
                                    <ul className="address-list" style={{ width: '60%' }} >
                                        <li><b>Gift Recipient Email</b> <br />
                                            <TextInput title="Gift Recipient Email" isDisabled={confirmationTab ? true : false} isRequired value={giftEmail} onChange={(event) => setGiftEmail(event.target.value)} />
                                        </li><br />
                                        <li> <b>Gift Recipient Message (Optional)</b>
                                            <MultilineTextInput value={giftMessage}
                                                horizontalConstraint={6}
                                                isDisabled={confirmationTab ? true : false}
                                                onChange={(event) => setGiftMessage(event.target.value)} />
                                        </li>
                                    </ul>
                                </div> : null}
                                <div>
                                    <div><CartIcon /><b>{productArray.length} items in your cart</b></div>
                                </div>
                                <div style={{ marginTop: "10px" }}>
                                    <Grid
                                        gridGap="16px"
                                        gridAutoColumns="1fr"
                                        gridTemplateColumns="repeat(2, 1fr)"
                                    >
                                        <Grid.Item>
                                            <div className="order-detail-card">
                                                <Card>
                                                    <Grid
                                                        gridGap="16px"
                                                        gridAutoColumns="1fr"
                                                        gridTemplateColumns="repeat(3, 1fr)"
                                                    >
                                                        {
                                                            productArray?.map((productValue) => {
                                                                return (
                                                                    <Grid.Item>
                                                                        {/* {console.log(productValue, 'productValue')} */}
                                                                        <div className="customer">
                                                                            <div style={{ marginRight: '10px' }}>{productValue.name} {productValue.price}</div>
                                                                            {countryCode == 'US' && !giftOrder && productValue?.subscription ? <div><strong>Includes {productValue.subscription} mos. of filters</strong></div> : null}
                                                                            {/* <div className="order-customer"><b>SKU</b><br />{productValue.sku}</div> */}
                                                                            {countryCode == 'US' && productValue?.clydeEnabled ? <SecondaryButton label='Add Product Protection'
                                                                                onClick={() => {setAddClyde(true), setClydeView(productValue)}}>Add Product Protection</SecondaryButton> : ''}

                                                                            {!productValue.disabled? <NumberInput
                                                                                key={productValue.id}
                                                                                value={productValue.quantity}
                                                                                min="1"
                                                                                max="20"
                                                                                onChange={(e) => {
                                                                                    e.preventDefault();
                                                                                    addProduct(e.target.value, productValue)
                                                                                }}
                                                                            /> :
                                                                            <TextInput
                                                                                value={productValue.quantity}
                                                                                isDisabled={true}
                                                                            />}
                                                                            <div style={{ textAlign: 'center', marginTop: '10px' }}>
                                                                                <SecondaryButton
                                                                                    onClick={(e) => { 
                                                                                        e.preventDefault()
                                                                                        addProduct(null, productValue) }
                                                                                    }
                                                                                    iconLeft={<CheckInactiveIcon />}
                                                                                // label='Delete'
                                                                                /></div>
                                                                        </div>
                                                                        {addClyde ? <ConfirmationDialog
                                                                            title={"Protect your purifier"}
                                                                            isOpen={addClyde}
                                                                            onClose={closeClyde}
                                                                            onCancel={closeClyde}
                                                                            onConfirm={() => addProductProtection(productValue)}
                                                                            labelPrimary="Add protection plan"
                                                                            topBarPreviousPathLabel="Back"
                                                                        >
                                                                            {
                                                                                clydeData?.map((clydePlan) => {
                                                                                    return (
                                                                                        <Grid.Item>
                                                                                            <Card>
                                                                                                <CheckboxInput
                                                                                                    id={clydePlan.id}
                                                                                                    isChecked={clydePlan.checked}
                                                                                                    onChange={() => {
                                                                                                        showrowId(clydePlan.id), addProtection(clydePlan, productValue)
                                                                                                    }}
                                                                                                >
                                                                                                    <div className="customer">
                                                                                                        <div className="order-customer" style={{ marginRight: '10px' }}>{clydePlan.attributes.term} Year Protection plan</div>
                                                                                                        <div className="order-customer">{clydePlan.attributes.recommendedPrice}</div>
                                                                                                    </div></CheckboxInput></Card>
                                                                                        </Grid.Item>
                                                                                    )
                                                                                })
                                                                            }
                                                                        </ConfirmationDialog> : ''}
                                                                    </Grid.Item>
                                                                )
                                                            })
                                                        }
                                                    </Grid>
                                                </Card>
                                            </div>
                                        </Grid.Item>
                                    </Grid>
                                </div>
                                <div style={{ marginTop: "5px" }}>
                                    <Card>
                                        <div style={{ display: 'flex' }}>
                                            <div className="customer">
                                                <div><b>Subtotal</b></div>
                                                <div className="order-customer"><b> {countryCode == 'US' ? 'Tax' : 'VAT'}</b> </div>
                                                <div className="order-customer"><b>Shipping</b> </div>
                                                <div className="order-customer"><b>Handling cost</b> </div>
                                                <div className="order-customer"><b>Discounts</b></div>
                                                <div className="order-customer"><b>Order total</b> </div>
                                            </div>
                                            <div className="customer">
                                                <div> {centsToDollars(priceDetails?.custom?.fields?.subTotal)} </div>
                                                <div className="order-customer">{centsToDollars(priceDetails?.custom?.fields?.totalTax)}</div>
                                                <div className="order-customer">{centsToDollars(priceDetails?.custom?.fields?.shippingCost)}</div>
                                                <div className="order-customer">{centsToDollars(priceDetails?.custom?.fields?.handlingCost)}</div>
                                                <div className="order-customer">{centsToDollars(priceDetails?.custom?.fields?.totalDiscountPrice)}</div>
                                                <div className="order-customer">{centsToDollars(priceDetails?.custom?.fields?.orderTotal)}</div>
                                            </div>
                                        </div>
                                    </Card>
                                </div>
                                <div style={{ marginTop: "10px" }}>
                                    <Card>
                                        Add discount code
                                        <TextInput
                                            title="discount code"
                                            value={discountCode}
                                            onChange={event => setDiscountCode(event.target.value)}
                                        />
                                        <SecondaryButton
                                            style={{ margin: "10px 10px 5px 0px" }}
                                            onClick={() => { applyDiscount() }}
                                            iconLeft={<PlusBoldIcon />}
                                            label="Apply"
                                        />
                                        <SecondaryButton
                                            onClick={() => { removeDiscount() }}
                                            iconLeft={<CheckInactiveIcon />}
                                            label='Remove'
                                        />
                                    </Card>
                                    {discountCode && errorNotifiaction ?
                                        <div>
                                            <ContentNotification type="error"> {errorMessage} </ContentNotification>
                                            {hideNotification()}
                                        </div>
                                        : null
                                    }
                                    {discountMsg ? <div><ContentNotification type="info"> {discountMsg} </ContentNotification>{hideNotification()}</div> : null}
                                    {discountErrorMsg ? <div><ContentNotification type="error"> {discountErrorMsg} </ContentNotification>{hideNotification()}</div> : null}
                                </div>
                                <div>
                                    <MultilineTextField
                                        title="Comments"
                                        value={comment}
                                        onChange={(event) => setComment(event.target.value)}
                                    />
                                </div>
                                <div style={{ marginTop: "10px", display: "flex" }}>
                                    <SelectField
                                        title="Order Tag"
                                        value={orderTagData}
                                        options={orderTags.map(data => {
                                            return { value: data.label, label: data.label }
                                        })}
                                        onChange={(event) => setOrderTagData(event.target.value)}
                                    />
                                    <div style={{ marginTop: "28px", marginLeft: "10px" }}>
                                        {orderTagData ? <CloseIcon onClick={handleIconClick} /> : null}
                                    </div>
                                </div>

                                {orderTagData ?
                                <div style={{display:"flex" , marginTop:"10px"}}>
                                <div style={{marginRight:"10px", width:"300px"}}>
                               <TextField title="Original Order Number" 
                                onChange={event => setWarrentyOrderNumber(event.target.value)} />
                               </div>
                               <div style={{marginTop:"20px"}}>
                            <SecondaryButton label="Update" onClick={handleWarrenty}/></div>
                            </div>:null}
                            {warrentyOrderNumber ? <div style={{marginTop:"10px" , width :"60%"}}>
                            {warrantyNotification ? 
                            <ContentNotification type="success"> {warrantynotificationMessage} </ContentNotification> : null}
                            {warrantyerrorNotifiaction ?
                            <ContentNotification type="error"> {warrantyerrorMessage} </ContentNotification> :null}
                            </div>: null}
                            </div> : null}
                        </div>
                    </Grid>
                </div> : null}
        </div>
    );
}

export default AddOrder;