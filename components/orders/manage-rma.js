import React from "react";
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';

const ManageRMA = () =>{
    const columns = [
        { key: 'rmaNumber', label: 'RMA Number'},
        { key: 'orderNumber', label: 'Order Number' },
        { key: 'customerEmail', label: 'Customer Email' },
        { key: 'rmaStatus', label: 'RMA Status' },
        { key: 'initiatedDate', label: 'Initiated Date' },
        { key: 'updatedDate', label: 'Updated Date' },
    ];
    const rmaList =[]
    return(
        <div>
            <DataTableManager columns={columns}>
                <DataTable
                    rows={rmaList}
                />
            </DataTableManager>
        </div>
    );

}
export default ManageRMA;