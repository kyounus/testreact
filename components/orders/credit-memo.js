import React, { useState } from "react";
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import { SERVICES, centsToDollars } from "../../config/api";
import Grid from '@commercetools-uikit/grid';
import MultilineTextField from '@commercetools-uikit/multiline-text-field';
import PrimaryButton from '@commercetools-uikit/primary-button';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import Text from '@commercetools-uikit/text';
import TextInput from '@commercetools-uikit/text-input';
import { CallApi } from "../../plugin/Axios";
import CheckboxInput from '@commercetools-uikit/checkbox-input';
import { useRowSelection, useSorting } from '@commercetools-uikit/hooks';
import { ContentNotification } from '@commercetools-uikit/notifications';
import { CloseBoldIcon ,CloseIcon} from '@commercetools-uikit/icons';
import LoadingSpinner from '@commercetools-uikit/loading-spinner';

const CreditMemo = (props) => {
    const [arrIds, setArrIds] = useState([]);
    const [array, setArrayLength] = useState(0);
    const [rowId, setRowId] = useState('')
    const [selectedRows ,setSelectedRows] =useState([])
    const [selectedRefundRows , setSelectedRefundRows]=useState([])
    const items = [];
      const {items: rows} = useSorting(items);
      const {
            rows: rowsWithSelection,
            toggleRow,
            selectAllRows,
            deselectAllRows,
            getIsRowSelected,
            getNumberOfSelectedRows,
          } = useRowSelection('checkbox', rows);
          
          const countSelectedRows = getNumberOfSelectedRows();
          const isSelectColumnHeaderIndeterminate =
          countSelectedRows > 0 && countSelectedRows < rowsWithSelection.length;
          const handleSelectColumnHeaderChange =
          countSelectedRows === 0 ? selectAllRows : deselectAllRows;
    
    const showrowId = (item) => {
        console.log(item,arrIds,"row")
        const creditIndexInArray = arrIds.indexOf(item);
                if(creditIndexInArray > -1 ) {
                arrIds.splice(creditIndexInArray, 1);
                } else {
                arrIds.push(item);
                }
                if(arrIds.length === 0)
                {
                  setArrayLength(0)
                }
                (arrIds.length)
                console.log(arrIds,"arrId")
      }
    const orderData = props.orderData
    const invoiceNumber = props.invoiceNumber
    const [comment, setComment] = useState('')
    const [refundQty, setRefundQty] = useState('')
    const [refundShipping , setRefundShipping] =useState('')
    const [adjustmentFee , setAdjustmentFee] = useState('')
    const [adjustmentRefund , setAdjustmentRefund] = useState('')
    const [successNotification , setSuccessNotification] = useState(false)
    const [errorNotifiaction , setErrorNotification] = useState(false)
    const [message , setMessage] = useState('')
    const [spinner ,setSpinner] = useState(false)
    const [tableData,setTableData] = useState(true)
    let [subTotal,setSubTotal] = useState(0)
    let [taxAmount,setTaxAmount] = useState(0)
    let [rowTotal ,setRowTotal] =useState(0)
    let total =0
    const handleSelectedRow = (rowId , qty) =>{
        if(qty.length > 0){
            setSelectedRows([...selectedRows ,{"lineItemId" : rowId ,"quantityToRefund" :qty}])
        }   
    }

    const columns = [
        { key: 'checkbox',
        shouldIgnoreRowClick: true,
        align: 'center',
        renderItem: (row) => (
            row.refundedItemId.includes(row.id) ? 
            <div className="return-row-data"></div>
            :
          <CheckboxInput
            isChecked={getIsRowSelected(row.id)}
            onChange={() => {
              toggleRow(row.id)
              setRowId(row.id)
              handleSelectedRefundRows(row.id,row)
              showrowId(row.id)}
            }
          />
          
        ),disableResizing: true,},
        { key: 'product', label: 'Product' },
        { key: 'price', label: 'Price' },
        { key: 'qty', label: 'Qty' },
        { key: 'qtyToRefund', label: 'Qty to Refund',
        renderItem:(row) =>(
            row.refundedItemId.includes(row.id) ? 
            <div className="return-row-data">{refundQuantity[row.id]}</div>
            :
            <TextInput value={refundQty[row.id]}  onChange={(e) => { setRefundQty(e.target.value),handleSelectedRow(row.id,e.target.value) }} />
         ) },
        { key: 'subTotal', label: 'Subtotal' },
        { key: 'taxAmount', label: 'Tax Amount' },
        { key: 'rowTotal', label: 'Row Total' },
    ]
    let memoList = []
    let returnInfo = props.orderData.returnInfo
    let refundedItemId=[]
    let refundQuantity ={}
    returnInfo.map(data => {
        data.items.map( data => {
        if(data.paymentState === "Refunded"){
            refundedItemId.push(data.lineItemId)
            refundQuantity[data.lineItemId] =data.quantity
        }})
    })

    tableData ? 
    props.memo.map((data,index) => {
        let amount = ((data.taxedPrice.totalGross.centAmount) - (data.taxedPrice.totalNet.centAmount))
        total +=(data.price.value.centAmount) * (data.quantity) + amount
        const memoDetails = {
            id:data.id,
            product: data.name["en-US"],
            price: centsToDollars(data.variant?.prices[0].value.centAmount),
            qty: data.quantity,
            subTotal: centsToDollars(data.totalPrice.centAmount),
            taxAmount: centsToDollars((data.taxedPrice.totalGross.centAmount) - (data.taxedPrice.totalNet.centAmount)),
            rowTotal: centsToDollars((data.price.value.centAmount) * (data.quantity) + amount),
            refundSubTotal:data.totalPrice.centAmount,
            refundTaxAmount:(data.taxedPrice.totalGross.centAmount) - (data.taxedPrice.totalNet.centAmount),
            refundRowTotal:(data.price.value.centAmount) * (data.quantity) + amount,
            refundedItemId : refundedItemId,
            refundQuantity: refundQuantity
        }
        memoList = [...memoList, memoDetails]
    })
    :null

    let [grandTotal ,setGrandTotal] = useState(total)
    const handleSelectedRefundRows =(rowId,row) => {
        setAdjustmentRefund('')
        setRefundShipping('')
        setAdjustmentFee('')
        if(selectedRefundRows.includes(rowId)){
            setSubTotal(subTotal-=row.refundSubTotal)
            setTaxAmount(taxAmount-=row.refundTaxAmount)
            setRowTotal(rowTotal-=row.refundRowTotal)
            let selected = selectedRefundRows.filter(id => id !== rowId)
            setSelectedRefundRows(selected)  
        }
        else{
            setSubTotal(subTotal+=row.refundSubTotal)
            setTaxAmount(taxAmount+=row.refundTaxAmount)
            setRowTotal(rowTotal+=row.refundRowTotal)
            setSelectedRefundRows([...selectedRefundRows ,rowId])
        }
            setGrandTotal(rowTotal)
            if(rowTotal === 0){
                setGrandTotal(total)
            }
    }
    const refundAction =() => { 
        setSpinner(true)
        setTableData(false)
        if(selectedRefundRows.length === 0){
            setSuccessNotification(false)
            setErrorNotification(true)
            setMessage("Select the Product")
            setSpinner(false)
            setTableData(true)
        return
    }
      if(selectedRefundRows.length >0){
      let tempSelctedArray =[]
        selectedRefundRows.map(data => {
          let tempArray = selectedRows.filter(obj => obj.lineItemId === data)
          if (tempArray.length !== 0){
             tempSelctedArray = tempSelctedArray.concat(selectedRows.filter(obj => obj.lineItemId === data))
          }   
        })
        if(tempSelctedArray.length === 0){
            setSuccessNotification(false)
            setErrorNotification(true)
            setMessage("Enter the Refund Qunatity")
            setSpinner(false)
            setTableData(true)
            return
        }
        const payload={
            "adjustmentFee": adjustmentFee,
            "adjustmentRefund": adjustmentRefund,
            "comments": comment,
            "invoiceNumber": invoiceNumber,
            "lineItemsList" : tempSelctedArray,
            "refundShipping": refundShipping
        }
        console.log(payload,"payload")
        const url = `${SERVICES.ORDER}/api/v1/users/orders/${orderData.id}/refund/`
        CallApi(url,'post',payload).then(res => 
            {
            setSuccessNotification(true)
            setErrorNotification(false)
            setMessage("Refund has been created")
            handleReload()
            console.log(res)})
        .catch(err => 
            {console.log(err)
                setSpinner(false)
                setTableData(true)
                setSuccessNotification(false)
                setErrorNotification(true)
                setMessage(err.message)}
            )    
    }
    }
    const updateTotal = () =>{
        let addedTotal = Number(refundShipping*100)+Number(adjustmentRefund*100)+Number(adjustmentFee*100)
        if(selectedRefundRows.length === 0){
            setGrandTotal(total+addedTotal)
        }
        else{
            setGrandTotal(rowTotal+addedTotal)
        }
    }
    const hanldeNotification =()=>{
        setErrorNotification(false)
        setSuccessNotification(false)
    }
    const handleReload =()=>{
        props.handleReload(true)
        setTimeout(() => {
            setTableData(true)
            setSpinner(false)    
        }, 3000);
    }
    return (
        <div>
            <Text.Headline as="h4"><b>{'Items to Refund'}</b></Text.Headline>
            <hr></hr>
            <DataTableManager columns={columns}>
                <DataTable
                    rows={memoList}
                />
            </DataTableManager>
            {spinner ? <div className="spinner"><LoadingSpinner size="s">Loading</LoadingSpinner></div> : null}
            <div className="invoice_section">
                <Text.Headline as="h4"><b>{'Order Total'}</b></Text.Headline>
                <hr></hr>
                <Grid
                    gridGap="16px"
                    gridAutoColumns="1fr"
                    gridTemplateColumns="repeat(2, 1fr)"
                >
                    <Grid.Item>
                        <div className="customer">
                            <div className="section_heading"><b>Credit Memo Comments</b></div>
                            <div className="invoice_textbox">
                                <MultilineTextField
                                    title="Comment Text"
                                    value={comment}
                                    onChange={(event) => setComment(event.target.value)}
                                />
                            </div>
                        </div>
                    </Grid.Item>
                    <Grid.Item>
                        <div className="customer">
                            <div className="section_heading"><b>Refund Totals </b></div>
                            <Grid
                                gridGap="16px"
                                gridAutoColumns="1fr"
                                gridTemplateColumns="repeat(2, 1fr)"
                            >
                                <Grid.Item>
                                    <div>Subtotal </div>
                                    <div className="refund-totals">Refund Shipping </div>
                                    <div className="refund-totals">Adjustment Refund </div>
                                    <div className="refund-totals">Adjustment Fee </div>
                                    <div className="order-customer">Tax </div>
                                    <div style={{ fontWeight: '700' }} className="order-customer">Grand total</div>
                                </Grid.Item>
                                <Grid.Item>
                                    <div>{subTotal === 0 ? centsToDollars(orderData.custom.fields.subTotal) : centsToDollars(subTotal)} </div>
                                    <div className="refund-total"><TextInput value={refundShipping} onChange={(event) => setRefundShipping(event.target.value)} /></div>
                                    <div className="refund-total"><TextInput value={adjustmentRefund} onChange={(event) => setAdjustmentRefund(event.target.value)} /></div>
                                    <div className="refund-total"><TextInput value={adjustmentFee} onChange={(event) => setAdjustmentFee(event.target.value)} /></div>
                                    <div className="refund-total">{taxAmount === 0 ? centsToDollars(orderData.custom.fields.totalTax) : centsToDollars(taxAmount) }</div>
                                    <div style={{ fontWeight: '700' }}>{centsToDollars(grandTotal)}</div>
                                    <div className="refund-total-update"><SecondaryButton label="Update Totals" onClick={updateTotal}></SecondaryButton></div>
                                </Grid.Item>
                            </Grid>
                            <div className="refund-button">
                                <PrimaryButton label="Refund" isDisabled ={false} onClick={refundAction}/>
                            </div>
                        </div>
                    </Grid.Item>
                    <Grid.Item></Grid.Item>
                    <Grid.Item>
                    <div className="refund-notification">
                        <div className="refund-message">
                            {successNotification ? <ContentNotification type="success">{message}</ContentNotification> : null}
                            {errorNotifiaction ? <ContentNotification type="error">{message}</ContentNotification>: null}
                        </div>
                        <div className="refund-icon">                         
                            {successNotification  || errorNotifiaction ? <CloseBoldIcon onClick={hanldeNotification}/>: null}
                        </div>
                    </div>
                    </Grid.Item>
                </Grid>
            </div>
        </div>
    );
}
export default CreditMemo;