import React, { useEffect, useState } from 'react';
import { useRowSelection, useSorting } from '@commercetools-uikit/hooks';
import { useRouteMatch } from 'react-router-dom';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import { Pagination } from '@commercetools-uikit/pagination';
import LoadingSpinner from '@commercetools-uikit/loading-spinner';
import {SERVICES, centsToDollars} from '../../config/api';
import Spacings from '@commercetools-uikit/spacings';
import { useIntl } from 'react-intl';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import { FilterIcon,SearchIcon, CloseBoldIcon, CopyIcon} from '@commercetools-uikit/icons';
import TextInput from '@commercetools-uikit/text-input';
import SelectInput from '@commercetools-uikit/select-input';
import { addHyperLink } from '../../utils';
import {Header} from "../../common/Header";
import IconButton from '@commercetools-uikit/icon-button';
import FlatButton from '@commercetools-uikit/flat-button';
import "./order-list.css";
import { CallApi } from '../../plugin/Axios';
import { ProjectKey } from '../../config/api';


const UPDATE_ACTIONS = {
  COLUMNS_UPDATE: 'columnsUpdate',
  IS_TABLE_WRAPPING_TEXT_UPDATE:'isTableWrappingTextUpdate', 
  IS_TABLE_CONDENSED_UPDATE: 'isTableCondensedUpdate'
}

const OrderList=(props) =>{
    const[orderList , setOrderList] = useState([]);
    const { formatTime, formatDate } = useIntl();
    const[email ,setEmail] = useState('');
    const[giftEmail ,setGiftEmail] = useState('');
    const[orderNumber,setOrderNumber] = useState('')
    const [pageLimit, setPageLimit] = useState(20);
    const [pageNumber, setPageNumber] = useState(1);
    const [totalResults, setTotalResults] = useState(0);
    const [spinner, setSpinner] = useState(true);
    const [emailSearch, setEmailSearch] = useState('');
    const [giftEmailSearch, setGiftEmailSearch] = useState('');
    const [orderNumberSearch, setOrderNumberSearch] = useState('');
    const [searchByField, setSearchByField] = useState('');
    const [isClicked, setIsClicked] = useState(false);
    const [customerOriginB2B, setCustomerOriginB2B] = useState(true);
    const [customerOriginD2C, setCustomerOriginD2C] = useState(false);
    const [showOrderSearch, setShowOrderSearch] = useState(true)
    const [showEmailSearch, setShowEmailSearch] = useState(false)
    const [showGiftEmailSearch, setShowGiftEmailSearch] = useState(false)
    const [filterCustomer, setFilterCustomer] = useState('');
    const [filterData, setFilterData] = useState();
    const [filterGift, setFilterGift] = useState('');
    const [isCondensed, setIsCondensed] = useState(false);
    const [isWrappingText, setIsWrappingText] = useState(false);
    const [sortAction, setSortAction] = useState('DATE_CREATED');
    const [sortOrder, setSortOrder] = useState('DESC')
    const [sortOption, setSortOption] = useState(false)

    let countryCode = 'US';
    if (ProjectKey && (ProjectKey === 'mo-dev-eu-1' || ProjectKey === 'mo-preprod1-eu' || ProjectKey === 'mo-prod-eu')) {
      countryCode = 'EU';
    }
  
    const match = useRouteMatch();
    
 useEffect(() => {
    let offset= (pageNumber - 1) * pageLimit;
    if(totalResults && totalResults < offset){
        setPageNumber(0);
        return 
    }
    setSpinner(true);
    let query = '';
    if (email) {
        query += `&email=${encodeURIComponent(email)}`; 
    }
    if (giftEmail) {
      query += `&giftReceiptMail=${encodeURIComponent(giftEmail)}`; 
  }
    if (orderNumber) {
        query += `&orderNumber=${encodeURIComponent(orderNumber)}`; 
    }
    if (sortAction) {
      query += `&sortAction=${encodeURIComponent(sortAction)}`; 
    }
    if (sortOrder) {
      query += `&sortOrder=${encodeURIComponent(sortOrder)}`; 
    }
    if (countryCode == 'EU') {
      setTableData({ columns: columnsUK, visibleColumnKeys: columnsUK.map(({ key }) => key) })
    }
   let url = `${SERVICES.ORDER}api/v1/users/orders?&isGift=${filterGift}&channel=${filterCustomer}&limit=${pageLimit}&offset=${offset}&${query}`
    CallApi(url, 'get')
    .then((res) => {
      //  console.log(res , "response")
        setOrderList(res.data.results)
        setTotalResults(res.data.total);
        
    })
    .then(res =>
        setSpinner(false)
        )
        .catch(error => {
            console.log("error", error)
            setSpinner(false)
          })
 },[pageLimit, pageNumber,email,orderNumber, filterCustomer, filterGift, sortAction, sortOrder, giftEmail, match])

 const orders = []
 const getOrderStatusReason = (order) => {
    let retVal = [];
    let { paymentMethod = '', orderStatus = '', holds = [], paymentStatus} = order;
    if(paymentMethod === 'ACH') {

    }
    // Do ACH Check
    if(paymentMethod === 'ACH' && orderStatus !== 'Complete' && (paymentStatus !== 'Paid' && paymentStatus !== 'Success')) {
        retVal = [(<div>Pending ACH payment</div>)]
    }

    holds?.forEach(eachHold =>{
        switch(eachHold){
            case 'FreightHold' :
                retVal = [...retVal, (<div>Awaiting freight information</div>)]
            break;
            case 'CreditHold' : 
                retVal = [...retVal, (<div>Pending net terms review</div>)]
            break;
            case 'TaxHold' : 
                retVal = [...retVal, (<div>Pending tax exemption review</div>)]
            break;
        }
    })
     return retVal;
  }
  orderList && orderList.map((item)=>{
   let giftStatus
   if (item.isGiftOrder == 'true' && item.channel == 'D2C') {
     giftStatus = 'Yes'
   } if (item.isGiftOrder == null && item.channel == 'D2C') {
     giftStatus = 'No'
   }
   if (item.isGiftOrder == null && item.channel !== 'D2C') {
     giftStatus = '--'
   }
    const createdOn = formatDate(item.orderDate) + " " + formatTime(item.orderDate)
    const modifiedOn = formatDate(item.lastModifiedAt) + " " + formatTime(item.lastModifiedAt)
    orders.push({orderId:item.orderId, 
    ORDER_NUMBER:item.orderNumber, 
    orderTotal:centsToDollars(item.orderTotal),   
    giftOrder: giftStatus,
    giftRecipientEmail: item.recipientsEmail,
    giftRecipientAddress: (item.shippingAddress?.firstName+" "+item.shippingAddress?.lastName+" "+item.shippingAddress?.streetName+" "+item.shippingAddress?.city+" "+item.shippingAddress?.state+" "+item.shippingAddress?.postalCode+" "+item.shippingAddress?.phone),
    orderDate:item.orderDate, 
    ORDER_STATUS: item.orderStatus,
   // orderState:item.orderState,
    noOfItems:item.lineOfItems,
    orderWorkFlowStatus: item.orderState,
    paymentStatus:item.paymentStatus,
    paymentMethod: item.paymentMethod || '',
    shipmentStatus: item.shipmentStatus || '',
    EMAIL:item.email ,
    DATE_CREATED:createdOn,
    DATE_MODIFIED:modifiedOn,
    orderStatusReasonCode: getOrderStatusReason(item),
    channel: item.channel,
    copyOrder:  <div><IconButton
    onClick={(event) => copyOrder(event, item.orderId)}
    icon={<CopyIcon />}
  /></div>
    })
  }) 
    const columnsB2B = [
       // { key: 'orderId', label: 'OrderId' },
        { key: 'ORDER_NUMBER', label: 'OrderNumber', isSortable: true  },
        { key: 'orderTotal', label: 'OrderTotal' },
        { key: 'noOfItems', label: 'No of items' },
        { key: 'channel', label: 'Channel'},
        { key: 'giftOrder', label: 'Gift order'},
        { key: 'orderWorkFlowStatus', label: 'Order work flow status' },
        { key: 'ORDER_STATUS', label: 'Order status', isSortable: true  },
        //{ key: 'orderState', label: 'Order state' },
        { key: 'orderStatusReasonCode', label: 'Order status Reason Code' },
        { key: 'paymentStatus', label: 'Payment status' },
        { key: 'paymentMethod', label: 'Payment method'},
        { key: 'shipmentStatus', label: 'Shipment Status'  },
        { key: 'EMAIL', label: 'Email(order)', isSortable: true  },
        { key: 'DATE_CREATED', label: 'Date created', isSortable: true },
        { key: 'DATE_MODIFIED', label: 'Date modified', isSortable: true },
        { key: 'copyOrder', label: 'Copy order'}
    ];


    const columnsD2C = [
      // { key: 'orderId', label: 'OrderId' },
       { key: 'ORDER_NUMBER', label: 'OrderNumber', isSortable: true },
       { key: 'orderTotal', label: 'OrderTotal' },
       { key: 'noOfItems', label: 'No of items' },
       { key: 'channel', label: 'Channel'},
       { key: 'giftOrder', label: 'Gift order'},
       { key: 'giftRecipientEmail', label: 'Gift recipient email'},
       { key: 'giftRecipientAddress', label: 'Gift recipient address'},
       { key: 'orderWorkFlowStatus', label: 'Order work flow status' },
       { key: 'ORDER_STATUS', label: 'Order status', isSortable: true },
       //{ key: 'orderState', label: 'Order state' },
       { key: 'orderStatusReasonCode', label: 'Order status Reason Code' },
       { key: 'paymentStatus', label: 'Payment status' },
       { key: 'paymentMethod', label: 'Payment method'},
       { key: 'shipmentStatus', label: 'Shipment Status'  },
       { key: 'EMAIL', label: 'Email(order)', isSortable: true  },
       { key: 'DATE_CREATED', label: 'Date created', isSortable: true },
       { key: 'DATE_MODIFIED', label: 'Date modified', isSortable: true },
       { key: 'copyOrder', label: 'Copy order'}
   ];

  const columnsUK = [
    { key: 'ORDER_NUMBER', label: 'OrderNumber', isSortable: true },
    { key: 'orderTotal', label: 'OrderTotal' },
    { key: 'noOfItems', label: 'No of items' },
    { key: 'channel', label: 'Channel' },
    { key: 'orderWorkFlowStatus', label: 'Order work flow status' },
    { key: 'ORDER_STATUS', label: 'Order status', isSortable: true },
    { key: 'orderStatusReasonCode', label: 'Order status Reason Code' },
    { key: 'paymentStatus', label: 'Payment status' },
    { key: 'paymentMethod', label: 'Payment method'},
    { key: 'shipmentStatus', label: 'Shipment Status' },
    { key: 'EMAIL', label: 'Email(order)', isSortable: true },
    { key: 'DATE_CREATED', label: 'Date created', isSortable: true },
    { key: 'DATE_MODIFIED', label: 'Date modified', isSortable: true },
    { key: 'copyOrder', label: 'Copy order' }
  ];

   const copyOrder = (event, orderId) => {
    event.stopPropagation();
     console.log(orderId, 'orderId')
    props.history.push("orders/new/"+ orderId);
    return false;
   }

   const searchEmails = () => {
    setEmail(emailSearch)
    setFilterCustomer('')
    setSortAction('')
    setSortOrder('')
 }

 const searchGiftEmails = () => {
  setGiftEmail(giftEmailSearch)
  setFilterCustomer('')
  setSortAction('')
  setSortOrder('')
 }
 const searchOrderNumber = () => {
     setOrderNumber(orderNumberSearch)
     setSortAction('')
     setSortOrder('')
 }
  
    const controlView = (controlValue) => {
        if(controlValue === "email" ){
         
         
          setOrderNumber('')
          setShowEmailSearch(true)
          setShowOrderSearch(false)
          setShowGiftEmailSearch(false)
          setGiftEmail('')
          setEmailSearch('')
          setOrderNumberSearch('')
          setGiftEmailSearch('')
          setEmail('')
          setOrderNumber('');
          setSearchByField('');
                         
        }
        if (controlValue === "orderNumber")
        {
         
          setEmail('')
          setOrderNumber('')
          setGiftEmail('')
          setEmailSearch('')
          setOrderNumberSearch('')
          setShowEmailSearch(false)
          setShowOrderSearch(true)
          setShowGiftEmailSearch(false)
          setGiftEmailSearch('')
          setSearchByField('');
        }
        if(controlValue === "giftRecipientEmail" ){
         
          setEmail('')
          setOrderNumber('')
          setShowEmailSearch(false)
          setSearchByField('');
          setShowOrderSearch(false)
          setShowGiftEmailSearch(true)
          setGiftEmailSearch('')
        }
       
  }

  const handleEnter = (event) => {
    event.preventDefault()
    searchOrderNumber();
    return false;
}

const handleEnterEmail = (event) => {
  event.preventDefault()
  searchEmails();
  setFilterCustomer('');
  return false;
}

const handleEnterGiftEmail = (event) => {
  event.preventDefault()
  searchGiftEmails();
  setFilterCustomer('');
  return false;
}

    const handleRowClick = (item) => {
      props.history.push("orders/"+item.orderId);
      props.history.push(item)
    }

  const filterChange = (e) => {
    let filterValue = e.target.value
    setFilterCustomer(filterValue)
    if (filterValue == "D2C") {
      setCustomerOriginB2B(false);
      setCustomerOriginD2C(true);
      if (countryCode = 'US') {
        setTableData({ columns: columnsD2C, visibleColumnKeys: columnsD2C.map(({ key }) => key) });
      } else {
        setTableData({ columns: columnsUK, visibleColumnKeys: columnsUK.map(({ key }) => key) })
      }
    } else {
      setCustomerOriginB2B(true);
      setCustomerOriginD2C(false);
      if (countryCode = 'US') {
        setTableData({ columns: columnsB2B, visibleColumnKeys: columnsB2B.map(({ key }) => key) })
      } else {
        setTableData({ columns: columnsUK, visibleColumnKeys: columnsUK.map(({ key }) => key) })
      }
    }
  }

  const filterChangeData = (e) => {
    let filterValue = e.target.value
    if(filterValue == 'gift'){
      setFilterCustomer('')
      setFilterGift('true')
    }
    setFilterData(e.target.value)
  }

  const [tableData, setTableData] = useState({
    columns: columnsB2B,
    visibleColumnKeys: columnsB2B.map(({ key }) => key),
  });

  const showDisplaySettingsConfirmationButtons = false;
  const showColumnManagerConfirmationButtons = false;
  const withRowSelection = true;

  const {
    items: rows,
  } = useSorting(orders);

  const {
    rows: rowsWithSelection,
    toggleRow,
    selectAllRows,
    deselectAllRows,
    getIsRowSelected,
    getNumberOfSelectedRows,
  } = useRowSelection('checkbox', rows);

  const countSelectedRows = getNumberOfSelectedRows();
  const isSelectColumnHeaderIndeterminate =
    countSelectedRows > 0 && countSelectedRows < rowsWithSelection.length;
  const handleSelectColumnHeaderChange =
    countSelectedRows === 0 ? selectAllRows : deselectAllRows;

  const mappedColumns = tableData.columns.reduce(
    (columns, column) => ({
      ...columns,
      [column.key]: column,
    }),
    {}
  );
  const visibleColumns = tableData.visibleColumnKeys.map(
    (columnKey) => mappedColumns[columnKey]
  );

  const tableSettingsChangeHandler = {
    [UPDATE_ACTIONS.COLUMNS_UPDATE]: (visibleColumnKeys) =>
      setTableData({
        ...tableData,
        visibleColumnKeys,
      }),
    [UPDATE_ACTIONS.IS_TABLE_CONDENSED_UPDATE]: setIsCondensed,
    [UPDATE_ACTIONS.IS_TABLE_WRAPPING_TEXT_UPDATE]: setIsWrappingText,
  };

  const displaySettingsButtons = showDisplaySettingsConfirmationButtons
    ? {
      primaryButton: <FooterPrimaryButton />,
      secondaryButton: <FooterSecondaryButton />,
    }
    : {};

  const columnManagerButtons = showColumnManagerConfirmationButtons
    ? {
      primaryButton: <FooterPrimaryButton />,
      secondaryButton: <FooterSecondaryButton />,
    }
    : {};

  const displaySettings = {
    disableDisplaySettings: false,
    isCondensed,
    isWrappingText,
    ...displaySettingsButtons,
  };

  const columnManager = {
    areHiddenColumnsSearchable: true,
    searchHiddenColumns: () => { },
    disableColumnManager: false,
    visibleColumnKeys: tableData.visibleColumnKeys,
    hideableColumns: tableData.columns,
    ...columnManagerButtons,
  };

  const onSortChange = (columnName) => {
    console.log(columnName);
    setSortAction(columnName)
    setSortOption(!sortOption)
    sortDirectionColumn(columnName, sortOption);
  }

  const sortDirectionColumn = (columnName, event) => {
    console.log(columnName, sortOption, 'sortOption');
    if (event == false) {
      setSortOrder('ASC')
    }
    if (event == true) {
      setSortOrder('DESC')
    }
  }
    
    return(
        <div className="the-big-container">
            <Spacings.Stack scale="m">
                <Header
                    title={'Orders'}
                    results={totalResults}
                    linkUrl={addHyperLink('orders/new', props)}
                    linkLabel={'Add Order'}
                />
            </Spacings.Stack>
            <div className="info-box">
              <Spacings.Inline scale='m'>
                 <div className="filter-icon">
                 <SecondaryButton
                    iconLeft={<FilterIcon />}
                    label="Filter"
                    onClick={() => setIsClicked(!isClicked)}
                 />
                 </div>
                 <div style={{ width: '15%', marginBottom: "30px" , paddingTop:'10px'}}>
                      <Spacings.Stack scale="xs">
                         <SelectInput
                            placeholder="Search by"
                            name="form-field-name"
                            value={searchByField}
                            onChange={
                              (event) => {
                                controlView(event.target.value);
                                setSearchByField(event.target.value);
                              } }
                               options={ countryCode == 'US' ? [
                                { value: 'email', label: 'Email' },
                                { value: 'giftRecipientEmail', label: 'Gift Recipient Email' },
                                { value: 'orderNumber', label: 'Order Number' },
                               
                            ]: [{ value: 'email', label: 'Email' }, { value: 'orderNumber', label: 'Order Number' }]}
                         />
                      </Spacings.Stack>
                    </div>
                 
                   {showEmailSearch ? 
                   <div style={{display: 'flex',width: '50%'}}>
                     <div style={{width:'75%' ,paddingTop:'10px'}}>
                     <form onSubmit={handleEnterEmail}>
                     <TextInput
                         placeholder="Search by Email"
                         title=""
                         value={emailSearch}
                         isReadOnly={false}
                         onChange={event => setEmailSearch(event.target.value)}
                      />
                      </form>
                     </div>
                      
                      <div style={{ paddingTop: '10px'}}>
                        <SecondaryButton iconLeft={<SearchIcon />} label="" onClick={searchEmails} />
                      </div>
                    </div> 
                    : null
                    }
                    {showGiftEmailSearch ? 
                   <div style={{display: 'flex',width: '50%'}}>
                     <div style={{width:'75%' ,paddingTop:'10px'}}>
                     <form onSubmit={handleEnterGiftEmail}>
                     <TextInput
                         placeholder="Search by Email"
                         title=""
                         value={giftEmailSearch}
                         isReadOnly={false}
                         onChange={event => setGiftEmailSearch(event.target.value)}
                      />
                      </form>
                     </div>
                      
                      <div style={{ paddingTop: '10px'}}>
                        <SecondaryButton iconLeft={<SearchIcon />} label="" onClick={searchGiftEmails} />
                      </div>
                    </div> 
                    : null}
                    
                    { showOrderSearch ?
                      <div style={{display: 'flex', width: '50%'}}>
                        <div style={{width:'75%',paddingTop:'10px'}}>
                          <form onSubmit={handleEnter}>
                          <TextInput
                            placeholder="Search by Order Number"
                            value={orderNumberSearch}
                            title=""
                            isReadOnly={false}
                            onChange={event => setOrderNumberSearch(event.target.value)}
                         />
                          </form>
                        
                        </div>
                        <div style={{ paddingTop: '10px'}}>
                          <SecondaryButton iconLeft={<SearchIcon />} label="" onClick={searchOrderNumber} />
                        </div>
                      </div>
                    :null
                    } 

             <div className="cross-button">
             <Spacings.Inline scale="xs" >
             <div>
                 <FlatButton 
                  tone='secondary'
                     size="medium"
                     icon={<CloseBoldIcon />}
                     label='Clear all'
                     onClick={() => {
                         setOrderNumber('');
                         setOrderNumberSearch('')
                         setEmailSearch('')
                         setEmail('');
                         setFilterGift('');
                         setFilterCustomer('');
                         setSearchByField('');
                         setGiftEmailSearch('')
                         setIsClicked(false)
                     }}
                 />  
                 </div> 
                 {/* <div> Clear all </div>  */}
             </Spacings.Inline>
             </div>
             
             
     
                </Spacings.Inline>

              
           </div>
           {
              isClicked ?
                <div className="input-select">
                  <Spacings.Stack scale="xs">
                    <SelectInput
                      placeholder="Add filter"
                      name="form-field-name"
                      horizontalConstraint={7}
                      onChange={(e) => {
                        filterChangeData(e)
                      }}
                      value={filterData}
                      options={ countryCode == 'US' ? 
                        [{ value: 'channel', label: 'Channel type' },
                        { value: 'gift', label: 'Gift Order' }]:
                        [{ value: 'channel', label: 'Channel type' }]}
                    />
                  </Spacings.Stack>
                </div>
                : null}
           {
              isClicked && filterData == 'channel' ?
                <div className="input-select">
                  <Spacings.Stack scale="xs">
                    <SelectInput
                      placeholder="Customers Origin"
                      name="form-field-name"
                      horizontalConstraint={7}
                      onChange={(e) => {
                        filterChange(e)
                      }}
                      value={filterCustomer}
                      options={[{ value: 'D2C', label: 'D2C Customers' },
                      { value: 'B2B', label: 'B2B Customers' },
                     ]}
                    />
                  </Spacings.Stack>
                </div>
                : null}
           {customerOriginB2B && countryCode == 'US' ? 
          <DataTableManager columns={visibleColumns}
            onSettingsChange={(action, nextValue) => {
              tableSettingsChangeHandler[action](nextValue);
            }}
            columnManager={columnManager}
            displaySettings={displaySettings}>
            <DataTable
              rows={orders}
              onSortChange={onSortChange}
              onRowClick={
                (item, index) => handleRowClick(item)
              }
            />
            </DataTableManager>: null}
            {customerOriginD2C && countryCode == 'US' ? <DataTableManager columns={visibleColumns}
            onSettingsChange={(action, nextValue) => {
              tableSettingsChangeHandler[action](nextValue);
            }}
            columnManager={columnManager}
            displaySettings={displaySettings}>
                <DataTable rows={orders}  
                onSortChange={onSortChange}
                    onRowClick={
                        (item ,index) => handleRowClick(item)
                    }
                  />
            </DataTableManager>: null}
            {countryCode == 'EU' ? <DataTableManager columns={visibleColumns}
            onSettingsChange={(action, nextValue) => {
              tableSettingsChangeHandler[action](nextValue);
            }}
            columnManager={columnManager}
            displaySettings={displaySettings}>
                <DataTable rows={orders}  
                onSortChange={onSortChange}
                    onRowClick={
                        (item ,index) => handleRowClick(item)
                    }
                  />
            </DataTableManager>: null}
            {spinner ? <div className="spinner">
           <LoadingSpinner size="s">Loading</LoadingSpinner></div> : null}
           {!spinner ?
             <Pagination
                totalItems={totalResults}
                page={pageNumber}
                perPage={pageLimit}
                perPageRange="m"
                onPageChange={(page) => {
                    console.log(page)
                    setPageNumber(page)
                }}
                onPerPageChange={(lmt) => {
                    console.log(lmt)
                    setPageLimit(lmt)
                }}
             /> 
            : null}
        </div>
    )
    }


export default OrderList

