import React , {useState} from "react";
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import { useIntl } from 'react-intl';
import LinkButton from '@commercetools-uikit/link-button';
import InvoiceDetails from "./invoiceDetails";
import SERVICES ,{ centsToDollars }from "../../config/api";

const Invoice = (props) => {
    const { formatTime, formatDate } = useIntl();
    const [invoiceDetails , setInvoiceDetails] = useState(false)
    const [invoiceNumber , setInvoiceNumber] = useState('')
    let [tableDetails , setTableDetails] = useState(true)
    const orderData = props.orderData
    const tableData = props.invoiceData
    let invoiceData=[]
    let lineItem;
    for(let i=0 ;i<tableData.length;i++){
        lineItem = tableData[i]
         if(!lineItem.custom?.fields.subscriptionEnabled) {
            invoiceData.push(lineItem)
        }
    }
    let modifiedInvoiceData = invoiceData.filter( (e, i) => i === invoiceData.findIndex( e => e.custom?.fields?.itemFulfillmentNumber === e.custom?.fields?.itemFulfillmentNumber))
    let invoices = []
         const orderNumber = orderData.orderNumber
         const orderDate = orderData.createdAt
         const grandTotal = centsToDollars(orderData.custom?.fields.orderTotal)
         const action = <LinkButton label="View" isDisabled={false} onClick={()=> {setInvoiceDetails(true); setTableDetails(false)} }/>
         modifiedInvoiceData.map( data =>{
            const invoiceDetails = {
                invoiceNumber : data.custom?.fields.itemFulfillmentNumber,
                orderNumber : orderNumber,
                 orderDate : formatDate(orderDate) + " " + formatTime(orderDate),
                 grandTotal : grandTotal,
                 action: action
            }
         if( data.custom?.fields?.itemFulfillmentNumber ){
            invoices = [...invoices, invoiceDetails]
         }
           })
    const columns = [
        { key: 'invoiceNumber', label: 'Invoice number' },
        { key: 'invoiceDate', label: 'Invoice date' },
        { key: 'orderNumber', label: 'Order number' },
        { key: 'orderDate', label: 'Order date' },
        { key: 'billToName', label: 'Bill-to Name' },
        { key: 'status', label: 'Status' },
        { key: 'grandTotal', label: 'Grand total' },
        { key: 'action', label: 'Action' },
    ];
    const table =() =>{
        setTableDetails(!tableDetails)
        setInvoiceDetails(!invoiceDetails)
    }
    const handleRowClick =(item) =>{
        setInvoiceNumber(item.invoiceNumber)
    }
    return (
        <div>
            {tableDetails ? 
            <DataTableManager columns={columns}>
                <DataTable 
                onRowClick={
                    (item) => handleRowClick(item)
                  }
                rows={invoices}/>
            </DataTableManager>
           : null}
            {invoiceDetails ? <InvoiceDetails orderData={orderData} table={table} invoiceNumber={invoiceNumber} handleReload={props.handleReload}/> :null}
        </div>
    )

}

export default Invoice;