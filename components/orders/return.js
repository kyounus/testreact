import React, { useState , useEffect } from "react";
import SelectField from '@commercetools-uikit/select-field';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import NumberInput from '@commercetools-uikit/number-input';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import "./return.css";
import CheckboxInput from '@commercetools-uikit/checkbox-input';
import { useRowSelection, useSorting } from '@commercetools-uikit/hooks';
import { CallApi } from "../../plugin/Axios";
import {SERVICES} from '../../config/api';
import { ContentNotification } from '@commercetools-uikit/notifications';
import TextInput from '@commercetools-uikit/text-input';
import LoadingSpinner from '@commercetools-uikit/loading-spinner';
import { ProjectKey } from "../../config/api";

const Return = (props) => {
    const [arrIds, setArrIds] = useState([]);
    const [array, setArrayLength] = useState(0);

    const items = [];
      const {items: rows} = useSorting(items);
      const {
            rows: rowsWithSelection,
            toggleRow,
            selectAllRows,
            deselectAllRows,
            getIsRowSelected,
            getNumberOfSelectedRows,
          } = useRowSelection('checkbox', rows);
          
          const countSelectedRows = getNumberOfSelectedRows();
          const isSelectColumnHeaderIndeterminate =
          countSelectedRows > 0 && countSelectedRows < rowsWithSelection.length;
          const handleSelectColumnHeaderChange =
          countSelectedRows === 0 ? selectAllRows : deselectAllRows;
    
    const showrowId = (item) => {
        const returnIndexInArray = arrIds.indexOf(item);
                if(returnIndexInArray > -1 ) {
                arrIds.splice(returnIndexInArray, 1);
                } else {
                arrIds.push(item);
                }
                if(arrIds.length === 0)
                {
                  setArrayLength(0)
                }
                (arrIds.length)
      }
      const [resolution, setResolution] = useState('');
      const [resolutionList, setResolutionList] = useState([]);
      const [functional, setFunctional] = useState('');
      const [functionalList, setFunctionalList] = useState([]);
      const [packageCondition, setPackageCondition] = useState('')
      const [packageConditionList, setPackageConditionList] = useState([]);
      const [returnReason, setReturnReason] = useState('')
      const [returnReasonList, setReturnReasonList] = useState([])
      const [failureSymptom, setFailureSymptom] = useState('')
      const [failureSymptomList, setFailureSymptomList] = useState([]);
      const [returnQuantity, setReturnQuantity] = useState('')
      const [returnLocation, setReturnLocation] = useState('')
      const [returnLocationList, setReturnLocationList] = useState([])
      const [serialNumber, setSerialNumber] = useState('')
      const [productDamaged, setProductDamaged] = useState(true)
      const [rowId, setRowId] = useState('')
      const [rmaStatus , setRmaStatus] = useState('')
      const [successNotification , setSuccessNotification] = useState(false)
      const [errorNotifiaction , setErrorNotification] = useState(false)
      const [message , setMessage] = useState('')
      const [selectedRefundRows , setSelectedRefundRows]=useState([])
      const [returnObj , setReturnObj] = useState({})
      const [serialNumbersList , setSerialNumbersList] = useState({})
      const [spinner ,setSpinner] = useState(false)
      let [tableData,setTableData] = useState(true)

      let countryCode = 'US';
    if (ProjectKey && (ProjectKey === 'mo-dev-eu-1' || ProjectKey === 'mo-preprod1-eu' || ProjectKey === 'mo-prod-eu')) {
        countryCode = 'EU';
    }
     let returnId =[]
      props?.data?.returnInfo?.map((item) =>{
         returnId.push (item.items[0].lineItemId)
      })

      const handleSelectedRefundRows =(rowId) => {
        if(selectedRefundRows.includes(rowId)){
            let selected = selectedRefundRows.filter(id => id !== rowId)
            setSelectedRefundRows(selected)
        }
        else{
            setSelectedRefundRows([...selectedRefundRows ,rowId])
        }
    }

    useEffect(() => {
        const url = `${SERVICES.ORDER}api/v1/users/orders/rma-initiation/`
        CallApi(url,'get').then((res) => {
           console.log(res , "response")
         const response= res.data.value
             setResolutionList(response.resolution.types)   
             setFunctionalList(response.functional.issue)
             setPackageConditionList(response.package.conditions)
             setReturnLocationList(response.return.locations)
             setReturnReasonList(response.return.reasons)
             setFailureSymptomList(response.failure.symptoms)
        })
        .catch(error => {
                console.log("error", error)
              })

     },[])
     let returnData = []
     let lineItem;
 
     for (let i = 0; i < props.returnData.length; i++) {
         lineItem = props.returnData[i]
         if(!lineItem.custom?.fields.subscriptionEnabled){
            returnData.push(lineItem)
         }
     }

     let returns = []
     let returnInfo = props.data.returnInfo
     let tempItemId=[]
    returnInfo?.map(data => {
        data.items.map(id => tempItemId.push(id.lineItemId))
    })
    let returnItemId = Array.from(new Set(tempItemId));

     let warrentyNumber
     let serialNumbers

   tableData ? 
    returnData?.map((data,index) => {
        warrentyNumber = data?.custom?.fields?.warrantyOrderNumbers?.length > 0 ? data?.custom?.fields?.warrantyOrderNumbers[0] : "---"
        serialNumbers = data?.custom?.fields?.serialNumbers?.join()
        let serialNumberList = serialNumbers?.split(',')
        const returnDetails = {
            id:data.id,
            product: <div className="return-row-data">{countryCode == 'US' ? data.name["en-US"] : data.name["en-GB"]}</div>,
            warrentyOrderNumber : <div className="return-row-data">{warrentyNumber}</div>,
            orderedQuantity: <div className="return-row-data">{data.quantity}</div>,
            serialNumberData : serialNumberList,
            returnedData : data?.custom?.fields,
            returnInfo: returnItemId
        }
        returns = [...returns, returnDetails]
    })
    : null
    const columns = [
        { key: 'checkbox',
        shouldIgnoreRowClick: true,
        align: 'center',
        renderItem: (row) => (
            // row.returnInfo.includes(row.id) ? 
            // <div className="return-row-data"></div>
            // :
            <div className="return-row-data"> 
          <CheckboxInput
            isChecked={getIsRowSelected(row.id)}
            onChange={() => {
              toggleRow(row.id)
              setRowId(row.id)
              handleSelectedRefundRows(row.id) 
              showrowId(row.id)}
            }
          />
          </div>
        ),disableResizing: true,},
        { key: 'product', label: <div className="return-label">Product</div> },
        { key: 'serialNumber', label: <div className="return-label">Serial Number</div> ,width: 'minmax(200px, auto)',
         renderItem : (row) =>(
            row.returnInfo.includes(row.id)  ? 
            <div className="return-row-data">{row.returnedData.serialNumbers[0]}</div>
            :
             <div>
            {row?.serialNumberData?.length>0 ? 
             <SelectField
            title=""
            value={serialNumbersList[row.id] ? serialNumbersList[row.id] : []}
            isMulti={true}
            // backspaceRemovesValue={true}
            onChange={(e) => selectedSerialNumber(e,row)}
            options={row.serialNumberData?.map((items) => {
                        return { label: items, value: items }
                    })}
        />
        :
        <TextInput value={serialNumber[row.id]} onChange={(e) => selectedSerialNumber(e,row,true)} />
        }
        </div>)
        },
        { key: 'warrentyOrderNumber', label: <div className="return-label">Warranty Order Number</div> },
        { key: 'returnReason', label: <div className="return-label">Return Reason</div> ,width: 'minmax(340px, auto)',
        renderItem: (row) =>(
            row.returnInfo.includes(row.id) || row.returnedData?.warrantyOrderNumbers?.length > 0? 
            <div className="return-row-data">{row?.returnedData?.returnReasons?.length >0 ? row?.returnedData?.returnReasons : "---"}</div>
            :
            <SelectField
                title=""
                onChange={(e)=>selectedReturnReason(e,row)}
                value={returnReason[row.id]}
                options={returnReasonList.map((items) => {
                    return { label: items, value: items }
                })}
            />
        ) },
        { key: 'failureSymptom', label: <div className="return-label">Failure Symptom </div> ,width: 'minmax(150px, auto)',
        renderItem: (row) =>(
            row.returnInfo.includes(row.id) ? 
            <div className="return-row-data">{row.returnedData.failureSymptoms?.length>0 ? row.returnedData.failureSymptoms : "---"}</div>
            :
            <SelectField
                title=""
                isReadOnly={productDamaged}
                onChange={(e)=>selectedFailureSymptom(e,row)}
                value={failureSymptom[row.id]}
                options={failureSymptomList.map((items) => {
                    return { label: items, value: items }
                })}
            />
        )},
        { key: 'orderedQuantity', label: <div className="return-label">Ordered Quantity</div> },
        { key: 'returnQuantity', label: <div className="return-label">Return Quantity</div> ,
         renderItem:(row) =>(
            row.returnInfo.includes(row.id) ? 
            <div className="return-row-data">{row.returnedData.returnQuantity}</div>
            :
            <div className="return-row-data">
            <NumberInput
            value={returnQuantity[row.id]}
            min="0"
            max="15"
            onChange={(e) => { selectedReturnQuantity(e,row) }}
        />
        </div>
         ) },
        { key: 'returnLocation', label: <div className="return-label">Return Location</div> ,width: 'minmax(120px, auto)', 
        renderItem: (row) =>(
            row.returnInfo.includes(row.id) ? 
            <div className="return-row-data">{row.returnedData?.returnLocation?.length >0 ? row.returnedData?.returnLocation : "---"}</div>
            :
            <SelectField
                title=""
                onChange={(e)=>selectedReturnLoc(e,row)}
                value={returnLocation[row.id]}
                options={returnLocationList.map((items) => {
                    return { label: items, value: items }
                })}
            />
        ) },
    ];
    
    const selectedReturnQuantity = (e,row)=>{
        setErrorNotification(false)
        if(row.orderedQuantity.props.children < Number(e.target.value)){
            setSuccessNotification(false)
            setErrorNotification(true)
            setMessage("Value is more than the Ordered Quantity")
            return
        }
        setReturnQuantity(e.target.value)
        if(returnObj[row.id] ){
            let selectedReturnObj = returnObj[row.id]
            selectedReturnObj["quantity"] = e.target.value
               returnObj[row.id]=selectedReturnObj
           }
           else{
            let selectedReturnObj ={}
            selectedReturnObj["quantity"] = e.target.value
            returnObj[row.id]=selectedReturnObj
           }
    }

    const selectedSerialNumber = (e,row,isTextField)=>{
        if(isTextField){
            setSerialNumbersList((previousState) => ({...previousState, [row.id]: [e?.target?.value]}))
        }
        else{
            if(serialNumbersList[row.id]){
                setSerialNumbersList((previousState) => ({...previousState, [row.id]: e.target.value}))
            }
            else{
                setSerialNumbersList((previousState) => ({...previousState, [row.id]: e.target.value}))
            }
        }
        if(returnObj[row.id] ){
            let selectedReturnObj = returnObj[row.id]
            if(isTextField){
                selectedReturnObj["serialNumber"] = e.target.value
            }
            else{
                 selectedReturnObj["serialNumber"] = e.target.value.join()
            }
               returnObj[row.id]=selectedReturnObj
           }
           else{
            let selectedReturnObj ={}
            if(isTextField){
                selectedReturnObj["serialNumber"] = e.target.value
            }
            else{
                 selectedReturnObj["serialNumber"] = e.target.value.join()
            }
               returnObj[row.id]=selectedReturnObj
           }
    }
    const selectedFailureSymptom = (e,row)=>{
        setFailureSymptom((previousState) => ({...previousState, [row.id]: e.target.value}));
        if(returnObj[row.id] ){
            let selectedReturnObj = returnObj[row.id]
            selectedReturnObj["failureSymptoms"] = e.target.value
               returnObj[row.id]=selectedReturnObj
           }
           else{
            let selectedReturnObj ={}
            selectedReturnObj["failureSymptoms"] = e.target.value
            returnObj[row.id]=selectedReturnObj
           }
    }
    const selectedReturnReason = (e,row)=>{
           setReturnReason((previousState) => ({...previousState, [row.id]: e.target.value}));
           if(returnObj[row.id] ){
            let selectedReturnObj = returnObj[row.id]
            selectedReturnObj["returnReasons"] = e.target.value
               returnObj[row.id]=selectedReturnObj
           }
           else{
            let selectedReturnObj ={}
            selectedReturnObj["returnReasons"] = e.target.value
            returnObj[row.id]=selectedReturnObj
           }
       }

   const selectedReturnLoc = (e,row)=>{
       setReturnLocation((previousState) => ({...previousState, [row.id]: e.target.value}));
       if(returnObj[row.id] ){
        let selectedReturnObj = returnObj[row.id]
        selectedReturnObj["returnLocation"] = e.target.value
           returnObj[row.id]=selectedReturnObj
       }
       else{
        let selectedReturnObj ={}
        selectedReturnObj["returnLocation"] = e.target.value
        returnObj[row.id]=selectedReturnObj
       }
   }

    const handleRowClick = (item,index) => {
        console.log(item,index,"handlee")
    }

    const handleSubmit = () =>{
        setSpinner(true)
        setTableData(false)
        if(selectedRefundRows.length === 0){
                setSuccessNotification(false)
                setErrorNotification(true)
                setMessage("Select the Product")
                setSpinner(false)
                setTableData(true)
            return
        }
        for ( let i=0 ; i<returnItemId.length ; i++){
            if(selectedRefundRows.includes(returnItemId[i])){
                setSuccessNotification(false)
                setErrorNotification(true)
                setMessage("Product is already returned")
                setSpinner(false)
                setTableData(true)  
                return
            }
        }
    let payloadArray =[]
        if(selectedRefundRows.length >0){
            if(resolution.length === 0){
                setSuccessNotification(false)
                setErrorNotification(true)
                setMessage("Select the reslution")
                setSpinner(false)
             setTableData(true)
                return
             }
             if (functional.length === 0){
                setSuccessNotification(false)
                setErrorNotification(true)
                setMessage("Select the Functional Issue")
                setSpinner(false)
                setTableData(true)
                return
             }
             if (packageCondition.length === 0){
                setSuccessNotification(false)
                setErrorNotification(true)
                setMessage("Select the Package Condition")
                setSpinner(false)
                setTableData(true)
                return
             }
            selectedRefundRows?.map(data => {
                let filteredArray = Object.keys(returnObj).filter(obj => obj === data)
  
          if (filteredArray.length !== 0)
          {
            let selectedReturnObj=returnObj[filteredArray[0]]
            selectedReturnObj["lineItemId"]=filteredArray[0]
            selectedReturnObj["resolution"]=resolution
            selectedReturnObj["functionalIssue"]=functional
            selectedReturnObj["packageCondition"]=packageCondition
            returnObj[filteredArray[0]]=selectedReturnObj
             payloadArray = payloadArray.concat(returnObj[filteredArray[0]])
          }  
        })
        }
        for (let i = 0; i < payloadArray.length; i++) {
            if(!payloadArray[i].quantity){
            setSuccessNotification(false)
            setErrorNotification(true)
            setMessage("Enter the Quantity")
            setSpinner(false)
            setTableData(true)
            return 
            }
            if(!payloadArray[i].returnLocation){
                setSuccessNotification(false)
                setErrorNotification(true)
                setMessage("Enter Return Location")
                setSpinner(false)
                setTableData(true)
                return 
            }
            if(!payloadArray[i].returnReasons){
                setSuccessNotification(false)
                setErrorNotification(true)
                setMessage("Enter Return Reason")
                setSpinner(false)
                setTableData(true)
                return 
            }
            if(!payloadArray[i].serialNumber){
                setSuccessNotification(false)
                setErrorNotification(true)
                setMessage("Enter Serial Number")
                setSpinner(false)
                setTableData(true)
                return 
            }
        }
        
        const payload=payloadArray
        console.log(payload)
        const url=`${SERVICES.ORDER}/api/v1/users/orders/${props.orderId}/return/`
        CallApi(url,'post',payload).then(res =>
            {console.log(res)
                if(res.data.error){
                    setSuccessNotification(false)
                    setErrorNotification(true)
                    setMessage(res.data.error)
                    handleReload()
                }
                else{
                setSuccessNotification(true)
                setErrorNotification(false)
                setMessage("Return has been created")
                handleReload()
                }

        })
        .catch(error =>
           {console.log(error)
            setSpinner(false)
            setTableData(true)
            setSuccessNotification(false)
            setErrorNotification(true)
            setMessage(error.message)}
            )
    }
   
    const functionalIssue =(e) =>{
        setFunctional(e.target.value)
       e.target.value === "Yes" ? setProductDamaged(false) : setProductDamaged(true)
    }
    const rmaOptions =['Set to Pending Approval' , 'Set to Approve' ,'Set to Closed']
    const handleRma =(e) =>{
        if(selectedRefundRows.length === 0){
            setSuccessNotification(false)
            setErrorNotification(true)
            setMessage("Select the Product")
            return
        }
        let lineItemid=[]
        selectedRefundRows.map(id =>{
            lineItemid.push("lineItemIdList="+id)
        })
        let rmaData=e.target.value
        let lineItemIdList = lineItemid.join('&');
        const url= `${SERVICES.ORDER}/api/v1/users/orders/${props.orderId}/rma-status?${lineItemIdList}&rmaStatus=${rmaData}`
        CallApi(url,'post').then(res =>
                { 
                  console.log(res)
                  setSuccessNotification(true)
                  setErrorNotification(false)
                  if(rmaData === "PENDINGAPPROVAL") rmaData = "PENDING APPROVAL"
                  setMessage("RMA has been set to " + rmaData)
                })
              .catch(error =>
                { 
                  console.log(error)
                  setSuccessNotification(false)
                  setErrorNotification(true)
                  setMessage(error.message)})
    }

    const handleCancel =() =>{
        const url = `${SERVICES.ORDER}/api/v1/users/orders/${props.orderId}/cancel/`
        CallApi(url,'post').then(res =>
            {console.log(res)
            setSuccessNotification(true)
            setErrorNotification(false)
            setMessage("Order has been cancelled")

        })
        .catch(error =>
           {console.log(error)
            setSuccessNotification(false)
            setErrorNotification(true)
            setMessage(error.message)}
            )
    }

const handleReload =()=>{
    props.handleReload(true)
    setTimeout(() => {
        setTableData(true)
        setSpinner(false)    
    }, 3000);
}
    return (
        <div>
            <div className="return_button">
                <div className="return_buttons">
                    <SecondaryButton
                        onClick={() => { }}
                        label='Print Label'
                    />
                </div>
                <div className="return_buttons">
                    <SecondaryButton
                        onClick={handleCancel}
                        label='Cancel'
                    />
                </div>
                <div className="return_buttons">
                    <SecondaryButton
                        onClick={handleSubmit}
                        label='Save'
                    />
                </div>
                <div style={{width:"15%" , marginTop:"8px"}}>
                <SelectField
                    title=""
                    placeholder={rmaOptions[0]}
                    onChange={(e) =>{setRmaStatus(e.target.value),handleRma(e)}}
                    value={rmaStatus}
                    options={[
                        { value: 'PENDINGAPPROVAL', label: 'Set to Pending Approval' },
                        { value: 'APPROVED', label: 'Set to Approve' },
                        { value: 'CLOSED', label: 'Set to Closed' },
                        ]}
                />
                </div>
            </div>
            {successNotification ? <ContentNotification type="success">{message}</ContentNotification> : null}
            {errorNotifiaction ? <ContentNotification type="error">{message}</ContentNotification> : null}
            
            <div className="return-info">
            <SelectField
                    title="Resolution"
                    onChange={(e) => setResolution(e.target.value)}
                    value={resolution}
                    options={resolutionList?.map((items) => {
                        return { label: items, value: items }
                    })}
                />
                
                <SelectField
                    title="Functional Issue"
                    onChange={(e) => functionalIssue(e)}
                    value={functional}
                    options={functionalList?.map((items) => {
                        return { label: items, value: items }
                    })}
                
                />
                <SelectField
                    title="Package Condition"
                    onChange={(e) => setPackageCondition(e.target.value)}
                    value={packageCondition}
                    options={packageConditionList?.map((items) => {
                        return { label: items, value: items }
                    })}
                
                />
            </div>
            <div className="order-table">
                <DataTableManager columns={columns}>
                    <DataTable
                    disableSelfContainment="true"
                        rows={returns}
                        onChange={
                            (item, index) => handleRowClick(item,index)
                        }
                    />
                </DataTableManager>
            {spinner ? <div className="spinner"><LoadingSpinner size="s">Loading</LoadingSpinner></div> : null}
            </div>
        </div>
    );
}
export default Return;