import React, { useState } from "react";
import Text from '@commercetools-uikit/text';
import Grid from '@commercetools-uikit/grid';
import Label from '@commercetools-uikit/label';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import "./invoiceDetails.css";
import FlatButton from '@commercetools-uikit/flat-button';
import { useIntl } from 'react-intl';
import MultilineTextField from '@commercetools-uikit/multiline-text-field';
import CheckboxInput from '@commercetools-uikit/checkbox-input';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import {SERVICES  ,centsToDollars}from "../../config/api";
import { BackIcon } from '@commercetools-uikit/icons';
import SecondaryIconButton from '@commercetools-uikit/secondary-icon-button'
import { CallApi } from "../../plugin/Axios";
import CreditMemo  from "./credit-memo";

const InvoiceDetails = (props) => {
    const orderData = props.orderData
    const tableData =props.orderData.lineItems
    const { formatTime, formatDate } = useIntl();
    const [comment , setComment] = useState('')
    const [isChecked, setIsChecked] = useState(false)
    const [details,setDetails] =useState(true)
    const [creditMemo , setCreditMemo] = useState(false)
    let deviceNumber;
    let invoiceData=[]
    let lineItem;
    for(let i=0 ;i<tableData.length;i++){
        lineItem = tableData[i]
         if(!lineItem.custom?.fields.subscriptionEnabled 
            // && lineItem.custom?.fields?.itemFulfillmentNumber == props?.invoiceNumber
            ) {
            invoiceData.push(lineItem)
            deviceNumber=lineItem.custom ? lineItem.custom?.fields?.serialNumbers[0] :null 
        }
    }
    let invoicesList = []
    invoiceData.map( data =>{
    let amount = ((data.taxedPrice.totalGross.centAmount)-(data.taxedPrice.totalNet.centAmount))
    const invoiceDetails = {
        product : data.name["en-US"],
        price: centsToDollars(data.variant?.prices[0].value.centAmount),
        qty : data.quantity,
        subtotal: centsToDollars(data.totalPrice.centAmount),
        taxamount:centsToDollars((data.taxedPrice.totalGross.centAmount)-(data.taxedPrice.totalNet.centAmount)),
        //discountAmount:centsToDollars(data.discountedPrice ? data.discountedPrice?.value.centAmount :null),
        rowtotal: centsToDollars((data.price.value.centAmount)*(data.quantity)+ amount)

    }
    invoicesList = [...invoicesList, invoiceDetails]
      })
    const columns = [
        { key: 'product', label: 'Product' },
        { key: 'price', label: 'Price' },
        { key: 'qty', label: 'Qty' },
        { key: 'subtotal', label: 'Subtotal' },
        { key: 'taxamount', label: 'Tax amount' },
       // { key: 'discountAmount', label: 'Discount Amount' },
        { key: 'rowtotal', label: 'Row total' }
    ];
    const handleEmail =()=>{
        const url =`${SERVICES.CHECKOUT}/api/v1/users/checkout/sendOrderInvoiceEmail/${orderData.orderNumber}/`
        CallApi(url,'get').then(response =>{
            console.log(response)
        })
        .catch ( error => {
            console.log(error)
        }
        )
    }
    return (
        <div>
            {details ? 
            <>
           {deviceNumber?.length >0 ? <h3>Device Number : {deviceNumber}</h3> :null}
           {creditMemo ? <h3>New Memo for {orderData.orderNumber}</h3> :null}
            <div className="invoice_button">
            {/* <BackIcon /> */}
            <div className="invoice_buttons">
                <SecondaryIconButton
                    onClick={() => {setDetails(false);props.table()}}
                    icon={<BackIcon size='medium'/>}
                    label=''
                />
                </div>
                <div className="invoice_buttons">
                <FlatButton
                    tone="primary"
                    label="Back"
                    onClick={() => {setDetails(false);props.table()}}
                    isDisabled={false}
                />
                </div>
                {!creditMemo ?
                <> 
                <div className="invoice_buttons">
                <FlatButton
                    tone="primary"
                    label="Send Email"
                    onClick={handleEmail}
                    isDisabled={false}
                />
                </div>
                <div className="invoice_buttons">
                <FlatButton
                    tone="primary"
                    label="Credit Memo"
                    onClick={() =>setCreditMemo(true)}
                    isDisabled={false}
                />
                </div>
                <div className="invoice_buttons">
                <FlatButton
                    tone="primary"
                    label="Print"
                    onClick={() => { }}
                    isDisabled={false}
                />
                </div>
                </>
                : null}
                </div>
            <div className="invoice_section">
            <Text.Headline as="h4"><b>{'Order & Account Information'}</b></Text.Headline>
            <hr></hr>
            <div className="order-label">
            <div className="customer" style={{ color: 'dodgerblue'}}><b>{'Order # '}{orderData.orderNumber}</b></div>
            <div className="customer"><b>{'Account Information'}</b></div>
            </div>
            <Grid
                gridGap="16px"
                gridAutoColumns="1fr"
                gridTemplateColumns="repeat(2, 1fr)"
            >
                <Grid.Item>
                    <Grid
                        gridGap="16px"
                        gridAutoColumns="1fr"
                        gridTemplateColumns="repeat(2, 1fr)"
                    >
                        <Grid.Item>
                            <div className="customer">
                                <div><b>Order date  </b></div>
                                <div className="order-customer"><b>Order Status </b> </div>
                                <div className="order-customer"><b>Purchased from </b> </div>
                                <div className="order-customer"><b>Placed From IP  </b>
                                </div>
                            </div>
                        </Grid.Item>
                        <Grid.Item>
                            <div className="customer">
                                <div>{formatDate(orderData.createdAt) + " " + formatTime(orderData.createdAt)}</div>

                                <div className="order-customer">{orderData.orderState} </div>

                                <div className="order-customer">{orderData.store?.key}</div>

                                <div className="order-customer">-</div>
                            </div>
                        </Grid.Item>
                    </Grid>
                </Grid.Item>
                <Grid.Item>
                    <Grid
                        gridGap="16px"
                        gridAutoColumns="1fr"
                        gridTemplateColumns="repeat(2, 1fr)"
                    >
                        <Grid.Item>
                            <div className="customer">
                                <div className="order-customer"><b>Customer name </b> </div>
                                <div className="order-customer"><b>Email </b> </div>
                                <div className="order-customer"><b>Customer Group   </b>
                                </div>
                            </div>
                        </Grid.Item>
                        <Grid.Item>
                            <div className="customer">
                                <div className="order-customer">{orderData.customerFirstName +" "+orderData.customerLastName}</div>

                                <div className="order-customer">{orderData.customerEmail}</div>

                                <div className="order-customer">-</div>
                            </div>
                        </Grid.Item>
                    </Grid>
                </Grid.Item>
            </Grid>
            
            <div className="invoice_section">
                <Text.Headline as="h4"><b>{'Address Information'}</b></Text.Headline>
                <hr></hr>
                <Grid
                    gridGap="16px"
                    gridAutoColumns="1fr"
                    gridTemplateColumns="repeat(2, 1fr)"
                >
                    <Grid.Item>
                    <div className="customer">
                        <div className="section_heading"><b>Billing Address </b></div>
                        <div>{orderData.billingAddress?.firstName+" "}{orderData.billingAddress?.lastName}</div>
                        <div>{orderData.billingAddress?.streetName}</div>
                        <div>{orderData.billingAddress?.city+" "}{orderData.billingAddress?.state+" "}{orderData.billingAddress?.postalCode}</div>
                        <div>{orderData.billingAddress?.phone}</div>
                        </div>
                    </Grid.Item>
                    <Grid.Item>
                    <div className="customer">
                        <div className="section_heading"><b>Shipping Address </b></div>
                        <div>{orderData.shippingAddress?.firstName+" "}{orderData.shippingAddress?.lastName}</div>
                        <div>{orderData.shippingAddress?.streetName}</div>
                        <div>{orderData.shippingAddress?.city+" "}{orderData.shippingAddress?.state+" "}{orderData.shippingAddress?.postalCode}</div>
                        <div>{orderData.shippingAddress?.phone}</div>
                        </div>
                    </Grid.Item>
                </Grid>
            </div>
           
            <div className="invoice_section">
                <Text.Headline as="h4"><b>{'Payment & Shipping Method'}</b></Text.Headline>
                <hr></hr>
                <Grid
                    gridGap="16px"
                    gridAutoColumns="1fr"
                    gridTemplateColumns="repeat(2, 1fr)"
                >
                    <Grid.Item>
                    <div className="customer">
                        <div className="section_heading"><b>Payment Information  </b></div>
                        <div>{orderData.paymentInfo?.payments[0].obj.paymentMethodInfo.method}</div>
                        <br></br>
                        <div>The order was placed using {orderData.paymentInfo?.payments[0].obj.amountPlanned?.currencyCode}</div>
                    </div>
                    </Grid.Item>
                    <Grid.Item>
                    <div className="customer">
                        <div className="section_heading"><b>Shipping Information  </b></div>
                        <div><b>{orderData.shippingInfo?.shippingMethodName}</b></div>
                        <div>Total Shipping charges:<b> ${orderData.shippingInfo?.shippingRate?.price.centAmount}</b></div>
                   </div>
                    </Grid.Item>
                </Grid>
            </div>
            
            {!creditMemo ?
            <div>
            <div className="invoice_section">
                <Text.Headline as="h4"><b>{'Items Invoiced'}</b></Text.Headline>
                <hr></hr>
                <DataTableManager columns={columns}>
                    <DataTable
                        rows={invoicesList}
                    />
                </DataTableManager>
            </div>
            
            <div className="invoice_section">
                <Text.Headline as="h4"><b>{'Order Total'}</b></Text.Headline>
                <hr></hr>
                <Grid
                    gridGap="16px"
                    gridAutoColumns="1fr"
                    gridTemplateColumns="repeat(2, 1fr)"
                >
                    <Grid.Item>
                    <div className="customer">
                        <div className="section_heading"><b>Invoice History</b></div>
                        <div className="invoice_textbox">
                        <MultilineTextField
                                        title="Comment Text"
                                        value={comment}
                                        onChange={(event) => setComment(event.target.value)}
                                    />
                        </div>
                         <CheckboxInput
                                value="foo-radio-value"
                                onChange={() => setIsChecked(!isChecked)}
                                isChecked={isChecked}
                            >
                                Visible on Storefront
                    </CheckboxInput>
                    <div className="invoice_submit">
                    <SecondaryButton label="Submit Comment" />
                    </div>
                    </div>
                    </Grid.Item>
                    <Grid.Item>
                    <div className="customer">
                    
                        <div className="section_heading"><b>Invoice Totals </b></div>
                        <Grid
                            gridGap="16px"
                            gridAutoColumns="1fr"
                            gridTemplateColumns="repeat(2, 1fr)"
                        >
                            <Grid.Item>
                                <div>Subtotal </div>
                                <div>Shipping & Handling </div>
                                <div>Tax </div>
                                <div style={{ fontWeight: '700'}}>Grand total</div>
                            </Grid.Item>
                            <Grid.Item>
                            <div>{centsToDollars(orderData.custom.fields.subTotal)}</div>
                                <div>{centsToDollars(orderData.custom.fields.shippingCost)} & {centsToDollars(orderData.custom.fields.handlingCost)}</div>
                                <div>{centsToDollars(orderData.custom.fields.totalTax)}</div>
                                <div style={{ fontWeight: '700'}}>{centsToDollars(orderData.custom.fields.orderTotal)}</div>
                            </Grid.Item>
                        </Grid>
                        </div>
                    </Grid.Item>
                </Grid>
            </div>
            </div>
            : null}
            </div>
            </>
            :null}
            {creditMemo ? 
            <CreditMemo memo={invoiceData}  orderData={orderData} invoiceNumber={props.invoiceNumber}  handleReload={props.handleReload}/>
            :null}
        </div>
    );
}
export default InvoiceDetails;