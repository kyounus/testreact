import React, { useEffect, useState } from "react";
import "./order-details.css";
import Text from '@commercetools-uikit/text';
import { useRouteMatch } from 'react-router-dom';
import CollapsiblePanel from '@commercetools-uikit/collapsible-panel';
import Spacings from '@commercetools-uikit/spacings'
import Grid from '@commercetools-uikit/grid';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import LinkButton from '@commercetools-uikit/link-button'
import { useIntl } from 'react-intl';
import { TruckIcon, PaperBillInvertedIcon } from '@commercetools-uikit/icons';
import Payment from "./payments"
import Card from '@commercetools-uikit/card';
import {SERVICES  ,centsToDollars  }from "../../config/api";
import { ListIcon } from '@commercetools-uikit/icons';
import Shipping from "./shippingTab";
import { addHyperLink } from '../../utils';
import Invoice from "./invoices";
import { CloseBoldIcon } from '@commercetools-uikit/icons';
import {ConfirmationDialog} from '@commercetools-frontend/application-components'
import { ContentNotification } from '@commercetools-uikit/notifications';
import Stamp from '@commercetools-uikit/stamp';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import PrimaryButton from '@commercetools-uikit/primary-button';
import MultilineTextInput from '@commercetools-uikit/multiline-text-input';
import Return from "./return";
import { CallApi } from "../../plugin/Axios";
import { ProjectKey } from "../../config/api";
import { CustomFormModalPage } from '@commercetools-frontend/application-components';
import CreditMemoTab from "./creditMemoTab";

const Order = (props) => {
    const [summaryPanel, setSummaryPanel] = useState(false);
    const [customerPanel, setCustomerPanel] = useState(false);
    const [orderPanel, setOrderPanel] = useState(false);
    const [subPanel, setSubPanel] = useState(false);
    const [tableData, setTableData] = useState([]);
    const [shippingAddress, setShippingAddress] = useState([])
    const [billingAddress, setBillingAddress] = useState([])
    const [tableData1, setTableData1] = useState([])
    const [orderNumber , setOrderNumber]= useState(false)
    const [address, setAddress] = useState(false)
    const { formatTime, formatDate } = useIntl();
    const [generalTab, setGeneralTab] = useState(true)
    const [paymentTab, setPaymentTab] = useState(false)
    const [shippingTab, setShippingTab] = useState(false)
    const [invoicesTab, setInvoicesTab] = useState(false);
    const [returnTab, setReturnTab] = useState(false);
    const [creditMemoTab, setCreditMemoTab] = useState(false);
    const [richText, setRichText]=useState('');
    const [channelValue, setChannelValue] = useState('');
    const [giftValue, setGiftValue]=useState(false);
    const [giftEmail, setGiftEmail] = useState('');
    const [message, setMessage] = useState('');
    const [confirm, setConfirm] = useState(false)
    const [reload, setReload] = useState(false)
    const [notification , setNotification] =  useState(false); 
    const [notificationMessage, setNotificationMessage] = useState('Updated');
    const [errorNotifiaction , setErrorNotification] =  useState(false);
    const [ordererrorNotifiaction , setOrderErrorNotification] =  useState(false);
    const [errorMessage , setErrorMessage] =  useState('');
    const [countryName, setCountryName] = useState('')
    const [newReturn, setNewReturn] = useState(false);
    const [modal , setModal] =useState(false);
    const [taxPortions, setTaxPortions] = useState([])

    let data = props.history.location
    const { match } = props;

    const getLastItem = thePath => thePath.substring(thePath.lastIndexOf('/') + 1)
    let orderId= data.orderId ? data.orderId : getLastItem(props.location.pathname)

    let countryCode = 'US';
    if (ProjectKey && (ProjectKey === 'mo-dev-eu-1' || ProjectKey === 'mo-preprod1-eu' || ProjectKey === 'mo-prod-eu')) {
        countryCode = 'EU';
    }

    useEffect(() => {
        const url = `${SERVICES.ORDER}/api/v1/users/orders/${orderId}/`;
        CallApi(url, 'get')
            .then(response => {
                if (response.data){
                    if(response.data.errorMessage){
                        setOrderErrorNotification(true)
                        setErrorMessage(response.data.errorMessage)
                        return
                    }
                console.log(response.data.country,"country")
                setChannelValue(response.data.custom.fields.channel)
                setGiftValue(response.data.custom?.fields.gift)
                setGiftEmail(response.data.custom?.fields.recipientsEmail)
                setMessage(response.data.custom?.fields.giftMessage)
                setShippingAddress(response.data.shippingAddress)
                setBillingAddress(response.data.billingAddress)
                setCountryName(response.data.country)
                setAddress(true)
                setTableData1(response.data)
                setOrderNumber(true)
                setTableData(response.data.lineItems)
                setTaxPortions(response?.data?.taxedPrice?.taxPortions)
               }
            })
            .catch(error => {
                console.log('Error', error)
                
            })
    }, [reload]);


    let itemsList = []
 
    let lineItems=[]
    let lineItem;
    for(let i=0 ;i<tableData?.length;i++){
        lineItem = tableData[i]
         if(!lineItem.custom?.fields.subscriptionEnabled) {
            lineItems.push(lineItem)
        }
      
    }

    let lineItemStatus = []

    let subSkuDeviceId = []

    lineItems.map((data) => {
        let amount = ((data.taxedPrice?.totalGross?.centAmount)-(data.taxedPrice?.totalNet?.centAmount))
        const eachLineStatus = data.state[0]?.state?.obj?.name.en

        const subDetails = {
            productsku: data.variant.sku,
            deviceids : data?.custom?.fields?.serialNumbers ? 
                        data?.custom?.fields?.serialNumbers : 
                        <div>Not Available</div>
        }
        taxPortions?.map((taxData)=>{
            const lineDetails = {
                productname: countryCode == 'US' ? data.name["en-US"] : data.name["en-GB"] ,
                productsku: data.variant.sku,
                itemstatus: data.state[0]?.state?.obj?.name.en,
                originalprice: centsToDollars(data.variant.prices[0].value.centAmount),
                price:centsToDollars(data.price.value.centAmount),
                quantity: data.quantity,
                subtotal: centsToDollars(data.totalPrice.centAmount),
                taxamount: data.taxedPrice ? centsToDollars((data.taxedPrice.totalGross.centAmount)-(data.taxedPrice.totalNet.centAmount)) : "",
                taxpercent: taxData?.rate*100+'%',
                rowtotal:centsToDollars((data.price.value.centAmount)*(data.quantity)+ amount)
            }
            itemsList = [...itemsList, lineDetails]
            lineItemStatus=[...lineItemStatus, eachLineStatus]
            subSkuDeviceId=[...subSkuDeviceId, subDetails]
            })
        })


        
        let returnFlag
        let shippedArray =  lineItemStatus.filter( item => item === "Shipped")
        if (shippedArray.length >0){
            returnFlag = true
        }

        let flag = false
        let disableArray = lineItemStatus.filter( (item) =>{ 
            return item === "Shipped" || item === "Released"
        })
       
        if(disableArray.length>0)
        {
            flag=true
        }
        
    const columns = [
        { key: 'productname', label: 'Product name' },
        { key: 'productsku', label: 'Product SKU' },
        { key: 'itemstatus', label: 'Item status' },
        { key: 'originalprice', label: 'Original price (MSRP)' },
        { key: 'price', label: 'Price' },
        { key: 'quantity', label: 'Quantity' },
        { key: 'subtotal', label: 'Subtotal' },
        // { key: 'taxamount', label: countryCode == 'US' ? 'Tax amount' : 'VAT'},
        { key: 'taxpercent', label: countryCode == 'US' ? 'Tax percent' : 'VAT percent'},
       // { key: 'discount', label: 'Discount' },
        // { key: 'rowtotal', label: 'Row total' }
    ];

    const columnsSubscription = [
        { key: 'productsku', label: 'Product SKU' },
        { key: 'deviceids', label: 'Serial Numbers' },
    ]

    const handleGeneralTab = () => {
        setGeneralTab(true)
        setPaymentTab(false)
        setShippingTab(false)
        setInvoicesTab(false)
        setReturnTab(false)
        setCreditMemoTab(false)
    }
    const handlePaymentTab = () => {
        setPaymentTab(true)
        setGeneralTab(false)
        setShippingTab(false)
        setInvoicesTab(false)
        setReturnTab(false)
        setCreditMemoTab(false)
    }
    const handleShippingTab =() => {
        setPaymentTab(false)
        setGeneralTab(false)
        setShippingTab(true)
        setInvoicesTab(false)
        setReturnTab(false)
        setCreditMemoTab(false)
    }
    const handleInvoicesTab =() => {
        setInvoicesTab(true)
        setPaymentTab(false)
        setGeneralTab(false)
        setShippingTab(false)
        setReturnTab(false)
        setCreditMemoTab(false)
    }
    const handleReturnTab =() => {
        setReturnTab(true)
        setInvoicesTab(false)
        setPaymentTab(false)
        setGeneralTab(false)
        setShippingTab(false)
        setCreditMemoTab(false)
    }
    const hanldeCreditMemoTab =() => {
        setCreditMemoTab(true)
        setReturnTab(false)
        setInvoicesTab(false)
        setPaymentTab(false)
        setGeneralTab(false)
        setShippingTab(false)
    }
    const handleEmail =()=>{
        if(tableData1.orderState !== "Cancelled"){
        const url = `${SERVICES.CHECKOUT}api/v1/users/checkout/sendOrderConfirmationEmail/${tableData1.orderNumber}/`;
        CallApi(url, 'get')
        .then(response =>{
            console.log(response)
        })
        .catch ( error => {
            console.log(error)
        }
        )}
    }

    const CancelOrder = () => {
            const url = `${SERVICES.ORDER}api/v1/users/orders/${orderId}/cancel/`
            CallApi(url, 'post')
            .then(response =>{
                console.log(response.data.cancelled, "cancel res");
                if(response.data.cancelled=== true){
                setErrorNotification(false)       
                setNotification(true);
                setNotificationMessage('Order Cancelled.')
                }
                else{
                    setErrorMessage("Cannot cancel this order")
                    setErrorNotification(true)
                }
            })
            .then(re=>{
                setReload(true)
                setTimeout(() => {
                    setConfirm(false)
                }, 3000)
               
            })
            .catch ( error => {
                console.log(error)
                       
            }
            )
    }

const handleReload =() =>{
    setReload(!reload)
}
const handleClose =() =>{
    setNewReturn(false)
}
const handleNewReturn =() =>{
    setNewReturn(true)
    setModal(true)
}
    return (
        <div className="order-container">
            <div className="sub-order">
            <LinkButton
                to={addHyperLink('orders', props)}
                iconLeft={<ListIcon />}
                label="To Order List"
                isDisabled={false}
            />
                <div className="order-heading">
                    <Text.Headline as="h2">{orderNumber  ? 'Order' + "  " + tableData1.orderNumber: 'Order'}</Text.Headline>
                    <div className="link-button">
                    <Spacings.Inline scale="m">
                    <LinkButton
                            style={{marginTop: "5px"}}
                            label="New Return"
                            isDisabled={false}
                            onClick={handleNewReturn}
                        />
                        <div>
                        <LinkButton
                            style={{marginTop: "5px"}}
                            label="Send Email"
                            isDisabled={false}
                            onClick={handleEmail}
                        />
                        </div>
                        <PrimaryButton 
                         iconLeft={<CloseBoldIcon color="white"/>}
                         style={{marginLeft: "10px", backgroundColor: "red", color: "white"}} 
                         label="Cancel Order" 
                         isDisabled=
                         {flag || (tableData1.state?.obj.name.en === "Cancelled")}
                         onClick={()=>setConfirm(true)}/>
                        </Spacings.Inline>
                    </div>
                </div>
                <div className="main-tab-list">
                    <ul className="sub-tab-list">
                        <li><a
                            className={generalTab ? 'background_green' : 'background_black'}
                            onClick={() => handleGeneralTab()}
                        >General</a></li>
                        <li><a
                            className={paymentTab ? 'background_green' : 'background_black'}
                            onClick={() => handlePaymentTab()}
                        >Payments </a></li>
                         <li><a
                            className={shippingTab ? 'background_green' : 'background_black'}
                            onClick={() => handleShippingTab()}
                        >Shipping & Delivery </a></li>
                        <li><a
                            className={invoicesTab ? 'background_green' : 'background_black'}
                            onClick={() => handleInvoicesTab()}
                        >Invoices </a></li>
                        <li>
                        {returnFlag ? <a
                            className={returnTab ? 'background_green' : 'background_black'}
                            onClick={() => handleReturnTab()}
                        >Returns </a>
                        : <a className="diasable-return">Returns</a>}</li>
                         <li><a
                            className={creditMemoTab ? 'background_green' : 'background_black'}
                            onClick={() => hanldeCreditMemoTab()}
                        >Credit Memo </a></li>
                    </ul>
                </div>
            </div>

            <ConfirmationDialog
            title={`Cancel order number ${tableData1.orderNumber} ?`}
            isOpen={confirm}
            onClose={()=>setConfirm(false)}
            onCancel={()=>setConfirm(false)}
            onConfirm={CancelOrder}
          >
            <Spacings.Stack scale="m">
              <div style={{margin: "5px"}}>
              <Text.Body>{`Are you sure you want to cancel the order? `}</Text.Body>
              </div>
              <div>
                        {notification ?
                            <div>
                                <ContentNotification type="success"> {notificationMessage} </ContentNotification>
                               
                            </div>
                            : null
                        }
                        {errorNotifiaction ?
                            <div>
                                <ContentNotification type="error"> {errorMessage} </ContentNotification>
                                
                            </div>
                            : null
                        }
                    </div>
            </Spacings.Stack>
           
          </ConfirmationDialog>

            {generalTab ?
                <div>
                      {ordererrorNotifiaction ?
                                <ContentNotification type="error"> {errorMessage} </ContentNotification>
                            : null
                        }
                    <Text.Headline as="h5">Order Date &nbsp; {formatDate(tableData1.createdAt) + " " + formatTime(tableData1.createdAt)}</Text.Headline>
                    <div className="order-panel">
                        <CollapsiblePanel
                            isClosed={summaryPanel}
                            onToggle={() => setSummaryPanel(!summaryPanel)}
                            header="Order summary"
                        >
                            <div className="order-grid">
                                <Grid
                                    gridGap="16px"
                                    gridAutoColumns="1fr"
                                    gridTemplateColumns="repeat(2, 1fr)"
                                >
                                    <Grid.Item>
                                        <div className="customer">
                                            <div><b>Order Tag : </b>&nbsp;&nbsp;-</div>

                                            <div className="order-customer"><b>Store :</b> {tableData1.store?.key}</div>

                                            <div className="order-customer"><b>Order Status :</b> {tableData1.orderState}</div>

                                            <div className="order-customer"><b>Payment Status : </b>
                                            {tableData1.paymentInfo?.payments[0].obj.paymentStatus.interfaceText || tableData1.paymentInfo?.payments[0]?.obj?.transactions[0]?.state}
                                            </div>

                                            <div className="order-customer"><b>Purchased from :</b>{channelValue} </div>
                                            {giftValue?
                                            <div style={{width: "50%", textAlign: "center", marginTop: "5px"}}><Stamp tone="positive">
                                            <Text.Detail as="h2">This is a gift order</Text.Detail>
                                          </Stamp></div>
                                            :null
                                            }
                                            
                                        </div>
                                    </Grid.Item>
                                    <Grid.Item>
                                        <div className="order-detail-card">
                                            <Card>
                                                <Grid
                                                    gridGap="16px"
                                                    gridAutoColumns="1fr"
                                                    gridTemplateColumns="repeat(2, 1fr)"
                                                >
                                                    <Grid.Item>
                                                        <div><b>Subtotal</b> </div>
                                                        <div><b>{countryCode == 'US' ? 'Tax' : 'VAT (included in SubTotal)'}</b>
                                                        
                                                         </div>
                                                        <div><b>Shipping</b>
                                                      
                                                         </div>
                                                        <div><b>Handling cost</b>
                                                       
                                                         </div>
                                                        <div><b>Discounts</b>
                                                       
                                                        </div>
                                                        <div><b>Order total</b> 
                                                       
                                                        </div>
                                                    </Grid.Item>
                                                   
                                                    <Grid.Item>
                                                        <div> {centsToDollars(tableData1.custom?.fields.subTotal)}
                                                        </div>
                                                        <div >{centsToDollars(tableData1.custom?.fields.totalTax)}</div>
                                                        <div>{centsToDollars(tableData1.custom?.fields.shippingCost)}</div>
                                                        <div>{centsToDollars(tableData1.custom?.fields.handlingCost)}</div>
                                                        <div>{centsToDollars(tableData1.custom?.fields.totalDiscountPrice)}</div>
                                                        <div>{centsToDollars(tableData1.custom?.fields.orderTotal)}</div>
                                                    </Grid.Item>
                                                </Grid>
                                            </Card>
                                        </div>
                                    </Grid.Item>
                                </Grid>
                            </div>
                        </CollapsiblePanel>
                    </div>
                   <div className="order-panel">
                        <CollapsiblePanel
                            isClosed={customerPanel}
                            onToggle={() => setCustomerPanel(!customerPanel)}
                            header="Customer"
                        >
                            <div className="customer">
                            <Grid
                                        gridGap="16px"
                                        gridAutoColumns="1fr"
                                        gridTemplateColumns="repeat(2, 1fr)"
                                    >
                                <Grid.Item>
                               
                                <label>
                                <b>{giftValue? 'Gift purchaser email:'  : 'Customer email:' }</b>  {tableData1.customerEmail} 
                                </label> 
                               <br />
                                {giftValue? <label> <b>Gift recipient email:  </b> {giftEmail}<br/></label>: null}  
             
                                
                                <label><b>Customer name :</b> {tableData1.customerFirstName} {tableData1.customerLastName}</label>
                                <br />
                                <label><b>Customer group :</b> {tableData1.customerGroup && tableData1.customerGroup.obj && tableData1.customerGroup.obj ? tableData1.customerGroup.obj.name : ''}</label>
                                <br />
                                <label><b>Po Number :</b> {tableData1.custom?.fields.poNumber}</label>
                                <br />
                                <br/>
                                </Grid.Item>
                                
                                {giftValue?
                                    <Grid.Item>
                                    <label><b> Send Gift Receipt :</b> <br />
                                    <SecondaryButton 
                                    label="Send Mail" onClick={() => alert('Button clicked')} />
                                    </label>
                                    <br /><br/>
                                    <label><b>Gift Recipient Message :</b> <br />
                                    
                                    <MultilineTextInput value={message}
                                    horizontalConstraint={12} isReadOnly="true" isDisabled="true"
                                    onChange={(event) => setRichText(event.target.value)} /> 
                                    </label>
                                    <br />
                                    <br />
                                    </Grid.Item>    
                                :
                                null
                                }
                                                            
                                
                            </Grid>

                                <div className="addresses">
                                    <Grid
                                        gridGap="16px"
                                        gridAutoColumns="1fr"
                                        gridTemplateColumns="repeat(3, 1fr)"
                                    >
                                        <Grid.Item>
                                            <div className="order-details-address">
                                                <div className="address-icon"><PaperBillInvertedIcon /></div>
                                                <Text.Headline as="h4">Billing address</Text.Headline>
                                            </div>
                                            <ul className="address-list">
                                                {/* <li><b>Company Name:</b>&nbsp;&nbsp;{billingAddress?.company} </li> */}
                                                <li><b>Phone Number:</b> &nbsp;&nbsp;{billingAddress?.phone} </li>
                                                <li><b>First name:</b>&nbsp;&nbsp;{billingAddress?.firstName}</li>
                                                <li><b>Last name:</b>&nbsp;&nbsp;{billingAddress?.lastName}</li>
                                                <li><b>Street address 1:</b>&nbsp;&nbsp;{billingAddress?.streetName}</li>
                                                <li><b>Street address 2:</b>&nbsp;&nbsp;</li>
                                                <li><b>City:</b>&nbsp;&nbsp;{billingAddress?.city}</li>

                                               { countryName === 'US' ? <li><b>State:</b>&nbsp;&nbsp;{billingAddress?.state}</li> : null}

                                                <li><b>Postal code:</b>&nbsp;&nbsp;{billingAddress?.postalCode}</li>
                                                <li><b>Country:</b>&nbsp;&nbsp;{billingAddress?.country}</li>
                                            </ul>
                                        </Grid.Item>
                                        <Grid.Item>
                                            <div className="order-details-address">
                                                <div className="address-icon"><TruckIcon /></div>
                                                <Text.Headline as="h4">
                                                    {
                                                        giftValue?
                                                        'Recipient’s Address'
                                                        :
                                                        'Shipping Address'
                                                    }
                                                </Text.Headline>
                                            </div>
                                            <ul className="address-list">
                                                <li><b>Company Name:</b>&nbsp;&nbsp;{shippingAddress?.company}</li>
                                                <li><b>Phone Number:</b>&nbsp;&nbsp;{shippingAddress?.phone}</li>
                                                <li><b>First name:</b>&nbsp;&nbsp;{shippingAddress?.firstName}</li>
                                                <li><b>Last name:</b>&nbsp;&nbsp;{shippingAddress?.lastName}</li>
                                                <li><b>Street address 1:</b>&nbsp;&nbsp;{shippingAddress?.streetName}</li>
                                                <li><b>Street address 2:</b>&nbsp;&nbsp;</li>
                                                <li><b>City:</b>&nbsp;&nbsp;{shippingAddress?.city}</li>
                                                { countryName ==='US' ? <li><b>State:</b>&nbsp;&nbsp;{shippingAddress?.state}</li>: null}
                                                <li><b>Postal code:</b>&nbsp;&nbsp;{shippingAddress?.postalCode}</li>
                                                <li><b>Country:</b>&nbsp;&nbsp;{shippingAddress?.country}</li>
                                            </ul>
                                        </Grid.Item>
                                    </Grid>
                                </div>
                                <div>
                                </div>
                            </div>
                        </CollapsiblePanel>
                    </div>
                    <div className="order-panel">
                        <CollapsiblePanel
                            isClosed={orderPanel}
                            onToggle={() => setOrderPanel(!orderPanel)}
                            header="Order items"
                        >
                            <div className="order-table">
                                <DataTableManager columns={columns}>
                                    <DataTable
                                        rows={itemsList}
                                    />
                                </DataTableManager>
                            </div>
                        </CollapsiblePanel>
                    </div>
                                            
                        <div className="order-panel">
                        {
                            countryName === 'US' ?
                            <CollapsiblePanel
                            isClosed={subPanel}
                            onToggle={() => setSubPanel(!subPanel)}
                            // isDisabled={countryName === 'US'? false : true}
                            header="Subscription Information"
                        >
                            
                            
                            <div className="order-table">
                                <DataTableManager columns={columnsSubscription}>
                                    <DataTable
                                        rows={subSkuDeviceId}
                                    />
                                </DataTableManager>
                            </div>
                           
                            
                        </CollapsiblePanel>: null}
                    </div>
                </div>
                : null
            }
        <CustomFormModalPage
          title={"New return info"}
          isOpen={newReturn}
          onClose={handleClose}
          topBarCurrentPathLabel={"New return info"}
          topBarPreviousPathLabel="Back"
        >
         <Return returnData ={tableData} data={tableData1} orderId={orderId} handleReload={handleReload}/> 
        </CustomFormModalPage>
            {paymentTab ? <Payment paymentData={tableData1} billingAddress={billingAddress} /> : null}
            {shippingTab ? <Shipping giftEmail={giftEmail} giftMessage={message}
             giftValue={giftValue} shippingData={tableData1} shippingAddress={shippingAddress}/> : null}
            {invoicesTab ? <Invoice invoiceData={tableData} orderData={tableData1} shippingAddress={shippingAddress}  handleReload={handleReload}/> : null}
            {returnTab ? <Return returnData ={tableData} data={tableData1} orderId={orderId} handleReload={handleReload}/> : null}
            {creditMemoTab ? <CreditMemoTab /> : null}
        </div>
    );

}

export default Order;