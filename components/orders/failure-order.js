import React, { useState,useEffect } from 'react';
import { Header } from '../../common/Header';
import { addHyperLink } from '../../utils';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import { SERVICES } from '../../config/api';
import LoadingSpinner from '@commercetools-uikit/loading-spinner';
import { useIntl } from 'react-intl';
import { Pagination } from '@commercetools-uikit/pagination';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import CheckboxInput from '@commercetools-uikit/checkbox-input';
import { useRowSelection, useSorting  } from '@commercetools-uikit/hooks';
import Text from '@commercetools-uikit/text';
import { FilterIcon,CloseBoldIcon} from '@commercetools-uikit/icons';
import Spacings from '@commercetools-uikit/spacings';
import SelectInput from '@commercetools-uikit/select-input';
import PrimaryButton from '@commercetools-uikit/primary-button';
import { ContentNotification } from '@commercetools-uikit/notifications';
import FlatButton from '@commercetools-uikit/flat-button';
import { CallApi } from '../../plugin/Axios';
import axios from "axios";
import { ProjectKey } from '../../config/api';


const OrderProcess = (props) => {

    const [pageLimit, setPageLimit] = useState(20);
    const [pageNumber, setPageNumber] = useState(1);
    const [totalResults, setTotalResults] = useState(0);
    const [spinner, setSpinner] = useState(true);
    const [orderFailure,setOrderFailure] = useState([])
    const { formatTime, formatDate } = useIntl();
    const [isChecked,setIsChecked] = useState(false)
    const [arrIds, setArrIds] = useState([]);
    const [array, setArrayLength] = useState(0); 
    const [isClicked, setIsClicked] = useState(false);
    const[filterCustomer,setFilterCustomer] = useState('')
    const [customerDetails, setCustomerDetails] = useState(true);
    const[reload,setReload] = useState(false)
    const[retried,setRetried] = useState(false)
    const [notificationCheckbox , setNotificationCheckbox] =  useState(false); 
    const [notificationMessageCheckbox, setNotificationMessageCheckbox] = useState('Updated');
    const [errorNotificationCheckbox , setErrorNotificationCheckbox] =  useState(false);
    const [errorMessageSearchCheckbox , setErrorMessageSearchCheckbox] =  useState('');
    const [_arrIds, set_ArrIds] = useState([]);

   
    useEffect(() => {
        let offset= (pageNumber - 1) * pageLimit;
        if(totalResults && totalResults < offset){
            setPageNumber(0);
            return 
        }
        setSpinner(true);
        let url = `${SERVICES.ORDER}/api/v1/orders/reprocess?status=${filterCustomer}&limit=${pageLimit}&offset=${offset}`
        CallApi(url,'get')
        .then((res) => {
            console.log(res,"response")
            setOrderFailure(res.data)
            setTotalResults(res.data || 20);
        })
        .then(res =>
            setSpinner(false)
            )
            .catch(error => {
                console.log("error", error)
                setSpinner(false)
              })
    },[pageLimit,pageNumber,filterCustomer,reload])
    const otherStatus = orderFailure.filter(items => items.status === 'FAILED' || items.status === 'RETRY' )
    let count = 0
    otherStatus.map((data) =>{
      if(data){
        count ++ 
      }
    })
   
    const showrowId = (item) => {
      const orderIndexInArray = arrIds.indexOf(item);
              if(orderIndexInArray > -1 ) {
              arrIds.splice(orderIndexInArray, 1);
              } else {
              arrIds.push(item);
              }
              if(arrIds.length === 0)
              {
                setArrayLength(0)
              }
              setArrayLength(arrIds.length)

              console.log(arrIds,"ids")
    }
    const reloadPage = () => {
      setTimeout(() => {
        window.location.reload()
    }, 3000);}
    const columns = [ 
      { key: 'checkbox',
        label: (
         <CheckboxInput
           isChecked={isChecked}
           onChange={(event) => {setIsChecked(event.target.checked)
            setRetried(!retried)
            let _arrayId = []
            if(event.target.checked) {
              orderFailureList.map((arr) => {
                if(arr.status === 'FAILED'){
                  _arrayId.push(arr.ORDER_NUMBER);
                }
              })
            } 
            setArrIds(_arrayId);
          }
         }
        />
      ),
      renderItem: (orderFailureList) => (
        <CheckboxInput
          isChecked={arrIds.indexOf(orderFailureList.ORDER_NUMBER) > -1}
          onChange={(event) => {
            toggleRow(orderFailureList.ORDER_NUMBER)
            showrowId(orderFailureList.ORDER_NUMBER)
            setRetried(!retried)
            if(!event.target.checked){
              let orderIndex = arrIds.indexOf(orderFailureList.ORDER_NUMBER)
              if(orderIndex > -1){
                arrIds.splice(orderIndex, 1);
              }
            } else {
              let orderIndex = arrIds.indexOf(orderFailureList.ORDER_NUMBER)
              if(orderIndex === -1){
                arrIds.push(orderFailureList.ORDER_NUMBER)
              }
            }            
            setArrIds(arrIds);
          }} /> 
      ),disableResizing: true},    
        { key: 'ORDER_NUMBER', label: 'OrderNumber',shouldIgnoreRowClick: true  },
        { key: 'DATE_CREATED', label: 'Date created', shouldIgnoreRowClick: true  },
        { key: 'DATE_MODIFIED', label: 'Date modified',shouldIgnoreRowClick: true },
        { key: 'status', label: 'Status', shouldIgnoreRowClick: true },
     ] 
     let orderFailureList = []
     {otherStatus.length > 0 ? 
     otherStatus.map((items) => {
       const createdOn = formatDate(otherStatus.createdAt) + " " + formatTime(otherStatus.createdAt)
       const modifiedOn = formatDate(otherStatus.lastModifiedAt) + " " + formatTime(otherStatus.lastModifiedAt)
       const orderFailureData = {
           ORDER_NUMBER :items.orderNumber,
           DATE_CREATED : createdOn,
           DATE_MODIFIED: modifiedOn,
           status:items.status
       }
       orderFailureList = [...orderFailureList,orderFailureData]
   },[]) : null } 
 const {
    rows: rowsWithSelection,
    toggleRow,
    selectAllRows,
    deselectAllRows,
    getIsRowSelected, 
    getNumberOfSelectedRows,
  } = useRowSelection('checkbox', orderFailureList);
  
  const countSelectedRows = getNumberOfSelectedRows();
   const handleSelectAll =
    countSelectedRows === 0 ? selectAllRows : deselectAllRows;

    const filterChange = (e) => {
      let Value = e.target.value
      setFilterCustomer(Value)
      if (Value === "failed") {
        setCustomerDetails(true);
      } else {
        setCustomerDetails(true); 
      }
    }
   const handleRetryButton = () => {
        setReload(!reload)
        const retryButton =
        {
            "Order Id": arrIds,
            "status": "FAILED",
        }
        console.log(_arrIds,"_aarr")
        let url = `${SERVICES.ORDER}api/v1/orders/reprocess?orderNumberList=${arrIds}&status=RETRY`
        let code = 'US';
        if (ProjectKey && (ProjectKey === 'mo-dev-eu-1' || ProjectKey === 'mo-preprod1-eu' || ProjectKey === 'mo-prod-eu')) {
        code = 'EU';
        }
        console.log(url);
        // CallApi(url,'post',retryButton)
        axios.post(url, retryButton, {headers: {country: code}})
          .then(res => {
            console.log(res)
            if(retried){
                setErrorNotificationCheckbox(false)
                setNotificationCheckbox(true);
                setNotificationMessageCheckbox('Successfully Updated the order'); 
                reloadPage()
              }
            else{
                setErrorNotificationCheckbox(true)
                setErrorMessageSearchCheckbox("No order selected")
                }        
            })
              .catch(error => {
                 setErrorMessageSearchCheckbox(error.message)
                 setErrorNotificationCheckbox(true)
                 setNotificationCheckbox(false);
        })   
      }     
    return(
        <div className="failure-order box-style">
               <Header
                 title={'Failover Handling Approach'}
                 results={count}
               />
               <div style={{marginTop:'10px'}}>
               <SecondaryButton
                    iconLeft={<FilterIcon />}
                    label="Filter"
                    onClick={() => setIsClicked(!isClicked)}
              />
               </div>
               <div className="notification">
            { notificationCheckbox ?
            <div>
            <ContentNotification type="success"> {notificationMessageCheckbox} </ContentNotification>
            </div>
            :null
            }
            { errorNotificationCheckbox ?
            <div>
                <ContentNotification type="error"> {errorMessageSearchCheckbox} </ContentNotification>
              </div>  
            :null
            }
            </div>
               {
                isClicked ?
                <div>
                 <div className="input-select">
                   <Spacings.Stack scale="xs">
                     <SelectInput
                       placeholder="Select Status"
                       name="form-field-name"
                       horizontalConstraint={7}
                       onChange={(e) => { 
                        filterChange(e)
                      }}
                      value={filterCustomer}
                      options={[{ value: 'FAILED', label: 'FAILED' },
                                 { value: 'RETRY', label: 'RETRY' },
                     ]}
                    />
                  </Spacings.Stack>
                </div>
                <div style={{ alignSelf:'flex-end',display: 'flex',flex: '2', justifyContent: 'flex-end', paddingRight: '20px'}}>
                <Spacings.Inline scale="xs" >
                 <div>
                   <FlatButton 
                      tone='secondary'
                      size="medium"
                      icon={<CloseBoldIcon />}
                      label='Clear all'
                      onClick={() => {
                          setIsClicked(false)
                          setFilterCustomer('')   
                      }}
                   />  
                </div> 
              </Spacings.Inline>
                </div>
                </div>
                : null}
               <div style={{marginTop:'20px'}}>
                   <PrimaryButton  label='Update to Retry' onClick={handleRetryButton}/>
               </div>
               <div style={{marginTop:'22px'}}>
               <Text.Headline as="h4">{` You have selected ${arrIds.length} orders.`}</Text.Headline>
               </div>
               {customerDetails ? 
               <div style={{marginBottom:'20px'}}>
               <DataTableManager columns={columns}>
                 <DataTable
                    rows={orderFailureList}
                 />
               </DataTableManager> 
                </div>: null }
                {spinner ? <div className="spinner">
               <LoadingSpinner size="s">Loading</LoadingSpinner></div> : null}
               {/* {!spinner ?
                  <Pagination
                    totalItems={count}
                    page={pageNumber}
                    perPageRange="m"
                    perPage={pageLimit}
                    onPageChange={(page) => {
                        console.log(page)
                        setPageNumber(page)
                    }}
                    onPerPageChange={(lmt) => {
                        console.log(lmt)
                        setPageNumber(lmt)
                        setPageLimit(lmt);
                    }}
             /> 
            : null} */}
        </div>
    )

}
export default OrderProcess
