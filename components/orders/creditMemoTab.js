import React from "react";
import { Table } from "../../common/Table";
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';

const CreditMemoTab = () => {

    const columns = [
        { key: 'creditMemo', label: 'Credit Memo'},
        { key: 'created', label: 'Created' },
        { key: 'orderNumber', label: 'Order' },
        { key: 'orderDate', label: 'Order Date' },
        { key: 'products', label: 'Products' },
        { key: 'status', label: 'Status' },
        { key: 'refunded', label: 'Refunded' },
        { key: 'Action', label: 'Action' },
    ];
    const creditMemoList =[]
    return (
        <div>
            <DataTableManager columns={columns}>
                <DataTable
                    rows={creditMemoList}
                />
            </DataTableManager>
        </div>
    );
}

export default CreditMemoTab;