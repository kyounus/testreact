import React, { useState } from "react";
import Card from '@commercetools-uikit/card'
import Grid from '@commercetools-uikit/grid';
import CollapsiblePanel from '@commercetools-uikit/collapsible-panel';
import "./payment.css";
import { PaperBillInvertedIcon } from '@commercetools-uikit/icons';
import Text from '@commercetools-uikit/text';
import { centsToDollars }from "../../config/api";

const Payment = (props) => {
    const paymentInfo = props.paymentData.paymentInfo
    const payment = paymentInfo?.payments[paymentInfo.payments.length-1].obj;
    const billingAddress = props.billingAddress
    const paymentMethod = payment?.paymentMethodInfo?.method
    var flag=payment?.custom?.fields.captured
    var flag1=payment?.custom?.fields.refunded
    var LinkToPayView=payment?.custom?.fields.paymentView
    var LinkToCustView=payment?.custom?.fields.customerView 
   
    return (
        <div className="payment-panel">
            <CollapsiblePanel
                header="Payment"
            >
               {paymentMethod == "CREDIT_CARD" ?  
                <div className="order-payment-card">
                    <Card>
                        <Grid
                            gridGap="16px"
                            gridAutoColumns="1fr"
                            gridTemplateColumns="repeat(4, 1fr)"
                        >
                            <Grid.Item>
                                <div><b>Payment method</b></div>
                                <div className="payment-data"><b>Payment service provider</b></div>
                                <div className="payment-data"><b>Payment provider ID</b></div>
                                <div className="payment-data"><b>Amount planned</b></div>
                                <div className="payment-data"><b>Payment status</b></div>
                                <div className="payment-data"><b>Card</b></div>
                                <div className="payment-data"><b>Expires</b></div>
                                <div className="payment-data"><b>Charge</b></div>
                                <div className="payment-data"><b>Radar risk</b></div>
                                <div className="payment-data"><b>Street check</b></div>
                                <div className="payment-data"><b>Zip check</b></div>
                                <div className="payment-data"><b>CVC check</b></div>
                                <div className="payment-data"><b>Captured</b></div>
                                <div className="payment-data"><b>Refunded</b></div>
                                <div className="payment-data"><b>Payment</b></div>
                                <div className="payment-data"><b>Customer</b></div>
                            </Grid.Item>
                            <Grid.Item>
                                <div>{payment.paymentMethodInfo.method}</div>
                                <div className="payment-data">{payment.paymentMethodInfo.paymentInterface}</div>
                                <div className="payment-data">{payment.interfaceId}</div>
                                <div className="payment-data">{centsToDollars(payment.amountPlanned.centAmount)}</div>
                                <div className="payment-data">{payment.paymentStatus.interfaceText}</div>
                                <div className="payment-data">{payment.custom ? (payment.custom?.fields?.brand) + " " + (payment.custom?.fields?.last4) : "" } </div>
                                <div className="payment-data">{payment.custom ? (payment.custom?.fields?.expMonth + "/" +  payment.custom?.fields.expYear) : "" } </div>
                                <div className="payment-data">{payment.custom ? (payment.custom?.fields?.charge) : "-" } </div>
                                <div className="payment-data">{payment.custom?.fields?.radarRisk ? (payment.custom?.fields?.radarRisk  === "" ? "-" :payment.custom?.fields?.radarRisk) : "-" }</div>
                                <div className="payment-data">{payment.custom ? (payment.custom?.fields?.streetCheck  === "" ? "-" :payment.custom?.fields?.streetCheck) : "-" }</div>
                                <div className="payment-data">{payment.custom ? (payment.custom?.fields?.zipCheck === "" ? "-" :payment.custom?.fields?.zipCheck) : "-" }</div>
                                <div className="payment-data">{payment.custom?.fields?.cvcCheck ? (payment.custom?.fields?.cvcCheck) : "-" }</div>
                                <div className="payment-data">{payment.custom ? (flag.toString()) : "-" }</div>
                                <div className="payment-data">{payment.custom ? (flag1.toString()) : "-" }</div>
                                <div className="payment-data">{payment.custom?.fields?.paymentView ? <a href={LinkToPayView} target="_blank">view in stripe</a>: "-" }</div>
                                <div className="payment-data">{payment.custom?.fields?.customerView ? <a href ={LinkToCustView} target="_blank">view in stripe</a>: "-" }</div>
                            </Grid.Item>
                        </Grid>
                    </Card>
                </div>
                : null} 

                {/* AFFIRM */}

                {paymentMethod == "AFFIRM" ?  
                <div className="order-payment-card">
                    <Card>
                        <Grid
                            gridGap="16px"
                            gridAutoColumns="1fr"
                            gridTemplateColumns="repeat(4, 1fr)"
                        >
                            <Grid.Item>
                                <div><b>Payment method</b></div>
                                <div className="payment-data"><b>Payment service provider</b></div>
                                <div className="payment-data"><b>Payment provider ID</b></div>
                                <div className="payment-data"><b>Amount planned</b></div>
                                <div className="payment-data"><b>Payment status</b></div>
 
                            </Grid.Item>
                            <Grid.Item>
                                <div>Affirm</div>
                                <div className="payment-data">Affirm</div>
                                <div className="payment-data"> - </div>
                                <div className="payment-data">{centsToDollars(payment.amountPlanned.centAmount)}</div>
                                <div className="payment-data">{payment.transactions[0].state}</div>
                                
                            </Grid.Item>
                        </Grid>
                    </Card>
                </div>
                : null} 


                {/* AFFIRM PAYMENTS ENDS */}


              
                 {paymentMethod == "PAYMENTTERMS" ?
                 <div className="order-payment-card">
                     <Card>
                        <Grid
                            gridGap="16px"
                            gridAutoColumns="1fr"
                            gridTemplateColumns="repeat(4, 1fr)"
                        >
                            <Grid.Item>
                               <div><b>Payment method</b>:&nbsp;&nbsp;{payment.paymentMethodInfo.name.en}</div>
                               <div className="order-details-address">
                                                <div className="address-icon"><PaperBillInvertedIcon /></div>
                                                <Text.Headline as="h4">Billing address</Text.Headline>
                                </div>
                               <ul className="address-list">
                                               <li><b>Company Name:</b> </li>
                                               <li><b>Phone Number:</b> &nbsp;&nbsp;{billingAddress?.phone} </li>
                                               <li><b>First name:</b>&nbsp;&nbsp;{billingAddress?.firstName}</li>
                                               <li><b>Last name:</b>&nbsp;&nbsp;{billingAddress?.lastName}</li>
                                               <li><b>Street address 1:</b>&nbsp;&nbsp;{billingAddress?.streetName}</li>
                                               <li><b>Street address 2:</b>&nbsp;&nbsp;</li>
                                               <li><b>City:</b>&nbsp;&nbsp;{billingAddress?.city}</li>
                                               <li><b>State:</b>&nbsp;&nbsp;{billingAddress?.state}</li>
                                               <li><b>Postal code:</b>&nbsp;&nbsp;{billingAddress?.postalCode}</li>
                                               <li><b>Country:</b>&nbsp;&nbsp;{billingAddress?.country}</li>
                                </ul>
                            </Grid.Item>
                            <Grid.Item>                        
                             </Grid.Item>
                         </Grid>
                     </Card>
                 </div>
                 :null}
                  {paymentMethod == "ACH" ?
                 <div className="order-payment-card">
                     <Card>
                        <Grid
                            gridGap="16px"
                            gridAutoColumns="1fr"
                            gridTemplateColumns="repeat(4, 1fr)"
                        >
                            <Grid.Item>
                               <div><b>Payment method</b>:&nbsp;&nbsp;{payment.paymentMethodInfo.method}</div>
                               <div className="order-details-address">
                                                <div className="address-icon"><PaperBillInvertedIcon /></div>
                                                <Text.Headline as="h4">Billing address</Text.Headline>
                                </div>
                               <ul className="address-list">
                                                 <li><b>Company Name:</b> </li>
                                                 <li><b>Phone Number:</b> &nbsp;&nbsp;{billingAddress?.phone} </li>
                                                 <li><b>First name:</b>&nbsp;&nbsp;{billingAddress?.firstName}</li>
                                                 <li><b>Last name:</b>&nbsp;&nbsp;{billingAddress?.lastName}</li>
                                                 <li><b>Street address 1:</b>&nbsp;&nbsp;{billingAddress?.streetName}</li>
                                                 <li><b>Street address 2:</b>&nbsp;&nbsp;</li>
                                                 <li><b>City:</b>&nbsp;&nbsp;{billingAddress?.city}</li>
                                                 <li><b>State:</b>&nbsp;&nbsp;{billingAddress?.state}</li>
                                                 <li><b>Postal code:</b>&nbsp;&nbsp;{billingAddress?.postalCode}</li>
                                                 <li><b>Country:</b>&nbsp;&nbsp;{billingAddress?.country}</li>
                                </ul>
                             </Grid.Item>
                             <Grid.Item> 
                             </Grid.Item>
                         </Grid>
                     </Card>
                 </div>
                 :null}
            </CollapsiblePanel>
        </div>
    );
}
export default Payment;