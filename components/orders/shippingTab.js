import React, { useState } from "react";
import Card from '@commercetools-uikit/card'
import Grid from '@commercetools-uikit/grid';
import CollapsiblePanel from '@commercetools-uikit/collapsible-panel';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import "./shippingTab.css";
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import { centsToDollars }from "../../config/api";
import { TruckIcon } from '@commercetools-uikit/icons';
import Text from '@commercetools-uikit/text';
import MultilineTextInput from '@commercetools-uikit/multiline-text-input'
import "./order-details.css";
import Spacings from '@commercetools-uikit/spacings';
import TextInput from '@commercetools-uikit/text-input'

const Shipping = (props) => {
    // console.log(props, "props")
   
    const [summaryPanel, setSummaryPanel] = useState(false);
    const [shippingPanel, setShippingPanel] = useState(false);
    // const [gift, setGift] = useState(false)
    const shippingData = props.shippingData
    const shippingAddress = props.shippingAddress
    const shippingMethod = props.shippingData.shippingInfo;
    const gift = props.shippingData.custom.fields.gift;
    const giftEmail = props.giftEmail
    const giftMessage = props.giftMessage
    console.log(giftEmail,"Email")
    console.log(giftMessage, "Message");
    // console.log(gift, "Gift")
    
    const columns = [
        { key: 'shipmentNumber', label: 'Shipment number' },
        { key: 'carrierInformation', label: 'Carrier information' },
        { key: 'shipmentStatus', label: 'Shipment status' },
        { key: 'shippingCharges', label: 'Shipping charges' },
        { key: 'tracking', label: 'Tracking' },
    ];
     
    let deliveries = []
       const trackingId = shippingMethod?.deliveries[0]?.parcels[0].trackingData.trackingId;
            const addressDetails = {
                shippingCharges : centsToDollars(shippingMethod?.shippingRate.price.centAmount),
                tracking : trackingId,
                shipmentStatus : shippingData?.state?.obj?.name.en
            }
            deliveries = [...deliveries, addressDetails]
        const trackingStatus = trackingId ? false : true
    const handleTrackOrder = () => {
        window.open(`https://www.fedex.com/fedextrack/no-results-found?trknbr=${trackingId}`)
    }
    return (
        <div>
            <div className="order-panel">
                <CollapsiblePanel
                    isClosed={summaryPanel}
                    onToggle={() => setSummaryPanel(!summaryPanel)}
                    header="Order summary"
                >

                    <div className="order-grid">
                        <Grid
                            gridGap="16px"
                            gridAutoColumns="1fr"
                            gridTemplateColumns="repeat(3, 1fr)"
                        >
                            <Grid.Item>
                                <div className="customer">
                                    <div className="order-customer"><b>Purchased from :</b> {shippingData.store?.key}</div>
                                    <div className="order-customer"><b>Order Status :</b> {shippingData.orderState}</div>
                                    <div className="order-customer"><b>Order status Reason code :</b> </div>
                                    <div className="order-customer"><b>Payment Status : </b>{shippingData.paymentInfo?.payments[0].obj.paymentStatus.interfaceText}
                                    </div>
                                </div>
                            </Grid.Item>

                            <Grid.Item>
                                <div className="order-detail-card">
                                    <Card>
                                        <Grid
                                            gridGap="16px"
                                            gridAutoColumns="1fr"
                                            gridTemplateColumns="repeat(3, 1fr)"
                                        >
                                            <Grid.Item>
                                                <div><b>Subtotal</b></div>
                                                <div><b>Tax</b> </div>
                                                <div><b>Shipping</b> </div>
                                                <div><b>Discounts</b></div>
                                                <div><b>Order total</b> </div>
                                            </Grid.Item>
                                            <Grid.Item>
                                            </Grid.Item>
                                            <Grid.Item>
                                                <div> {centsToDollars(shippingData.custom?.fields.subTotal)}</div>
                                                <div >{centsToDollars(shippingData.custom?.fields.totalTax)}</div>
                                                <div>{centsToDollars(shippingData.custom?.fields.shippingCost)}</div>
                                                <div>{centsToDollars(shippingData.custom?.fields.totalDiscountPrice)}</div>
                                                <div>{centsToDollars(shippingData.custom?.fields.orderTotal)}</div>
                                            </Grid.Item>
                                        </Grid>
                                    </Card>
                                </div>
                            </Grid.Item>
                        </Grid>
                    </div>
                </CollapsiblePanel>
            </div>
            <div className="order-panel">
                <CollapsiblePanel
                    isClosed={shippingPanel}
                    onToggle={() => setShippingPanel(!shippingPanel)}
                    header="Shipping details"
                >
                                    
                    <div className="shipping-addresses">
                        <Grid
                            gridGap="16px"
                            gridAutoColumns="1fr"
                            gridTemplateColumns="repeat(3, 1fr)"
                        >
                            <Grid.Item>
                            
                            <div style={{marginTop: "10px"}}>
                                         <Spacings.Inline scale="xs">
                                         <TruckIcon />
                                        <Text.Headline as="h4">
                                             {
                                                 gift?
                                                 'Recipient Address'
                                                 :
                                                 'Shipping Address'
                                                
                                             }
                                        </Text.Headline>
                                             </Spacings.Inline>  
                                         
                                    </div>
                            <ul className="address-list">
                                        <li><b>Company Name:</b>&nbsp;&nbsp;{shippingAddress.company}</li>
                                        <li><b>Phone Number:</b>&nbsp;&nbsp;{shippingAddress.phone}</li>
                                        <li><b>First name:</b>&nbsp;&nbsp;{shippingAddress.firstName}</li>
                                        <li><b>Last name:</b>&nbsp;&nbsp;{shippingAddress.lastName}</li>
                                        <li><b>Street address 1:</b>&nbsp;&nbsp;{shippingAddress.streetName}</li>
                                        <li><b>Street address 2:</b>&nbsp;&nbsp;</li>
                                        <li><b>City:</b>&nbsp;&nbsp;{shippingAddress.city}</li>
                                       { shippingAddress.country === 'US'?
                                       <li><b>State:</b>&nbsp;&nbsp;{shippingAddress.state}</li>
                                       :
                                       null
                                       }
                                        <li><b>Postal code:</b>&nbsp;&nbsp;{shippingAddress.postalCode}</li>
                                        <li><b>Country:</b>&nbsp;&nbsp;{shippingAddress.country}</li>
                                        <br />
                                </ul>
                               
                            </Grid.Item>
                            {
                                gift?
                                <Grid.Item>
                                <ul className="address-list">
                                <li><b>Gift Recipient Email</b> <br/>
                                <TextInput value={giftEmail} isDisabled onChange={(event) => alert(event.target.value)} />
                                        </li><br />
                                        <li> <b>Gift Recipient Message :</b>
                                        <MultilineTextInput value={giftMessage}
                                        horizontalConstraint={12} isReadOnly="true" isDisabled="true"
                                        onChange={(event) => console.log(event.target.value)} />
                                        </li>
                                </ul>
                            </Grid.Item>
                            :
                            null
                            }
                            
                        </Grid>
                    </div>
                </CollapsiblePanel>
            </div>
            <div className="order-panel">
                <CollapsiblePanel
                    isClosed={shippingPanel}
                    onToggle={() => setShippingPanel(!shippingPanel)}
                    header="Shipping method"
                >
                    <div className="shipping-addresses">
                        <Grid
                            gridGap="16px"
                            gridAutoColumns="1fr"
                            gridTemplateColumns="repeat(3, 1fr)"
                        >
                            <Grid.Item>
                                <div>
                                <ul className="address-list">
                                        <li><b>Shipping method:</b>&nbsp;&nbsp;{shippingMethod?.shippingMethodName}</li>   
                                        <li><b>Tracking Number: </b> {trackingId}</li>
                                    </ul>
                                    <SecondaryButton label="Track Order" style={{marginBottom: "10px"}}
                                        isDisabled ={trackingStatus}
                                        onClick={handleTrackOrder} />
                                {shippingData.additionalFreightInfos ? 
                                <>
                                <div className="freight_info_order"><b>Additional freight information :</b></div>
                                    <ul className="address-list">
                                        <li><b>Loading dock:</b>&nbsp;&nbsp;{(shippingData?.additionalFreightInfos?.loadingDock).toString()}</li>
                                        <li><b>Delivery appointment:</b>&nbsp;&nbsp;{(shippingData?.additionalFreightInfos?.deliveryAppointmentRequired).toString()}</li>
                                        <li><b>Has forklift:</b>&nbsp;&nbsp;{(shippingData?.additionalFreightInfos?.forkLift).toString()}</li>
                                        <li><b>Receiving hours:</b>&nbsp;&nbsp;{shippingData?.additionalFreightInfos?.receivingHours.from} - {shippingData?.additionalFreightInfos?.receivingHours.to}</li>   
                                    </ul>
                                    </>
                                    :null}
                                </div>
                            </Grid.Item>
                        </Grid>
                    </div>
                </CollapsiblePanel> 
            </div>
            <div className="order-panel">
                <CollapsiblePanel
                    isClosed={shippingPanel}
                    onToggle={() => setShippingPanel(!shippingPanel)}
                    header="Deliveries"
                >
                    <div className="order-table">
                                <DataTableManager columns={columns}>
                                    <DataTable
                                        rows={deliveries}
                                    />
                                </DataTableManager>
                            </div>
                </CollapsiblePanel>
            </div>
        </div>
    );
}
export default Shipping;


