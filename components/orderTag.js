import React, { useState } from 'react';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import TextInput from '@commercetools-uikit/text-input';
import CheckboxInput from '@commercetools-uikit/checkbox-input';

const OrderTag = () =>{
    const [editOrderTag ,setEditOrderTag ] = useState(false)
    const [frontEndDefault, setFrontEndDefault] = useState(false)
    const [zeroDollarOrder, setZeroOrderDollar] = useState(false)

    const handleEditClick = () => {
       setEditOrderTag(!editOrderTag)
       
    }
    const handleResponse = (selectedCheckBox) => {
        setFrontEndDefault(false)
        setZeroOrderDollar(false)
        if (selectedCheckBox) {
            setFrontEndDefault(!frontEndDefault)
        }
        else {
            setZeroOrderDollar(!zeroDollarOrder)
        }
    }
    const items = [
        { id: '1 ', label: '', created: '', modified: '', actions: <div> <p onClick={handleEditClick}>Edit</p></div> },
        {id: '2 ', label: '', created: '', modified: '', actions: <div><p onClick={handleEditClick}>Edit</p></div>},
    ];
    const columns = [
        { key: 'id', label: 'ID',align :'center' },
        { key: 'label', label: 'Label',align :'center' },
        { key: 'created', label: 'Created', align :'center'},
        { key: 'modified', label: 'Modified' ,align :'center'},
        { key: 'actions', label: 'Actions',align :'center',} 
    ]
    return(
        <div>
             <DataTableManager columns={columns} 
                   onSettingsChange={(action ,nextValue ) => {}}
             >
                <DataTable rows={items}/>
             </DataTableManager>
              {
                editOrderTag && 
                <div>  
                    <div style={{width:'25%'}}>
                        <div style={{display:'flex'}}>
                        <label><h4>label:<span className="asterisk-color">*</span></h4></label>
                        <TextInput  
                            title=""
                            isReadOnly={false}             
                        />
                        </div>
                    <div><br/>
                    <CheckboxInput
                      value="foo-radio-value"
                      onChange={() => handleResponse(true)}
                      isChecked={frontEndDefault}
                    >Frontend default</CheckboxInput>
                    <CheckboxInput
                      value="foo-radio-value"
                      onChange={() => handleResponse(false)}
                      isChecked={zeroDollarOrder}
                    >Zero dollar order</CheckboxInput>
                    </div>
                    </div>
                </div>
            }
        </div>
    )
}

export default OrderTag
