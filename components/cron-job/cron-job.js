import React, { useState, useEffect } from 'react'
import Grid from '@commercetools-uikit/grid';
import Spacings from '@commercetools-uikit/spacings';
import './cron-job.css';
import axios from "axios";
import Text from '@commercetools-uikit/text';
import TextField from '@commercetools-uikit/text-field';
import SelectField from '@commercetools-uikit/select-field';
import TimeField from '@commercetools-uikit/time-field';
import PrimaryButton from '@commercetools-uikit/primary-button';
import RadioInput from '@commercetools-uikit/radio-input';
import { ContentNotification } from '@commercetools-uikit/notifications';
import { SERVICES } from '../../config/api'

const cronJob = () => {

    const [startTime, setStartTime] = useState('');
    const [endTime, setEndTime] = useState('');
    const [firstRetry, setFirstRetry] = useState('');
    const [secondRetry, setSecondRetry] = useState('');
    const [thirdRetry, setThirdRetry] = useState('');
    const [shippingOption, setShippingOption] = useState('');
    const [shippingPrice, setShippingPrice] = useState(false);
    const [notificationMessage, setNotificationMessage] = useState();
    const [errorMessage , setErrorMessage] =  useState('');
    const [errorNotifiaction , setErrorNotification] =  useState(false);
    const [notification , setNotification] =  useState(false);

    useEffect(() => {

        axios.get(`${SERVICES.ACCOUNT}/api/v1/users/subscriptions/configure`)
        .then(res => {
           const {blackout = {}, "retry-delay": retryDelay, shipping = {}} = res.data.value
         
            setStartTime(blackout.startTime || '')
            setEndTime(blackout.endTime || '')
            setFirstRetry(retryDelay.first || '')
            setSecondRetry(retryDelay.second || '')
            setThirdRetry(retryDelay.third || '')
            setShippingOption(shipping.option.name || '')
            setShippingPrice(shipping.option.flag)
        })
        .catch((error) => {
            console.log('ERROR:', error);
        })
    }, []);

    const onSubmit = async () => {
       let requestData = {
            "blackoutEndTime": endTime,
            "blackoutStartTime": startTime,
            "firstPaymentRetryDays": firstRetry,
            "secondPaymentRetryDays": secondRetry,
            "shippingMethodName": shippingOption,
            "shippingPriceOveride": shippingPrice,
            "thirdPaymentRetryDays": thirdRetry
        }
        axios.post(`${SERVICES.ACCOUNT}/api/v1/users/subscriptions/configure`, requestData)
        .then(res => {
           setNotification(true)
           setNotificationMessage('Updated Successfully');
        })
        .catch((error) => {
            setErrorMessage(error.message)
            setErrorNotification(true)
        })
    }

    return (
        <div className="survey-container">
            <Text.Headline as="h1">{'Cron Configuration'}</Text.Headline>
            <Spacings.Inset scale="m">
                <Spacings.Stack scale="m">
                    <div style={{ paddingTop: '20px'}}>
                        <Grid
                        gridGap="16px"
                        gridAutoColumns="1fr"
                        gridTemplateColumns="repeat(2, 1fr)"
                        >
                            <Grid.Item>
                                <div>
                                    <label className="cron-subTitle">Blackout Start Time</label>
                                    <div>
                                        <TimeField
                                            title=""
                                            value={startTime}
                                            onChange={(event) => setStartTime(event.target.value)}
                                        />
                                    </div>
                                </div>
                                <label>Start time of the window where no subscriptions will be processed.
                                    <br /> Format is HH:MM, in pacific time</label>
                            </Grid.Item>
                            <Grid.Item>
                                <div>
                                    <label className="cron-subTitle">Blackout End Time</label>
                                    <div>
                                        <TimeField
                                            title=""
                                            value={endTime}
                                            onChange={(event) => setEndTime(event.target.value)}
                                        /></div>
                                </div>
                                <label>End time of the window where no subscriptions will be processed.
                                    <br /> Format is HH:MM, in pacific time</label>
                            </Grid.Item>
                            <Grid.Item>
                                <div>
                                    <label className="cron-subTitle">Days Until First Payment Retry</label>
                                    <div>
                                        <TextField
                                            title=""
                                            name="firstRetry"
                                            value={firstRetry}
                                            onChange={(event) => setFirstRetry(event.target.value)} />
                                    </div>
                                </div>
                                <label>Number of days until valid payment is first attempted again</label>
                            </Grid.Item>
                            <Grid.Item>
                                <div>
                                    <label className="cron-subTitle">Days Until Second Payment Retry</label>
                                    <div>
                                        <TextField title=""
                                            name="secondRetry"
                                            value={secondRetry}
                                            onChange={(event) => setSecondRetry(event.target.value)} />
                                    </div>
                                </div>
                            </Grid.Item>
                            <Grid.Item>
                                <div>
                                    <label className="cron-subTitle">Days Until Third Payment Retry</label>
                                    <div>
                                        <TextField title=""
                                            name="thirdRetry"
                                            value={thirdRetry}
                                            onChange={(event) => setThirdRetry(event.target.value)} />
                                    </div>
                                </div>
                            </Grid.Item>
                            <Grid.Item>
                                <div>
                                    <label className="cron-subTitle">Shipping Option</label>
                                    <div>
                                        <SelectField
                                            title=""
                                            name="shippingOption"
                                            value={shippingOption}
                                            options={[
                                                { value: 'Smart Post', label: 'Smart Post' },
                                            ]}
                                            onChange={(event) => setShippingOption(event.target.value)}
                                        />
                                    </div>
                                </div>
                            </Grid.Item>
                            <Grid.Item>
                                <div>
                                    <label className="cron-subTitle">Shipping Price Override</label>
                                    <div style={{ paddingTop: '15px'}}>
                                    <RadioInput.Group
                                        id={'Shipping'}
                                        name={'Shipping Price Override'}
                                        value={shippingPrice}
                                        onChange={() => setShippingPrice(!shippingPrice)}
                                        isDisabled={false}
                                        isReadOnly={false}
                                    >
                                        <RadioInput.Option
                                            value={true}
                                        >
                                            <Spacings.Inline scale="xs" alignItems="center">
                                                {'True'}
                                            </Spacings.Inline>
                                        </RadioInput.Option>
                                        <RadioInput.Option
                                            value={false}
                                        >
                                            <Spacings.Inline scale="xs" alignItems="center">
                                                {'False'}
                                            </Spacings.Inline>
                                        </RadioInput.Option>
                                    </RadioInput.Group>
                                    </div>
                                </div>
                            </Grid.Item>
                        </Grid>
                    </div>
                    <div className="save-button">
                        <PrimaryButton label="Save" onClick={onSubmit} />
                    </div>
                </Spacings.Stack>
            </Spacings.Inset>
            <div className="notification">
                {notification ?
                    <div>
                        <ContentNotification type="success"> {notificationMessage} </ContentNotification>
                    </div>
                    : null
                }
                {errorNotifiaction ?
                    <div>
                        <ContentNotification type="error"> {errorMessage} </ContentNotification>
                    </div>
                    : null
                }
            </div>
        </div>
    )
}

export default cronJob;
