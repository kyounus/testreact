import React, { useState } from 'react';
import Spacings from '@commercetools-uikit/spacings';
import Text from '@commercetools-uikit/text';
import { useIntl } from 'react-intl';
import { FilterOption } from '../../common/Filter';
import { Header } from '../../common/Header';
import { addHyperLink } from '../../utils';
import { Table } from '../../common/Table';
import { SERVICES } from '../../config/api';
import "./company-list.css";

  const CompanyList = (props) => {
  const [companyData, setCompanyData] = useState([]);
  const [companyName, setCompanyName] = useState('');
  const [termStatus, setTermStatus] = useState("");
  const [totalItems, setTotalItems] = useState(0);
  const { formatTime, formatDate } = useIntl();
  const [searchByField, setSearchByField] = useState('');


  let companyList = []
  companyData?.length > 0 ? 
  companyData.map((companyData) => {
    const createdOn = formatDate(companyData.createdAt) + " " + formatTime(companyData.createdAt)
    const modifiedOn = formatDate(companyData.lastModifiedAt) + " " + formatTime(companyData.lastModifiedAt)
    const tempAddress = (companyData.value?.addresses?.length > 0) ? companyData.value?.addresses[0] : "-----"
    let address = <div className="emptyData">-----</div>
    if (tempAddress && tempAddress?.firstName) {
      address = <Spacings.Inline>
        {tempAddress?.firstName && <Text.Body>{tempAddress?.firstName}</Text.Body>}
        {tempAddress?.lastName && <Text.Body>{tempAddress?.lastName}</Text.Body>}
        {tempAddress?.streetAddress1?.street && <Text.Body>{tempAddress?.streetAddress1?.street}</Text.Body>}
        {tempAddress?.streetAddress1?.landMark && <Text.Body>{tempAddress?.streetAddress1?.landMark}</Text.Body>}
        {tempAddress?.city && <Text.Body>{tempAddress?.city}</Text.Body>}
        {tempAddress?.postalCode && <Text.Body>{tempAddress?.postalCode}</Text.Body>}
        {tempAddress?.state && <Text.Body>{tempAddress?.state}</Text.Body>}
      </Spacings.Inline>
    }
    const getPaymentTermStatus = (payments) => {
      let retVal = 'Not Applied';
      if(payments && payments.length) {
        const paymentTermsFetch = payments.filter(item => item.paymentType === 'PAYMENTTERMS');
        if(paymentTermsFetch.length > 0) {
          retVal = paymentTermsFetch[0].termsStatus === 'APPROVED' ? 'Approved' : 'Pending Review';
        }
      }
      return retVal;
    }
    const companyDetails = {
      id: companyData.id,
      name: companyData.value.companyName ?? <div className="emptyData">-----</div>,
      address: address,
      SalesRepresentative: companyData.value.salesRepresentative?.name ?? <div className="emptyData">-----</div>,
      category: companyData.value.companyCategory?.name ?? <div className="emptyData">-----</div>,
      paymentterms: getPaymentTermStatus(companyData.value.payments),
      createdon: createdOn,
      modifiedon: modifiedOn,
    }
    companyList = [...companyList, companyDetails]
  }) : null;

  const columns = [
    { key: 'name', label: 'Name', isSortable: true  },
    { key: 'address', label: 'Address' },
    { key: 'SalesRepresentative', label: 'Sales Representative' },
    { key: 'category', label: 'Category' },
    { key: 'paymentterms', label: 'Payment terms' },
    { key: 'createdon', label: 'Created on' },
    { key: 'modifiedon', label: 'Modified on' },
  ];

  const teamStatusOption = [
    { value: 'PENDINGREVIEW', label: 'Pending Review' },
    { value: 'APPROVED', label: 'Approved' },
    { value: '', label: 'Not Applied' }
  ];

  return (
    <div className="company-list box-style">
      <Spacings.Stack scale="m">
        <Header
          title={'Companies'}
          results={totalItems}
          linkUrl={addHyperLink('companies/new', props)}
          linkLabel={'Add company'}
        />
        <FilterOption
          searchFilter = {(searchValue) => setCompanyName(searchValue)}
          searchSelectOption = {teamStatusOption}
          searchSelectChange = {(status) => setTermStatus(status)}
          searchBoxText = "Search by company name"
          searchSelectText = "Payment terms under"
          searchByField={(value) => setSearchByField(value)}
        />
        <Table
          baseUrl={`${SERVICES.ACCOUNT}api/v1/accounts`}
          detailPage={'companies'}
          searchText={companyName ? `&companyName=${encodeURIComponent(companyName)}` : ''}
          searchSelect={termStatus ? `&termStatus=${encodeURIComponent(termStatus)}` : ''}
          tableColumns={columns}
          tableRow={companyList}
          getTabelData={(data) => setCompanyData(data)}
          getTableRecord={(totlaCount) => setTotalItems(totlaCount)}
        />
      </Spacings.Stack>
    </div>
  )
}

CompanyList.displayName = 'CompanyList'
export default CompanyList;