import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'CompanyList.title',
    description: 'The page title of main view',
    defaultMessage: 'Hello, world',
  },
  labelLinkOne: {
    id: 'CompanyList.labelLinkOne',
    description: 'The label for the link to page one',
    defaultMessage: 'Page one',
  },
  labelLinkTwo: {
    id: 'CompanyList.labelLinkTwo',
    description: 'The label for the link to page two',
    defaultMessage: 'Page two',
  },
});
