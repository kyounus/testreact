import React, {useState, useEffect} from 'react';
import { Header } from '../../common/Header';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import {SERVICES} from '../../config/api';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import { useIntl } from 'react-intl';
import LoadingSpinner from '@commercetools-uikit/loading-spinner';
import { Pagination } from '@commercetools-uikit/pagination';
import SelectInput from '@commercetools-uikit/select-input';
import Spacings from '@commercetools-uikit/spacings';
import {SearchIcon,CloseBoldIcon} from '@commercetools-uikit/icons';
import TextInput from '@commercetools-uikit/text-input';
import { CallApi } from '../../plugin/Axios'
import FlatButton from '@commercetools-uikit/flat-button';
import '../orders/order-list.css'

import "./checkoutdetail.css"

const checkoutsurvey = (props) => {

    const [totalItems, setTotalItems] = useState(0);
    const [surveyData, setSurveyData] = useState([]);
    const { formatTime, formatDate } = useIntl();
    const [spinner, setSpinner] = useState(true);
    const [pageLimit, setPageLimit] = useState(20);
  const [pageNumber, setPageNumber] = useState(1);
  const [emailSearch, setEmailSearch] = useState(true);
  const [answerSearch, setAnswerSearch] = useState(false);

  const [ansQuery, setansQuery] = useState('')
  const [emailQuery, setEmailQuery] = useState('')
  const [numberQuery, setNumberQuery] = useState('')
  
  const [searchByField, setSearchByField] = useState('');
  const [custSearch, setCustSearch] = useState(false);
  const[email ,setEmail] = useState('');
  const[answer,setAnswer] = useState('');
  const[cust,setCust] = useState('');

    const columns = [
        { key: 'custNo', label: 'Customer Number',shouldIgnoreRowClick: true},
        { key: 'email', label: 'Customer Email',shouldIgnoreRowClick: true},
        { key: 'answer', label: 'Answer',shouldIgnoreRowClick: true},
        { key: 'answerDetail', label: 'Answer Details',shouldIgnoreRowClick: true},
        { key: 'created', label: 'Created At',shouldIgnoreRowClick: true},
        { key: 'updated', label: 'Updated at',shouldIgnoreRowClick: true},
        {key: 'customRenderer', label: 'Action',
                renderItem: (rows) => (
                <SecondaryButton
                label="Edit"  
                />)}
        
      ];

      const handleRowClick = (item) => {
        props.history.push("checkout-survey-id/" + item.id1);
        props.history.push(item)
      }

      useEffect(() => {
        let offset = (pageNumber - 1) * pageLimit;
            if (totalItems && totalItems < offset) {
              setPageNumber(0);
              return;
            }
        setSpinner(true)
        setSurveyData([])
        let queryEmail='';
        let queryAnswer='';
        let queryCust=''
        if(email)
        {
          queryEmail = `email=${encodeURIComponent(email)}`;
        }
        if(answer)
        {
          queryAnswer =`answer=${answer}`
        }
        if(cust)
        {
          queryCust =`customerNumber=${cust}`
        }
        let url= `${SERVICES.CHECKOUT}/api/v1/users/checkout/surveys/response?${queryEmail}&${queryAnswer}&${queryCust}&limit=${pageLimit}&offset=${offset}`
        CallApi(url,'get')
          .then(res => {
              if(res.data){
              setSurveyData(res.data.checkoutSurveyList)
              setTotalItems(res.data.total)
              
            }  
            })
            .then(res =>
                setSpinner(false)
              )
          .catch(error => console.log("error", error))
      }, [pageNumber, pageLimit,answer,email,cust]);


      const searchEmails = () => {
        setEmail(emailQuery)
     }
     const searchAnswer = () => {
      setAnswer(ansQuery)
     }

     const searchCust = () => {
      setCust(numberQuery)
     }

      const controlView = (controlValue) => {
        if(controlValue === "email" ){
          setEmailSearch(true)
          setAnswerSearch(false)
          setCustSearch(false)
        }
        if (controlValue === "answer")
        {
          setEmailSearch(false)
          setAnswerSearch(true)
          setCustSearch(false)
        }
        if (controlValue === "custNo")
        {
          setEmailSearch(false)
          setAnswerSearch(false)
          setCustSearch(true)
          
        }
       
  }

      let surveyList = []
      Object.values(surveyData).map((eachData)=> {
        const createdOn = formatDate(eachData.createdDate) + " " + formatTime(eachData.createdDate)
        const modifiedOn = formatDate(eachData.lastModifiedDate) + " " + formatTime(eachData.lastModifiedDate)

          let surveyDetails = {
            ...eachData,
          id: eachData.id,
          custid: eachData.customerId,
          answer: eachData.other? <div>Others</div> : eachData.surveyResponse,
          answerDetail: eachData.other? eachData.surveyResponse : <div> </div>,
          created: createdOn,
          updated: modifiedOn,
          email: eachData && eachData.email ? eachData.email : '',
          custNo: eachData?.customerNumber
            }
            surveyList = [...surveyList, surveyDetails]
      })

    return (
        
        <div className="big">
            <Header
                title={'Checkout Survey'}
                results={totalItems}
            />
            <Spacings.Inline scale="xs">
            <div style={{ width: '25%', marginBottom: "30px" , paddingTop:'10px'}}>
                      <Spacings.Stack scale="xs">
                         <SelectInput
                            placeholder="Search by"
                            name="form-field-name"
                            value={searchByField}
                            onChange={
                              (event) => {
                                controlView(event.target.value);
                                setSearchByField(event.target.value);
                              } }
                               options={[
                                { value: 'email', label: 'Email' },
                                { value: 'answer', label: 'Answer' },
                                { value: 'custNo', label: 'Customer Number' },
                            ]}
                         />
                      </Spacings.Stack>
                    </div>
                    
                   {emailSearch ? 
                   <div style={{display: 'flex',width: '50%'}}>
                     <div style={{width:'100%' ,paddingTop:'10px'}}>
                     <TextInput
                         placeholder="Search by Email"
                         title=""
                         value={emailQuery}
                         isReadOnly={false}
                         onChange={event => setEmailQuery(event.target.value)}
                      />
                     </div>
                      
                      <div style={{ paddingTop: '10px'}}>
                        <SecondaryButton iconLeft={<SearchIcon />} label="" onClick={searchEmails} />
                      </div>
                    </div> 
                    : null
                    }

                    {custSearch ? 
                   <div style={{display: 'flex',width: '50%'}}>
                     <div style={{width:'100%' ,paddingTop:'10px'}}>
                     <TextInput
                         placeholder="Search by Customer Number"
                         title=""
                         value={numberQuery}
                         isReadOnly={false}
                         onChange={event => setNumberQuery(event.target.value)}
                      />
                     </div>
                      
                      <div style={{ paddingTop: '10px'}}>
                        <SecondaryButton iconLeft={<SearchIcon />} label="" onClick={searchCust} />
                      </div>
                    </div> 
                    : null
                    }
                    
                    { answerSearch ?
                      <div style={{display: 'flex', width: '50%'}}>
                        <div style={{width:'100%',paddingTop:'10px'}}>
                         <TextInput
                            placeholder="Search by Answer"
                            title=""
                            value={ansQuery}
                            isReadOnly={false}
                            onChange={event => setansQuery(event.target.value)}
                         />
                        </div>
                        <div style={{ paddingTop: '10px'}}>
                          <SecondaryButton iconLeft={<SearchIcon />} label="" onClick={searchAnswer} />
                        </div>
                      </div>
                    :null
                    } 
                   <div style={{paddingTop: "15px", paddingLeft: "30px"}}>
             <Spacings.Inline scale="xs" >
             <div>
                 <FlatButton 
                  tone='secondary'
                     size="medium"
                     icon={<CloseBoldIcon />}
                     label='Clear all'
                     onClick={() => {
                         setSearchByField('')
                         setEmail('');
                         setCust('')
                         setAnswer('')
                         setansQuery('')
                         setEmailQuery('')
                         setNumberQuery('')
                     }}
                 />  
                 </div> 
                 {/* <div> Clear all </div>  */}
             </Spacings.Inline>
             </div>
                 </Spacings.Inline>

                

            <DataTableManager columns={columns} >
                <DataTable rows={surveyList}
                    onRowClick={
                    (item, index) => handleRowClick(item.id)
                    }

                />
        </DataTableManager>
        {spinner ? 
        <div className="spinner">
          <LoadingSpinner size="s">Loading</LoadingSpinner>
          </div> : 
        null}
        <div style={{marginTop: "10px"}}>
        <Pagination 
              totalItems={totalItems}
              page={pageNumber}
              perPageRange="m"
              perPage={pageLimit}
              onPageChange={(page) => {
                console.log(page)
                setPageNumber(page)
              }}
              onPerPageChange={(lmt) => {
                console.log(lmt)
                setPageLimit(lmt);
              }}
            />
            </div>
        </div>
    )
}

export default checkoutsurvey
