import React, {useEffect, useState} from 'react';
import Text from '@commercetools-uikit/text';
import LinkButton from '@commercetools-uikit/link-button';
import { ListIcon} from '@commercetools-uikit/icons';
import { addHyperLink } from '../../utils';
import TextField from '@commercetools-uikit/text-field';
import DateField from '@commercetools-uikit/date-field';
import PrimaryButton from '@commercetools-uikit/primary-button';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import { CheckBoldIcon,BinLinearIcon,BackIcon } from '@commercetools-uikit/icons';
import MultilineTextField from '@commercetools-uikit/multiline-text-field';
import {SERVICES} from '../../config/api';
import { ContentNotification } from '@commercetools-uikit/notifications';
import SelectField from '@commercetools-uikit/select-field';
import "./checkoutdetail.css"
import axios from 'axios';
import { CallApi } from '../../plugin/Axios'

const checkoutsurveydetail = (props) => {

    

    let data = props.history.location
    const { match,location } = props;

    const getLastItem = thePath => thePath.substring(thePath.lastIndexOf('/') + 1)
    let subID= data.subID ? data.subID : getLastItem(props.location.pathname);
    subID = match.params.id;
    

    const [custNumber, setCustNumber] = useState('');
    const [custEmail, setCustEmail] = useState('');
    const [ans, setAns] = useState('');
    const [otherAns, setOtherAns] = useState('')
    const [created, setCreated] = useState('');
    const [update, setUpdate] = useState('');
    const [spinner, setSpinner] = useState(true);
    const [isOtherTrue, setIsOtherTrue] = useState(false)
   

    const [notification , setNotification] =  useState(false); 
    const [notificationMessage, setNotificationMessage] = useState('Updated');
    const [errorNotifiaction , setErrorNotification] =  useState(false);
    const [errorMessage , setErrorMessage] =  useState('');
    
    const [othersVisibilty, setOthersVisibility] = useState(false)

    const [surveyQuestion, setSurveyQuestion] = useState([])

    useEffect(()=> {
        let url = `${SERVICES.CHECKOUT}/api/v1/users/checkout/surveys/${subID}/`
        CallApi(url, 'get')
          .then(res => {
              if(res.data){
              
              setCustEmail(res.data.email)
              setCustNumber(res.data?.customerNumber)
              setAns(res.data.surveyResponse)
              setCreated(res.data.createdDate)
              setUpdate(res.data.lastModifiedDate)
              setIsOtherTrue(res.data.other)  
              setOthersVisibility(!isOtherTrue)  
            }  
            })
            .then(res =>
                setSpinner(false)
              )
          .catch(error => console.log("error", error))

          CallApi(`${SERVICES.CHECKOUT}/api/v1/users/checkout/surveys/`, 'get')
          .then(resp => {
              if(resp.data){
                setSurveyQuestion(resp.data.value.surveyQuestions[0].responses)
            
            }  
            })
            .then(resp =>
                setSpinner(false)
              )
          .catch(error => console.log("error", error))
      }, [])

    
      const controlView = (visibilty) => {
          if(visibilty)
          {
            setOthersVisibility(true)
          }
          if(!visibilty)
          {
              setOthersVisibility(false)
          }
      }

      let count = 0
      const handleUpdate = () => {
          console.log('Answer', ans)
          console.log('Others',isOtherTrue)
          if(ans)
          {
              CallApi(`${SERVICES.CHECKOUT}/api/v1/users/checkout/surveys/${subID}?other=${isOtherTrue}&surveyResponse=${ans}`, 'post')
              .then(response =>{
                  console.log('Upated')
                  setErrorNotification(false)       
                  setNotification(true);
                  setNotificationMessage('Successfully Updated the record');
                  count++
                  reloadPage() 
              })
              .catch(error => {
                  console.log(error.message)
                  setErrorMessage(error.message)
                  setErrorNotification(true)
              });
          }
      }

      console.log(count, "Count");

      const reloadPage = () => {
        setTimeout(() => {
          window.location.reload()
      }, 3000);}

      const reloadPageAfterDelete = () => {
        setTimeout(() => {
            props.history.push(addHyperLink('checkout-survey', props))
      }, 3000);}


      const handleDelete = () => {
        console.log('Answer', ans)
        if(ans)
        {

            CallApi(`${SERVICES.CHECKOUT}/api/v1/users/checkout/surveys/${subID}/`, 'delete')
            .then(response =>{
                console.log('Upated')
                setErrorNotification(false)       
                setNotification(true);
                setNotificationMessage('Deleted the Record');
                reloadPageAfterDelete() 
            })
            .catch(error => {
                console.log(error.message)
                setErrorMessage(error.message)
                setErrorNotification(true)
            });
        }
    }

 



    return (
        <div className="big">
              <LinkButton
                to={addHyperLink('checkout-survey', props)}
                iconLeft={<ListIcon />}
                label="To Checkout-Survey List"
                isDisabled={false}
            />
            <div className="headline">
          
                <Text.Headline as="h1">{'Edit Survey'}</Text.Headline>
                <hr />
                <div className="all-fields">
                <div className="each-field">
                <TextField title="Customer Number" isReadOnly="true"
                value={custNumber} onChange={(event) => alert(event)} />
                </div>

                <div className="each-field">
                <TextField title="Customer Email" isReadOnly="true"
                value={custEmail} onChange={(event) => alert(event)} />
                </div>

                {othersVisibilty && isOtherTrue ?
                <>
                <div className="each-field">
                <SelectField
                                    title="Answer"
                                    value="Others"
                                    options={surveyQuestion.map((items)=> {
                                        return { label: items, value: items }
                                    })}
                                    onChange={(event) => 
                                        setAns(event.target.value)

                                    }
                                    
                                    backspaceRemovesValue={true}
                                    isReadOnly={true}
                                />
               
                </div>
                <div className="each-field">
                <SelectField
                                    title="Others"
                                    value={isOtherTrue}
                                    options={[
                                        { value: true, label: 'Yes' },
                                        { value: false, label: 'No' },
                                    ]}
                                    onChange={(event) => {
                                        setIsOtherTrue(event.target.value);
                                        controlView(event.target.value)
                                        setAns('')
                                    }}
                                    
                                    backspaceRemovesValue={true}
                                    isDisabled={false}
                                />
                </div>
                  <div className="each-field">
                  <MultilineTextField
                          title="Answer Detail"
                          value={ans}
                          onChange={(event) => {
                              setAns(event.target.value)
                              
                            }}
                          isReadOnly={false}
                      />
                    
                </div>
                </>
                :
                <>
                <div className="each-field">
                <SelectField
                                    title="Answer"
                                    value={ans}
                                    options={surveyQuestion.map((items)=> {
                                        return { label: items, value: items }
                                    })}
                                    onChange={(event) => setAns(event.target.value)}
                                    
                                    backspaceRemovesValue={true}
                                    isDisabled={false}
                                />
               
                </div>
                <div className="each-field">
                <SelectField
                                    title="Others"
                                    value={isOtherTrue}
                                    options={[
                                        { value: true, label: 'Yes' },
                                        { value: false, label: 'No' },
                                    ]}
                                    onChange={(event) => {
                                        setIsOtherTrue(event.target.value);
                                        controlView(event.target.value)
                                        setAns('')
                                    }}
                                    
                                    backspaceRemovesValue={true}
                                    isDisabled={false}
                                />
                </div>
                  <div className="each-field">
                  <MultilineTextField
                          title="Answer Detail"
                          value=""
                          onChange={(event) => setAns(event.target.value)}
                          isReadOnly={true}
                      />
                    
                </div>
                </>
               }
                
         

            


                <div className="each-date">
                <DateField
                        title="Created At"
                        isReadOnly="true"
                        value={created}
                        onChange={() => {}}
                    />
                </div>

                <div className="each-date">
                <DateField
                        title="Updated At"
                        isReadOnly="true"
                        value={update}
                        onChange={() => {}}
                    />
                </div>
                </div>
                <hr />
            <div className="survey-buttons">
                        <PrimaryButton
                                iconLeft={<CheckBoldIcon />}
                                label="Save Survey"
                                onClick={handleUpdate}
                                isDisabled={false}
                        />


                        <SecondaryButton style={{marginLeft: "5px", marginRight: "5px"}}
                            iconLeft={<BinLinearIcon />}
                            label="Delete Record"
                            onClick={handleDelete}
                            isDisabled={false}
                        />
                      

                        <SecondaryButton iconLeft={<BackIcon />} 
                        label="Go Back" onClick={() => props.history.push(addHyperLink('checkout-survey', props))} />
            </div>
                

            </div>
            { notification ?
                <div style={{paddingTop: "5px"}}>
                <ContentNotification type="success"> {notificationMessage} </ContentNotification>
                </div>
            :null
            }
            { errorNotifiaction ?
            <div>
                <ContentNotification type="error"> {errorMessage} </ContentNotification>
            </div>  
            :null
            }
        </div>
    )
}

export default checkoutsurveydetail
