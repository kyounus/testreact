import React, { Component } from 'react';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import Grid from '@commercetools-uikit/grid';
import Card from '@commercetools-uikit/card';
import TextField from '@commercetools-uikit/text-field';
import Spacings from '@commercetools-uikit/spacings';
import Text from '@commercetools-uikit/text';
import TextInput from '@commercetools-uikit/text-input';
import AddAddress from '../add-company/add-address';
import Label from '@commercetools-uikit/label';
import RadioInput from '@commercetools-uikit/radio-input';
import CheckboxInput from '@commercetools-uikit/checkbox-input';
import axios from "axios";
import './payments.css';

class Payments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            businessType: ['public', 'private', 'Government', 'Non_profit'],
            public:false,
            private:false,
            Government:false,
            Non_profit:false,
            stockTicker: '',
            stock_exchange: '',
            government_email: '',
            department_Phone_Number: '',
            firstName: '',
            lastName: '',
            email: '',
            phone_number: '',
            street_adress1: '',
            street_adress2: '',
            city: '',
            postalCode: '',
            isSaved: false,
            isChecked: false,
            isAch: false,
            isReviewRequire:false,
            isApproved:false,
            companyField: false
        }
        this.paymentTerms = {
            "businessType": "PUBLIC",
            "stockTicker": "ST",
            "stockExchange": "STE",
            "references": null,
            "bankInformation": null,
            "email": null,
            "phoneNumber": null,
            "documentProof": null,
            "currency": null,
            "transactionNeedApproval": null,
            "totalApprovedCredit": null,
            "totalUsedCredit": null,
            "netTerms": null,
            "termsApprovalDate": null,
            "termsStatus": "NOTAPPLIED"
        };
    }




    publicButtonClick = (value) => {
        this.setState({
            businessType: value,
            public:true,
            private:false,
            Government:false,
            Non_profit:false
        })
    }
    privateButtonClick = (value) =>{
        this.setState({
            businessType: value,
            public:false,
            private:true,
            Government:false,
            Non_profit:false
        })
        
    }
    governmentButtonClick = (value) =>{
        this.setState({
            businessType: value,
            public:false,
            private:false,
            Government:true,
            Non_profit:false
        })
        
    }
    nonProfitButtonClick = (value) =>{
        this.setState({
            businessType: value,
            public:false,
            private:false,
            Government:false,
            Non_profit:true
        })
        
    }

    onSubmit = (e) => {
        console.log(e.target.value);
    }

    render() {
        return (
            <div>
                <div >
                    <Text.Headline
                        className="heading"
                        as="h2">{'Payments'}</Text.Headline>
                </div>
                <div className="payment-terms">
                    <Spacings.Inline scale="m">
                        <RadioInput.Option 
                        onChange={() => this.setState(state => ({ isChecked: !state.isChecked }))}
                        isChecked={this.state.isChecked}
                        > <Spacings.Inline scale="m"> 
                            Payment terms 
                         </Spacings.Inline> 
                         </RadioInput.Option>
                         <RadioInput.Option 
                        onChange={() => this.setState(state => ({ isAch: !state.isAch }))}
                        isChecked={this.state.isAch}
                        > <Spacings.Inline scale="m"> 
                            Pay by - ACH / Wire / Check 
                         </Spacings.Inline> 
                         </RadioInput.Option>
                    </Spacings.Inline>
                </div>
                {this.state.isChecked ?
                <div>
                <Spacings.Inline>
                <div className="review-payment">
                    <CheckboxInput
                            //value="foo-radio-value"
                            onChange={() => this.setState(state => ({ isReviewRequire: !state.isReviewRequire }))}
                            isChecked={this.state.isReviewRequire}
                        >
                            Payment Terms Requiring Review
                    </CheckboxInput>
                </div>
                {this.state.isReviewRequire ?
                <div className="approval">
                    <CheckboxInput
                            //value="foo-radio-value"
                            onChange={() => {this.setState(state => ({ isApproved: !state.isApproved })),console.log("Approved")}}
                            isChecked={this.state.isApproved}
                        >
                            Approved
                    </CheckboxInput>
                </div>
                : null}
                </Spacings.Inline>
                </div>                
                : null}       
                {this.state.isReviewRequire ?
                    <div className='payment-body'>
                        <Spacings.Inline>
                            <div className="buttons">
                                <div className="public">
                                    <SecondaryButton
                                        className={this.state.public ?  "_btn_public" : "test"}
                                        label="Public"
                                        onClick={() => this.publicButtonClick('public')} />
                                </div>
                                <div className="private">
                                    <SecondaryButton
                                        className={this.state.private ?  "_btn_private" : "test"}
                                        label="Private"
                                        onClick={() => this.privateButtonClick('private')} />
                                </div>
                                <div className="Government">
                                    <SecondaryButton
                                        className={this.state.Government ?  "_btn_Government" : "test"}
                                        label="Government"
                                        onClick={() => this.governmentButtonClick('Government')} />
                                </div>
                                <div className="Non_profit">
                                    <SecondaryButton
                                        className={this.state.Non_profit ?  "_btn_Non_profit" : "test"}
                                        label="Non-Profit"
                                        onClick={() => this.nonProfitButtonClick('Non_profit')} />
                                </div>
                            </div>
                        </Spacings.Inline>
                        <div>
                            {/* Start Public */}
                            {
                                this.state.businessType === 'public' &&
                                <div className="forPublic">
                                    <hr />
                                    <div className='text_for_btns'>
                                        <Text.Headline as="h4">{'Stock Details'}</Text.Headline>
                                    </div>
                                    <Grid
                                        gridGap="16px"
                                        gridAutoColumns="1fr"
                                        gridTemplateColumns="repeat(2, 1fr)"
                                    >
                                        <Grid.Item>
                                            <Card>
                                                <TextField
                                                    title="Stock Sticker Name"
                                                    name="stockTicker"
                                                    value={this.paymentTerms.stockTicker}
                                                    onChange={(event) => console.log(event.target.value)} />
                                                {/* {errors.stockTicker ? <div className="validation">{errors.stockTicker}</div> : null} */}

                                            </Card>
                                        </Grid.Item>
                                        <Grid.Item>
                                            <Card>
                                                <TextField
                                                    title="Stock exchange"
                                                    name="stockExchange"
                                                    value={this.paymentTerms.stockExchange}
                                                    onChange={(event) => event.target.value} />
                                                {/* {errors.stockExchange ? <div className="validation">{errors.stockExchange}</div> : null} */}
                                            </Card>
                                        </Grid.Item>
                                    </Grid>
                                </div>
                            }
                            {/* End Public */}

                            {/* Start Private */}
                            {
                                this.state.businessType === 'private' &&
                                <div>
                                    <div className="for-private">
                                        <hr />
                                        <div className='text_for_btns'>
                                            <Text.Headline as="h4">{'Business Preference'}</Text.Headline>
                                        </div>
                                        <Grid
                                            gridGap="16px"
                                            gridAutoColumns="1fr"
                                            gridTemplateColumns="repeat(2, 1fr)"
                                        >
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="First name"
                                                        name="firstName"
                                                        value={this.state.firstName}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.firstName ? <div className="validation">{errors.firstName}</div> : null} */}

                                                </Card>
                                            </Grid.Item>
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="Last name"
                                                        name="lastName"
                                                        value={this.state.lastName}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.lastName ? <div className="validation">{errors.lastName}</div> : null} */}
                                                </Card>
                                            </Grid.Item>
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="Email"
                                                        name="firstName"
                                                        value={this.state.email}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.email ? <div className="validation">{errors.email}</div> : null} */}

                                                </Card>
                                            </Grid.Item>
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="Phone number"
                                                        name="phone_number"
                                                        value={this.state.phone_number}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.phone_number ? <div className="validation">{errors.phone_number}</div> : null} */}
                                                </Card>
                                            </Grid.Item>
                                        </Grid>
                                    </div>
                                    
                                        <hr />
                                        <div className='text_for_btns'>
                                            <Text.Headline as="h4">{'Bank Information'}</Text.Headline>
                                        </div>
                                        <div className="bank-information">
                                        <Grid
                                            gridGap="16px"
                                            gridAutoColumns="1fr"
                                            gridTemplateColumns="repeat(2, 1fr)"
                                        >
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="Name"
                                                        name="firstName"
                                                        value={this.state.firstName}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.firstName ? <div className="validation">{errors.firstName}</div> : null} */}

                                                </Card>
                                            </Grid.Item>
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="Phone number"
                                                        name="phone_number"
                                                        value={this.state.phone_number}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.phone_number ? <div className="validation">{errors.phone_number}</div> : null} */}
                                                </Card>
                                            </Grid.Item>
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="Street address 1"
                                                        name="street_adress1"
                                                        value={this.state.street_adress1}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.street_adress1 ? <div className="validation">{errors.street_adress1}</div> : null} */}
                                                </Card>
                                                </Grid.Item>
                                                <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="Street address 2"
                                                        name="street_adress2"
                                                        value={this.state.street_adress2}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.street_adress2 ? <div className="validation">{errors.street_adress2}</div> : null} */}
                                                </Card>
                                            </Grid.Item>
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="City"
                                                        name="city"
                                                        value={this.state.city}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.city ? <div className="validation">{errors.city}</div> : null} */}
                                                </Card>
                                            </Grid.Item>
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="State/Province"
                                                        name="state_Province"
                                                        value={this.state.state_Province}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.state_Province ? <div className="validation">{errors.state_Province}</div> : null} */}
                                                </Card>
                                            </Grid.Item>
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="Postal Code"
                                                        name="postalCode"
                                                        value={this.state.postalCode}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.postalCode ? <div className="validation">{errors.postalCode}</div> : null} */}
                                                </Card>
                                            </Grid.Item>
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="Country"
                                                        name="country"
                                                        value={this.state.country}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.country ? <div className="validation">{errors.country}</div> : null} */}
                                                </Card>
                                            </Grid.Item>
                                        </Grid>
                                    </div>

                                </div>
                            }
                            {/* End Private */}

                            {/* Start Government */}
                            {
                                this.state.businessType === 'Government' &&
                                <div className="for-Govt">
                                    <hr />
                                    <div className='text_for_btns'>
                                        <Text.Headline as="h4">{'Business Preference'}</Text.Headline>
                                    </div>
                                    <div className="govt-info">
                                    <Grid
                                        gridGap="16px"
                                        gridAutoColumns="1fr"
                                        gridTemplateColumns="repeat(2, 1fr)"
                                    >
                                        <Grid.Item>
                                            <Card>
                                                <TextField
                                                    title="Government email"
                                                    name="government_email"
                                                    value={this.state.government_email}
                                                    onChange={(event) => this.setState({government_email:event.target.value})} />
                                                {/* {errors.government_email ? <div className="validation">{errors.government_email}</div> : null} */}

                                            </Card>
                                        </Grid.Item>
                                        <Grid.Item>
                                            <Card>
                                                <TextField
                                                    title="Department phone number"
                                                    name="department_Phone_Number"
                                                    value={this.state.department_Phone_Number}
                                                    onChange={(event) => this.setState({department_Phone_Number:event.target.value})} />
                                                {/* {errors.department_Phone_Number ? <div className="validation">{errors.department_Phone_Number}</div> : null} */}
                                            </Card>
                                        </Grid.Item>
                                    </Grid>
                                    </div>
                                    <hr />
                                        <div className='text_for_btns'>
                                            <Text.Headline as="h4">{'Bank Information'}</Text.Headline>
                                        </div>
                                        <div className="bank-information">
                                        <Grid
                                            gridGap="16px"
                                            gridAutoColumns="1fr"
                                            gridTemplateColumns="repeat(2, 1fr)"
                                        >
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="Name"
                                                        name="firstName"
                                                        value={this.state.firstName}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.firstName ? <div className="validation">{errors.firstName}</div> : null} */}

                                                </Card>
                                            </Grid.Item>
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="Phone number"
                                                        name="phone_number"
                                                        value={this.state.phone_number}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.phone_number ? <div className="validation">{errors.phone_number}</div> : null} */}
                                                </Card>
                                            </Grid.Item>
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="Street address 1"
                                                        name="street_adress1"
                                                        value={this.state.street_adress1}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.street_adress1 ? <div className="validation">{errors.street_adress1}</div> : null} */}
                                                </Card>
                                                </Grid.Item>
                                                <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="Street address 2"
                                                        name="street_adress2"
                                                        value={this.state.street_adress2}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.street_adress2 ? <div className="validation">{errors.street_adress2}</div> : null} */}
                                                </Card>
                                            </Grid.Item>
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="City"
                                                        name="city"
                                                        value={this.state.city}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.city ? <div className="validation">{errors.city}</div> : null} */}
                                                </Card>
                                            </Grid.Item>
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="State/Province"
                                                        name="state_Province"
                                                        value={this.state.state_Province}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.state_Province ? <div className="validation">{errors.state_Province}</div> : null} */}
                                                </Card>
                                            </Grid.Item>
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="Postal Code"
                                                        name="postalCode"
                                                        value={this.state.state_Province}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.postalCode ? <div className="validation">{errors.postalCode}</div> : null} */}
                                                </Card>
                                            </Grid.Item>
                                            <Grid.Item>
                                                <Card>
                                                    <TextField
                                                        title="Country"
                                                        name="country"
                                                        value={this.state.postalCode}
                                                        onChange={(event) => console.log(event.target.value)} />
                                                    {/* {errors.country ? <div className="validation">{errors.country}</div> : null} */}
                                                </Card>
                                            </Grid.Item>
                                        </Grid>
                                        </div>
                                    </div>    
                            }
                            {/* End Government */}

                            {/* Start Non-profit */}

                            {
                                this.state.businessType === 'Non_profit' &&
                                <div className="for-Non-profit">
                                    <hr />
                                    <div className="non-profit-info">
                                        <Text.Subheadline as="h4">{'Upload Document'}</Text.Subheadline>
                                    </div>

                                </div>
                            }
                            {/* End Non-profit */}
                        </div>
                    </div>
                    : null}
                
                <hr />
                
                <div>
                    <div className="billing-address">
                        <Label>Billing Address</Label>
                    </div>
                    <AddAddress companyField={this.state.companyField}
                        address={this.props?.paymentData?.length ? this.props.paymentData[0] : null} />
                    <div >
                        <SecondaryButton
                            className="payment-save-button"
                            label="Save"
                            onClick={this.onSubmit}
                            isDisabled={this.state.isSaved} />
                    </div>
                </div>
            </div>
        );
    }
}

Payments.displayName = 'Payments'
export default Payments;