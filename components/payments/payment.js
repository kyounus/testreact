import React, { useState, useEffect } from 'react';
import Card from '@commercetools-uikit/card';
import PrimaryButton from '@commercetools-uikit/primary-button';
import ToggleInput from '@commercetools-uikit/toggle-input';
import Text from '@commercetools-uikit/text';
import Stamp from '@commercetools-uikit/stamp';
import TextField from '@commercetools-uikit/text-field';
import SelectField from '@commercetools-uikit/select-field';
import NumberInput from '@commercetools-uikit/number-input';
import Link from '@commercetools-uikit/link';
import { CallApi } from '../../plugin/Axios';
import {SERVICES} from "../../config/api";
import './payments.css';

import NoRecords from '../shared/messages';
const PaymentDetails = (props) => {
    const [isTermsEnabled, setTermsEnabled] = useState(false);
    const [isSaved, setSaved] = useState(false);
    const [creditLimit, setCreditLimit] = useState(0)
    const [creditTerms, setCreditTerms] = useState('');

    const { accountId, payments, fnDisplayNotification } =  props;
    let paymentTermsFetch = [];
    let details = {};
    let paymentId = 0;
    let paymentTerms = {};
    const dataDisplay = [];

    if(payments.length) {
        paymentTermsFetch = payments.filter(item => item.paymentType === 'PAYMENTTERMS');
    }


    if(paymentTermsFetch.length) {
        paymentTerms = paymentTermsFetch[0];
        paymentId = paymentTerms.paymentId;
        console.log(paymentTerms)
        const { business } = paymentTerms;
        const { businessType } = business;

        details['business_type'] = business.businessType;
        
        if(businessType == 'PUBLIC'){
            details['stock_ticker_name'] = business.stockTicker
            details['stock_exchange'] = business.stockExchange
        } else if(businessType == 'PRIVATE'){
            // details['business_reference_one_address'] = setAddress('business_reference_one_', business.references.firstReference)
            // details['business_reference_two_address'] = setAddress('business_reference_two_', business.references.secondReference)
            // details['private_type_bank_address'] = setAddress('private_type_bank_', business.bankInformation)
            details['first_business_reference'] = displayAddress(business.references.firstReference)
            details['second_business_reference'] = displayAddress(business.references.secondReference)
            details['bank_information'] = displayAddress(business.bankInformation)
        } else if(businessType == 'GOVERNMENT'){
            details['government_email'] = business.email
            details['department_phone_number'] = business.phoneNumber
        } else if(businessType == 'NONPROFIT') {
            // const obj = business.documentProof[0];
            // business.documentProof = [];
            // for(let i =0; i < 5; i++) {
            //     business.documentProof.push(obj)
            // }
            let fileCount = 1;
            if(business.documentProof && business.documentProof.length && business.documentProof[0].id) {
                let displayDocumentProoof = []
                business.documentProof.forEach(item => {
                    const url = `${SERVICES.ACCOUNT}/api/v1/user/files/download/${item.id}`;
                    let displayName = item.id;
                    if(displayName.indexOf('_') > 0) {
                        const _temp = displayName.substring(displayName.indexOf('_'))
                        if(_temp.length > 1) {
                            displayName = _temp;
                        }
                    }
                    displayDocumentProoof = [
                        ...displayDocumentProoof, 
                        <div style={{ marginLeft: "10px"}}>
                            <span>{fileCount++}.&nbsp;</span>
                            <Link isExternal={true} to={url}>{displayName}</Link>
                        </div>
                    ];
                });
                
                details['proof_of_non_profit'] = displayDocumentProoof;
            }
        }
        
        Object.entries(details).map(([keyName, val], index) => {
            dataDisplay.push({
                keyName: keyName.replace(/_/g, ' '),
                keyValue: val
            })
         });
    }

    useEffect(() => {
         const termsStatus = paymentTerms && paymentTerms.termsStatus;
         if(termsStatus && termsStatus === 'APPROVED') {
            setTermsEnabled(true);
        } 
        setCreditLimit(paymentTerms.totalApprovedCredit || 0);
        setCreditTerms(paymentTerms.netTerms || '');
        console.log(paymentTerms)

    }, [])

    const submitApproval = async () => {
        const payload = {
            action: 'CHANGE',
            termsStatus: isTermsEnabled ? 'APPROVED' : 'PENDINGREVIEW',
            totalApprovedCredit: creditLimit,
            netTerms: creditTerms
        };
        console.log(payload);
        const url = `${SERVICES.ACCOUNT}/api/v1/accounts/${accountId}/payment-options/${paymentId}/`
        await CallApi(url,'post',payload)
        .then(res => {
            console.log(res);
            fnDisplayNotification(`Payment terms status : ${isTermsEnabled ? 'APPROVED': 'IN-REVIEW'}`)
        }).catch(e => console.log(e));
        setSaved(true);
    }
    return (
        <div>
            { 
            details['business_type'] ? 
                <div>
                    <div>
                        <table className="payment-table">
                            <tr>
                                <td>Payment term status:</td>
                                <td>
                                <Stamp tone={isTermsEnabled ? 'positive' : 'warning'}><Text.Detail>{isTermsEnabled ? 'Approved' : 'In-Review'}</Text.Detail></Stamp>
                                </td>
                            </tr>
                            <tr>
                                <td>Credit Limit</td>
                                <td>
                                    <NumberInput name="creditLimit"  onChange={(e) => setCreditLimit(e.target.value)} value={creditLimit} title="" />
                                </td>
                            </tr>
                            <tr>
                                <td>Credit Terms</td>
                                <td>
                                <SelectField
                                    title=""
                                    containerId="credit-select-field"
                                    value={creditTerms}
                                    options={[
                                        { value: 'NET15', label: 'Net 15' },
                                        { value: 'NET30', label: 'Net 30' },
                                    ]}
                                    onChange={(event) => setCreditTerms(event.target.value)}
                                />
                                </td>
                            </tr>
                            <tr>
                                <td>Approve payment terms</td>
                                <td>
                                    <ToggleInput isDisabled={false} isChecked={isTermsEnabled} onChange={(event) => setTermsEnabled(event.target.checked)}  size="small"/>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div className="payment-card">
                    <Card theme="light" type="raised" insetScale="m">
                        <table theme="dark" className="payment-list-table">
                            <tbody>
                            {
                              dataDisplay.map( (item) => {
                                    return (
                                        <tr key={item.keyName}>
                                            <th style={{textTransform: 'capitalize', textAlign:'left'}}>{item.keyName}</th>
                                            <td>{item.keyValue}</td>
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                    </Card>
                    </div>
                    <div>
                        <PrimaryButton  label="Save" onClick={submitApproval} isDisabled={isSaved}/>
                    </div>
                </div>
                :
                <NoRecords message="Payment terms not submitted by customer"/>
            }
        </div>
    )
}

const displayAddress = (data) => {
    let display = '';
    const fields = ['firstName', 'lastName', 'name', 'streetAddress1', 'streetAddress2', 'city', 'state', 'postalCode', 'country', 'email', 'phoneNumber'];
    fields.forEach(fieldName => {
        if(data[fieldName]) {
            display += data[fieldName] + ', '
        }
    });
    return <div className="payment-textFeild"><TextField title="" value={display} onChange={() => {}} /></div>
  }

const setAddress=($prefix, $value, $key)=>{
    let $address = {}
    $address[$prefix+'first_name']=$value.firstName;
    $address[$prefix+'last_name']=$value.lastName;
    $address[$prefix+'address_1']=$value.streetAddress1;
    $address[$prefix+'address_2']=$value.streetAddress2 || '';
    $address[$prefix+'city']=$value.city;
    $address[$prefix+'state']=$value.state;
    $address[$prefix+'postal_code']=$value.postalCode;
    $address[$prefix+'country']=$value.country || 'US';
    $address[$prefix+'phone_number']=$value.phoneNumber || '';
    if($value.name){
      $address[$prefix+'name']=$value.name;
    }
    if($value.email){
      $address[$prefix+'email'] = $value.email;
    }
    if($key){
      if($value[$key]!=undefined){
        $address[$key] = $value[$key];
      }
    }
    console.log($address)
    return $address
  }

export default PaymentDetails;