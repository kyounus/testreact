import React, {useState, useEffect} from 'react'
import CheckboxInput from '@commercetools-uikit/checkbox-input';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import PrimaryButton from '@commercetools-uikit/primary-button';
import Grid from '@commercetools-uikit/grid';
import TextInput from '@commercetools-uikit/text-input';
import './survey.css';
import { CallApi } from '../../plugin/Axios'
import {SERVICES} from '../../config/api';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import { ContentNotification } from '@commercetools-uikit/notifications';
import Text from '@commercetools-uikit/text';
import Card from '@commercetools-uikit/card';
import {FormModalPage } from '@commercetools-frontend/application-components';

const survey = () => {
    
    const [BtoB, setBtoB] = useState(true)
    const [DtoC, setDtoC] = useState(false)
    const [channel, setChannel] = useState('B2B')
    const [reload, setReload]=useState('false')
    const [questionData, setQuestionData] = useState([]);
    const [newQuestionData, setNewQuestionData] = useState('');
    const [notification , setNotification] =  useState(false); 
    const [notificationMessage, setNotificationMessage] = useState('Updated');
    const [errorNotifiaction , setErrorNotification] =  useState(false);
    const [errorMessage , setErrorMessage] =  useState('');
    const [questionID, setQuestionID] =useState('');
    const [enableEditQues, setEnableEditQues] = useState(false);
    const [disableButton, setDisableButton] = useState(false);
    const [editableQuestion, setEditableQuestion] = useState('')
    const [updatedCount, setUpdatedCount] = useState(0);

            const handleRowClick = (item) => {
                setQuestionID(item.id)
                setEditableQuestion(item.questionValue)
                setEnableEditQues(true);
                setDisableButton(false);
            }
            const handleUpdate = () => {
                let url = `${SERVICES.ACCOUNT}api/v1/users/subscriptions/surveys/${questionID}?question=${editableQuestion}`
             CallApi(url, 'post')
                .then(response =>{
            
            setErrorNotification(false)       
            setNotification(true);
            setNotificationMessage('Successfully Updated the record');
            setUpdatedCount(updatedCount + 1);
        })
        .catch(error => {
            setErrorMessage(error.message)
            setErrorNotification(true)
        });
    }
    
    const handleCancel = () => setEnableEditQues(false)
    const handleClose = () => setEnableEditQues(false)
    const handleResponse = (selectedCheckBox) => {
        setDtoC(false)
        setBtoB(false)
        if (selectedCheckBox) {
            setDtoC(true)
            setBtoB(false)
            setChannel('D2C')
        }
        else {
            setBtoB(true)
            setDtoC(false)
            setChannel('B2B')
        }
    }

    console.log("channel", channel);
    
    useEffect(() => {
        if(channel)
        
        {
            let url = `${SERVICES.ACCOUNT}api/v1/users/subscriptions/surveys?channel=${channel}`
        CallApi(url, 'get')
          .then(res => setQuestionData(res.data.results))
          .catch(error => console.log("error", error))
        }
      }, [updatedCount, channel,reload]);

        let surveyList = [];
        {
            questionData.length>0 ?
            Object.values(questionData).map((eachQuestion) => {
                let surveyDetails = { 
                    id: eachQuestion.id,
                    questionValue: eachQuestion.question
            }
            surveyList=[...surveyList, surveyDetails]
            }) : null;
        }

      const hideNotification =() =>{
           setTimeout(() => {
               setErrorNotification(false)
               setNotification(false)}, 5000);
        }
  
    const handleSubmit = () =>{
        if(newQuestionData && channel)
            {
                let url = `${SERVICES.ACCOUNT}/api/v1/users/subscriptions/surveys?channel=${channel}&question=${newQuestionData}`
                CallApi(url, 'post')
                    .then(response =>{
                        setErrorNotification(false)       
                        setNotification(true);
                        setNotificationMessage('Successfully Updated the record');
                    })
                    .then(re=>{
                        setReload(true)
                        setNewQuestionData('')
                    })
                    .catch(error => {
                        console.log(error)
                        setErrorMessage('Reached the limit to survey questions')
                        setErrorNotification(true)
                    });
            }}
    const columns = [
            { key: 'questionValue', label: 'Title' },
            { key: 'customRenderer', label: 'Action', renderItem: (rows) =>(
                <SecondaryButton  
                    label="Edit" onClick={handleRowClick} />
                    ) },
        ];

        return (
        <div className="survey-container">
            <Text.Headline as="h1">{'Survey Questions'}</Text.Headline>
        <div className="info-text">
            <Grid
            gridGap="5px"
            gridAutoColumns="1fr"
            gridTemplateColumns="repeat(12, 1fr)" className="choices"
            >
            <Grid.Item> <CheckboxInput
             value="foo-radio-value"
                onChange={() => handleResponse(false)}
                isChecked={BtoB}
            >B2B</CheckboxInput></Grid.Item>
            
            <Grid.Item> <CheckboxInput
             value="foo-radio-value"
                onChange={() => handleResponse(true)}
                isChecked={DtoC}
            >D2C</CheckboxInput></Grid.Item>
            </Grid>

            {/* >>>>>>>>>>>>MODAL STARTS HERE<<<<<<<<<<<<<< */}

            <FormModalPage
                    title="Edit the question"
                    isOpen={enableEditQues}
                    onClose={handleClose}
                    
                    topBarCurrentPathLabel={"Edit the question"}
                    topBarPreviousPathLabel="Back"
                    onSecondaryButtonClick={handleCancel}
                    onPrimaryButtonClick={handleUpdate}
                    isPrimaryButtonDisabled={disableButton}
                    >
                    
                    <div className="formModal-body">
                        <Card>
                        <TextInput title="Edit the Question" name="editableQuestion"
                         value={editableQuestion}
                        
                        onChange={(event)=> setEditableQuestion(event.target.value)} />
                       
                        </Card>
                    </div>


                    <div className="notification">
                        {notification ?
                            <div>
                                <ContentNotification type="success"> {notificationMessage} </ContentNotification>
                               
                            </div>
                            : null
                        }
                        {errorNotifiaction ?
                            <div>
                                <ContentNotification type="error"> {errorMessage} </ContentNotification>
                                
                            </div>
                            : null
                        }
                    </div>


            </FormModalPage>



            {/* >>>>>>>>>>>MODAL ENDS HERE<<<<<<<<<<<<<<<<<< */}


           { 
            !enableEditQues ?
           
           <DataTableManager columns={columns}>
                <DataTable rows={surveyList}
                 onRowClick={
                    (item, index) => handleRowClick(item)
                }
                />
            </DataTableManager> : null
            }

          
            <div className="display-input-text">
            <TextInput value={newQuestionData} onChange={(event) => setNewQuestionData(event.target.value)} />
                <div style={{paddingTop: "10px"}}>
                    <PrimaryButton  label="Add new question" onClick={handleSubmit}/>
                </div>
            </div>

          
             { notification ?
            <div style={{width: "50%", margin: "10px"}}>
            <ContentNotification type="success"> {notificationMessage} </ContentNotification>
            {hideNotification()}
            </div>
            
            :null
            }
            { errorNotifiaction ?
            <div style={{width: "50%", margin: "10px"}}>
                <ContentNotification type="error"> {errorMessage} </ContentNotification>
                {hideNotification()}
              </div>  
            :null
            } 
            </div>

            

            
        </div>
    )
}

export default survey
