import React, { useEffect, useState } from 'react'
import axios from 'axios'
import Text from '@commercetools-uikit/text';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import SelectField from '@commercetools-uikit/select-field';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import "./sales-representative.css"
import LoadingSpinner from '@commercetools-uikit/loading-spinner';
import Spacings from '@commercetools-uikit/spacings';
import { PlusBoldIcon } from '@commercetools-uikit/icons';
import { FormModalPage, ConfirmationDialog } from '@commercetools-frontend/application-components';
import Grid from '@commercetools-uikit/grid';
import Card from '@commercetools-uikit/card';
import TextField from '@commercetools-uikit/text-field';
import { ContentNotification } from '@commercetools-uikit/notifications';
import { SERVICES } from '../../config/api';
import Label from '@commercetools-uikit/label';
import { CallApi } from '../../plugin/Axios'

const SalesRepresentative = () => {
  const [allMails, setAllMails] = useState([]);
  const [category, setCategory] = useState([]);
  const [addSalesRepId , setAddSalesRepId] = useState('')
  let   [getCompanyCategory , setgetCompanyCategory] = useState([])
  let   [categories , setcategories] = useState(['Dental','Gym/Fitness','Hospital','Other Healthcare','Nursing Home/Assisted Living','Office','School','Government','Salon','Spa','Real Estate','Hotel','Restaurant','Retail','Other'])
  let   [categoryName , setcategoryName] = useState('');
  const [salesRepList,setsalesRepList] =useState([])
  const [otherCategoryId, setOtherCategoryId] = useState('');
  const [spinner, setSpinner] = useState(true);
  const [reloadList, setReloadList] = useState(false);
  const [enableAddCategory, setEnableAddCategory] = useState(false);
  const [notification, setNotification] = useState(false);
  const [notificationMessage, setNotificationMessage] = useState('');
  const [errorNotifiaction, setErrorNotification] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [pageLimit, setPageLimit] = useState(20);
  const [pageNumber, setPageNumber] = useState(1);
  const [removeCategoryDialog, setRemoveCategoryDialog] = useState(false);
  const [removeTitle, setRemoveTitle] = useState('');
  const [removeCategoryId, setRemoveCategoryId] = useState('')


  function sortListByMember(a, b) {
    const bandA = a.name.toUpperCase();
    const bandB = b.name.toUpperCase();
    let comparison = 0;
    if (bandA > bandB) {
      comparison = 1;
    } else if (bandA < bandB) {
      comparison = -1;
    }
    return comparison;
  }
  

  useEffect(() => {
    setCategory([])
    setSpinner(true);
    let url = `${SERVICES.ACCOUNT}/api/v1/accounts/categories/`
    CallApi(url, 'get')
      .then((res) => {
        setgetCompanyCategory(res.data.value.companyCategories.companyCategory.map(data =>data.name))
        let catList = res.data.value.companyCategories['companyCategory'];

        //todo : optimize below filter
        const otherCategory = catList.filter(item => item.name == 'Other');
        catList = catList.filter(item => item.name != 'Other');
        catList = catList.sort(sortListByMember);

        if (otherCategory.length) {
          setOtherCategoryId(otherCategory[0]['id'])
        }
        setCategory([otherCategory[0], ...catList]);
      })
      .then(res =>
        setSpinner(false)
      )
      .catch((error) => {
        console.log('ERROR:', error);
      })
  }, [reloadList])

  useEffect(() => {
    setAllMails([])
    let offset = (pageNumber - 1) * pageLimit;
    let url = `${SERVICES.ACCOUNT}/api/v1/accounts/salesrep?limit=200&offset=${offset}`
    CallApi(url, 'get')
      .then((res) => {
        handleSalesRep(res.data.salesReps)
        setAllMails(res.data.salesReps);
      })
      .catch((error) => {
        console.log('ERROR:', error);
      })
  }, [reloadList])

  const changeSalesRep = (e) => {
    const salesRepId = e.target.value;
    const fieldName = e.target.name;
    const categoryId = fieldName.replace('cat_', '') //todo: find right way for categoryId
    setSpinner(true)
    let url = `${SERVICES.ACCOUNT}/api/v1/accounts/categories/${categoryId}/sales-rep/${salesRepId}/`
    CallApi(url, 'post')
      .then((res) => {
        setReloadList(!reloadList);
      })
      .catch((error) => {
        console.log('ERROR:', error);
      });
  };

  const removeCategory = (categoryId, categoryName) => {
    setRemoveTitle(`Removing '${categoryName}' will re-assign customers to 'Other' Continue?`);
    setRemoveCategoryDialog(true)
    setRemoveCategoryId(categoryId)
  }

  let companyCategory = []
    category.map((items) => {
      let mail = []
      allMails.map((mails) => {
        if (items?.['sales-rep-id'] === mails.salesRep.id) {
          mail.push(mails.email)
        }
      }
    )
    const details = {
      id: items?.id,
      companyCategory: items?.name,
      salesRepresentative: <div className="sales">
        <SelectField 
          title="" 
          options={allMails.map((eachRep) => { return { label: eachRep.salesRep.email, value:eachRep.salesRep.id } })} 
          selected={true}
          value={items?.['sales-rep-id']}
          data-category={items?.id}
          name={`cat_${items?.id}`}
          onChange={changeSalesRep}
          />
        </div>,
      remove: <div>
        {(items?.['id'] !== otherCategoryId) ?
        <SecondaryButton 
          label="Remove" 
          onClick={() => removeCategory(items?.id, items?.name)} 
        />
        :
        <div className="default-button">
        <Label isBold={true}>Default</Label>
        </div>
        }
      </div>
    }
    companyCategory = [...companyCategory, details]
  });

  const columns = [
    { key: 'companyCategory', label: 'Company category' },
    { key: 'salesRepresentative', label: 'Sales representative' },
    { key: 'remove', label: 'Remove' }
  ];
  const handleSalesRep =( data)=>{
    let tempCategories =[]
    {data.map((category) =>{
       const tempObj = {
           value: category.salesRep.id, label : category.salesRep.email
       }
       tempCategories = [...tempCategories, tempObj ]
    })}
   setsalesRepList(tempCategories)
  }
  getCompanyCategory.length>0 ? categories = categories.filter( ( el ) => !getCompanyCategory.includes( el ) ) :null

  const handleSubmit =() =>{
    if(categoryName && addSalesRepId){
      let url = `${SERVICES.ACCOUNT}/api/v1/accounts/categories?categoryName=${categoryName}&salesRepId=${addSalesRepId}`
    CallApi(url, 'post')
    .then(res => {
      setReloadList(!reloadList);
      setNotification(true)
      setNotificationMessage('Successfully added category')
    })
    .catch(error => {
      setErrorMessage(error.message);
      setErrorNotification(true);
  })
    } else {
      setErrorMessage('Please fill in required fields');
      setErrorNotification(true);
    }
}

const handleRemove = () =>{
  let url = `${SERVICES.ACCOUNT}/api/v1/accounts/categories/${removeCategoryId}/`
  CallApi(url, 'post')
  .then((res) => {
    setReloadList(!reloadList);
    setRemoveCategoryDialog(false)
  })
  .catch((error) => {
    setErrorMessage(error.message);
    setErrorNotification(true);
  });
}

  const handleClose = () => {
    setEnableAddCategory(false);
    setRemoveCategoryDialog(false);
  }

  const hideNotification = () => {
    setTimeout(() => {
        setErrorNotification(false)
        setNotification(false)
    }, 3000);
}

  return (
    <div>
      <Spacings.Stack scale="m">
      <Text.Headline as="h1">{'Sales to company assignments'}</Text.Headline>
      <Text.Subheadline as="h4">{'Companies assigned to these categories will be assigned to their assigned sales representative on account creation. You can override these values within the customers companys record. Any unassigned categories will re-assign affected customers to Other'}</Text.Subheadline>
      <div>
          <div className="add-button">
            <SecondaryButton iconLeft={<PlusBoldIcon />} label="Add Another Category" onClick={() => { setEnableAddCategory(true);  setcategoryName(''); setAddSalesRepId();}} />
          </div>
      </div>
      <div>
          <ConfirmationDialog
            title="Remove Category"
            isOpen={removeCategoryDialog}
            onClose={handleClose}
            onCancel={handleClose}
            onConfirm={handleRemove}
          >
            <Spacings.Stack scale="m">
              <Text.Body>{removeTitle}</Text.Body>
            </Spacings.Stack>
          </ConfirmationDialog>
        </div>
      <DataTableManager columns={columns}>
        <DataTable rows={companyCategory} />
      </DataTableManager>
      {spinner ? <div className="spinner">
          <LoadingSpinner size="s">Loading</LoadingSpinner></div> : null}
        <FormModalPage
          title={"Add New Category"}
          isOpen={enableAddCategory}
          onClose={handleClose}
          topBarCurrentPathLabel={"Add New Category"}
          topBarPreviousPathLabel="Back"
          onSecondaryButtonClick={handleClose}
          onPrimaryButtonClick={handleSubmit}
        >
          <div className="notification">
            {notification && enableAddCategory ?
              <div>
                <ContentNotification type="success"> {notificationMessage} </ContentNotification>
                {hideNotification()}
              </div>
              : null
            }
            {errorNotifiaction ?
              <div>
                <ContentNotification type="error"> {errorMessage} </ContentNotification>
                {hideNotification()}
              </div>
              : null
            }
          </div>
          <Grid
            gridGap="16px"
            gridAutoColumns="1fr"
            gridTemplateColumns="repeat(2, 1fr)"
          >
             <Grid.Item>
              <Card>
                <Spacings.Inline>
                  <TextField title="Category" name="category" value={categoryName} onChange={event => setcategoryName(event.target.value)} />
                </Spacings.Inline>
              </Card>
            </Grid.Item>
            <Grid.Item>
              <Card className="card">
                <SelectField
                  title="SalesRep"
                  value={addSalesRepId}
                  options={salesRepList}
                  onChange={(event) => setAddSalesRepId(event.target.value)}
                />
              </Card>
            </Grid.Item>
          </Grid>
        </FormModalPage>
        </Spacings.Stack>
    </div>
  );
}
export default SalesRepresentative