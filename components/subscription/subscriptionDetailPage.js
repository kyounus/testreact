import React, {useState, useEffect} from 'react';
import Text from '@commercetools-uikit/text';
import CollapsiblePanel from '@commercetools-uikit/collapsible-panel';
import Grid from '@commercetools-uikit/grid';
import LinkButton from '@commercetools-uikit/link-button'
import { useIntl } from 'react-intl';
import { ListIcon} from '@commercetools-uikit/icons';
import './subscriptionDetail.css'
import Card from '@commercetools-uikit/card';
import {SERVICES, centsToDollars} from "../../config/api";
import TextField from '@commercetools-uikit/text-field';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import PrimaryButton from '@commercetools-uikit/primary-button';
import { ContentNotification } from '@commercetools-uikit/notifications';
import Stamp from '@commercetools-uikit/stamp';
import { addHyperLink } from '../../utils';
import DateInput from '@commercetools-uikit/date-input';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import { CallApi } from '../../plugin/Axios';

const subscriptionDetailPage = (props) => {
    const [generalTab, setGeneralTab] = useState(true)
    const { formatTime, formatDate } = useIntl();
    const [summaryPanel, setSummaryPanel] = useState(false);
    const [subsPanel, setSubsPanel] = useState(false);
    const [paymentPanel, setPaymentPanel] = useState(false);
    const [promoPanel, setPromoPanel] = useState(false);
    const [devicePanel, setDevicePanel] = useState(false);
    const [shippingAddress, setShippingAddress] = useState([]);
    const [customerName, setCustomerName] = useState([]);
    const [customerEmail, setCustomerEmail] = useState([]);
    const [stateSub, setStateSub] = useState('');
    const [subsPlan, setSubPlan] = useState([]);
    const [filterNo, setFilterNo] = useState([]);
    const [created, setcreated] = useState([])
    const [lastModified, setLastModified] = useState([])
    const [nextOrder, setNextOrder] = useState('')
    const [paymentType, setPaymentType] =useState([])
    const [totalPrice, setTotalPrice] = useState([]);
    const [taxedPrice, setTaxedPrice] = useState([]);
    const [lastFour, setLastFour] = useState([]);
    const [container, setContainer] = useState('');
    const [getKey, setGetKey] = useState([]);
    const [first, setFirst] = useState('');
    const [last, setLast] = useState('');
    const [street, setStreet] = useState('');
    const [cityName, setCityName] = useState('');
    const [countryName, setCountryName]= useState('');
    const [stateName, setStateName] = useState('');
    const [postal, setPostal] = useState('');
    const [productIdValue, setProductIdValue] = useState('');
    const [lineItemIdValue, setLineItemIdValue] = useState('');
    const [shippingAddressId, setShippingAddressId] = useState('');
    const [quantityValue, setQuantityValue] = useState('');
    const [statusValue, setStatusValue] = useState('');
    const [customerID, setCustomerID] = useState('');
    const [lastOrder, setLastOrder] = useState('');
    const [custChannel, setCustChannel] = useState('')
    const [promo, setpromo] = useState([]);
    const [deviceId, setDeviceId] = useState('')
    const [time, setTime] = useState(0);
    const [sku, setSku] =useState('')
    const [planName, setPlanName]=useState('')

    //for date
    const [notificationDate , setNotificationDate] =  useState(false); 
    const [notificationMessageDate, setNotificationMessageDate] = useState('Updated');
    const [errorNotifiactionDate , setErrorNotificationDate] =  useState(false);
    const [errorMessageDate , setErrorMessageDate] =  useState('');

    //for shipping address
    const [notificationAddress , setAdressNotification] =  useState(false); 
    const [notificationMessageAddress, setNotificationMessageAddress] = useState('Updated');
    const [errorNotifiactionAddress , setErrorNotificationAddress] =  useState(false);
    const [errorMessageAddress , setErrorMessageAddress] =  useState('');

    //for quantity
    const [notificationQuantity , setNotificationQuantity] =  useState(false); 
    const [notificationMessageQuantity, setNotificationMessageQuantity] = useState('Updated');
    const [errorNotifiactionQuantity , setErrorNotificationQuantity] =  useState(false);
    const [errorMessageQuantity , setErrorMessageQuantity] =  useState('');

    //for turning off
    const [notificationTurnOff , setNotificationTurnOff] =  useState(false); 
    const [notificationMessageTurnOff, setNotificationMessageTurnOff] = useState('Updated');
    const [errorNotifiactionTurnOff , setErrorNotificationTurnOff] =  useState(false);
    const [errorMessageTurnOff , setErrorMessageTurnOff] =  useState('');
    
    let data = props.history.location
    const { match,location } = props;
    
    const getLastItem = thePath => thePath.substring(thePath.lastIndexOf('/') + 1)
    let subID= data.subID ? data.subID : getLastItem(props.location.pathname);
    subID = match.params.id;
    if(!subID){
        // history.push('subscriptions/')
    }


    useEffect(() => {
        let url = `${SERVICES.ACCOUNT}/api/v1/users/subscriptions/${subID}/`
        CallApi(url, 'get')
            .then(response => {
               setCustomerID(response.data.results[0]?.customerId)
               setCustomerName(response.data.results[0]?.customerName)
               setCustomerEmail(response.data.results[0]?.customerEmail)
               setStateSub(response.data.results[0]?.state);
               setSubPlan(response.data.results[0]?.lineItemList[0]?.price);
               setFilterNo(response.data.results[0]?.period)
               setcreated(response.data.results[0]?.createdAt)
               setLastModified(response.data.results[0]?.lastModifiedAt)
               setNextOrder(response.data.results[0]?.nextOrderDate);
               setShippingAddress(response.data.results[0]?.shippingAddress);
               setProductIdValue(response.data.results[0]?.lineItemList[0]?.productId)
               setPaymentType(response.data.results[0]?.paymentMethod);
               setLastFour(response.data.results[0]?.creditCard?.last4);
               setTotalPrice(response.data.results[0]?.totalPrice);
               setTaxedPrice(response.data.results[0]?.taxedPrice);
               setContainer(response.data.results[0]?.container);
               setGetKey(response.data.results[0]?.key);
               setLineItemIdValue(response.data.results[0]?.lineItemList[0]?.lineItemId)
               setShippingAddressId(response.data.results[0]?.shippingAddress.id)
               setCountryName(response.data.results[0]?.shippingAddress.country)
               setFirst(response.data.results[0]?.shippingAddress.firstName)
               setLast(response.data.results[0]?.shippingAddress.lastName)
               setStreet(response.data.results[0]?.shippingAddress.streetName)
               setCityName(response.data.results[0]?.shippingAddress.city)
               setStateName(response.data.results[0]?.shippingAddress.state)
               setPostal(response.data.results[0]?.shippingAddress.postalCode)
               setQuantityValue(response.data.results[0]?.lineItemList[0]?.quantity);
               setStatusValue(response.data.results[0]?.subscriptionState);
               setLastOrder(response.data.results[0]?.lastOrderDate);
               setCustChannel(response.data.results[0]?.channel);
               setpromo(response.data.results[0]?.lineItemList[0]?.promoCodes)
               setDeviceId(response.data.results[0]?.serialNumber)
               setTime(response.data.results[0].period)
               setSku(response.data.results[0]?.lineItemList[0]?.sku)
               setPlanName(response.data.results[0]?.planName)
            })
        }, []);

const handleSubmit = () => {
    let requestData =   {
            
            "action":"UPDATE_SHIPPING_ADDRESS",
            "updateCartRequestBean":{
                        "productId":productIdValue,
                        "lineItemId":lineItemIdValue,
                        "variantId":"1",
                        "shippingAddress":{
                            "shippingAddressId":shippingAddressId,
                            "firstName":first,
                            "lastName":last,
                            "streetAddress1": street,
                            "streetAddress2":"",
                            "city":cityName,
                            "state":stateName,
                            "postalCode":postal,
                            "phoneNumber":"123456",
                            "state": stateName,
                            "country":countryName,
                            "isDefault":false,
                            "id":shippingAddressId,
                            "streetName":street
                        },
                        "action":"SET_SHIPPING_ADDRESS"
                    },
                        "shippingAddressId": shippingAddressId,
                        "shippingCarrierOptionsBean" :{
                        "shippingAddressId":shippingAddressId,
                        "shippingCarrierOptionsBean":{
                           "shippingAddress":{
                              "shippingAddressId":shippingAddressId,
                              "firstName":first,
                              "lastName":last,
                              "streetAddress1": street,
                              "streetAddress2":"",
                              "city":cityName,
                              "state":stateName,
                              "postalCode": postal,
                              "phoneNumber":"123456",
                              "country":countryName,
                              "isDefault":false,
                              "id":shippingAddressId,
                              "streetName":street
                                        }}}
                    } // request data end
        if(requestData)
        {
            let url = `${SERVICES.ACCOUNT}/api/v1/users/${customerID}/subscriptions/${container}/${getKey}/`
            CallApi(url,'post',requestData)
            .then(response =>{
                setErrorNotificationAddress(false)       
                setAdressNotification(true);
                setNotificationMessageAddress('Successfully Updated the record');
                reloadPage();
            })
            .catch(error => {
                setErrorMessageAddress(error.message)
                setErrorNotificationAddress(true)
            });
        }}

            console.log("period", time)
         const nextorder = nextOrder;
         let nd = new Date(nextorder),
         nomonth = '' + (nd.getMonth()+1),
         noday = '' + (nd.getDate()+1),
         noyear = (nd.getFullYear());
        const nextYear = noyear+"-"+nomonth+"-"+noday

        const maxorder = nextOrder;
         let md = new Date(maxorder),

         momonth = '' + (md.getMonth() + time) > 12 ? ((md.getMonth() + time+1)-12) : (md.getMonth() + time+1) ,
         moday = '' + md.getDate(),
         moyear = (time===12 || (md.getMonth() + time) > 12 )? 
                (md.getFullYear()+1)
                 : 
                 md.getFullYear();
        const maxYear = moyear+"-"+momonth+"-"+moday

        console.log("next" ,nextYear);
        console.log("next=>", maxYear);

        console.log("Promo", promo)

    const changeQuantity = () => {

        let requestData_changeQuantity ={
            "action":"UPDATE_QUANTITY",
                            "updateCartRequestBean":{
                            "productId":productIdValue,
                            "lineItemId":lineItemIdValue,
                            "variantId":"1",
                            "quantity":quantityValue,
                            "action":"CHANGE_LINE_ITEM_QTY"
                            }
        }

        if(requestData_changeQuantity)
        {
            let url = `${SERVICES.ACCOUNT}/api/v1/users/${customerID}/subscriptions/${container}/${getKey}/`
            CallApi(url, 'post', requestData_changeQuantity)
            .then(response =>{
                setErrorNotificationQuantity(false)       
                setNotificationQuantity(true);
                setNotificationMessageQuantity('Successfully Updated the record');
                reloadPage();
            })
            .catch(error => {
                setErrorMessageQuantity(error.message)
                setErrorNotificationQuantity(true)
            });
        }
        
    }

    const columns = [
        { key: 'plan', label: 'Plan' },
        { key: 'SKU', label: 'SKU'},
        { key: 'device', label: 'Device ID'}
    ]
    const rows = [
        { plan: planName, SKU: sku , device: deviceId},
       
      ];


    const changeDate = () => {

        let requestChangeDate ={
                "action":"UPDATE_RENEWAL_DATE",
                "renewalDate":nextOrder
            }
        if(requestChangeDate)
        {
            let url = `${SERVICES.ACCOUNT}/api/v1/users/${customerID}/subscriptions/${container}/${getKey}/`
            CallApi(url,'post',requestChangeDate)
            .then(response =>{
                setErrorNotificationDate(false)       
                setNotificationDate(true);
                setNotificationMessageDate('Successfully Updated the record');
                reloadPageAfterDelete()
            })
            .catch(error => {
                setErrorMessageDate(error.message)
                setErrorNotificationDate(true)
            });
        }
    }

    const reloadPageAfterDelete = () => {
        setTimeout(() => {
            props.history.push(addHyperLink('subscriptions', props))
      }, 3000);}


    const turnOffHandler = () => {
        let payload = {
            "action":"TURN_OFF",
            "surveyQuestionId":""
        }
        if(payload)
        {
            let url = `${SERVICES.ACCOUNT}/api/v1/users/${customerID}/subscriptions/${container}/${getKey}/`
            CallApi(url,'post', payload)
            .then(response =>{
                setErrorNotificationTurnOff(false)       
                setNotificationTurnOff(true);
                setNotificationMessageTurnOff('Successfully Updated the record');
                reloadPage();
            })
            .catch(error => {
                setErrorMessageTurnOff(error.message)
                setErrorNotificationTurnOff(true)
            });
        }
    }

    const reloadPage = () => {
        setTimeout(() => {
          window.location.reload()
      }, 3000);}


     

    const turnOnHandler = () => {
        let payload = {
            "action":"TURN_ON",
            "surveyQuestionId":""
        }
        if(payload)
        {
            let url = `${SERVICES.ACCOUNT}/api/v1/users/${customerID}/subscriptions/${container}/${getKey}/`
            CallApi(url,'post',payload)
            .then(response =>{
                setErrorNotificationTurnOff(false)       
                setNotificationTurnOff(true);
                setNotificationMessageTurnOff('Successfully Updated the record');
                reloadPage();
            })
            .catch(error => {
                setErrorMessageTurnOff(error.message)
                setErrorNotificationTurnOff(true)
            });
        }}
    
        const handleGeneralTab = () => {
        setGeneralTab(true)
            }

            

    const createdOn = formatDate(created) + " " + formatTime(created)
    const modifiedOn = formatDate(lastModified) + " " + formatTime(lastModified)
    
    return (
        <div className="order-container">
            <div className="sub-order">
            <LinkButton
                to={addHyperLink('subscriptions', props)}
                iconLeft={<ListIcon />}
                label="To Subscription List"
                isDisabled={false}
            />
               <Text.Headline as="h2">{'Subscription Details'}</Text.Headline>
               <div className="main-tab-list">
                    <ul className="sub-tab-list">
                        <li><a
                            className={generalTab ? 'background_green' : 'background_black'}
                            onClick={() => handleGeneralTab()}
                        >General</a></li>
                    </ul>
                </div>
            </div>

        <div className="panel">
                               <CollapsiblePanel
                                        isClosed={summaryPanel}
                                        onToggle={() => setSummaryPanel(!summaryPanel)}
                                        header="Subscription Info"
                                    >
        <div className="info-text">
                <div className="text-display"><TextField  isDisabled="true" title=" Subscription Plan" value={`${centsToDollars(subsPlan)} per filter every ${filterNo} months`}  /></div>  
                    {
                         statusValue === "AUTO_RENEW_ON"?
                         <div className="text-display"> <TextField isDisabled="true" title="Status" value="Auto Renew On" /></div>
                         :
                         <div className="text-display"> <TextField isDisabled="true" title="Status" value="Auto Renew Off" /></div>
                    }   
                    <div className="text-display"><TextField isDisabled="true" title="Subscription State" value={stateSub}  /></div>
                    <div className="text-display"> <TextField isDisabled="true" title="Created on" value={createdOn} /> </div>
                    <div className="text-display"><TextField isDisabled="true" title="Modified At" value={modifiedOn}  /></div>
                    
                    <div className="text-display"><TextField isDisabled="true" title="Last Order Date" value={formatDate(lastOrder)}  /></div>
                    <div className="text-display">
                    <label><b>Next Order Date</b></label>
                    <DateInput placeholder="Select a date..." label="Next Order Date" minValue={nextYear} maxValue={maxYear}
                    value={nextOrder} isDisabled={stateSub === "Active"? false : true}
                    onChange={(event) => setNextOrder(event.target.value) }
                    />    
                    </div>  
                    <div style={{paddingTop: "5px"}}>
                            <PrimaryButton 
                            isDisabled={stateSub === "Active"? false : true}
                             label="Update Next Order Date" onClick={changeDate} />
                    </div>
            { notificationDate ?
                <div style={{paddingTop: "5px"}}>
                <ContentNotification type="success"> {notificationMessageDate} </ContentNotification>
                </div>
            :null
            }
            { errorNotifiactionDate ?
            <div>
                <ContentNotification type="error"> {errorMessageDate} </ContentNotification>
            </div>  
            :null
            }
            </div>
        </CollapsiblePanel>
                    
                                <CollapsiblePanel
                                        isClosed={subsPanel}
                                        onToggle={() => setSubsPanel(!subsPanel)}
                                        header="Customer Info"
                                        >
                    <div className="order-grid">
                        <div>
                    <div className="info-text">
                    <div className="text-display"><TextField isDisabled="true"  title="Customer Name" value={customerName} /></div>    
                    <div className="text-display"><TextField isDisabled="true"  title="Customer Email" value={customerEmail} /></div>
                    <div className="text-display"><TextField isDisabled="true"  title="Channel" value={custChannel} /></div>
                    <div className="text-display"><TextField  title="Quantity" value={quantityValue} onChange={(event) => setQuantityValue(event.target.value)} /></div>
                    <div style={{paddingTop: "5px", paddingLeft:"2px"}}>
                         <PrimaryButton  label="Update Quantity" onClick={changeQuantity} />
                   </div>
                    </div>

                    { notificationQuantity ?
            <div style={{paddingTop: "5px"}}>
            <ContentNotification type="success"> {notificationMessageQuantity} </ContentNotification>
           
            </div>
            :null
            }
            { errorNotifiactionQuantity ?
            <div>
                <ContentNotification type="error"> {errorMessageQuantity} </ContentNotification>
               
              </div>  
            :null
            }
            <hr/>   
                <Text.Headline as="h5"><b> Shipping address</b> </Text.Headline>
                {
                    shippingAddress ?  
                    <div className="info-text">
                    <div className="text-display"><TextField title="First Name"  value={first} onChange={(event) => setFirst((event.target.value))} /></div>   
                    <div className="text-display"><TextField title="Last Name" value={last} onChange={(event) => setLast(event.target.value)} /></div>
                    <div className="text-display"> <TextField title="Street Name" value={street} onChange={(event) => setStreet(event.target.value)} /> </div>
                    <div className="text-display"><TextField  title="City" value={cityName} onChange={(event) => setCityName(event.target.value)} /></div>    
                    <div className="text-display"> <TextField title="State" value={stateName} onChange={(event) => setStateName(event.target.value)} /> </div>
                    <div className="text-display"> <TextField title="Postal Code" value={postal} onChange={(event) => setPostal(event.target.value)} /> </div>
                    <div className="text-display"> <TextField title="Country" value={countryName} onChange={(event) => setCountryName(event.target.value)} /> </div>
                    <div style={{paddingTop: "5px", paddingLeft: "10px"}}>
                            <PrimaryButton  label="Update Shipping Address" onClick={handleSubmit} />
                    </div>
                    { notificationAddress ?
            <div style={{paddingTop: "5px"}}>
            <ContentNotification type="success"> {notificationMessageAddress} </ContentNotification>
            
            </div>
            :null
            }
            { errorNotifiactionAddress ?
            <div>
                <ContentNotification type="error"> {errorMessageAddress} </ContentNotification>
            </div>  
            :null
            }
                    </div> 
            : 
                    <Stamp tone="critical">
                        <Text.Detail>No shipping information found for this subscription</Text.Detail>
                    </Stamp>
                }
                    </div>
                </div>
                
            </CollapsiblePanel>

            <CollapsiblePanel
                                        isClosed={devicePanel}
                                        onToggle={() => setDevicePanel(!devicePanel)}
                                        header="Device ids"
                                    >
                            <div className="customer">
                                <div className="order-customer">
                                <DataTableManager columns={columns}>
                                    <DataTable rows={rows} />
                                </DataTableManager>

                                    
                                </div>
                                
                            </div>
                           
                            
           
            </CollapsiblePanel>
            <CollapsiblePanel
                                        isClosed={promoPanel}
                                        onToggle={() => setPromoPanel(!promoPanel)}
                                        header="Promo Codes"
                                    >
                   
                        
                        
                        
                            <div className="customer">
                                <div className="order-customer">
                                  {
                                      promo  ?
                                      <div>
                                          {
                                              promo.map((eachPromo)=>{
                                                  return (
                                                      <Card>
                                                        <li>{eachPromo}</li>
                                                      </Card>
                                                      
                                                  )
                                              })
                                          }
                                      </div>
                                      :
                                      <Card>
 <ContentNotification type="error"> No Promos to display.</ContentNotification> 
                                      </Card>
                                  }
 
                                    
                                </div>
                                
                            </div>
                           
                            
           
            </CollapsiblePanel>
                    <CollapsiblePanel
                                        isClosed={paymentPanel}
                                        onToggle={() => setPaymentPanel(!paymentPanel)}
                                        header="Card Info"
                                    >
                    <div className="order-grid">
                        <div>
                        <Card>
                        <Grid
                            gridGap="16px"
                            gridAutoColumns="1fr"
                            gridTemplateColumns="repeat(4, 1fr)"
                            >
                            <Grid.Item>
                            <div className="customer">
                                <div className="order-customer"><b> Payment Method :</b> </div>
                                <div className="order-customer"><b> Card Last Four Digits:</b></div>
                                <div className="order-customer"><b>Total Price</b> </div>
                                <div className="order-customer"><b>Taxed Price : </b>  </div>
                            </div>
                            </Grid.Item>
                        <Grid.Item>
                            <div className="customer">
                                <div className="order-customer"> {paymentType}</div>
                                <div className="order-customer"> {lastFour??  <div style={{paddingRight: "10px"}}>-----</div> }</div>
                                <div className="order-customer">{centsToDollars(totalPrice)?? <div>------</div> } </div>
                                <div className="order-customer"> {centsToDollars(taxedPrice)?? <div>------</div> } </div>
                            </div>
                            </Grid.Item>
                           </Grid>  
                            </Card>

                    
                            {
                                stateSub === "Active" ?
                                <div style={{paddingTop: "5px", paddingLeft:"2px"}}>
                                <SecondaryButton  label="Turn off Subscription" onClick={turnOffHandler} />
                                <PrimaryButton style={{marginLeft: "5px"}} isDisabled="true" label="Turn on Subscription" onClick={()=> console.log('click')} />
                            </div>
                                :
                                <div style={{paddingTop: "5px", paddingLeft:"2px"}}>
                                <SecondaryButton  label="Turn off Subscription" isDisabled="true" onClick={turnOffHandler} />
                                <PrimaryButton style={{marginLeft: "5px"}} label="Turn on Subscription" onClick={turnOnHandler} />
                            </div>
                            }
                        </div>
                    </div>
                { notificationTurnOff ?
            <div>
            <ContentNotification type="success"> {notificationMessageTurnOff} </ContentNotification>
            </div>
            :null}

            { errorNotifiactionTurnOff ?
            <div>
                <ContentNotification type="error"> {errorMessageTurnOff} </ContentNotification>
            </div>  
            :null
            }
            </CollapsiblePanel>

           

                    </div>
        </div>
    )
}

export default subscriptionDetailPage
