import React, { useState, useEffect } from 'react';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import Text from '@commercetools-uikit/text';
import { Pagination } from '@commercetools-uikit/pagination';
import Spacings from '@commercetools-uikit/spacings';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import { SearchIcon,PlusBoldIcon, FilterIcon} from '@commercetools-uikit/icons';
import CheckboxInput from '@commercetools-uikit/checkbox-input';
import { useRowSelection, useSorting } from '@commercetools-uikit/hooks';
import './subscription.css';
import { SERVICES, centsToDollars } from '../../config/api';
import TextInput from '@commercetools-uikit/text-input';
import { useIntl } from 'react-intl';
import LoadingSpinner from '@commercetools-uikit/loading-spinner';
import Card from '@commercetools-uikit/card';
import {ConfirmationDialog,FormModalPage } from '@commercetools-frontend/application-components';
import DateInput from '@commercetools-uikit/date-input';
import { ContentNotification } from '@commercetools-uikit/notifications';
import SelectInput from '@commercetools-uikit/select-input';
import Stamp from '@commercetools-uikit/stamp';
import { Header } from '../../common/Header';
import { addHyperLink } from '../../utils';
import { CallApi } from '../../plugin/Axios'

const UPDATE_ACTIONS = {
  COLUMNS_UPDATE: 'columnsUpdate',
  IS_TABLE_WRAPPING_TEXT_UPDATE:'isTableWrappingTextUpdate', 
  IS_TABLE_CONDENSED_UPDATE: 'isTableCondensedUpdate'
}
  

const subscription = (props) => {
  
  const [customerData, setcustomerData] = useState([]);
  const [isCondensed, setIsCondensed] = useState(false);
  const [isWrappingText, setIsWrappingText] = useState(false);
  const [totalItems, setTotalItems] = useState(0);
  const [pageLimit, setPageLimit] = useState(20);
  const [pageNumber, setPageNumber] = useState(1);
  const [spinner, setSpinner] = useState(true);
  const [enableEditSub, steEnableEditSub] = useState(false);
  const [enableEditSubModal, steEnableEditSubModal] = useState(false);
  const [disableButton, setDisableButton] = useState(false);
  const [arrIds, setArrIds] = useState([]);
  const [newNextOrderDate, setNewNextOrderDate] = useState('');
  const [newArrayDate, setNewArrayDate] = useState('')
  const [queryNextOrderDate, setQueryNextOrderDate] = useState('');
  const [bulkOrderDate, setBulkOrderDate] = useState('')
  const [dateSearch, setDateSearch] = useState('');
  const [emailSearchName, setEmailSearchName] = useState('')
  const [emailQuery, setEmailQuery] = useState('')
  const [showEmailSearch, setShowEmailSearch] = useState(false)
  const [showDateSearch, setShowDateSearch] = useState(true);
  const [showSearchByDate, setShowSearchByDate] = useState(false);
  const [array, setArrayLength] = useState(0);
  const [removeCategoryDialog, setRemoveCategoryDialog] = useState(false);
  const [showCustomerIdSearch, setShowCustomerIdSearch] = useState(false);
  const [customerIdSearchName, setCustomerIdSearchName] = useState('');
  const [customerIdQuery, setCustomerIdQuery] = useState('');

  const [disableCategoryDialog, setDisableCategoryDialog]  = useState(false);

  const { formatTime, formatDate } = useIntl();

  //datebysearch
  const [notificationSearch , setNotificationSearch] =  useState(false); 
  const [notificationMessageSearch, setNotificationMessageSearch] = useState('Updated');
  const [errorNotifiactionSearch , setErrorNotificationSearch] =  useState(false);
  const [errorMessageSearch , setErrorMessageSearch] =  useState('');

  //datebycheckbox
  const [notificationCheckbox , setNotificationCheckbox] =  useState(false); 
  const [notificationMessageCheckbox, setNotificationMessageCheckbox] = useState('Updated');
  const [errorNotifiactionCheckbox , setErrorNotificationCheckbox] =  useState(false);
  const [errorMessageSearchCheckbox , setErrorMessageSearchCheckbox] =  useState('');

  //bulkenable
  const [notificationEnable , setNotificationEnable] =  useState(false); 
  const [notificationMessageEnable, setNotificationMessageEnable] = useState('Updated');
  const [errorNotifiactionEnable , setErrorNotificationEnable] =  useState(false);
  const [errorMessageEnable , setErrorMessageEnable] =  useState('');

  //bulkdisable
  const [notificationDisable , setNotificationDisable] =  useState(false); 
  const [notificationMessageDisable, setNotificationMessageDisable] = useState('Updated');
  const [errorNotifiactionDisable , setErrorNotificationDisable] =  useState(false);
  const [errorMessageDisable , setErrorMessageDisable] =  useState('');

  const [searchByField, setSearchByField] = useState('');
  const [searchByEmail, setSearchByEmail] = useState('');
  const [displayMessage, setDisplayMessage] = useState(false);

  const [isClicked, setIsClicked] = useState(false);
  const [customerOriginB2B, setCustomerOriginB2B] = useState(true);
  const [customerOriginD2C, setCustomerOriginD2C] = useState(false);
  const [filterCustomer, setFilterCustomer] = useState('')
  const [channel, setChannel] = useState('')

  const handleRowClick = (item) => {
    props.history.push("subscriptions/" + item.id1);
    props.history.push(item)
  }

  const commonURL = `${SERVICES.ACCOUNT}/api/v1/users/subscriptions?`

  useEffect(() => {
            let offset = (pageNumber - 1) * pageLimit;
            if (totalItems && totalItems < offset) {
              setPageNumber(0);
              return;
            }
            setSpinner(true);
            setcustomerData([])
            let queryEmail = '';
            let queryDate = '';
            let queryChannel = ''
            if(bulkOrderDate) {
              queryDate += `&nextOrderDate=${encodeURIComponent(bulkOrderDate)}`;
              setQueryNextOrderDate(bulkOrderDate);
              setShowSearchByDate(true);
            }
            if(emailQuery){
              // console.log(escape(emailQuery))
              queryEmail += `email=${encodeURIComponent(emailQuery)}`;
              setShowSearchByDate(false);
            }
            if(channel)
            {
              queryChannel += `channel=${channel}`;
            }
            if(customerIdQuery){
              queryEmail += `customerId=${encodeURIComponent(customerIdQuery)}`;
              setShowSearchByDate(false);
            }
      const url = `${commonURL}${queryEmail}&limit=${pageLimit}&offset=${offset}&${queryDate}&${queryChannel}`
      CallApi(url, 'get')
      .then(res => {
        setcustomerData(res.data.results);
        setTotalItems(res.data.total);
        setDisplayMessage(false)
      }
      )
      .then(res =>
        setSpinner(false)
      )
      .catch(error => {
        console.log("error", error)
        setDisplayMessage(true)
        setSpinner(false)
      })
  }, [pageNumber, pageLimit, bulkOrderDate, emailQuery, customerIdQuery,channel]);
  
  const showrowId = (item) => {
    setWithRowSelection(true)
    const subscriptionIndexInArray = arrIds.indexOf(item);
            if(subscriptionIndexInArray > -1 ) {
            //Subscription id exists in arrIds, 
            //Already selected. So, Splice it 
            arrIds.splice(subscriptionIndexInArray, 1);
            } else {
            //Subscription id NOT exists in arrIds, 
            //Add it
            arrIds.push(item);
            }
            // rowidArray.push(item)
            if(arrIds.length === 0)
            {
              setArrayLength(0)
            }
            setArrayLength(arrIds.length)
            // setIsClicked(true)
            console.log(arrIds)
  }
  console.log(arrIds)
  
  const columns = [
   { key: 'checkbox',
    label: (
      <CheckboxInput
        isIndeterminate={isSelectColumnHeaderIndeterminate}
        isChecked={false}
        onChange={handleSelectColumnHeaderChange}
      />
    ),
    shouldIgnoreRowClick: true,
    align: 'center',
    renderItem: (row) => (
      <CheckboxInput
        isChecked={getIsRowSelected(row.id1)}
        onChange={() => {
          toggleRow(row.id1)
          showrowId(row.id1)}
        }
      />
    ),disableResizing: true,},
    { key: 'firstname', label: 'Customer Name', shouldIgnoreRowClick: true },
    { key: 'email', label: 'Customer Email', shouldIgnoreRowClick: true },
    { key: 'subPlan', label: 'Subscription Plan', shouldIgnoreRowClick: true },
    { key: 'state', label: 'State', shouldIgnoreRowClick: true },
    { key: 'channel', label: 'Channel', shouldIgnoreRowClick: true },
    { key: 'deviceID', label: 'Device ID', shouldIgnoreRowClick: true },
    { key: 'status', label: 'Status', shouldIgnoreRowClick: true },
    { key: 'lorder', label: 'Last Order', shouldIgnoreRowClick: true },
    { key: 'nextorderdate', label: 'Next Order', shouldIgnoreRowClick: true },
    { key: 'createdon', label: 'Created', shouldIgnoreRowClick: true },
    { key: 'modifiedon', label: 'Modified', shouldIgnoreRowClick: true },
    {key: 'customRenderer', label: 'Action',
      renderItem: (rows) => (
        <SecondaryButton
          label="Edit"  
        />)},
    { key: 'lastfourdigits', label: 'Last 4 digits of the card for the subscription', shouldIgnoreRowClick: true },
    
  ];
  let customerList = []
  const hideNotification = () => {
    setTimeout(() => {
        setErrorNotificationSearch(false)
        setNotificationSearch(false)
        setErrorNotificationCheckbox(false)
        setNotificationCheckbox(false)
        setErrorNotificationEnable(false)
        setNotificationEnable(false)
        setErrorNotificationDisable(false)
        setNotificationDisable(false)
    }, 3000);
}
  {
    customerData.length > 0 ?
      Object.values(customerData).map((eachCustomerData) => {
        const createdOn = formatDate(eachCustomerData.createdAt) + " " + formatTime(eachCustomerData.createdAt)
        const modifiedOn = formatDate(eachCustomerData.lastModifiedAt) + " " + formatTime(eachCustomerData.lastModifiedAt)
        const nextOrderDate = formatDate(eachCustomerData.nextOrderDate);
        // const nextorder = eachCustomerData.nextOrderDate;
        // var array = new Array();
        // array = nextorder.split('-')
        // var newDate = (array[1]+"/"+array[2]+"/"+array[0])
        let plan = '';
        try {
          plan = `${centsToDollars(eachCustomerData.lineItemList[0].price)} per filter every ${eachCustomerData.period} months`;
        } catch (e) {
          console.log(e)
        }
        let customerDetails = {
          ...eachCustomerData,
          id1: eachCustomerData.id,
          id: eachCustomerData.customerId,
          firstname: eachCustomerData.customerName ?? <div className="emptyData">-----</div>,
          status: eachCustomerData.subscriptionState === "AUTO_RENEW_ON" ? <div>Auto Refills On</div> : <div>Auto Refills Off</div> ,
          email: eachCustomerData.customerEmail,
          subPlan: plan,
          lorder: eachCustomerData.lastOrderDate,
          state: eachCustomerData.state,
          nextorderdate: eachCustomerData.nextOrderDate, 
          createdon: createdOn,
          modifiedon: modifiedOn,
          lastfourdigits: eachCustomerData.creditCard?.last4 ?? <div style={{ paddingRight: "10px" }}>-----</div>,
          deviceID: eachCustomerData.serialNumber,
          channel: eachCustomerData.channel,
         
        }
        customerList = [...customerList, customerDetails];
      }) : null
  }
  const handleBulkUpdateOpen = () => 
  {
    steEnableEditSub(true);
    setDisableButton(false);
  }
  const handleCheckBox = () => {
    steEnableEditSubModal(true)
    steEnableEditSub(false);
    setDisableButton(false);
  }
  const handleClose = () => {
    steEnableEditSub(false);
    steEnableEditSubModal(false)
    setRemoveCategoryDialog(false);
    setDisableCategoryDialog(false)
  }
  const handleCloseAfterSomeTime = () => {
    setTimeout(() => {
    steEnableEditSub(false);
    steEnableEditSubModal(false)
    setRemoveCategoryDialog(false);
    setDisableCategoryDialog(false)
  }, 3000);
}
  const handleCancel = () => {
    steEnableEditSub(false);
    steEnableEditSubModal(false)
    setDisableCategoryDialog(false)
    setRemoveCategoryDialog(false);
  }
  //For Search option
  const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
  const inputChangeHandler = (inputEvent) =>  {
    const searchText = inputEvent.target.value;
    if(searchText) {
      console.log(searchText)
      delay(500).then(() => {
        console.log('searching for - ', searchText)
        setDateSearch(searchText);
      })
    }
  }
  const searchDate = () => {
    setBulkOrderDate(dateSearch)
  }
  const searchEmail = () => {
    setEmailQuery(emailSearchName)
    setCustomerIdQuery('')
  }

  const searchCustomerId = () => {
    setCustomerIdQuery(customerIdSearchName)
    setEmailQuery('')
  }

const arrayofBulkUpdate = () => {
              console.log(arrIds)
              console.log(newArrayDate)
              let lengthofarray = arrIds.length
              let requestPayload = {
                      "limit": lengthofarray,
                      "nextOrderRenewalDate": newArrayDate,
                      "offset": 0,
                      "subscriptionIdList": arrIds
                }
            if(arrIds.length > 0)
            {
              let url = `${commonURL}bulkUpdateActionEnum=UPDATE_NEXT_ORDER_DATE`
              CallApi(url,'post', requestPayload)
                .then(response =>{
                      setErrorNotificationCheckbox(false)
                      setNotificationCheckbox(true);
                      setNotificationMessageCheckbox('Successfully Updated the record');
                      handleCloseAfterSomeTime()
                      reloadPage()  
                })

                .catch(error => {
                    setErrorMessageSearchCheckbox(error.message)
                    setErrorNotificationCheckbox(true)
              });}}
  
  const reloadPage = () => {
    setTimeout(() => {
      window.location.reload()
  }, 3000);}

//BULK ENABLE
const handleEnable = () => {
  console.log(arrIds)
  console.log(newArrayDate)
  let requestPayload = 
  {
    "limit": 20,
    "offset": 0,
    "subscriptionIdList": arrIds
  }
if(arrIds.length > 0)
{
  
  let url = `${commonURL}bulkUpdateActionEnum=TURN_ON`
  CallApi(url,'post', requestPayload)
    .then(response =>{
          setErrorNotificationEnable(false)  
          setNotificationEnable(true);
          setNotificationMessageEnable('Success');
          handleCloseAfterSomeTime()
          reloadPage()
    })
    .catch(error => {
        setErrorMessageEnable(error.message)
        setErrorNotificationEnable(true)
  });
}
}
//BULK DISABLE
const handleDisable = () => {
  console.log(arrIds)
  let requestPayload = {
    "limit": 20,
    "offset": 0,
    "subscriptionIdList": arrIds,
    "surveyQuestionId": ""
    }

if(arrIds.length > 0)
{
  let url = `${commonURL}bulkUpdateActionEnum=TURN_OFF`
  CallApi(url, 'post', requestPayload)
    .then(response =>{
          setErrorNotificationDisable(false)
          setNotificationDisable(true);
          setNotificationMessageDisable('Success');
          handleCloseAfterSomeTime()
          reloadPage()  
    })
    .catch(error => {
        setErrorMessageDisable(error.message)
        setErrorNotificationDisable(true)
  });
}}
const handleDateUpdate = () => {
    let requestPayload = {
      
        "limit": 20,
        "nextOrderDate": queryNextOrderDate,
        "nextOrderRenewalDate": newNextOrderDate,
        "offset": 0
      
    }
    if(requestPayload)
    {
      let url = `${commonURL}bulkUpdateActionEnum=UPDATE_NEXT_ORDER_DATE`
      CallApi(url, 'post', requestPayload)
      .then(response =>{
       
        setErrorNotificationSearch(false)
        setNotificationSearch(true);
        setNotificationMessageSearch('Successfully Updated the record');
        handleCloseAfterSomeTime()
        reloadPage()  
    })
      .catch(error => {
        setErrorMessageSearch(error.message)
        setErrorNotificationSearch(true)
      });
    }
}
const controlView = (controlValue) => {
  console.log(controlValue, "controlValue")
        if(controlValue === "Email" ){
          setShowEmailSearch(true)
          setShowDateSearch(false)
          setShowCustomerIdSearch(false)
        }
        if (controlValue === "Date")
        {
          setShowEmailSearch(false)
          setShowDateSearch(true)
          setShowCustomerIdSearch(false)
        }
        if(controlValue == "CustomerId")
        {
          setShowEmailSearch(false)
          setShowDateSearch(false)
          setShowCustomerIdSearch(true)
        }
  }
const searchView = (value) => {
    if(value === "ByCheckbox")
    {
      handleCheckBox();
      setShowSearchByDate(false)
      setRemoveCategoryDialog(false)
      setDisableCategoryDialog(false)
    }
    if(value === "enable")
    {
      setShowSearchByDate(false)
      setRemoveCategoryDialog(true)
      setDisableCategoryDialog(false)
    }
    if(value === "disable")
    {
      setDisableCategoryDialog(true)
      setShowSearchByDate(false)  
      setRemoveCategoryDialog(false)
    }
}

  const filterChange = (e) => {
    let filterValue = e.target.value
    setFilterCustomer(filterValue)
    if (filterValue == "d2c") {
      setCustomerOriginB2B(false);
      setCustomerOriginD2C(true);
      setChannel('D2C')
    } else {
      setCustomerOriginB2B(true);
      setCustomerOriginD2C(false);
      setChannel('B2B')
    }
  }
// testing code
const items = [
  {
    id: '5e188c29791747d9c54250e2',
    name: 'Morgan Bean',
    customRenderer: 'CYCLONICA',
    phone: '+1 (895) 529-3300',
    age: 23,
    about:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Arcu dictum varius duis at consectetur lorem donec.',
  },
  {
    id: '5e188c295ae0bb19afbb115f',
    name: 'Franklin Cochran',
    customRenderer: 'TINGLES',
    phone: '+1 (835) 571-3268',
    age: 36,
    about:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Arcu dictum varius duis at consectetur lorem donec.',
  },
  {
    id: '5e188c298f0ea901553c517f',
    name: 'Salazar Craig',
    customRenderer: 'ECRAZE',
    phone: '+1 (944) 445-2594',
    age: 21,
    about:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Arcu dictum varius duis at consectetur lorem donec.',
  },
  {
    id: '5e188c29b09bb748df833ed0',
    name: 'Pamela Noble',
    customRenderer: 'FILODYNE',
    phone: '+1 (875) 421-3328',
    age: 34,
    about:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Arcu dictum varius duis at consectetur lorem donec.',
  },
  {
    id: '5e188c29bc14e3b97ab2ad7d',
    name: 'Terra Morrow',
    customRenderer: 'DAISU',
    phone: '+1 (807) 436-2026',
    age: 30,
    about:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Arcu dictum varius duis at consectetur lorem donec.',
  },
  {
    id: '5e188c296c9b7cf486a0479c',
    name: 'Cline Hansen',
    customRenderer: 'ULTRIMAX',
    phone: '+1 (934) 402-3675',
    age: 21,
    about:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Arcu dictum varius duis at consectetur lorem donec.',
  },
  {
    id: '5e188c29b45c669d8e60303f',
    name: 'Jefferson Rosario',
    customRenderer: 'COMTOURS',
    phone: '+1 (874) 437-2581',
    age: 32,
    about:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Arcu dictum varius duis at consectetur lorem donec.',
  },
  {
    id: '5e188c29ca865647af147b4a',
    name: 'Tania Waller',
    customRenderer: 'DOGSPA',
    phone: '+1 (964) 585-3040',
    age: 35,
    about:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Arcu dictum varius duis at consectetur lorem donec.',
  },
  {
    id: '5e188c2910b83f907e9c66ab',
    name: 'Butler Shepard',
    customRenderer: 'HOUSEDOWN',
    phone: '+1 (888) 434-2153',
    age: 21,
    about:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Arcu dictum varius duis at consectetur lorem donec.',
  },
  {
    id: '5e188c29a9ece9123d6a87a1',
    name: 'Diana Wise',
    customRenderer: 'SPEEDBOLT',
    phone: '+1 (992) 535-2912',
    age: 27,
    about:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Arcu dictum varius duis at consectetur lorem donec.',
  },
];
const {items: rows} = useSorting(items);
const {
      rows: rowsWithSelection,
      toggleRow,
      selectAllRows,
      deselectAllRows,
      getIsRowSelected,
      getNumberOfSelectedRows,
    } = useRowSelection('checkbox', rows);

    const [tableData, setTableData] = useState({
      columns: columns,
      visibleColumnKeys: columns.map(({ key }) => key),
    });
  
    const showDisplaySettingsConfirmationButtons = false;
    const showColumnManagerConfirmationButtons = false;
    const [withRowSelection, setWithRowSelection] = useState(true)
  
    
    
    const mappedColumns = tableData.columns.reduce(
      (columns, column) => ({
        ...columns,
        [column.key]: column,
      }),
      {}
    );
    const visibleColumns = tableData.visibleColumnKeys.map(
      (columnKey) => mappedColumns[columnKey]
    );
  
    const tableSettingsChangeHandler = {
      [UPDATE_ACTIONS.COLUMNS_UPDATE]: (visibleColumnKeys) => {
        setTableData({
          ...tableData,
          visibleColumnKeys,
        }), setWithRowSelection(false)},
      [UPDATE_ACTIONS.IS_TABLE_CONDENSED_UPDATE]: setIsCondensed,
      [UPDATE_ACTIONS.IS_TABLE_WRAPPING_TEXT_UPDATE]: setIsWrappingText,
    };
  
    const displaySettingsButtons = showDisplaySettingsConfirmationButtons
      ? {
        primaryButton: <FooterPrimaryButton />,
        secondaryButton: <FooterSecondaryButton />,
      }
      : {};
  
    const columnManagerButtons = showColumnManagerConfirmationButtons
      ? {
        primaryButton: <FooterPrimaryButton />,
        secondaryButton: <FooterSecondaryButton />,
      }
      : {};
  
    const displaySettings = {
      disableDisplaySettings: false,
      isCondensed,
      isWrappingText,
      ...displaySettingsButtons,
    };
  
    const columnManager = {
      areHiddenColumnsSearchable: true,
      searchHiddenColumns: () => { },
      disableColumnManager: false,
      visibleColumnKeys: tableData.visibleColumnKeys,
      hideableColumns: tableData.columns,
      ...columnManagerButtons,
    };
  
    
    const countSelectedRows = getNumberOfSelectedRows();
    const isSelectColumnHeaderIndeterminate =
      countSelectedRows > 0 && countSelectedRows < rowsWithSelection.length;
    const handleSelectColumnHeaderChange =
      countSelectedRows === 0 ? selectAllRows : deselectAllRows;
//testing code
  return (
    <div className="subscription-list">
        <Header
          title={'Subscriptions'}
          results={totalItems}
          linkUrl={addHyperLink('subscription/new', props)}
          linkLabel={'Add Subscription'}
        />
        <div className="info-box">
       
          <div style={{  display: "flex", width: "100%"}}>
          <div style={{ width: "15%", marginTop: "4px"}}>
            <SelectInput
              placeholder="Search by"
              name="form-field-name"
              value={searchByField}
              onChange={
                (event) => {
                  controlView(event.target.value);
                  setSearchByField(event.target.value);
                }}
              options={[
                { value: 'Email', label: 'Email' },
                { value: 'Date', label: 'Next Order Date' },
                { value: 'CustomerId', label: 'Customer Id'}
              ]}
            />
          </div>
           {
            showDateSearch ? 
            <div style={{display: 'flex', width: "50%",paddingTop: '4px'}}>
                      
                      <DateInput
                        placeholder="Search by next-order-date"
                        title=""
                        isReadOnly={false}
                        onChange={inputChangeHandler}
                      />
                      <div>
                        <SecondaryButton iconLeft={<SearchIcon />} label="" onClick={searchDate} />
                      </div>
            </div> 
              : null
          }
          { showEmailSearch ?
           <div style={{display: 'flex', width: "50%",paddingTop: '4px'}}>
                  
                  <TextInput 
                      placeholder="Search by customer email"
                      title=""
                      isReadOnly={false}
                      onChange={event => setEmailSearchName(event.target.value)}
                    />
                    
                    <div>
                      <SecondaryButton iconLeft={<SearchIcon />} label="" onClick={searchEmail} />
                    </div>
            </div>
            :null}
            { showCustomerIdSearch ?
           <div style={{display: 'flex', width: "50%",paddingTop: '4px'}}>
                  
                  <TextInput 
                      placeholder="Search by customer Id"
                      title=""
                      isReadOnly={false}
                      onChange={event => setCustomerIdSearchName(event.target.value)}
                    />
                    
                    <div>
                      <SecondaryButton iconLeft={<SearchIcon />} label="" onClick={searchCustomerId} />
                    </div>
            </div>
            :null}
            </div>
          <div className="search-input">
               <SecondaryButton
                  style={{  marginTop: "10px"}}
                  label="Bulk Update Renewal Date of all Records" 

                  onClick={handleBulkUpdateOpen} isDisabled={!(showSearchByDate && arrIds.length === 0) || !showSearchByDate}/>
            </div>
            <div style= {{ display: 'flex'}}>
            <div style={{ marginRight: '10px'}}>
            <SecondaryButton
                    iconLeft={<FilterIcon />}
                    label="Filter"
                    onClick={() => setIsClicked(!isClicked)}
                 />
            </div>

            <div style={{ width: '25%', marginBottom: "10px"}}>
            <SelectInput
              placeholder="Action"
              name="form-field-name"
              value={searchByEmail}
              onChange={
                (event) => {
                  searchView(event.target.value)
                  setSearchByEmail(event.target.value)
                }}
              options={[
                { value: 'ByCheckbox', label: 'Update renewal date' },
                { value: 'enable', label: 'Enable' },
                { value: 'disable', label: 'Disable' },
            ]}
            />
            </div>
            </div>
            {
              isClicked ?
                <div className="input-select">
                  <Spacings.Stack scale="xs">
                    <SelectInput
                      placeholder="Customers Origin"
                      name="form-field-name"
                      horizontalConstraint={7}
                      onChange={(e) => {
                        filterChange(e)
                      }}
                      value={filterCustomer}
                      options={[{ value: 'd2c', label: 'D2C Customers' },
                      { value: 'b2b', label: 'B2B Customers' },
                     ]}
                    />
                  </Spacings.Stack>
                </div>
                : null}
            <div>
            <Text.Headline as="h4">{` You have selected ${arrIds.length} records.`}</Text.Headline>
            </div>
            
      </div>

        {/* >>>>>>>>>>>>NORMAL MODAL STARTS HERE<<<<<<<<<<<<<< */}
        
            <FormModalPage
              title="Update Bulk Renewal Date"
              isOpen={enableEditSub}
              onClose={handleClose}

              topBarCurrentPathLabel={"Edit Bulk Renewal Date"}
              topBarPreviousPathLabel="Back"
              onSecondaryButtonClick={handleCancel}
              onPrimaryButtonClick={handleDateUpdate}
              isPrimaryButtonDisabled={disableButton}
            >

              <div className="formModal-body">
                <Card>
                <DateInput
                      placeholder="Select a date..."
                      value={newNextOrderDate}
                      onChange={(e)=> setNewNextOrderDate(e.target.value)}
                    />

                </Card>
              </div>

              <div className="notification">
                        {notificationSearch ?
                            <div>
                                <ContentNotification type="success"> {notificationMessageSearch} </ContentNotification>
                               {hideNotification()}
                            </div>
                            : null
                        }
                        {errorNotifiactionSearch ?
                            <div>
                                <ContentNotification type="error"> {errorMessageSearch} </ContentNotification>
                                {hideNotification()}
                            </div>
                            : null
                        }
                    </div>

            </FormModalPage>


        {/* >>>>>>>>>>>MODAL ENDS HERE<<<<<<<<<<<<<<<<<< */}

        {/* >>>>>>>>>>>>CHECKBOX MODAL STARTS HERE<<<<<<<<<<<<<< */}
        
            <FormModalPage
              title="Update Bulk Renewal Date by checkbox"
              isOpen={enableEditSubModal}
              onClose={handleClose}

              topBarCurrentPathLabel={"Edit Bulk Renewal Date"}
              topBarPreviousPathLabel="Back"
              onSecondaryButtonClick={handleClose}
              onPrimaryButtonClick={arrayofBulkUpdate}
              isPrimaryButtonDisabled={disableButton}
            >

              <div className="formModal-body">
                <Card>
                <DateInput
                      placeholder="Select a date..."
                      value={newArrayDate}
                      onChange={(e)=> setNewArrayDate(e.target.value)}
                    />

                </Card>
              </div>

              <div className="notification">
                        {notificationCheckbox ?
                            <div>
                                <ContentNotification type="success"> {notificationMessageCheckbox} </ContentNotification>
                                {hideNotification()}
                            </div>
                            : null
                        }
                        {errorNotifiactionCheckbox ?
                            <div>
                                <ContentNotification type="error"> {errorMessageSearchCheckbox} </ContentNotification>
                                {hideNotification()}
                            </div>
                            : null
                        }
                    </div>

            </FormModalPage>


        {/* >>>>>>>>>>>MODAL ENDS HERE<<<<<<<<<<<<<<<<<< */}

        {/* CONFIRMATION DIALOG BOX FOR ENABLE STARTS */}
          
         
            <ConfirmationDialog
            title="Bulk Enable Subscriptions"
            isOpen={removeCategoryDialog}
            onClose={handleClose}
            onCancel={handleClose}
            onConfirm={handleEnable}
          >
            <Spacings.Stack scale="m">
              <div style={{margin: "5px"}}>
              <Text.Body>{`Are you sure you want to Enable these subscriptions? `}</Text.Body>
              </div>
              
            </Spacings.Stack>
            <div className="notification">
                        {notificationEnable ?
                            <div>
                                <ContentNotification type="success"> {notificationMessageEnable} </ContentNotification>
                                {hideNotification()}
                            </div>
                            : null
                        }
                        {errorNotifiactionEnable ?
                            <div>
                                <ContentNotification type="error"> {errorMessageEnable} </ContentNotification>
                                {hideNotification()}
                            </div>
                            : null
                        }
                    </div>
          </ConfirmationDialog>
        {/* CONFIRMATION DIALOG BOX FOR ENABLE ENDS */}
        {/* CONFIRMATION DIALOG BOX FOR DISABLE STARTS */}
        <ConfirmationDialog
            title="Bulk Disable Subscriptions"
            isOpen={disableCategoryDialog}
            onClose={handleClose}
            onCancel={handleClose}
            onConfirm={handleDisable}
          >
            <Spacings.Stack scale="m">
              <div style={{margin: "5px"}}>
              <Text.Body>{`Are you sure you want to disable these subscriptions? `}</Text.Body>
              </div>
              
            </Spacings.Stack>
            <div className="notification">
                        {notificationDisable ?
                            <div>
                                <ContentNotification type="success"> {notificationMessageDisable} </ContentNotification>
                                {hideNotification()}
                            </div>
                            : null
                        }
                        {errorNotifiactionDisable ?
                            <div>
                                <ContentNotification type="error"> {errorMessageDisable} </ContentNotification>
                                {hideNotification()}
                            </div>
                            : null
                        }
                    </div>
          </ConfirmationDialog>
       
              

        {/* CONFIRMATION DIALOG BOX FOR DISABLE ENDS */}



        <DataTableManager columns={withRowSelection? columns: visibleColumns}
            onSettingsChange={(action, nextValue) => {
              tableSettingsChangeHandler[action](nextValue);
            }}
            columnManager={columnManager}
            displaySettings={displaySettings} >
          <DataTable rows={customerList}
            onRowClick={
              (item, index) => handleRowClick(item)
            }

          />
        </DataTableManager>
        {spinner ? <div className="spinner">
          <LoadingSpinner size="s">Loading</LoadingSpinner></div> : null}
        {!spinner ?
          <div style={{ marginTop: "10px" }}>
            {
              displayMessage? 
              <div style={{margin: "10px", textAlign: "center"}}>
              <Stamp tone="critical">
                <Text.Detail as="h3">No matching Records Available</Text.Detail>
              </Stamp>  
            </div>
            :
            null
            }
            
             
            <Pagination
              totalItems={totalItems}
              page={pageNumber}
              perPageRange="m"
              perPage={pageLimit}
              onPageChange={(page) => {
                console.log(page)
                setPageNumber(page)
              }}
              onPerPageChange={(lmt) => {
                console.log(lmt)
                setPageLimit(lmt);
              }}
            />
          </div>
        : null}
    </div>
  )
}

export default subscription;
