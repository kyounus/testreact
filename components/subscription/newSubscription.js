import React,{useState, useEffect} from 'react'
import DateField from '@commercetools-uikit/date-field';
import TextField from '@commercetools-uikit/text-field';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import PrimaryButton from '@commercetools-uikit/primary-button';
import SelectField from '@commercetools-uikit/select-field';
import Text from '@commercetools-uikit/text';
import axios from "axios";
import { SERVICES } from '../../config/api';
import { addHyperLink } from '../../utils';
import "./newsub.css";
import { PlusBoldIcon } from '@commercetools-uikit/icons';
import DataTable from '@commercetools-uikit/data-table';
import {FormModalPage } from '@commercetools-frontend/application-components';
import Grid from '@commercetools-uikit/grid';
import State from "../../state-list.json";
import { ContentNotification } from '@commercetools-uikit/notifications'
import Card from '@commercetools-uikit/card';
import NumberField from '@commercetools-uikit/number-field';
import { CallApi } from '../../plugin/Axios';

const newSubscription = (props) => {
    
    let data = props.history.location
    const { match,location } = props;
    const getLastItem = thePath => thePath.substring(thePath.lastIndexOf('/') + 1)
    let subID= data.subID ? data.subID : getLastItem(props.location.pathname);
    subID = match.params.id;

    //address
    const [first, setFirst] = useState('')
    const [second, setSecond] = useState('')
    const [streetname, setStreetName] = useState('')
    const [state, setState] = useState('')
    const [post, setPost] = useState('')
    const [country, setCountry] = useState('')
    const [city, setCity] = useState('')
    const [phone,setPhone] = useState('')
    
    const [address, setAddress] = useState([])
    const [payment, setPayment] = useState([])
    const [masterData, setMasterData] = useState([])
    const [enableEditSub, steEnableEditSub] = useState(false);
    const [disableButton, setDisableButton] = useState(false);

    //payload stuffs

    const [timeSub, setTimeSub] = useState(0)
    const [idOfFilter, setIdOfFiler] = useState('')
    const [tier, setTier] = useState('');
    const [ndate, setNDate] = useState('');
    const [shippingMethod, setShippingMethod] = useState('')
    const [shippingCostOverWrite, setShippingCostOverwrite] = useState(0)
    const [shippingID, setSHippingID] = useState('')
    const [paymentID, setPaymentID] = useState('')

    //UI stuffs
    const [whoseShipping, setWhoseShipping] = useState('0')
    const [whosePayment, setWhosePayment] = useState('0')
    const [promo, setPromo] = useState('');

    //Notifications
    const [notification , setNotification] =  useState(false); 
    const [notificationMessage, setNotificationMessage] = useState('Updated');
    const [errorNotifiaction , setErrorNotification] =  useState(false);
    const [errorMessage, setErrorMessage] =  useState('');

    const [notificationSub , setNotificationSub] =  useState(false); 
    const [notificationMessageSub, setNotificationMessageSub] = useState('Updated');
    const [errorNotifiactionSub , setErrorNotificationSub] =  useState(false);
    const [errorMessageSub, setErrorMessageSub] =  useState('');

    //API Reload
    const [reload, setReload] = useState(false)
    const [arrayFedex, setFedexArray] = useState([])

    const hideNotification = () => {
        setTimeout(() => {
            setErrorNotification(false)
            setNotification(false)
            steEnableEditSub(false)
            setReload(true)
        }, 3000);
    }

    const createNewSubscription= () => {
        let payload = 
            {
                "cost" : shippingCostOverWrite,
                "customerId": subID,
                "nextOrderDate": ndate,
                "paymentId": paymentID,
                "period": timeSub,
                "productId": idOfFilter,
                "promoCode": promo,
                "shippingAddressId": shippingID,
                "shippingMethod": shippingMethod
              }
        if(paymentID && shippingID && ndate)
        {
           
            CallApi(`${SERVICES.ACCOUNT}/api/v1/users/subscriptions/create/`, 'post',payload)
             .then(resp=>{
                 console.log("resi", resp);
                 if (res.data.status === 200)
                    {setNotificationSub(true)
                    setNotificationMessageSub('Created New Subscription')
                    setErrorNotificationSub(false)
                    setTimeout(() => {
                        const t = location.pathname.split('/').slice(0,-3).join('/') + '/subscriptions/'+resp.data.id
                        props.history.push(t)
                  }, 3000)}
                  else {
                    setNotificationSub(false)
                    setErrorNotificationSub(true)
                    setErrorMessageSub("Error!!Please check again")
                  }
             })
             .catch(err=>{
                 console.log("error", err)
                 setNotificationSub(false)
                 setErrorNotificationSub(true)
                 setErrorMessageSub("Error!!Please check again")
             })
        }
        else{
            setNotificationSub(false)
            setErrorNotificationSub(true)
            setErrorMessageSub("Error!!Please fill the required fields")
        }
    }

   //API CALLS HERE
    useEffect(()=>{
        let url = `${SERVICES.ACCOUNT}api/v1/users?customerId=${subID}`
            CallApi(url, 'get')
            .then(res => {
                console.log(res, "Response")
                setAddress(res.data?.shippingAddressList)
                setPayment(res.data?.payments)
            })
            .catch(err=>{
                console.log(Error,"Error")
            })
    },[reload])

    useEffect(()=>{
        
        CallApi(`${SERVICES.ACCOUNT}api/v1/users/subscriptions/plan/`, 'get')
        .then(res => {
            console.log(res.data.results, "Tier")
            setMasterData(res.data.results)
            
        })
        .catch(err=>{
            console.log(Error,"Error")
        })
},[])

    let addressesList=[]
  
    address?.map((eachAddress)=>{
        let addressDetails = {
            id: eachAddress.id,
            name: eachAddress.firstName +" "+ eachAddress.lastName,
            streetName: eachAddress.streetName,
            city: eachAddress.city,
            state:eachAddress.state,
            country: eachAddress.country,
            postal: eachAddress.postalCode
        }
        addressesList=[...addressesList, addressDetails]
    })
    
    const handleBulkUpdateOpen = () => 
    {
      steEnableEditSub(true);
      setDisableButton(false);
    }

    const generateShippingMethods = (item) => {
        console.log("Items", item);
        const payload = {
            "orderDate": ndate,
            "shippingAddress": {
                "city": item.city,
                "company": "",
                "country": item.country,
                "firstName": "",
                "id": item.id,
                "isBusiness": true,
                "lastName": "",
                "phone": "",
                "postalCode": item.postal,
                "region": "",
                "state": item.state,
                "streetName": item.streetName,
                "streetNumber": ""
            }
        }
        if(payload && ndate){
            let url = `${SERVICES.CHECKOUT}/api/v1/users/checkout/subscription/shipping-methods/`
        CallApi(url,'post', payload)
            .then((res)=>{
                console.log("Shipping brader", res)
                setFedexArray(res.data)
            })
            .catch(()=>{
                setNotification(false)
                setErrorNotification(true)
                setErrorMessage("No shipping methods available for this date.")
            })}
    }

    const handleClose = () => {
        setTimeout(() => {
            props.history.push(addHyperLink('subscriptions', props))
      }, 3000);
      }

        const Paymentcolumns = [
            { key: 'fourdigits', label: 'Last 4 digits' },
            { key: 'expYear', label: 'Expiry Year' },
            { key: 'brand', label: 'Brand' },
            { key: 'expMonth', label: 'Expiry Month' },
        ];
    
    let paymentList = []
    
    {   payment ?
        payment.map((payment)=>{
        let paymentDetails = {
            payID : payment.paymentId,
            fourdigits : payment.cardObject?.last4,
            expYear: payment.cardObject?.expYear,
            brand: payment.cardObject?.brand,
            expMonth : payment.cardObject?.expMonth
        }
        paymentList=[...paymentList, paymentDetails]
    })
    :
    null
    }
    
    const handleAddNewAddress = () => {
       
        let payload={
            "action": "ADD_SHIPPING_ADDRESS",
                "isDefaultAddress": true,
                "shippingAddress": {
                    "city": city,
                    "company": "",
                    "country": country,
                    "firstName": first,
                    "id": "",
                    "isBusiness": true,
                    "lastName": second,
                    "phone": phone,
                    "postalCode": post,
                    "region": state,
                    "state": state,
                    "streetName": streetname,
                    "streetNumber": ""
                  }
              }

              if(payload)
              {
                  let url = `${SERVICES.ACCOUNT}api/v1/users/${subID}/`
                  CallApi(url, 'post', payload)
                  .then(resp => {
                        setNotification(true)
                        setNotificationMessage('Successfully Added New Address')
                        setErrorNotification(false)
                  })
                  .then(resp=> {
                      setTimeout(()=>{
                        setFirst('')
                        setSecond('')
                        setCountry('')
                        setState('')
                        setStreetName('')
                        setCity('')
                        setPost('')
                        steEnableEditSub(false)
                      },3000)
                     
                  })
                  .catch(err => {
                        
                        setNotification(false)
                        setErrorNotification(true)
                        setErrorMessage("Error!!Please check again")
                  })
              }
    }

    const shippingMethodscolumns = [
        
        { key: 'name', label: 'Name' },
        { key: 'streetName', label: 'Street Name' },
        { key: 'city', label: 'City' },
        { key: 'state', label: 'State' },
        { key: 'country', label: 'Country' },
        { key: 'postal', label: 'Postal Code' }
    ];

    const columnforshipping = [
        {
            key: 'method',
            label: 'Shipping Method'
        },
       
    ]

    let arrayOfFedex = []

    arrayFedex.map((each)=>{
        const obFedex = {
            method: each
        }
        arrayOfFedex=[...arrayOfFedex, obFedex]
    })

    let choices = []

    {    masterData.length >=0 ?
        masterData.map((eachData)=>{
            const tierName = (eachData.masterData.current.name['en-US'])
            const filterId = (eachData.masterData.staged.masterVariant.attributes[0].value.id)
            const period = eachData.masterData.staged.masterVariant.attributes[5].value
            const optionObejct = {
                label: tierName,
                period: period,
                value: tierName,
                id:filterId
            }
            choices = [...choices, optionObejct]
        })
        : null
    }
    
        console.log(arrayFedex,"fedex")
    return (

        <div  className="formModal-body">
            <div className="very-big-container">
            <Text.Headline as="h1">{'Create New Subscription'}</Text.Headline>
                <hr />
        <div>
            <div class="each-items">
                
        {/* TIER CHOICE */}
            <SelectField
                    title="Tier"
                    value={tier}
                    options={choices}
                    onChange={(event) => {
                        const e =event.target.value
                        choices.map((each)=>{
                            if(each.value === e)
                            {
                                setTier(each.value)
                                setIdOfFiler(each.id)
                                setTimeSub(each.period)
                                return
                            }
                        })
                        console.log("event", event)
                        
                    }}
                />
            </div>
        
        {/* NEXT ORDER DATE */}
            <div class="each-items">
            <DateField
                    title="Next Order"
                    value={ndate}
                    onChange={(event) => setNDate(event.target.value)}
                />
            </div>
           
        <hr />

        {/* SHIPPING ADDRESS */}

        <Card theme="light" type="flat">
            <div className="card-inside-card">
            <div class="each-items-shipping">
            <Text.Headline  as="h3"><b>{'Shipping Address'}</b></Text.Headline>
            <hr/>
            </div>
                    <div className="address-Card">
                        <DataTable rows={addressesList}
                            columns={shippingMethodscolumns} 
                            onRowClick={
                                (item, index) => {
                                    setSHippingID(item.id)
                                    setWhoseShipping(item.name+"'s")
                                    generateShippingMethods(item)
                                }
                            }
                        />

                    
                    </div>
                    <div style={{margin: "10px"}}>
                    
                    <b>{`You have selected ${whoseShipping} shipping address. `}</b>
                     
                    </div>
                   

                    <PrimaryButton 
                    style={{
                        marginTop: "10px", marginLeft:"10px", marginBottom: "10px"
                    }} iconLeft={<PlusBoldIcon />} 
                    label="Add new address" onClick={handleBulkUpdateOpen} />
            
            </div>
           </Card>

           <hr/>
            
            {/* SHIPPING METHOD OVERWRITE */}
            <Card theme="light" type="flat">
                
                <div className="card-inside-card">
                <Text.Headline  as="h3"><b>{'Shipping Method Overwrite'}</b></Text.Headline>
                <hr/>
                <div>
           <DataTable rows={arrayOfFedex}
                            columns={columnforshipping} 
                            onRowClick={
                                (item, index) => {
                                    console.log("Item=>", item.method)
                                    setShippingMethod(item.method)
                                }
                            }
                        />
           </div>
                </div>
                <div style={{margin: "10px"}}>
                    
                    <b>{`You have selected ${shippingMethod}. `}</b>
                     
                    </div>
            <PrimaryButton style={{margin: "10px"}}
             label="Clear Selection" onClick={()=> setShippingMethod('')}
                isDisabled={false}
            />
            </Card>

         
            
            {/* SHIPPING COST OVERWRITE */}

           <div className="each-items">
           <NumberField
                title="Shipping Cost Overwrite"
                value={shippingCostOverWrite}
                isDisabled={shippingMethod ? false :true}
                onChange={(event) => setShippingCostOverwrite(event.target.value)}
            />
           </div>
           
           <hr/>
           {/* CREDIT CARDS AVAILABLE */}

            <Card theme="light" type="flat">
            <div className="card-inside-card">
            <Text.Headline  as="h3"><b>{'Cards Available'}</b></Text.Headline>
            
            <hr/>
            <div className="address-Card">
            <DataTable rows={paymentList}
                    columns={Paymentcolumns}
                    onRowClick={
                        (item, index) => {
                            
                            setPaymentID(item.payID)
                            setWhosePayment(item.fourdigits)
                        }
                      }
                    />
                   
            </div>

            <div style={{margin: "10px"}}>
            <b>{`You have selected card ending with ${whosePayment}. `}</b> 
            </div>
            </div> 
        </Card>
          <hr/>
            
            {/* PROMO CODES */}
            <div className="each-items">
            <TextField title="Promo Code" 
             value={promo} onChange={(event) => setPromo(event.target.value)} />
            </div>
            </div>

            <hr/>
            
            <SecondaryButton style={{marginLeft: "10px", marginRight: "5px", marginBottom: "10px"}}
            label="Cancel" onClick={handleClose} />

            <PrimaryButton style={{marginBottom: "10px"}}
             label="Save Subscription" onClick={createNewSubscription}
                isDisabled={false}
            />

                <div className="notification">
                        {notificationSub ?
                            <div>
                                <ContentNotification type="success"> {notificationMessageSub} </ContentNotification>
                                {hideNotification()}
                            </div>
                            : null
                        }
                        {errorNotifiactionSub ?
                            <div>
                                <ContentNotification type="error"> {errorMessageSub} </ContentNotification>
                                {hideNotification()}
                            </div>
                            : null
                        }
                    </div>

            {/* MODAL FOR ADDING SHIPPING Address */}

            <FormModalPage
              title="Add new address"
              isOpen={enableEditSub}
              onClose={()=>steEnableEditSub(false)}
              labelPrimaryButton="Add Address"
              topBarCurrentPathLabel={"new address"}
              topBarPreviousPathLabel="Back"
              onSecondaryButtonClick={()=>steEnableEditSub(false)}
              onPrimaryButtonClick={handleAddNewAddress}
              isPrimaryButtonDisabled={disableButton}
            >

              <div>
                
              <Grid
                gridGap="16px"
                gridAutoColumns="1fr"
                gridTemplateColumns="repeat(2, 1fr)"
                >
                <Grid.Item>
                    <TextField title="First Name" 
                    value={first} onChange={(event) => setFirst(event.target.value)} />
                </Grid.Item>
                <Grid.Item>
                    <TextField title="Second Name" 
                    value={second} onChange={(event) => setSecond(event.target.value)} />
                </Grid.Item>
                <Grid.Item>
                    <TextField title="Street Name" 
                    value={streetname} onChange={(event) => setStreetName(event.target.value)} />
                </Grid.Item>
                <Grid.Item>
                    <TextField title="City" 
                    value={city} onChange={(event) => setCity(event.target.value)} />
                </Grid.Item>
                <Grid.Item>
                <SelectField
                        title="State"
                        value={state}
                        options={State}
                        onChange={(event) => setState(event.target.value)}
                    />
                </Grid.Item>
                <Grid.Item>
               
                 <SelectField
                        title="Country"
                        value={country}
                        options={[
                        { value: 'US', label: 'US' },
                        { value: 'CA', label: 'CA' },
                        ]}
                        onChange={(event) => setCountry(event.target.value)}
                    />
                </Grid.Item>
                <Grid.Item>
                    <TextField title="Postal Code" 
                    value={post} onChange={(event) => setPost(event.target.value)} />
                </Grid.Item>
                <Grid.Item>
                    <TextField title="Phone Number" 
                    value={phone} onChange={(event) => setPhone(event.target.value)} />
                </Grid.Item>

              
                </Grid>

              </div>
              <div className="notification">
                        {notification ?
                            <div>
                                <ContentNotification type="success"> {notificationMessage} </ContentNotification>
                                {hideNotification()}
                            </div>
                            : null
                        }
                        {errorNotifiaction ?
                            <div>
                                <ContentNotification type="error"> {errorMessage} </ContentNotification>
                                {hideNotification()}
                            </div>
                            : null
                        }
                    </div>
             </FormModalPage>
        </div>
    </div>
    )
}

export default newSubscription
