import React, {useState, useEffect} from 'react'
import Text from '@commercetools-uikit/text';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import { FilterIcon} from '@commercetools-uikit/icons';
import Spacings from '@commercetools-uikit/spacings';
import DataTable from '@commercetools-uikit/data-table';
import SelectInput from '@commercetools-uikit/select-input';
import { Pagination } from '@commercetools-uikit/pagination';
import Card from '@commercetools-uikit/card';
import TextInput from '@commercetools-uikit/text-input';
import IconButton from '@commercetools-uikit/icon-button';
import { CloseBoldIcon } from '@commercetools-uikit/icons';
import PrimaryButton from '@commercetools-uikit/primary-button';
import './add-subscription.css'
import LoadingSpinner from '@commercetools-uikit/loading-spinner';
import { SERVICES } from '../../config/api';
import axios from "axios";
import { CallApi } from '../../plugin/Axios'
import { useIntl } from 'react-intl';

const AddSubscription = (props) => {
    
    
    
    const { formatTime, formatDate } = useIntl();
    const [showDropDown, setShowDropDown] = useState(false)
    const [pageLimit, setPageLimit] = useState(20);
    const [pageNumber, setPageNumber] = useState(1);
    const [employeeData, setEmployeeData] = useState([]);
    const [totalResults, setTotalResults] = useState(0);
    const [spinner, setSpinner] = useState(true);

    //filter-cards visibilty control
    const [showFirstName, setShowFirstName] = useState(false)
    const [showSecondName, setShowSecondName] = useState(false)
    const [showEmail, setShowEmail] = useState(false)
    const [showChannel, setShowChannel] = useState(false)
    
    const [hideFilterButton, setHideFilterButton] = useState(false);

    //filter-cards input values
    const [filterbyFirstName, setfilterbyFirstName] = useState('');
    const [filterbyLastName, setfilterbyLastName] = useState('');
    const [filterbyEmail, setfilterbyEmail] = useState('');
    const [filterbyChannel, setfilterbyChannel] = useState('');
    
    //search-query fields
    const [emailQuery, setEmailQuery] = useState('');
    const [nameFirstQuery, setNameFirstQuery] = useState('');
    const [nameSecondQuery, setNameSecondQuery] = useState('');
    const [channelQuery, setChannelQuery] = useState('');


    const controlview = (value) => {
        if(value === "fname")
        {
            setShowFirstName(true)
        }
        if(value === "lname")
        {
            setShowSecondName(true)
        }
        if(value === "email")
        {
            setShowEmail(true)
        }
        if(value === "channel")
        {
            setShowChannel(true)
        }
        
    }


    const columns = [
        { key: 'fname', label: 'First Name' },
        { key: 'lname', label: 'Second Name' },
        { key: 'email', label: 'Email' },
        { key: 'channel', label: 'Customer Channel' },
        { key: 'created', label: 'Created On' },
        { key: 'modified', label: 'Modified On' },
        
      ];

      const filerButtonView = (value) => {
            if(value)
            {
                setHideFilterButton(true)
            }
        }

    //customer list API
    useEffect(() => {
        let offset = (pageNumber - 1) * pageLimit;
        if (totalResults && totalResults < offset) {
            setPageNumber(0);
            return;
        }
        setSpinner(true);
        let query = '';
        let queryFirst ='';
        let querySecond = '';
        let queryChannel = ''
        if (emailQuery) {
            query = `email=${encodeURIComponent(emailQuery)}`;
        }
        if(nameFirstQuery) {
            queryFirst = `firstName=${nameFirstQuery}`;
        }
        if(nameSecondQuery)
        {
            querySecond = `lastName=${nameSecondQuery}`
        }
        if(channelQuery)
        {
            queryChannel = `customerChannel=${channelQuery}`
        }
        let url = `${SERVICES.ACCOUNT}/api/v1/customers?limit=${pageLimit}&offset=${offset}&${query}&${queryFirst}&${querySecond}&${queryChannel}`
        CallApi(url, 'get')
            .then(res => {
                if(res.data){
                console.log(res,'employeelist')
                setEmployeeData(res.data.results);
                setTotalResults(res.data.total);
            }
            })
            .then(res =>
                setSpinner(false)
            )
            .catch(error => console.log("error", error))
    }, [pageNumber, pageLimit, emailQuery,nameFirstQuery, nameSecondQuery, channelQuery]);

    const resetFields = () => {
        
        setShowFirstName(false)
        setShowSecondName(false)
        setShowEmail(false)
        setShowChannel(false)
        
        
      }

      const handleRowClick = (item) => {
        props.history.push("new-subscription/" + item.id);
        props.history.push(item)
        props.history.push(item.f)
        console.log(item)
        }

      

      let employeeList = [];

      employeeData.map((employeeData)=>{
          const createdOn = formatDate(employeeData.createdAt)+" "+formatTime(employeeData.createdAt)
          const modifiedOn= formatDate(employeeData.lastModifiedAt)+" "+formatTime(employeeData.lastModifiedAt)
          let employeeDetails = {
            id: employeeData.id,
            fname: employeeData.firstName,
            lname: employeeData.lastName,
            email: employeeData.email,
            created: createdOn,
            modified: modifiedOn,
            channel: employeeData?.custom?.fields?.channel
           
          }
          employeeList=[...employeeList, employeeDetails]
      })

      const searchEmail = () => {
        setEmailQuery(filterbyEmail)
        setNameFirstQuery(filterbyFirstName)
        setNameSecondQuery(filterbyLastName)
        setChannelQuery(filterbyChannel)
      }
    
      const handleformSubmit = (event) => {
        event.preventDefault()
        setEmailQuery(filterbyEmail)
        setNameFirstQuery(filterbyFirstName)
        setNameSecondQuery(filterbyLastName)
        setChannelQuery(filterbyChannel)
        return false
      }
      


    return (
        <div className="addsubs">
           <Text.Headline as="h2">{'Customer Chooser'}</Text.Headline>
           <hr/>
           <div className="info-box">
          <Spacings.Inline scale='m'>
                <div className="filter-icon">
                <SecondaryButton iconLeft={<FilterIcon />} 
                    label="Filters" onClick={() => setShowDropDown(!showDropDown)} 
                />
            </div>
        </Spacings.Inline>
        <Spacings.Inline scale="m">
        {showFirstName?
                <Card insetScale="m"
                className="card-style"
                 theme="light" type="raised">
                    Name is:
                        <Card className="inner-card" insetScale="m" theme="dark" type="rasied">
                        <div style={{margin: "2px", width: "100%", display: "flex"}}>
                        <div>
                            <form onSubmit={handleformSubmit}>
                            <TextInput value={filterbyFirstName}
                            onChange={(e) => setfilterbyFirstName(e.target.value)} /> 
                            </form>
                       
                            </div>    
                        <div className="cross-button">
                            <IconButton
                                size="medium"
                                icon={<CloseBoldIcon />}
                                label="Remove filter"
                                onClick={() => {setShowFirstName(false)
                                setNameFirstQuery('')
                            }}
                            />    
                        </div>   
                        </div>
                        </Card>
                    </Card>
            : null
        }
        
        {/* email */}
        {showEmail?
                <Card insetScale="m"
                className="card-style"
                 theme="light" type="raised">
                    Email is:
                        <Card className="inner-card" insetScale="m" theme="dark" type="rasied">
                        <div style={{margin: "2px", width: "100%", display: "flex"}}>
                        <div>
                            <form onSubmit={handleformSubmit}>
                            <TextInput value={filterbyEmail} 
                            onChange={(event) => setfilterbyEmail(event.target.value)} /> 
                            </form>
                        
                        </div>    
                    <div className="cross-button">
                        <IconButton
                            size="medium"
                            icon={<CloseBoldIcon />}
                            label="Remove filter"
                            onClick={() => {setShowEmail(false)
                            setEmailQuery('')
                        }}
                        />    
                        </div>   
                    </div>
                </Card>
                    </Card>
            : null
        }

        {/* last name */}
        {showSecondName?
                <Card insetScale="m"
                className="card-style"
                 theme="light" type="raised">
                    Last Name is:
                        <Card className="inner-card" insetScale="m" theme="dark" type="rasied">
                        <div style={{margin: "2px", width: "100%", display: "flex"}}>
                        <div>
                         <form onSubmit={handleformSubmit}>   
                        <TextInput value={filterbyLastName}
                        onChange={(event) => setfilterbyLastName(event.target.value)} /> 
                        </form>
                            </div>    
                        <div className="cross-button">
                        <IconButton
                            size="medium"
                            icon={<CloseBoldIcon />}
                            label="Remove filter"
                            onClick={() => {setShowSecondName(false)
                            setNameSecondQuery('')
                        }}
                        />    
                        </div>   
                    </div>
                    </Card>
                    </Card>
            : null
        }

        {/* Channel */}
        {showChannel?
                <Card insetScale="m"
                className="card-style"
                 theme="light" type="raised">
                    Channel is:
                        <Card className="inner-card" insetScale="m" theme="dark" type="rasied">
                            <div style={{margin: "2px", width: "100%", display: "flex"}}>
                            <div>
                                <form onSubmit={handleformSubmit}>
                                <TextInput value={filterbyChannel} 
                                onChange={(event) => setfilterbyChannel(event.target.value)} /> 
                                </form>
                        
                    </div>    
                <div className="cross-button">
                        <IconButton
                            size="medium"
                            icon={<CloseBoldIcon />}
                            label="Remove filter"
                            onClick={() =>{ setShowChannel(false)
                            setChannelQuery('')
                        }}
                        />    
                        </div>   
                    </div>
                </Card>
                    </Card>
            : null
        }
   </Spacings.Inline>
        
        {
                showDropDown ?
                <div style={{width :"25%", marginTop: "10px"}}>
                <SelectInput
                    placeholder="Filters"
                    name="form-field-name"
                    value=""
                    onChange={
                    (event ) => {
                        controlview(event.target.value),
                        filerButtonView(event.target.value)
                    }
                    }
                    options={[
                        { value: 'fname', label: 'First Name' },
                        { value: 'lname', label: 'Last Name' },
                        { value: 'email', label: 'Email' },
                        { value: 'channel', label: 'Channel' },
                       
                    ]}
                />
                </div>
                :
                null
            }

            {
                //this will call the function where API will be called with all parametres
                hideFilterButton && (showFirstName || showSecondName || showEmail || showChannel) ? 
                <div style={{marginTop: "10px"}}>
                    <PrimaryButton
                        
                        label="Apply Filter"
                        onClick={searchEmail}
                        isDisabled={false}
                    />
                     <SecondaryButton
                        style={{marginLeft: "10px"}}
                        label="Clear All Filters"
                        onClick={resetFields}
                        isDisabled={false}
                    />
                </div>
                :
                null
            }
        </div>

          

        <div style={{marginTop: "5px", marginBottom: "10px"}}>
        <DataTable 
        rows={employeeList} onRowClick={(item, id)=> handleRowClick(item)}
        columns={columns} />
        </div>
        {spinner ? <div className="spinner">
                    <LoadingSpinner size="s">Loading</LoadingSpinner></div> : null}
                {!spinner ? <Pagination
                    totalItems={totalResults}
                    page={pageNumber}
                    perPageRange="m"
                    perPage={pageLimit}
                    onPageChange={(page) => {
                        console.log(page)
                        setPageNumber(page)
                    }}
                    onPerPageChange={(lmt) => {
                        console.log(lmt)
                        setPageLimit(lmt);
                    }}
                /> : null}
        
        </div>
    )
}

export default AddSubscription


