import Grid from '@commercetools-uikit/grid'
import React from 'react'
import Card from '@commercetools-uikit/card'
import SearchSelectInput from '@commercetools-uikit/search-select-input';
import axios from 'axios';
import { SERVICES } from '../../config/api';
import { CallApi } from '../../plugin/Axios';


class customerGroup extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            customerGroup: {label: this.props.customerGroup, value: this.props.customerGroup},
            customerGroupList: [],
            customerGroupId: "",
            customerId: "",
            temp:{label: this.props.customerGroup, value: this.props.customerGroup}
        }
    }
    handleChange = (e) => {
        const { name, value } = e.target
        this.setState({ [name]: value  })
        this.props.selectedGroup(e.target.value)
    }
    componentDidMount() {
        let url = `${SERVICES.ACCOUNT}/api/v1/customer/group/`
        CallApi(url,'get')
            .then((res) => {
                console.log(res, "list")
                this.setState({
                    customerGroupList: res.data.results
                })
            })
            .catch((error) => {
                console.log('ERROR:', error);
            });
    }

    render(){
        return(
            <div>
                <Grid
                    gridGap="16px"
                    gridAutoColumns="1fr"
                    gridTemplateColumns="repeat(2, 1fr)"
                >
                    <Grid.Item>
                        <Card >
                          <label><h4>Customer Group <span className="asterisk-color">*</span></h4></label>
                                <SearchSelectInput
                                   title="customerGroup"
                                   id="customer"
                                   name="customerGroup"
                                   isRequired={true}
                                   optionType="single-lined"
                                   isAutofocussed={false}
                                   backspaceRemovesValue={true}
                                   isClearable={true}
                                   isDisabled={false}
                                   isReadOnly={false}
                                   isMulti={false}
                                   value={this.state.customerGroup}
                                   onChange={this.handleChange}
                                   placeholder="Select a Customer group"
                                   defaultOptions={this.state.customerGroupList?.map((item) => {
                                     return { label: item.name, value: item.id }

                                })}
                            />
                        </Card>
                    </Grid.Item>
                </Grid>
            </div>
        )
    }
}

export default customerGroup
