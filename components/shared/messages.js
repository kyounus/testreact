export const NoRecords = (props) => {
    const message = props.message || 'No Records found'
    return (
        <div>{message}</div>
    )
}


export default NoRecords;