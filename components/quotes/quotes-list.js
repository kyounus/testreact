import React ,{ useEffect, useState } from 'react'
import {SERVICES, centsToDollars} from '../../config/api';
import "./quotes-list.css"
import { FilterOption } from '../../common/Filter';
import { Header } from '../../common/Header';
import { Table } from '../../common/Table';


const QuotesList=(props) => {
    
    const [totalResults, setTotalResults] = useState(0);
    const [allData, setAllData] = useState([]);
    const [searchByField, setSearchByField] = useState('')
    const [searchValue, setSearchValue] = useState('')
    
    const rows = []
    allData.map((items) => {
        const {quoteNumber='', subTotal = 0, totalDiscountPrice = 0,email=''} = items.custom?.fields;
        let value = 0;
        items.lineItems.map((lineItem) => {
          if(!lineItem.custom?.fields.subscriptionEnabled)
            {
              value++;
            }
        })
        rows.push(
            {quoteNumber:quoteNumber,
             id:items.id,
             state: (items.cartState === 'Active') ? 'Pending Review': items.cartState,
             quoteOriginalTotal: centsToDollars(subTotal),
             quoteDiscount: centsToDollars(totalDiscountPrice),
             quoteFinalTotal:(items.custom.fields.Quote == true ? centsToDollars(items.custom.fields.subTotal) : "ret"),
             noOfItems: value,
             email:email
           }
        )
    })
 const columns = [
    { key: 'quoteNumber', label: 'Quote Number'},
    { key: 'state', label: 'State'},
    { key: 'quoteOriginalTotal', label: 'Quote Original Total'},
    { key: 'quoteDiscount', label: 'Quote Discount'},
    { key: 'quoteFinalTotal', label: 'Quote Final Total'},
    { key: 'noOfItems', label: 'No.of Items'},
    { key: 'email', label: 'Email'}
  ];

  const searchOption = [
    { value: 'email', label: 'Email' },
    { value: 'quoteNumber', label: 'Quote Number' },
    ];

 return(
    <div>
        
        <Header
          title={'Quotes'}
          results={totalResults}
        />

            <FilterOption
                multiSearch={true}
                searchFieldOption={searchOption}
                searchByField={(value) => setSearchByField(value)}
                searchFilter = {(searchValue) => setSearchValue(searchValue)}
                searchSelectText = "Select Type of Channel"
            />    
          <Table
                baseUrl={`${SERVICES.CART}/api/v1/quotes`}
                detailPage={'quotes'}
                searchText={searchValue ? `&${searchByField}=${encodeURIComponent(searchValue)}` : ''}
                tableColumns={columns}
                tableRow={rows}
                getTabelData={(data) => setAllData(data)}
                getTableRecord={(totlaCount) => setTotalResults(totlaCount)}
        />
    </div>
  )
}

export default QuotesList
 
 