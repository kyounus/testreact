import React, { useEffect, useState } from "react";
import "./quote-detail.css";
import Text from '@commercetools-uikit/text';
import Label from '@commercetools-uikit/label';
import CollapsiblePanel from '@commercetools-uikit/collapsible-panel';
import Grid from '@commercetools-uikit/grid';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import LinkButton from '@commercetools-uikit/link-button'
import { useIntl } from 'react-intl';
import Quoteslist from '../quotes/quotes-list';
import { centsToDollars, SERVICES } from "../../config/api";
import TextField from '@commercetools-uikit/text-field';
import MoneyField from '@commercetools-uikit/money-field'
import { ListIcon,MailIcon ,UserLinearIcon,PlusBoldIcon} from '@commercetools-uikit/icons';
import Card from '@commercetools-uikit/card';
import RadioInput from '@commercetools-uikit/radio-input';
import { addHyperLink } from '../../utils';
import { CallApi } from '../../plugin/Axios';

const Quote = (props) => {
    const [generalTab , setGeneralTab] =  useState(true)
    const [commentsTab , setCommentsTab] =  useState(false)
    const { formatTime, formatDate } = useIntl();
    const [allData,setAllData]=useState([])
    const [allData1,setAllData1]=useState([])
    const [summaryPanel, setSummaryPanel] = useState(false);
    const [employeePanel, setEmployeePanel] = useState(false);
    const [quotePanel, setQuotePanel] = useState(false)
    const [quoteNumber , setQuoteNumber]= useState(false)
    const[isChecked , setIsChecked] = useState(false)
    const[isPerc , setIsPerc] = useState(false)
    const[isChange , setIsChange] = useState(false)
    const [spinner, setSpinner] = useState(false);
    const [isClicked, setIsClicked] =useState(false);
    const [isClick, setIsClick] =useState(false);

    let data = props.history.location
    console.log(data,"d")
    const { match } = props;
    const getLastItem = thePath => thePath.substring(thePath.lastIndexOf('/') + 1)
    let id= data.id ? data.id : getLastItem(props.location.pathname)
    
    useEffect(() => {
        setSpinner(true);
        let url = `${SERVICES.CART}/api/v1//quotes/${id}/`
        CallApi(url,'get')
        .then(response => {
            setAllData(response.data)
            setAllData1(response.data.lineItems)
            setQuoteNumber(true)
            })
    }, []);
    let quotessum = []
    let lineItems=[]
    let lineItem;
    for(let i=0 ;i<allData1?.length;i++){
        lineItem = allData1[i]
         if(!lineItem.custom?.fields.subscriptionEnabled) {
            lineItems.push(lineItem)
        }
    }

    lineItems.length > 0 ? (
        lineItems.map((data) => {
            const quoteDetails = {
                product:data.name["en-US"],
                originalunitprice: centsToDollars(data.totalPrice.centAmount),
                unitprice:centsToDollars(data.variant.prices[0]?.value.centAmount),
                qty:data.quantity,
                subtotal: centsToDollars(data.custom?.fields.orderTotal),
                tax: "",
                total: centsToDollars(data.totalPrice.centAmount),
            }
            quotessum = [...quotessum, quoteDetails]
        })
    ) : null

    const columns = [
        { key: 'product', label: 'Product'},
        { key: 'originalunitprice', label: 'Original Unit Price'},
        { key: 'unitprice', label: 'Unit Price'},
        { key: 'qty', label: 'Qty'},
        { key: 'subtotal', label: 'Subtotal'},
        { key: 'tax', label: 'Tax'},
        { key: 'total', label: 'Total'}
    ];
    const handleGeneralTab = () => {
        setGeneralTab(true)
        setCommentsTab(false)
    }
    const handleCommentsTab = () => {
        setCommentsTab(true)
        setGeneralTab(false)
    }
    const handleChange = (event) =>{
        if(event){
            console.log("str")
           setIsClick(true)
        }
        else{
            setIsClick(false)
            setIsClicked(false)
        }
    }

    return (
        <div className="quote-container">
            <div className="sub-quote">
            <LinkButton
                to={addHyperLink('quotes', props)}
                iconLeft={<ListIcon />}
                label="To Quote List"
                isDisabled={false}
            />
              <div className="quote-heading">
                <Text.Headline as="h2">{quoteNumber  ? 'Quote' + "  " + allData.custom.fields.quoteNumber: 'Quote'}</Text.Headline>
              </div>
              <div className="main-tab-list">
                  <ul className="sub-tab-list">
                        <li><a 
                        className={generalTab ? 'background_green' : 'background_black'  }
                        onClick={()=>handleGeneralTab()}
                         >General</a></li>
                         <li><a 
                        className={commentsTab ? 'background_green' : 'background_black'  }
                        onClick={()=>handleCommentsTab()}
                         >Comments </a></li>
                    </ul>
                </div>
            </div>
           
            {generalTab ?
             <div>
                <div className="general">
                   <Text.Headline as="h5">Quote Date &nbsp;&nbsp; {formatDate(allData1.createdAt)+" "+ formatTime(allData1.createdAt)}</Text.Headline>
                </div>
                <div className="panel">
                    <CollapsiblePanel
                        isClosed={summaryPanel}
                        onToggle={() => setSummaryPanel(!summaryPanel)}
                        header="Quote summary"
                    >
                        <div className="quote-grid">
                          <Grid 
                            gridGap="16px"
                            gridAutoColumns="1fr"
                            gridTemplateColumns="repeat(3, 1fr)"
                          >
                            <Grid.Item>
                               <div  className="quote-detail">
                                 <Card>
                                    <Grid
                                        gridGap="16px"
                                        gridAutoColumns="1fr"
                                        gridTemplateColumns="repeat(1, 1fr)">
                                        <Grid.Item>
                                            <div><b>State</b><br/><br/>{allData.cartState}</div>
                                        </Grid.Item>
                                    </Grid>
                                 </Card>
                               </div>
                            </Grid.Item>
                            <Grid.Item>
                                <div className="quote-detail-card">
                                  <Card>
                                     <Grid
                                        gridGap="16px"
                                        gridAutoColumns="1fr"
                                        gridTemplateColumns="repeat(3, 1fr)"
                                     >
                                        <Grid.Item>
                                            <div><b>Original total</b></div>
                                            <div><b>Discount </b> </div>
                                        </Grid.Item>
                                        <Grid.Item>
                                        </Grid.Item>
                                        <Grid.Item>
                                           <div> {centsToDollars(allData.custom?.fields.subTotal)}</div>
                                           <div>{"$" + 0}</div>                         
                                        </Grid.Item>
                                        <Grid.Item>
                                           <div><b>Final Total</b> </div>
                                        </Grid.Item>
                                        <Grid.Item>
                                        </Grid.Item>
                                        <Grid.Item>
                                           <div>{centsToDollars(allData.custom?.fields.orderTotal)}</div>
                                        </Grid.Item>
                                     </Grid>
                                  </Card>
                               </div>
                            </Grid.Item>
                            <Grid.Item>
                            </Grid.Item>
                          </Grid>
                        </div>
                    </CollapsiblePanel>
                </div>
                <div className="panel">
                    <CollapsiblePanel
                        isClosed={employeePanel}
                        onToggle={() => setEmployeePanel(!employeePanel)}
                        header="Employee"
                        headerControls = {<UserLinearIcon/>}
                    >
                       <div className="emp-mail">
                            <div>
                               <label>Employee email :{allData.custom?.fields.email}</label>&nbsp;&nbsp;
                            </div>
                            <div className="mail-icon">
                               <MailIcon/>
                            </div>
                        </div>
                    </CollapsiblePanel>
                </div>
                <div className="panel">
                    <CollapsiblePanel
                        isClosed={quotePanel}
                        onToggle={() => setQuotePanel(!quotePanel)}
                        header="Quote items"
                    >
                        <div className="quote-grid">
                            <Grid
                                gridGap="16px"
                                gridAutoColumns="1fr"
                                gridTemplateColumns="repeat(3, 1fr)"
                            >
                               <Grid.Item>
                                  <Card>
                                     <Grid
                                        gridGap="10px"
                                        gridAutoColumns="1fr"
                                        gridTemplateColumns="repeat(3, 1fr)"
                                     >
                                        <Grid.Item>
                                            <h3>Change prices</h3><br/>
                                            <div>
                                                <RadioInput.Option 
                                                    onChange={() => setIsChecked(state => ({ isChecked: !state.isChecked }))}
                                                    isChecked={isChecked}
                                                    isDisabled={true}
                                                >
                                                    Amount Discount
                                                </RadioInput.Option>
                                            </div><br/>
                                            <div>
                                               <RadioInput.Option
                                                    onChange={() => setIsPerc(state => ({ isPerc: !state.isPerc }))}
                                                    isChecked={isPerc}
                                                    isDisabled={true}
                                                >
                                                    Percentage Discount
                                                </RadioInput.Option>
                                            </div>
                                        </Grid.Item>
                                        <Grid.Item>
                                        </Grid.Item>
                                        <Grid.Item>
                                          <div> 
                                            <MoneyField
                                                title=""
                                                value={{ amount: '0', currencyCode: 'USD' }}
                                                //onChange={(event) => alert(event.target.value)}
                                                currencies={['EUR', 'USD']}
                                                isDisabled={true}
                                            />
                                          </div>
                                          <div>
                                             <TextField placeholder="%" value="%" title="" isDisabled={true}/>
                                         </div>
                                        </Grid.Item>
                                        </Grid><br/>
                                        <div>
                                          <RadioInput.Option 
                                                value="changeItemPrices"
                                                onChange={() => setIsChange(state => ({ isChange: !state.isChecked }))}
                                                isChecked={isChange}
                                                isDisabled={true}
                                                > 
                                                Change Item prices
                                         </RadioInput.Option>
                                        </div>
                                  </Card>
                              </Grid.Item>
                          </Grid>
                        </div>
                        <div className="quote-table">
                            <DataTableManager columns={columns}>
                                <DataTable
                                    rows={quotessum}
                                />
                            </DataTableManager>
                        </div>
                    </CollapsiblePanel>
                </div>
            </div>
            :null}

            {commentsTab ? 
               <div>
                   <div className="comments-tab">
                    </div>
                    <div className="comments-section">
                      <Grid 
                         gridGap="16px"
                         gridAutoColumns="1fr"
                         gridTemplateColumns="repeat(3, 1fr)"
                       >
                        <Grid.Item>
                            <Card>
                                <div className="discount">
                                    {allData.custom?.fields?.comments?.split('\n').map((item, key) => {
                                        return <span key={key}>{item}<br/></span>
                                    })}
                                </div> 
                            </Card>    
                        </Grid.Item>
                      </Grid>
                    </div>
               </div>
            : null}
        </div>
    );
}
export default Quote;