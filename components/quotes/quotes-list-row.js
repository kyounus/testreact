import React from "react";
import DataTableManager from '@commercetools-uikit/data-table-manager';
import DataTable from '@commercetools-uikit/data-table';
import NoRecords from '../shared/messages';

const QuoteslistRow = (props) =>{
    let { quotes } = props;
    let quotesList = quotes || [];
    quotesList = quotesList.map(data => {
        return {
            ...data,
            id: data.shippingAddressId || data.billingAddressId,
            contactname: data.firstName + ' ' + data.lastName,
            companyname: data.companyName || '',
            address: data.streetAddress1 + (data.streetAddress2 ? ',' + data.streetAddress2 : ''),
        }
    });
    const columns = [
        { key: 'quoteNumber', label: 'Quote Number'},
        { key: 'state', label: 'State'},
        { key: 'quoteOriginalTotal', label: 'Quote Original Total'},
        { key: 'quoteDiscount', label: 'Quote Discount'},
        { key: 'quoteFinalTotal', label: 'Quote Final Total'},
        { key: 'noOfItems', label: 'No.of Items'},
        { key: 'email', label: 'Email'}
      ];
  
    return(
        <div className="address-list">
            {addressList.length > 0 ?
              <DataTableManager columns={columns}>
                  <DataTable rows={quotesList} />
              </DataTableManager>
          : <NoRecords message="No Addresses added" />}
        </div>
    );
  }
  export default QuoteslistRow;