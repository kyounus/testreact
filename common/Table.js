import React, { useEffect, useState } from 'react';
import { useRouteMatch } from 'react-router-dom';
import DataTable from '@commercetools-uikit/data-table';
import { Pagination } from '@commercetools-uikit/pagination';
import DataTableManager from '@commercetools-uikit/data-table-manager';
import { ContentNotification } from '@commercetools-uikit/notifications';
import LoadingSpinner from '@commercetools-uikit/loading-spinner';
import { useHistory } from "react-router-dom";
import { CallApi } from '../plugin/Axios';
import { useRowSelection, useSorting } from '@commercetools-uikit/hooks';
// import axios from "axios";

const UPDATE_ACTIONS = {
    COLUMNS_UPDATE: 'columnsUpdate',
    IS_TABLE_WRAPPING_TEXT_UPDATE:'isTableWrappingTextUpdate', 
    IS_TABLE_CONDENSED_UPDATE: 'isTableCondensedUpdate'
  }

export const Table = (props) => {
    const { baseUrl, detailPage, searchText, searchSelect, tableColumns, tableRow, getTabelData, getTableRecord } = props;
    const [totalItems, setTotalItems] = useState(0);
    const [pageNumber, setPageNumber] = useState(1);
    const [pageLimit, setPageLimit] = useState(20);
    const [apiError, setApiError] = useState('');
    const [spinner, setSpinner] = useState(true);
    const [isCondensed, setIsCondensed] = useState(false);
    const [isWrappingText, setIsWrappingText] = useState(false);
    let history = useHistory();
    const match = useRouteMatch();
    useEffect(() => {
        let offset = (pageNumber - 1) * pageLimit;
        if (totalItems && totalItems < offset) {
            setPageNumber(0);
            return;
        }
        setSpinner(true);
        getTabelData([]);
        let query = '';
        if(searchText) {
            query += `${searchText}`;
        }
        if(searchSelect) {
            query += `${searchSelect}`;
        }
        const url = `${baseUrl}?limit=${pageLimit}&offset=${offset}${query}`;
        CallApi(url, 'get').then(res => {
            console.log(res);
            if(res && res.status === 200) {
                getTabelData(res.data.results);
                getTableRecord(res.data.total);
                setTotalItems(res.data.total);
            }else {
                setApiError('There is error in server');
            }
            setSpinner(false);
        }).catch(error => console.log("error", error));
    }, [baseUrl, pageNumber, pageLimit, searchText, searchSelect, match]);
    
    const handleRowClick = (item) => {
        try {
            history.push(detailPage + "/" + item.id);
            history.push(item);
        } catch (error) {
            alert(error);
        }
    }

    const [tableData, setTableData] = useState({
        columns: tableColumns,
        visibleColumnKeys: tableColumns.map(({ key }) => key),
    });

    const showDisplaySettingsConfirmationButtons = false;
    const showColumnManagerConfirmationButtons = false;
    const withRowSelection = true;

    const {
        items: rows,
        sortedBy,
        sortDirection,
        onSortChange,
    } = useSorting(tableRow);

    const {
        rows: rowsWithSelection,
        toggleRow,
        selectAllRows,
        deselectAllRows,
        getIsRowSelected,
        getNumberOfSelectedRows,
    } = useRowSelection('checkbox', rows);

    const countSelectedRows = getNumberOfSelectedRows();
    const isSelectColumnHeaderIndeterminate =
        countSelectedRows > 0 && countSelectedRows < rowsWithSelection.length;
    const handleSelectColumnHeaderChange =
        countSelectedRows === 0 ? selectAllRows : deselectAllRows;

    const mappedColumns = tableData.columns.reduce(
        (columns, column) => ({
            ...columns,
            [column.key]: column,
        }),
        {}
    );
    const visibleColumns = tableData.visibleColumnKeys.map(
        (columnKey) => mappedColumns[columnKey]
    );

    const tableSettingsChangeHandler = {
        [UPDATE_ACTIONS.COLUMNS_UPDATE]: (visibleColumnKeys) =>
            setTableData({
                ...tableData,
                visibleColumnKeys,
            }),
        [UPDATE_ACTIONS.IS_TABLE_CONDENSED_UPDATE]: setIsCondensed,
        [UPDATE_ACTIONS.IS_TABLE_WRAPPING_TEXT_UPDATE]: setIsWrappingText,
    };

    const displaySettingsButtons = showDisplaySettingsConfirmationButtons
        ? {
            primaryButton: <FooterPrimaryButton />,
            secondaryButton: <FooterSecondaryButton />,
        }
        : {};

    const columnManagerButtons = showColumnManagerConfirmationButtons
        ? {
            primaryButton: <FooterPrimaryButton />,
            secondaryButton: <FooterSecondaryButton />,
        }
        : {};

    const displaySettings = {
        disableDisplaySettings: false,
        isCondensed,
        isWrappingText,
        ...displaySettingsButtons,
    };

    const columnManager = {
        areHiddenColumnsSearchable: true,
        searchHiddenColumns: () => { },
        disableColumnManager: false,
        visibleColumnKeys: tableData.visibleColumnKeys,
        hideableColumns: tableData.columns,
        ...columnManagerButtons,
    };

    // function onSortChange(columnName) {
    //     console.log(columnName);
    // }

    // function sortedBy(event) {
    //     console.log(event);
    // }
    // function sortDirection(event) {
    //     console.log(event);
    // }
    
    return (
        <div>
            {apiError && <ContentNotification type='error'>{apiError}</ContentNotification>}
            <DataTableManager columns={visibleColumns} 
               onSettingsChange={(action, nextValue) => {
                tableSettingsChangeHandler[action](nextValue);
              }}
              columnManager={columnManager}
              displaySettings={displaySettings}
            >
                <DataTable
                    rows={tableRow}
                    // onSortChange={onSortChange}
                    // sortedBy={sortedBy}
                    // sortDirection={sortDirection}
                    onRowClick={
                        (item, index) => handleRowClick(item)
                    }
                />
            </DataTableManager>
            {spinner ?
                <div className="spinner">
                    <LoadingSpinner size="s">Loading</LoadingSpinner>
                </div>
            : null}
            {!spinner ?
                <Pagination
                    totalItems={totalItems}
                    page={pageNumber}
                    perPage={pageLimit}
                    perPageRange="m"
                    onPageChange={(page) => {
                        setPageNumber(page)
                    }}
                    onPerPageChange={(lmt) => {
                        setPageLimit(lmt);
                    }}
                />
            : null}
        </div>
    )
}