import { Link } from 'react-router-dom';
import Text from '@commercetools-uikit/text';
import LinkButton from '@commercetools-uikit/link-button';
import { PlusBoldIcon } from '@commercetools-uikit/icons';
import SecondaryButton from '@commercetools-uikit/secondary-button';

export const Header = (props) => {
    const { listUrl, listLabel, title, results, linkUrl, linkLabel, popUp, popUpHandel } = props;
    return(
        <div className="company-title_text">
            {listLabel ? 
                <LinkButton
                    to={listUrl}
                    iconLeft={<ListIcon />}
                    label={listLabel}
                    isDisabled={false}
                />
            :null}
            <Text.Headline as="h2">
                {title}
                {results ? <Text.Detail isInline={true}>&nbsp;&nbsp;{results} {'results'}</Text.Detail> 
                : 
                <Text.Detail isInline={true}>&nbsp;&nbsp;  {'0 results'}</Text.Detail>}
            </Text.Headline>
            {linkUrl ? 
                <div className="add-button" >
                    <Link to={linkUrl}>
                        <SecondaryButton iconLeft={<PlusBoldIcon />} label={linkLabel} onClick={() => console.log('Button clicked')} />
                    </Link>
                </div>
            : null
            }
            {popUp ? 
                <div className="add-button" >
                    <SecondaryButton iconLeft={<PlusBoldIcon />} label={linkLabel} onClick={() => popUpHandel(true)} />
                </div>
            : null
            }
        </div>
    )
}