import React, { useState } from 'react';
import Spacings from '@commercetools-uikit/spacings';
import TextInput from '@commercetools-uikit/text-input';
import SelectInput from '@commercetools-uikit/select-input';
import SecondaryButton from '@commercetools-uikit/secondary-button';
import { FilterIcon, SearchIcon,CloseBoldIcon } from '@commercetools-uikit/icons';
import IconButton from '@commercetools-uikit/icon-button';


export const FilterOption = (props) => {
    const { searchFilter, searchSelectOption, searchSelectChange, searchText, searchSelectText } = props;
    const [searchBoxText, setSearchBoxText] = useState(props.searchBoxText);
    const [searchField, setSearchField] = useState('');
    const [searchValue, setSearchValue] = useState('');
    const [searchSelect, setSearchSelect] = useState('');
    const [isFilterClicked, setIsFilterClicked] = useState(false);
    
    let searchWidth = '100%';
    if(props.multiSearch) {
        searchWidth = '50%';
    }

    const changeSearchFied = (event) => {
        let value = event.target.value;
        searchFilter('');
        setSearchValue('');
        setSearchField(value);
        props.searchByField(value);
        value = value.charAt(0).toUpperCase() + value.slice(1);
        value = value.replace(/([A-Z])/g, ' $1').trim();
        setSearchBoxText('Search by '+ value);
    }

    const pressEnter = (event) => {
        event.preventDefault()
        searchFilter(searchValue);
        return false;
    }
    return(
        <div className="info-box">
            <Spacings.Inline scale='m'>
                <div className="filter-icon">
                    <SecondaryButton
                        iconLeft={<FilterIcon />}
                        label='Filter'
                        onClick={() => setIsFilterClicked(!isFilterClicked)}
                    />
                </div>
                <div className="search-input info-text" style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between', paddingTop:'11px',width:'80%'}}>
                    {props.multiSearch ? 
                        <div style={{width: `${searchWidth}`, marginRight: '2%'}}>
                            <SelectInput
                                placeholder="Search by"
                                name="form-field-name"
                                value={searchField}
                                onChange={changeSearchFied}
                                options={props.searchFieldOption}
                            />
                        </div>
                    : null}
                    {!props.multiSearch || searchField ?
                        <div style={{width: `${searchWidth}`}}>
                            <form onSubmit={pressEnter}>
                                <TextInput
                                    placeholder={searchBoxText}
                                    title=""
                                    isReadOnly={false}
                                    value={searchValue}
                                    onChange={event => {
                                        setSearchValue(event.target.value);
                                    }}
                                />
                            </form>
                        </div>
                    :null}
                </div>
                {!props.multiSearch || searchField ?
                    <div style={{display:'flex',alignItems:'center',justifyContent:'space-between',width:'20%'}}>
                        <SecondaryButton iconLeft={<SearchIcon />} label="" onClick={() => searchFilter(searchValue)} />
                        <div style={{padding:'14px',alignItems:'center',display:'flex'}}>
                        <IconButton 
                            size="medium"
                            icon={<CloseBoldIcon />}
                            label='clear all'
                            onClick={() => {
                                searchFilter('');
                                setSearchValue('');
                                setSearchField('');
                                setIsFilterClicked('')
                            }}
                        />  
                        <div> Clear all </div>
                        </div>  
                    </div>
                : null } 
            </Spacings.Inline>
            {isFilterClicked ?
                <div className="input-select">
                    <Spacings.Stack scale="xs">
                        <SelectInput
                            placeholder={searchSelectText}
                            name="form-field-name"
                            horizontalConstraint={7}
                            onChange={(e) => {
                                props.searchByField('');
                                searchFilter('');
                                setSearchValue('');
                                setSearchField('')
                                setSearchSelect(e.target.value);
                                searchSelectChange(e.target.value);
                            }}
                            value={searchSelect}
                            options={searchSelectOption}
                        />
                    </Spacings.Stack>
                </div>
            : null}
        </div>
    )
}