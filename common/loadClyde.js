import axios from "axios";
import { CLYDE_KEY, CLYDE_SECRET, CLYDE_URL } from "../config/api.js";

const loadClyde = (skuquery, callback)=>{
  axios.get(CLYDE_URL+'products?sku='+ skuquery,{
    headers:{ 
      "Authorization":CLYDE_KEY + ':' + CLYDE_SECRET
    }}).then((response)=>{
    console.log(response);
    return typeof(callback) === "function"? callback(response.data): null
  })
}

export const GetClydeProductionPlan = (skuquery, callback)=>{
  axios.get(CLYDE_URL+'products?sku='+ skuquery,{
    headers:{ 
      "Authorization":CLYDE_KEY + ':' + CLYDE_SECRET
    }}).then((response)=>{
    return typeof(callback) === "function"? callback(response.data): null
  })
}

export default loadClyde;