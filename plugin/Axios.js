import axios from "axios";
import { code } from '../config/api';

export const CallApi = async(url, type = 'POST', payload = {}, headers = {}) => {
    //console.log("ProjectKey => ", ProjectKey);
    // const ProjectKey
    
    const urlLastChara = url.slice(-1);
    url = urlLastChara === '/'? url+`?country=${code}` : url+`&country=${code}`;
    
    // creating Axios request
    const request = {
        headers:{
            ...headers,
            country:code
        },
        method: type,
        url,
        data: payload
    };
    let response = '';
    try {
        response = await axios(request);
    } catch(error) {
        response = error.response;
    } finally {
        return response;
    }
}