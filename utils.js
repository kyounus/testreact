import { code, ENV_EU } from "./config/api";

export const addHyperLink = (route, props) => {
    let retVal = route;
    try {
        retVal = `/${props.match.params.projectKey}/${props.match.params.entryPointUriPath}/${route}`
    } catch (e) {
        console.log('ERROR @ util.addLink', e);
    }
    return retVal;
}
const splitLimit = 5;
export const getPhoneFormat = (value = '') => {
    if (value && code === ENV_EU) {
        if (value[0] !== '0') {
            value = '0' + value;
        }
        //return value && value.length > 5 ? value.replaceAll(' ', '').substring(0, splitLimit) + " " + value.replace(' ', '').substring(splitLimit) : value;
    }
    return value
}

